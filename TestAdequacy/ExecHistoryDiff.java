import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import options.Options;

import util.PSlice;

/** Analyzes dynamic slices of changes in different versions to determine whether and which changes interacted. */
public class ExecHistoryDiff {
	
	private final int startId;
	private final String pathOrigOut;
	private final String pathModOut;
	private final String pathPSlice;
	
	public static void main(String[] args) {
		ExecHistoryDiff analysis = new ExecHistoryDiff(args);
		analysis.run();
	}
	
	/** arg 0: starting_sId   (e.g., 2501)
	    arg 1: verId          (e.g., v1)
	    arg 2: seedId         (e.g., s1)
	    NOTE: out dir format, from which out files are read, is 'out-arg1arg2-arg0' */
	private ExecHistoryDiff(String[] args) {
		String currentDir = new File(".").getAbsolutePath();
		System.out.println(currentDir);
		args = Options.process(args);
		this.startId = Integer.valueOf(args[0]);
		final String verId = args[1];
		final String seedId = args[2];
		String origPostFormat = null;
		if (args.length > 3) {
			assert args.length == 4;
			origPostFormat = args[3];
		}
		System.out.println("dbg: origPostFormat " + origPostFormat);
		
		this.pathOrigOut = "outdyn-" + verId + (origPostFormat==null?"":origPostFormat) + "-" + startId + File.separator;
		System.out.println("dbg: pathOrigOut " + this.pathOrigOut);
		this.pathModOut = "outdyn-" + verId + seedId + "-" + startId + File.separator;
		System.out.println("dbg: pathModOut " + this.pathModOut);
		this.pathPSlice = "slidyn-" + verId + (origPostFormat==null?"":origPostFormat) + "-" + startId + File.separator;
		System.out.println("dbg: pathPSlice " + this.pathPSlice);
	}
	
	private final static int TESTS_PER_DOT = 5;
	private final static int DOTS_PER_LINE = 80;
	private void run() {
		// for each test out file t pair:
		//   read both exec hists for all stmts
		//   for each stmt s:
		//     determine and store whether eh(s) != eh'(s) for t
		// for each s found at least once:
		//   determine % of out files for which e.h. differed (i.e., degree of impact)
		if(Options.pSlice()){
		Map<Integer, Integer> pntToDiffs = new HashMap<Integer, Integer>();
		Set<Integer> pntsCovered = new HashSet<Integer>();
		final int numTests = readAndDiffExecHistories(pntToDiffs, pntsCovered) - 1;
		assert numTests > 0;
		System.out.println("Read " + numTests + " test-output files");
		
		// DEBUG: covered points
		List<Integer> pntsCovSorted = new ArrayList<Integer>(pntsCovered);
		Collections.sort(pntsCovSorted);
		System.out.println("Covered: " + pntsCovSorted.size());
		
		// determine ranking of exhist-diff statements with respect to p-slice
		PSlice pslice = new PSlice(startId, pathPSlice + "pslice.out"); //-all.out");
		Map<Integer,Integer> rankingPSlice = computePSliceRanking(pslice);
		final int sizeSlice = rankingPSlice.size();
		
		// filter out diffs not in pslice
		final int numDiffsPrev = pntToDiffs.size();
		System.out.println("Number of dyn impacted points pre-filter: " + numDiffsPrev);
		for (Integer p : new ArrayList<Integer>(pntToDiffs.keySet())) {
			if (!rankingPSlice.containsKey(p))
				pntToDiffs.remove(p);
		}
		final int numDiffs = pntToDiffs.size();
		System.out.println("Number of dyn impacted points post-filter: " + numDiffs + "  (" + (numDiffsPrev - numDiffs) + " not in p-slice)");
		List<Integer> pntsDiffSorted = new ArrayList<Integer>(pntToDiffs.keySet());
		Collections.sort(pntsDiffSorted);
		final float costFactor = 1.0f / (sizeSlice * numDiffs);
		
		System.out.println("Rankings of diff points");
		float costPSlice = 0.0f; // cost of visitng slice in p-slicing order
		for (Integer pntDiff : pntsDiffSorted) {
			System.out.println(pntDiff + ": " + rankingPSlice.get(pntDiff));
			costPSlice += rankingPSlice.get(pntDiff);
		}
		costPSlice *= costFactor;
		System.out.println("Cost of pslice order: " + (costPSlice * 100) + "%");
		
		// generate points of effectiveness-order curve for pslice
		List<Integer> sortedPSlicePnts = sortPointsByRank(rankingPSlice);
		int impactedSoFar = 0;
		int pntsSoFar = 0;
		System.out.println("P-slice effectiveness density: ");
		final float pntFactor = 1.0f / sizeSlice;
		for (int pnt : sortedPSlicePnts) {
			++pntsSoFar;
			if (pntToDiffs.containsKey(pnt))
				++impactedSoFar;
			final float x = pntsSoFar * pntFactor;
			final float y = impactedSoFar / (float)pntsSoFar;
			System.out.println(x + ":" + y);
		}
		System.out.println("P-slice effectiveness accumulated: ");
		final float impactFactor = 1.0f / numDiffs;
		pntsSoFar = 0;
		impactedSoFar = 0;
		for (int pnt : sortedPSlicePnts) {
			++pntsSoFar;
			if (pntToDiffs.containsKey(pnt))
				++impactedSoFar;
			final float x = pntsSoFar * pntFactor;
			final float y = impactedSoFar * impactFactor;
			System.out.println(x + ":" + y);
		}
		
		// generate random orders
		final int MAX_RAND_ORDERS = 100;
		float costAllRandOrders = 0.0f;
		for (int i = 0; i < MAX_RAND_ORDERS; ++i) {
			int[] randRanking = genRandomRanking(sizeSlice);
			float costSlice = 0.0f; // cost of visitng slice in p-slicing order
			int diffIdx = 0;
			for (Integer pntDiff : pntsDiffSorted) {
//				System.out.println(pntDiff + ": " + rankingPSlice.get(pntDiff));
				costSlice += randRanking[diffIdx++];
			}
			costSlice *= costFactor;
			costAllRandOrders += costSlice;
		}
		costAllRandOrders /= MAX_RAND_ORDERS;
		System.out.println("Cost of random order: " + (costAllRandOrders * 100) + "%");
		}
	}
	private static Random rand = new Random();
	private static int[] genRandomRanking(int size) {
		List<Integer> pickFrom = new LinkedList<Integer>();
		for (int i = 1; i <= size; ++i)
			pickFrom.add(i);
		
		int[] ranking = new int[size];
		int i = 0;
		while (!pickFrom.isEmpty()) {
			final int posPick = rand.nextInt(pickFrom.size()); // pos btw 0 and pick_rem_size-1
			final int rank = pickFrom.remove(posPick); // content in range 1..size
			ranking[i++] = rank;
		}
		
		return ranking;
	}
	
	/** For each test output on the orig and mod program, read and diff execution histories of reported points covered AFTER the start point. */
	private int readAndDiffExecHistories(Map<Integer, Integer> pntToDiffs, Set<Integer> pntsCovered) {
		int tId;
		int leftDot = TESTS_PER_DOT;
		int leftDotLines = DOTS_PER_LINE;
		for (tId = 1; true; ++tId) {
			// read exec histories for test tId's output
			Map<Integer,Object> exHistOrig = readExHist(pathOrigOut + tId + ".out");
			if (exHistOrig == null)
				break; // reached end of test outputs
			Map<Integer,Object> exHistMod = readExHist(pathModOut + tId + ".out");
			
			// determine diffs for each point and update point diff counter
			Set<Integer> pnts = new HashSet<Integer>(exHistOrig.keySet());
			pnts.addAll(exHistMod.keySet());
			for (Integer pnt : pnts) {
				// one of the following lists/hashes might be null, but not both
				Object valsOrig = exHistOrig.get(pnt);
				Object valsMod = exHistMod.get(pnt);
				assert valsOrig != null || valsMod != null;
				
				Integer count = pntToDiffs.get(pnt);
				if (count == null)
					count = 0;
				if (valsOrig == null || !valsOrig.equals(valsMod))
					pntToDiffs.put(pnt, ++count); // increment diff counter for this point
			}
			
			// for later
			pntsCovered.addAll(pnts);
			
			// update "dots"
			if (--leftDot == 0) {
				leftDot = TESTS_PER_DOT;
				System.out.print(".");
				
				if (--leftDotLines == 0) {
					leftDotLines = DOTS_PER_LINE;
					System.out.println();
				}
			}
		}
		if (leftDotLines != DOTS_PER_LINE)
			System.out.println();
		return tId;
	}
	
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	/** Ignore data occurring BEFORE start point. */
	private Map<Integer,Object> readExHist(String fpathname) {
		boolean reachedStart = false;
		try{
			Map<Integer,Object> exHist = new HashMap<Integer,Object>();
			
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			String s;
			while ((s = reader.readLine()) != null) {
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));
				
				// store exec hist entry point->val
				if (Options.useHashing()) {
					Long prevHash = (Long) exHist.get(signedPoint);
					long hash = (prevHash == null)? 0 : prevHash;
					exHist.put(signedPoint, hash + (long)val.hashCode());
				}
				else {
					List<String> valsForPnt = (List<String>) exHist.get(signedPoint);
					if (valsForPnt == null) {
						valsForPnt = new ArrayList<String>();
						exHist.put(signedPoint, valsForPnt);
					}
					valsForPnt.add(val);
				}
			}
			
			return exHist;
		}
		catch (FileNotFoundException e) { return null; }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr)
				s = s.substring(0, addrPos + 1);
		}
		return s;
	}
	
	private static Map<Integer,Integer> computePSliceRanking(PSlice pslice) {
		// ensure we access pnts in sorted order
		final Integer start = pslice.getStartPoint();
		List<Integer> pntsSorted = new ArrayList<Integer>(pslice.getAllTargetPoints(start));
		Collections.sort(pntsSorted);
		
		// compute inverse prob->pnts map
		Map<Float,List<Integer>> probToPnts = new HashMap<Float, List<Integer>>();
		for (Integer pntTgt : pntsSorted) {
			final Float prob = pslice.getProb(start, pntTgt);
			// get/create list of pnts for prob
			List<Integer> pntsForProb = probToPnts.get(prob);
			if (pntsForProb == null) {
				pntsForProb = new ArrayList<Integer>();
				probToPnts.put(prob, pntsForProb);
			}
			pntsForProb.add(pntTgt);
		}
		
		// compute sorted list of probs, from greatest to smallest
		List<Float> probsSorted = new ArrayList<Float>(probToPnts.keySet());
		Collections.sort(probsSorted);
		Collections.reverse(probsSorted);
		
		// now, iterate over this sorted probs to assign rank to each point
		Map<Integer,Integer> pntsToRank = new HashMap<Integer, Integer>();
		Integer rank = 0;
		for (Float prob : probsSorted) {
			List<Integer> pnts = probToPnts.get(prob);
			rank += pnts.size();
			for (Integer pnt : pnts)
				pntsToRank.put(pnt, rank);
		}
		return pntsToRank;
	}
	
	/** Right now, if two points have the same rank, sort them by integer order. */
	private static List<Integer> sortPointsByRank(Map<Integer,Integer> rankingPSlice) {
		List<Integer> sortedPnts = new ArrayList<Integer>();
		// create map rank->pnts
		Map<Integer,List<Integer>> rankToPnts = new HashMap<Integer, List<Integer>>();
		for (Integer pnt : rankingPSlice.keySet()) {
			Integer rankOfPnt = rankingPSlice.get(pnt);
			// get create list of points for rank of current pnt
			List<Integer> pntsForRank = rankToPnts.get(rankOfPnt);
			if (pntsForRank == null) {
				pntsForRank = new ArrayList<Integer>();
				rankToPnts.put(rankOfPnt, pntsForRank);
			}
			pntsForRank.add(pnt);
		}
		
		for (int r = 1; r < rankingPSlice.size(); ++r) {
			List<Integer> pntsForRank = rankToPnts.get(r);
			if (pntsForRank == null)
				continue;
			Collections.sort(pntsForRank); // make it deterministic
			sortedPnts.addAll(pntsForRank);
		}
		
		return sortedPnts;
	}
	
}
