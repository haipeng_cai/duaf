import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

/** Create a data structure for Augmented Execution History read from a given file, which is the execution output of the subject;
 *   Here the  AEH is defined a sequence of statement occurrences where each element is in the format of
 *  	<Statement Id with occurrence number, {[value of the variable defined in the statement], program counter>
 */
public class AugExecHistory
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class DynaNode {
		public int sid; // statement id
		public int rhsPos; // PRE_RHS or POST_RHS
		public long or; // order of occurrence
		
		DynaNode() {
			sid = -1; // -1 indicates that this is unknown so far
			rhsPos = NodePoint.POST_RHS; // by default this is 1
			or = 1; // once this instance instantiated, the statement must have at least appeared once
		}
		
		public DynaNode(int n, int rhsPos, long or) { this.sid = n; this.rhsPos = rhsPos; this.or = or; }
		@Override public int hashCode() { return sid << 16 | (int)or << 8 | rhsPos; } // to be finalized if necessary
		@Override public boolean equals(Object o) {
			return sid == ((DynaNode)o).sid && rhsPos == ((DynaNode)o).rhsPos && or == ((DynaNode)o).or; 
		}
		@Override public String toString() { return "{"+sid+"["+rhsPos+"]}^" + or; }
	}
	
	// NodePoint with occurrence no.
	public static class NodePointEx {
		public NodePoint n;
		public long or; // order of occurrence
		
		NodePointEx() {
			n = null; // null indicates that this is unknown so far
			or = 1; // once this instance instantiated, the statement must have at least appeared once
		}
		
		public NodePointEx(NodePoint n, long or) { this.n = n; this.or = or; }
		@Override public int hashCode() { return n.hashCode() << or; } // to be finalized if necessary
		@Override public boolean equals(Object o) {
			return n == ((NodePointEx)o).n && or == ((NodePointEx)o).or; 
		}
		@Override public String toString() { return "{"+n+"}^" + or; }
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static class NodeRecord {
		public Object v; // value of defined variable
		public NodePoint pc; // program counter
		
		NodeRecord() {
			v = null; // null indicates that this is unknown so far
			pc = null; // null indicates that this is unknown so far
		}
		
		public NodeRecord(Object v, NodePoint pc) { this.v = v; this.pc = pc;}
		@Override public int hashCode() { return pc.hashCode() + v.hashCode(); }
		@Override public boolean equals(Object o) {
			return pc == ((NodeRecord)o).pc && v == ((NodeRecord)o).v; 
		}
		@Override public String toString() { return "{v=" + v + ", pc=" + pc + "}"; }
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	
	private String inFile; // file where the history is to be loaded
	public Map<NodePointEx, NodeRecord> stash; // the storage, a map from NodePoint to AEH record
	public Map<Integer, Integer> quickidx; // a quick indexing map: statement id -> number of occurrences
	
	private boolean bStoreValue; // Store values can incur much higher memory consumption 
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	AugExecHistory() { this.inFile = ""; bStoreValue = false;}
	AugExecHistory(String inFile) {	this.inFile = inFile; bStoreValue = false;}
	boolean storeValue() { return this.bStoreValue; }
	void storeValue(boolean bStore) { this.bStoreValue = bStore; }
		
	public int LoadAEH() {
		return LoadAEH(this.inFile);
	}
	public int LoadAEH(String inFile) {
		if (inFile.length() < 1) return -1;
		this.inFile = inFile;
		String infile = this.inFile;
		
		boolean reachedStart = true;
		int startId = 0;
		int lStmtCnt = 0;
		NodePoint lastNode = null, curNode = null;
		String val = null;
		Integer nor = null;
		
		stash = new HashMap<NodePointEx, NodeRecord>();
		quickidx = new HashMap<Integer, Integer>();
		
		try{
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(infile));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX)) {
					continue;
				}
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				//final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// store exec hist entry point->val
				nor = (Integer) quickidx.get(point);
				if ( null == nor ) {
					nor = 0;
				}
				nor++;
				quickidx.put(point, nor);
				
				curNode = new NodePoint(StmtMapper.getNodeFromGlobalId(point), sign-'0');
				
				if ( lStmtCnt >= 1 ) {
					if ( storeValue() ) {
						// parse value
						val = removeFinalAddress(s.substring(bracketPos + 4));
					}
					else {
						val = null;
					}
					stash.put( new NodePointEx(lastNode, nor), new NodeRecord(val, curNode) );
				}
				
				lastNode = curNode;
				lStmtCnt ++;
			}
			
			// the last one read in
			stash.put( new NodePointEx(curNode, nor), new NodeRecord(val, null) );
			return 0;
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return -2; 
		}
		catch (IOException e) {
			throw new RuntimeException(e); 
		}
	}
	
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr) {
				s = s.substring(0, addrPos + 1);
			}
		}
		return s;
	}

	// DEBUG PURPOSES
	public void dumpStash() {
		Set<NodePointEx> pntexs = new HashSet<NodePointEx>(stash.keySet());
		System.out.println("======================= All Records in AEH Stash =====================");
		for (NodePointEx pntex : pntexs) {
			System.out.println("<" + pntex + "," + (NodeRecord)stash.get(pntex));
		}
		System.out.println("============================================================");
	}
	
	public void dumpQuickidx() {
		Set<Integer> pnts = new HashSet<Integer>(quickidx.keySet());
		System.out.println("======================= All Records in AEH Index ====================");
		for (Integer pnt : pnts) {
			System.out.println("(" + (Integer)quickidx.get(pnt) + ")");
		}
		System.out.println("===========================================================");
	}
}

// the end
