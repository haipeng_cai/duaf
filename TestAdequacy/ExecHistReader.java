package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import options.Options;

//Siyuan: This class is used when execution history is read 
public class ExecHistReader {
	private final int startId;

	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();

	private static int totalTestRun;

	// Siyuan: this initialization only works for readExHist no more. It is for
	// sensa run.
	public ExecHistReader(int sId) {
		this.startId = sId;
	}

	// siyuan: this method is used in sensa run(the codes was in sensa run
	// class, but is moved here),
	// return -1 if the exechist is null
	// return -2 if there is a filenotfound exception
	// return 0 if the method succeeds
	public int readExHist(HashMap<Integer, Object> exHist, String fpathname) {
		boolean reachedStart = false;
		totalTestRun = 0;
		
		/** hcai: use the Integer.MIN_VALUE as the default start statement id for full-scope instrumentation, namely
		 * instrument very where in the program, where the start stmt id (giving the change location) is not required
		 */
		if (Integer.MIN_VALUE == this.startId) {
			reachedStart = true;
		}
		// -

		if (null == exHist)
			return -1;
		try {

			// process lines in file, one by one
			//BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			
			BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream(fpathname), "UTF8"));
			
			String s;
			while ((s = reader.readLine()) != null) {

				// find DATA_PREFIX and remove the part before DATA_PREFIX
				if ((!s.startsWith(DATA_PREFIX))
						&& (s.indexOf(DATA_PREFIX) != -1)) {
					s = s.substring(s.indexOf(DATA_PREFIX));
				}

				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;

				// parse point as integer value that is positive if followed by
				// [1] and negative if followed by [0]
				 int bracketPos = 0;
				 int point = 0;
				bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				
				// hcai: DATA_PREFIX could happen to be a part of some values being monitored
				if (bracketPos == -1) continue;
				try {
					point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				}
				catch (Exception e) {
					System.err.println("a freak line skipped.");
					continue;
				}
				// -
				
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}

				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1' ? point : -point;
				assert s.charAt(bracketPos + 2) == ']'
						&& s.charAt(bracketPos + 3) == '=';
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));

				// store exec hist entry point->val
				if (Options.useHashing()) {
					Long prevHash = (Long) exHist.get(signedPoint);
					long hash = (prevHash == null) ? 0 : prevHash;
					exHist.put(signedPoint, hash + (long) val.hashCode());
				} else {

					@SuppressWarnings("unchecked")
					List<String> valsForPnt = (List<String>) exHist.get(signedPoint);
					if (valsForPnt == null) {
						valsForPnt = new ArrayList<String>();
						exHist.put(signedPoint, valsForPnt);
					}
					valsForPnt.add(val);
				}
			}
			if (reachedStart) {
				totalTestRun++;
			}

			reader.close();
			return 0;
		} catch (FileNotFoundException e) {
			return -2;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	/** Ignore data occurring BEFORE start point. */
	public Map<Integer, Object> readExHist(String fpathname) {

		HashMap<Integer, Object> exHist = new HashMap<Integer, Object>();
		int ret = this.readExHist(exHist, fpathname);
		if (ret == -2) {
			return null;
		}
		return exHist;
	}

	public int lastTotalTestRun() {
		return totalTestRun;
	}

	/**
	 * Parse object string by removing its final address part
	 * 
	 * @param s
	 *            the string to be parsed, typically the result of calling
	 *            Object.toString()
	 * @return parsed object string, without final address
	 */
	protected static String removeFinalAddress_TY(String s) {
		int addrStartPos = s.indexOf('@');
		// if there are some '@' to be processed
		while (addrStartPos != -1) {
			int addrEndPos = addrStartPos + 1;

			// find the end index of the address
			// suppose the max length of object address is 8
			for (; addrEndPos <= addrStartPos + 8 && addrEndPos < s.length();) {
				char c = s.charAt(addrEndPos);
				// if still in the hex number sequence
				if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))
					addrEndPos++;
				else
					break;
			}

			// if address founded
			if (addrEndPos > addrStartPos + 1) {
				s = s.substring(0, addrStartPos) + s.substring(addrEndPos);
				addrStartPos = s.indexOf('@');
			} else {
				addrStartPos = s.indexOf('@', addrStartPos + 1);
				continue;
			}
		}

		return s;
	}
	
	/**  hcai: simply remove any possible address part in the given string s: the hashcode as the address part is usually
	 *	  7 to 8 hexadecimal digits
	 */
	private static String removeFinalAddress(String s) {
		return s=s.replaceAll("@[0-9a-f]{1,8}", "");
	}
}
