package experiment.sensa;

import java.util.Comparator;
import java.util.Map;

public class Comparator_stmtdepth implements Comparator<Integer>{
	private Map<Integer, Integer> stmtdepth;
	
	public Comparator_stmtdepth(Map<Integer, Integer> map){
		stmtdepth = map;
	}
	
	public int compare(Integer o1,Integer o2){
		Integer v1=stmtdepth.get(o1), v2=stmtdepth.get(o2);
		if (v1>v2)
			return 1;
		if (v1<v2)
			return -1;
		return 0;
	}
}