package experiment.sensa;

import java.util.Comparator;
import java.util.Map;

public class Comparator_stmtscore implements Comparator<Integer>{
	private Map<Integer, Integer> stmtscores;
	
	public Comparator_stmtscore(Map<Integer, Integer> map){
		stmtscores = map;
	}
	
	public int compare(Integer o1,Integer o2){
		Integer v1=stmtscores.get(o1), v2=stmtscores.get(o2);
		if (v1<v2)
			return 1;
		if (v1>v2)
			return -1;
		return 0;
	}
}