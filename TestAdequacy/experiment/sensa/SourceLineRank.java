package experiment.sensa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import soot.tagkit.Tag;



public class SourceLineRank {
	public static void main(String args[]){
		if(args.length != 3){
			System.out.println("Usage: java experiment.sensa.SourceLineRank <linenumberToStmtidFile> <stmtidRankFile> <outputfile>");
			return;
		}
		
		String linenumberToStmtidFile = args[0];
		String stmtidRankFile = args[1];
		String outputfile = args[2];
		
		Map<Integer, Double> stmtidRanking = Experiment.readRankingFile(stmtidRankFile);
		Map<Integer, CompleteLocation> stmtidLinenumber = readLinenumberToStmtidFile(linenumberToStmtidFile);
		
		Map<String, Double> lineNumberRanking = getLineNumberRanking_highest_string(stmtidRanking, stmtidLinenumber);
		printf_lineNumberRanking_string(lineNumberRanking, outputfile);
		return;
    }
	
	public static void printf_lineNumberRanking(
			Map<CompleteLocation, Double> lineNumberRanking, String outputfilepath) {
		
		try {
			File ranking_output = new File(outputfilepath);
			if(!ranking_output.exists()){
				if(null != ranking_output.getParentFile() && !ranking_output.getParentFile().exists()){
					ranking_output.getParentFile().mkdirs();
				}
				ranking_output.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(ranking_output.getAbsolutePath()));
			
			// sort the ranking
			Map<Double, List<CompleteLocation>> rankToLineNumbers = reverseMap_rankToLineNumbers(lineNumberRanking);
			List<Double> ranklist=new ArrayList<Double>(rankToLineNumbers.keySet());
			Comparator_double comp = new Comparator_double();
			Collections.sort(ranklist, comp);
			
			for(Double rank : ranklist){
				List<CompleteLocation> cls = rankToLineNumbers.get(rank);
				for(CompleteLocation cl : cls){
					out.write(cl.classname + "\t"
							+ cl.methodname + "\t"
							+ cl.linenumber + "\t"
							+ rank + "\n");
				}
			}

			//
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void printf_lineNumberRanking_string(
			Map<String, Double> lineNumberRanking, String outputfilepath) {
		
		try {
			File ranking_output = new File(outputfilepath);
			if(!ranking_output.exists()){
				if(null != ranking_output.getParentFile() && !ranking_output.getParentFile().exists()){
					ranking_output.getParentFile().mkdirs();
				}
				ranking_output.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(ranking_output.getAbsolutePath()));
			
			// sort the ranking
			Map<Double, List<String>> rankToLineNumbers = reverseMap_rankToLineNumbers_string(lineNumberRanking);
			List<Double> ranklist=new ArrayList<Double>(rankToLineNumbers.keySet());
			Comparator_double comp = new Comparator_double();
			Collections.sort(ranklist, comp);
			
			for(Double rank : ranklist){
				List<String> cls = rankToLineNumbers.get(rank);
				for(String cl : cls){
					out.write(cl + "\t" + rank + "\n");
				}
			}

			//
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Map<Double, List<String>> reverseMap_rankToLineNumbers_string(
			Map<String, Double> ranking) {
		Map<Double,List<String>> rankToLineNumbers = new HashMap<Double, List<String>>();
		for (String cl : ranking.keySet()) {
			Double rank = ranking.get(cl);
			
			List<String> clsForRank = rankToLineNumbers.get(rank);
			if (clsForRank == null) {
				clsForRank = new ArrayList<String>();
				rankToLineNumbers.put(rank, clsForRank);
			}
			if(clsForRank.contains(cl)){
				;
			}else{
				clsForRank.add(cl);
			}
		}
		return rankToLineNumbers;
	}
	
	private static Map<Double, List<CompleteLocation>> reverseMap_rankToLineNumbers(
			Map<CompleteLocation, Double> ranking) {
		Map<Double,List<CompleteLocation>> rankToLineNumbers = new HashMap<Double, List<CompleteLocation>>();
		for (CompleteLocation cl : ranking.keySet()) {
			Double rank = ranking.get(cl);
			
			List<CompleteLocation> clsForRank = rankToLineNumbers.get(rank);
			if (clsForRank == null) {
				clsForRank = new ArrayList<CompleteLocation>();
				rankToLineNumbers.put(rank, clsForRank);
			}
			if(clsForRank.contains(cl)){
				;
			}else{
				clsForRank.add(cl);
			}
		}
		return rankToLineNumbers;
	}
	
	private static Map<CompleteLocation, Double> getLineNumberRanking_highest(
			Map<Integer, Double> stmtidRanking,
			Map<Integer, CompleteLocation> stmtidLinenumberTable) {
		Map<CompleteLocation, Double> lineNumberRanking = new HashMap<CompleteLocation, Double>();
		
		for(Map.Entry<Integer, Double> ent : stmtidRanking.entrySet()){
			Integer stmtid = ent.getKey();
			if(stmtid < 0){
				stmtid = -stmtid;
			}
			CompleteLocation cl = stmtidLinenumberTable.get(stmtid);
			if(cl == null){
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"+stmtid+"\n!!!!!!!!!!!!!!!!!!!!!!!!\n");
			}else{
				Double rank = lineNumberRanking.get(cl);
				if(rank == null){
					lineNumberRanking.put(cl, ent.getValue());
				}else{
					if(rank.compareTo(ent.getValue()) <= 0 ){
						;
					}
					else{
						lineNumberRanking.put(cl, ent.getValue());
					}
				}
			}
		}
		return lineNumberRanking;
	}
	
	private static Map<String, Double> getLineNumberRanking_highest_string(
			Map<Integer, Double> stmtidRanking,
			Map<Integer, CompleteLocation> stmtidLinenumberTable) {
		Map<String, Double> lineNumberRanking = new HashMap<String, Double>();
		
		for(Map.Entry<Integer, Double> ent : stmtidRanking.entrySet()){
			Integer stmtid = ent.getKey();
			if(stmtid < 0){
				stmtid = -stmtid;
			}
			CompleteLocation cl = stmtidLinenumberTable.get(stmtid);
			if(cl == null){
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n"+stmtid+"\n!!!!!!!!!!!!!!!!!!!!!!!!\n");
			}else{
				String cl_string = cl.toString();
				Double rank = lineNumberRanking.get(cl_string);
				if(rank == null){
					lineNumberRanking.put(cl_string, ent.getValue());
				}else{
					if(rank.compareTo(ent.getValue()) <= 0 ){
						;
					}
					else{
						lineNumberRanking.put(cl_string, ent.getValue());
					}
				}
			}
		}
		return lineNumberRanking;
	}
	private static Map<Integer, CompleteLocation> readLinenumberToStmtidFile(
			String filename) {
		
		File file = new File(filename);
		assert(file.exists());
		assert(file.isFile());
		
		Map<Integer, CompleteLocation> stmtidLinenumber = new HashMap<Integer, CompleteLocation>();
		try {
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				 String[] stmtlocation = strLine.split("\t");
				 if(stmtlocation.length < 5)
					 continue;
				 CompleteLocation cl = new CompleteLocation();
				 cl.classname = stmtlocation[0].trim();
				 cl.methodname = stmtlocation[1].trim();
				 cl.linenumber = Integer.parseInt(stmtlocation[2].trim());

				 Integer stmtid = Integer.parseInt(stmtlocation[3].trim());
				 stmtidLinenumber.put(stmtid, cl);
			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
		
		return stmtidLinenumber;

	}
}

class CompleteLocation{
	public String classname;
	public String methodname;
	public Integer linenumber;
	
	@Override
	public boolean equals(Object obj){
		if (obj == null && this == null){
            return true;
        }

		if (obj == null && this != null){
			return false;
		}
		
		if (obj != null){
			CompleteLocation clobj = (CompleteLocation)obj;
			if(clobj.classname.equals(this.classname)
					&& clobj.methodname.equals(this.methodname)
					&& clobj.linenumber.equals(this.linenumber)){
				return true;
			}
			else{
				return false;
			}
		}
		return false;
	}
	
	@Override
	public String toString(){
		return classname + "\t" + methodname + "\t" + linenumber;
	}
}
