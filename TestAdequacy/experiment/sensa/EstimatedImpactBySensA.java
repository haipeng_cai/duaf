/* Siyuan: Refactor from ExecHistProb.java
 *       : Uses some functions of ActualImpactByExecHistDiff to compute impact*/

package experiment.sensa;

import java.io.*;
import java.util.*;

public class EstimatedImpactBySensA {
	private ActualImpactByExecHistDiff impactCalculator;
	HashMap<Integer, Integer[]> original_to_newtestids; //initialized at createTmpDirForReadExHist. fake testids created for using actual impact calculating
	

	public static void main(String [] args){
		//
		return;
	}
	
	public EstimatedImpactBySensA(int startid,  String outfiles_path, String run_mode) throws Exception{
				
		if( (!Experiment.isDirectory(outfiles_path)) )
			Experiment.argsError();
		
		String[] outfiles = createTmpDirForReadExHist(outfiles_path);
		String originalOutfile = outfiles[0];
		String modifiedOutfile = outfiles[1];
				
		this.impactCalculator = new ActualImpactByExecHistDiff(startid, originalOutfile, modifiedOutfile, run_mode);

		//clean the temp files
		File tmpfile_original = new File(originalOutfile);
		File tmpfile = tmpfile_original.getParentFile();
		deleteDir(tmpfile);
	}
	
	public Map<Integer, Double> compute_sensaimpact(String output, int[] sampleids) throws Exception{
		
		int []new_sampleids = this.transferToNewsampleidsFromOriginal(sampleids);
		
		/* Only for debug
		for(int i : new_sampleids){
			int [] tmp_sampleid = {i}; 
			Map<Integer, Double> tmpranking = this.impactCalculator.compute_actualimpact(output, tmp_sampleid);
			ArrayList<HashMap<Integer, Double>> tmprankinglist = new ArrayList<HashMap<Integer, Double>>();
			tmprankinglist.add(Experiment.mergeranking(tmpranking, tmpranking));
			Experiment.printf_sensa_rankings("\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\tmp\\tmp"+i,tmprankinglist);
		}
		*/
		
		//use actual impact calculator to calculates sensa impact
		Map<Integer, Double> ranking = this.impactCalculator.compute_actualimpact(output, new_sampleids);
		
		return ranking;
	}
	
	private int[] transferToNewsampleidsFromOriginal(int[] sampleids) {
		ArrayList<Integer> newsampleids = new ArrayList<Integer>();
		
		for(int original_id : sampleids){
			Integer[] tmpnew_sampleids = this.original_to_newtestids.get(original_id);
			if(tmpnew_sampleids == null)
				continue;
			newsampleids.addAll(Experiment.integerarrayToList(tmpnew_sampleids));
		}
		
		return Experiment.listToIntarray(newsampleids);
	}



	// return 2 outfile names
	private String[] createTmpDirForReadExHist(String outfiles_path) {
		//outfiles_path must be a directory
		File outfiledir = new File(outfiles_path);
		File outfiledirparent = outfiledir.getParentFile();
		File tmpout_rootdir = new File(outfiledirparent.getAbsolutePath()+"\\"+outfiledir.getName()+"tmp");
		tmpout_rootdir.mkdirs();
		File tmporiginal_outfile = new File(tmpout_rootdir.getAbsolutePath() + "\\Original");
		tmporiginal_outfile.mkdirs();
		File tmpmodified_outfile = new File(tmpout_rootdir.getAbsolutePath() + "\\Modified");
		tmpmodified_outfile.mkdirs();
				
		this.original_to_newtestids = new HashMap<Integer, Integer[]>();
		String[] tests = outfiledir.list();
		int previous_test_modifiedrun_maxnumber = -1;
		int modifiedrun_counter = 0;
		for(String test : tests){
			try {
				File testfile = new File(outfiledir.getAbsolutePath() + "\\" + test);
				File original_src = new File(testfile.getAbsolutePath() +"\\run1.out");
				
				//System.out.println(test);
				String testid_string = test.substring(4, test.length());
				int testid = Integer.parseInt(testid_string) - 1; //testid is zero-based

				if(! testfile.exists()){
					return null;
				}else if(testfile.list().equals(null)){
					return null;
				}else if(testfile.list().length == 0){
					return null;
				}else if(! testfile.isDirectory()){
					return null;
				}

				
				String[] outputfiles = testfile.list();
				ArrayList<String> runs = ActualImpactByExecHistDiff.filterErrFiles(outputfiles);
				//remove 1.out from runs(because it is the original run)
				for(int index=0;index<runs.size();index++){
					if(runs.get(index).equals("run1.out")){
						runs.remove(index);
						break;
					}
				}
				
				for(String run : runs){
					//For every modified run, create a copy of original run to team up with it
					File original_dst = new File(
							tmporiginal_outfile.getAbsolutePath() + "\\" + (modifiedrun_counter+1) +".out");
					copyFile(original_src, original_dst);
					
					//Create a copy of modified run out file
					File modified_src = new File(testfile.getAbsolutePath() + "\\" + run);
					File modified_dst = new File(
							tmpmodified_outfile.getAbsolutePath() + "\\" + (modifiedrun_counter+1) +".out");
					copyFile(modified_src, modified_dst);
					
					modifiedrun_counter ++;
				}
				
				Integer[] modifiedrun_ids = new Integer[modifiedrun_counter-1-previous_test_modifiedrun_maxnumber];
				for(int id=previous_test_modifiedrun_maxnumber+1;id<modifiedrun_counter;id++){
					modifiedrun_ids[id-previous_test_modifiedrun_maxnumber-1] = id;
				}
				
				this.original_to_newtestids.put(testid, modifiedrun_ids);  //testid is zero based
				previous_test_modifiedrun_maxnumber = modifiedrun_counter-1;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		String[] outfiles = new String[2];
		outfiles[0] = tmporiginal_outfile.getAbsolutePath();
		outfiles[1] = tmpmodified_outfile.getAbsolutePath();
		return outfiles;
	}

	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	public static void copyFile(File src, File dst) throws IOException {
	    InputStream in = new FileInputStream(src);
	    
	    if(!dst.exists()){
	    	dst.getParentFile().mkdirs();
	    	dst.createNewFile();
	    }
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	
	public static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }

	    // The directory is now empty so delete it
	    return dir.delete();
	}
}
