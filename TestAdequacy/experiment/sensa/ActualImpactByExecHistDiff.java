/** Siyuan: 
 *  Compare two versions of execution history and get a result.
 *  *Re-used ExecHistoryDiff.java, Re-used ActualImpactProb.java 
 *  *Increased the max number of stmts recorded(from 10000 to 1000000)
 *  *Doesn't use hashing option(do not use hash function to store execute history diff table).*/

package experiment.sensa;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ActualImpactByExecHistDiff {	
	private int startId;
	private static int MAXM = 1000000; //Siyuan: increase the number of stmts recorded
	private Map<Integer, Map<Integer, Integer>> pntToDiffs_everytest_list; //hold counts of differences for every test in memory
	
	//1st arg: start id. to know where is the start point
	//2nd arg: a folder contains out file generated by original subject code, does not contain last slash
	//3rd arg: a folder contains out file generated by modified subject code, does not contain last slash
	//    For the 2nd and 3rd arg, their output files' name should match.
	//4th arg: runmode
	public ActualImpactByExecHistDiff(int i_startid, String i_originaldir, 
			String i_modifieddir, String run_mode) throws Exception {
		
		startId = i_startid;
		String originalOutfileDir = i_originaldir;
		String modifiedOutfileDir = i_modifieddir;
		pntToDiffs_everytest_list = new HashMap<Integer, Map<Integer, Integer>>();
		
		ArrayList<String> originalOutfile_list = this.ReadFolders(originalOutfileDir, modifiedOutfileDir);
		
		for(String originalOutfilename : originalOutfile_list){
			String originalOutfile = originalOutfileDir + "\\" + originalOutfilename;
			String modifiedOutfile = modifiedOutfileDir + "\\" + originalOutfilename;
			File tmpfile = new File(modifiedOutfile);
			if(! tmpfile.isFile())
				continue;

			String testid_string = originalOutfilename.substring(0, originalOutfilename.length()-4);
			int testid = Integer.parseInt(testid_string) - 1; //testid is zero-based
			if(run_mode.equals("normal")){
				//if(originalOutfilename.equals("140.out")){
				//	int x= 0;
				//}
				Map<Integer,Object> OriginalHistoryAfterStartId = readExHist(originalOutfile);
				Map<Integer,Object> ModifiedHistoryAfterStartId = readExHist(modifiedOutfile);
	
				//if(originalOutfilename.equals("140.out")){
				//	printf_execHist(OriginalHistoryAfterStartId, "\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\v1s1\\tmp\\140.ori.execHist.txt");
				//	printf_execHist(ModifiedHistoryAfterStartId, "\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\v1s1\\tmp\\140.mod.execHist.txt");
				//}
				
				Map<Integer, Integer> pntToDiffs_onetest = new HashMap<Integer, Integer>();
				readAndDiffExecHistories(pntToDiffs_onetest,
						OriginalHistoryAfterStartId, ModifiedHistoryAfterStartId);
				
				/* Debug only
				if(pntToDiffs_onetest.isEmpty()){
					try{
						// Create file 
						FileWriter fstream = new FileWriter("\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\v1s1\\tmp\\"
								+"nohit.txt", true);
						BufferedWriter out = new BufferedWriter(fstream);
						out.write(originalOutfilename+"\n");
						//Close the output stream
						out.close();
					}catch (Exception e){//Catch exception if any
						System.err.println("Error: " + e.getMessage());
					}
				}
				else{
					try{
						// Create file 
						FileWriter fstream = new FileWriter("\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\v1s1\\tmp\\"
								+"hits.txt", true);
						BufferedWriter out = new BufferedWriter(fstream);
						out.write(originalOutfilename+"\n");
						//Close the output stream
						out.close();
					}catch (Exception e){//Catch exception if any
						System.err.println("Error: " + e.getMessage());
					}
				}
				*/
				
				pntToDiffs_everytest_list.put(testid, pntToDiffs_onetest);
				//printf_pntToDiffs(pntToDiffs_onetest, "\\Work\\SensaExperiments\\subject\\trunk\\Subjects\\NanoXML\\experimentresults\\v1s1\\tmp\\"+testid+".pntToDiffs.txt");
				continue;
			}
			
			if(run_mode.equals("pntToDiffs")){
				Map<Integer, Integer> pntToDiffs_onetest = new HashMap<Integer, Integer>();
				//Load pntToDiffs
				pntToDiffs_everytest_list.put(testid, pntToDiffs_onetest);
				throw new Exception("Not finished!");
				//Remember to return!
			}
			
			throw new Exception("run_mode un-recognized");
		}
	}
		
	@SuppressWarnings("unused")
	private void printf_execHist(
			Map<Integer, Object> execHist, String outputfilename) throws IOException{
		File outputfile = new File(outputfilename);
		if(!outputfile.exists()){
			if(!outputfile.getParentFile().exists()){
				outputfile.getParentFile().mkdirs();
			}
			outputfile.createNewFile();
		}
		BufferedWriter out = new BufferedWriter(new FileWriter(outputfile.getAbsolutePath()));
		
		for(Map.Entry<Integer, Object> entry : execHist.entrySet()){
			out.write(entry.getKey() + ":" );
			@SuppressWarnings("unchecked")
			List<String> valsForPnt = (List<String>) entry.getValue();
			for(String s : valsForPnt){
				out.write(s + "," );
			}
			out.write("\n");
		}
		
		out.close();
		
	}

	public void printf_pntToDiffs(
			Map<Integer, Integer> pntToDiffs_onetest, String outputfilename) throws IOException {
		File outputfile = new File(outputfilename);
		if(!outputfile.exists()){
			if(!outputfile.getParentFile().exists()){
				outputfile.getParentFile().mkdirs();
			}
			outputfile.createNewFile();
		}
		BufferedWriter out = new BufferedWriter(new FileWriter(outputfile.getAbsolutePath()));
		
		for(Map.Entry<Integer, Integer> entry : pntToDiffs_onetest.entrySet()){
			out.write(entry.getKey() + ":" +entry.getValue() + "\n");
		}
		
		out.close();

	}

	public Map<Integer, Double>  compute_actualimpact(String actualimpact_output,
			int[] unsampledids) throws Exception {
				
		
		//for(Map.Entry<Integer, Map<Integer,Integer>> entry : pntToDiffs_everytest_list.entrySet()){
		//	if(entry.getValue() == null || entry.getValue().isEmpty()){
		//		;
		//	}else{
		//		System.out.println(""+(entry.getKey()+1)+" has pnts that differ from each other.");
		//	}
		//}
		
		Map<Integer, Integer> allPntToDiffs = new HashMap<Integer, Integer>();
		allPntToDiffs = this.summarizePntToDiffs(unsampledids);
		Map<Integer, Double> ranking =  this.getActualImpact(allPntToDiffs, actualimpact_output);
				
		return ranking;
	}


	private Map<Integer, Double> getActualImpact(Map<Integer, Integer> pntToDiffs, 
			String outputFile) {

		File output = new File(outputFile);
		if(! output.isFile()){
			try {
				File outputpath = output.getParentFile();
				if(! outputpath.exists()){
					outputpath.mkdirs();
				}
				output.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}

		Map<Integer, Double> ranking = getStmtImpact(output, pntToDiffs);
		return ranking;
	}
	
	private Map<Integer, Integer> summarizePntToDiffs(int[] unsampledids) {
		Map<Integer, Integer> summaryPntToDiffs = new HashMap<Integer, Integer>();
		Set<Integer> pnts = new HashSet<Integer>();
		
		for(int unsampledid : unsampledids){ //sampleid is zero-based
			Map<Integer, Integer> pntToDiffs_onetest = 
					pntToDiffs_everytest_list.get(unsampledid); //testid is zero based
			if(pntToDiffs_onetest==null)
				continue;
			
			pnts.addAll(pntToDiffs_onetest.keySet());
			for (Integer pnt : pnts) {
				Integer count = pntToDiffs_onetest.get(pnt);
				Integer basecount = summaryPntToDiffs.get(pnt);
				
				if (count == null)
					count = 0;
				if (basecount == null)
					basecount = 0;
				
				summaryPntToDiffs.put(pnt, count+basecount);
			}
		}
		
		return summaryPntToDiffs;
	}

	private Map<Integer, Double> getStmtImpact(File output, Map<Integer, Integer> stmtscores) {
		Map<Integer, Double> ranking = new HashMap<Integer, Double>();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(output.getAbsolutePath()));
			int stmtids[] = new int[MAXM];
			int i = 1, tmpScore = 0, l = 0, h = 1;
			double no = -1;
			
			List<Integer> keylist=new ArrayList<Integer>(stmtscores.keySet());
			Comparator_stmtscore comp = new Comparator_stmtscore(stmtscores);
			Collections.sort(keylist, comp);
			
			for(Integer key : keylist){
				int value = stmtscores.get(key);
				if(tmpScore == value){
					h=i;
					stmtids[i-1] = key.intValue();
				}else{
					if(l != 0){
						no = (double)(h+l)/2;
						for(int j=l;j<=h;j++){
							out.write(stmtids[j-1]+":"+no+"\n");
							ranking.put(stmtids[j-1], no);
						}
					}
					h = l = i;
					stmtids[i-1] = key.intValue();
					tmpScore = value;
				}
				i++;
			}
			no = (double)(h+l)/2;
			if(l>0){
				for(int j=l; j<=h; j++){
					out.write(stmtids[j-1]+":"+no+"\n");
					ranking.put(stmtids[j-1], no);
				}
			}
			else{
				for(int j=l; j<=h; j++){
					out.write(stmtids[j]+":"+no+"\n");
					ranking.put(stmtids[j], no);
				}
			}
			out.close();
			return ranking;
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		return null;
	}

	private void readAndDiffExecHistories(
			Map<Integer, Integer> pntToDiffs,
			Map<Integer,Object> OriginalHistoryAfterStartId,
			Map<Integer,Object> ModifiedHistoryAfterStartId) {
		// determine diffs for each point and update point diff counter
		Set<Integer> pnts = new HashSet<Integer>(OriginalHistoryAfterStartId.keySet());
		pnts.addAll(ModifiedHistoryAfterStartId.keySet());
		for (Integer pnt : pnts) {
			// one of the following lists/hashes might be null, but not both
			Object valsOrig = OriginalHistoryAfterStartId.get(pnt);
			Object valsMod = ModifiedHistoryAfterStartId.get(pnt);
			assert valsOrig != null || valsMod != null;
			
			Integer count = pntToDiffs.get(pnt);
			if (count == null)
				count = 0;
			
			if (valsOrig == null || !valsOrig.equals(valsMod)){
				pntToDiffs.put(pnt, ++count); // increment diff counter for this point
			}
		}
	}

	private ArrayList<String> ReadFolders(String originalOutfileDir, String modifiedOutfileDir) {
		//need to be called after the initialization of fields
		if( (!Experiment.isDirectory(originalOutfileDir)) || 
				(! Experiment.isDirectory(modifiedOutfileDir) ))
			Experiment.argsError();
		
		File originalOutfileDirFile = new File(originalOutfileDir);
		String[] originalOutfiles = originalOutfileDirFile.list();
		File modifiedOutfileDirFile = new File(modifiedOutfileDir);
		String[] modifiedOutfiles = modifiedOutfileDirFile.list();
		
		ArrayList<String> originalOutfile_list = filterErrFiles(originalOutfiles);
		ArrayList<String> modifiedOutfile_list = filterErrFiles(modifiedOutfiles);

		if(originalOutfile_list.size() == 0) {
			System.out.println("No out file in "+originalOutfileDir+"!");
			System.exit(0);
		}
		if(modifiedOutfile_list.size() == 0) {
			System.out.println("No out file in "+modifiedOutfileDir+"!");
			System.exit(0);
		}
		return originalOutfile_list;
	}
	
	public static ArrayList<String> filterErrFiles(String[] files) {
		ArrayList<String> outfiles = new ArrayList<String>();
		
		for(int index=0;index<files.length;index++){
			if(files[index].endsWith(".out"))
				outfiles.add(files[index]);
		}
		
		return outfiles;
	}

	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	/** Ignore data occurring BEFORE start point. */
	private Map<Integer,Object> readExHist(String fpathname) {
		boolean reachedStart = false;
		try{
			Map<Integer,Object> exHist = new HashMap<Integer,Object>();
			
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				
				if(point == 3283){
					int y =0;
				}
				
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));
				
				// store exec hist entry point->val
				Object tmpvalue = exHist.get(signedPoint);
				@SuppressWarnings("unchecked")
				List<String> valsForPnt = (List<String>) tmpvalue;
				if (valsForPnt == null) {
					valsForPnt = new ArrayList<String>();
					exHist.put(signedPoint, valsForPnt);
				}
				valsForPnt.add(val);
			}
			//Finished reading
			reader.close();
			
			//if (reachedStart)
			//{
			//	System.out.println("Get the startId = "+startId);
			//}
			return exHist;
		}
		catch (FileNotFoundException e) { 
			e.printStackTrace();
			return null; 
		}
		catch (IOException e) { 
			throw new RuntimeException(e); 
		}
	}

	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr)
				s = s.substring(0, addrPos + 1);
		}
		return s;
	}
	
}


