package experiment.sensa;

/*
 * Mainly copy from: Yiji-Zhang
 * 
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EstimatedImpactByWslice {
	private final Map<Integer,Map<Integer,Integer>> wslice = new HashMap<Integer, Map<Integer,Integer>>();
	private Map<Integer,Integer> tgtToDepth;
	private static int MAXM = 1000000; //the number of stmts recorded

	public EstimatedImpactByWslice(int pntStart, String fname) {
		// parse src->tgt->depth map from file
		try{
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fname));
			String s;
			while ((s = reader.readLine()) != null) {
				// parse src pnt (if wslice-all) and set start pos in line
				Integer signedSrcPnt;
				int maxPos = s.length();
				int start;
				boolean vertical;
				if (s.indexOf(':') == -1) {
					signedSrcPnt = pntStart;
					start = 0;
					vertical = true;
				}else{
					// parse src point as integer value that is positive if followed by [1] and negative if followed by [0]
					final int bracketPos = s.indexOf('[');
					final Integer pSrc = Integer.valueOf(s.substring(0, bracketPos));
					final char signSrc = s.charAt(bracketPos + 1);
					assert signSrc == '0' || signSrc == '1';
					signedSrcPnt = signSrc == '1'? pSrc : -pSrc;
					
					start = bracketPos + 5;
					if (start > maxPos)
						continue; // list is empty!
					assert s.substring(bracketPos + 2, start).equals("]: ");
					
					vertical = false;
				}

				tgtToDepth = new HashMap<Integer, Integer>();
				wslice.put(signedSrcPnt, tgtToDepth);

				// parse list of tgt=depth point-depth list for this src pnt
				int end;
				while (start < maxPos) {
					// parse tgt point as integer value that is positive if followed by [1] and negative if followed by [0]
					end = s.indexOf('[', start);
					Integer pTgt = Integer.valueOf(s.substring(start, end));
					final char signTgt = s.charAt(end + 1);
					if (signTgt == '0')
						pTgt = -pTgt;
					else
						assert signTgt == '1';
					
					assert s.charAt(end + 2) == ']' && s.charAt(end + 3) == '=';
					
					// parse tgt point's depth from src
					start = end + 4;
					end = s.indexOf(' ', start);
					if (end == -1)
						end = maxPos;
					final Integer depth = Integer.valueOf(s.substring(start, end));
					
					if (vertical) {
						s = reader.readLine();
						if (s == null)
							break;
						start = 0;
						maxPos = s.length();
					}
					else
						start = end + 1;
					
					tgtToDepth.put(pTgt, depth);
				}
			}
			reader.close();
		}catch (Exception e) { throw new RuntimeException(e); }
	}
	
	public Map<Integer, Double> compute_wsliceimpact(String wsliceimpact_output) {
		Map<Integer, Double> ranking =  this.getWsliceImpact(tgtToDepth, wsliceimpact_output);
		return ranking;
	}

	private Map<Integer, Double> getWsliceImpact(
			Map<Integer, Integer> statement_depths, String outputfile) {
		
		File output = new File(outputfile);
		if(! output.isFile()){
			try {
				File outputpath = output.getParentFile();
				if(! outputpath.exists()){
					outputpath.mkdirs();
				}
				output.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}

		Map<Integer, Double> ranking = new HashMap<Integer, Double>();
		
		BufferedWriter out;
		try {
			out = new BufferedWriter(new FileWriter(output.getAbsolutePath()));
			int stmtids[] = new int[MAXM];
			int i = 1, tmpDepth = 0, l = 0, h = 1;
			double no = -1;
			
			List<Integer> keylist=new ArrayList<Integer>(statement_depths.keySet());
			Comparator_stmtdepth comp = new Comparator_stmtdepth(statement_depths);
			Collections.sort(keylist, comp);
			
			for(Integer key : keylist){
				int value = statement_depths.get(key);
				if(tmpDepth == value){
					h=i;
					stmtids[i-1] = key.intValue();
				}else{
					if(l != 0){
						no = (double)(h+l)/2;
						for(int j=l;j<=h;j++){
							out.write(stmtids[j-1]+":"+no+"\n");
							ranking.put(stmtids[j-1], no);
						}
					}
					h = l = i;
					stmtids[i-1] = key.intValue();
					tmpDepth = value;
				}
				i++;
			}
			no = (double)(h+l)/2;
			if(l>0){
				for(int j=l; j<=h; j++){
					out.write(stmtids[j-1]+":"+no+"\n");
					ranking.put(stmtids[j-1], no);
				}
			}
			else{
				for(int j=l; j<=h; j++){
					out.write(stmtids[j]+":"+no+"\n");
					ranking.put(stmtids[j], no);
				}
			}
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ranking;
	}
}
