package experiment.sensa;

import java.util.ArrayList;

public class getCostOfEffectiveness
{
	public static void main(String[] args)
	{
		if (args.length < 5) {
			Experiment.argsError();
		}
		try {
			if (5 == args.length) {
				Experiment.get_Effectiveness_and_cost_from2rankings(args[0],args[1],args[2],args[3],args[4]);
				return;
			}
			
			ArrayList<String> curvefiles = new ArrayList<String>();
			for (int i=0; i < args.length-1; ++i) {
				curvefiles.add(args[i]);
			}
			Experiment.merge_EffectivenessCurves_sameInterval(curvefiles, args[args.length-1]);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
