package experiment.sensa;


import java.io.*;
import java.math.BigDecimal;
import java.util.*;

//Calling ActualImpactProb, ExecHistProb, StmtProbAnalysis
//Doing experiments specified in "SensA Design" document 
public class Experiment {
	//hardcodes the experiment design's setting.
	private int[] samplesizes = {1,5,10,20,50};
	//private String[] sensa_strategies = {"rand", "inc", "obsv"}; //rand: random; inc: increment; obsv: observed
	static private String[] sensa_strategies = {"rand", "inc"}; //rand: random; inc: increment; obsv: observed
	//static private String[] sensa_strategies = {"rand"}; //only rand will be experimented
	//static private String[] sensa_strategies = {"inc"}; //only inc will be experimented
	//static private String[] sensa_strategies = {"obsv"}; //only inc will be experimented
	//
	
	//need to be initialized by arguments
	static private int runnumber_persamplesize;
	private int startid;
	private int[] testinputsids;
	private String actualexechist_outfiledir_original; // a directory contains outfiles or pntToDiffs files, corresponding to the run_mode in run method
	private String actualexechist_outfiledir_modified; // a directory contains outfiles or pntToDiffs files, corresponding to the run_mode in run method
	private String sensaexechist_outfiledir; // or directory contains pntToDiffs files, corresponding to the run_mode in run method
	private String forward_slicing_path; //the path specifies the file containing forward slicing of the change location in original code 
	private String outputroot_dir;
	private ArrayList<ArrayList<Integer>> picked_samples; //track every runnumber_persamplesize runs' sample. make sure they are not same with each other
	//
	
	//have default value
	public boolean allsetanalysis = false; //if it is true, actual impact will be computed from the same set from which sensa impact will be computed.
	
	public void overwriteSamplesizes(int[] i_samplesizes){
		samplesizes = i_samplesizes;
		return;
	}
	
	public Experiment(int i_runnumber_persamplesize, 
			int i_startid,
			int testnumber, //Please make sure every test has its running outputs
			String i_actualexechist_outfiledir_original,
			String i_actualexechist_outfiledir_modified,
			String i_sensaexechist_outfiledir,
			String i_forward_slicing_path,
			String i_outputroot) {
		runnumber_persamplesize = i_runnumber_persamplesize;
		startid = i_startid;
		testinputsids = new int[testnumber];
		for(int counter=0;counter<testnumber;counter++){
			testinputsids[counter] = counter;   //sample id and test id is zero based
		}
		actualexechist_outfiledir_original = i_actualexechist_outfiledir_original;
		actualexechist_outfiledir_modified = i_actualexechist_outfiledir_modified;
		sensaexechist_outfiledir = i_sensaexechist_outfiledir;
		forward_slicing_path = i_forward_slicing_path;
		outputroot_dir = i_outputroot;
		
		picked_samples = new ArrayList<ArrayList<Integer>>();
	}

	public void get_ranking_wslice(String wsliceimpact_output, String wslice_outfiledir) {
		if(wslice_outfiledir.equals(null) || wslice_outfiledir.equals(0)){
			; // if there is not wslice, do nothing
		}else{
			/*
			EstimatedImpactByWslice estimatedImapctByWsliceCalculator;
			estimatedImapctByWsliceCalculator = new EstimatedImpactByWslice(startid, wslice_outfiledir);
			
			Map<Integer, Double> wslice_ranking = estimatedImapctByWsliceCalculator.compute_wsliceimpact(wsliceimpact_output+".ori.txt");
			append_pntsInStaticSlicing(wslice_ranking, forward_slicing_path);
			
			printf_ranking(wsliceimpact_output, wslice_ranking);
			*/
		}
	}

	public void get_ranking_ideal(String actual_ranking_file, String idealranking_output) {
		// Check if the actual ranking exist
		File f = new File(actual_ranking_file);
		if(! f.exists()){
			System.err.println("Error: please actual_ranking_file doesn't exist, cannot generate ideal ranking.");
			return;
		}
		
		// Generate ideal ranking by adding points into actual ranking
		Map<Integer, Double> actualranking = readRankingFile(outputroot_dir + "\\actualimpact.txt");
		append_pntsInStaticSlicing(actualranking, forward_slicing_path);
		
		// output ideal ranking
		printf_ranking(idealranking_output, actualranking);
	}

	public void get_ranking_sensa(String run_mode) throws Exception {
		//import all sensa execution history outfiles to memory
		EstimatedImpactBySensA[] estimatedimpactCalculators = new EstimatedImpactBySensA[sensa_strategies.length];
		for(int index=0;index<sensa_strategies.length;index++){
			String sensa_strategy = sensa_strategies[index];
			String sensaexechist_outfiledir_oneseed_onestrategy = sensaexechist_outfiledir + "-" + sensa_strategy;
			estimatedimpactCalculators[index] = new EstimatedImpactBySensA(startid,  
					sensaexechist_outfiledir_oneseed_onestrategy,
					run_mode);
		}

		//
		for(int samplesize : samplesizes ){
			List<HashMap<Integer, Double>> aggregate_rankings = new ArrayList<HashMap<Integer, Double>>();
			// there is one aggregate ranking for each strategy
			for(int i=0;i<sensa_strategies.length;i++){
				aggregate_rankings.add(new HashMap<Integer, Double>());
			}
			
			//for every analysis, repeat the analysis "runnumber_persamplesize" times
			for(int runcounter=0;runcounter<runnumber_persamplesize;runcounter++){
				int[] sampleids; 
				if(this.allsetanalysis == true)
					sampleids = testinputsids;
				else{
					sampleids = pickNRandom(testinputsids, samplesize); //<---------------Regular process
					//sampleids = new int[1];     //<-----------------------debug mode to see every test's performance
				}
				
				try {
					inner_calculateSensaImpact(estimatedimpactCalculators,
							samplesize, aggregate_rankings, runcounter,
							sampleids);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			String ranking_output_prefix = outputroot_dir + "\\agg_size" + samplesize;
			for(HashMap<Integer, Double> ranking : aggregate_rankings){
				append_pntsInStaticSlicing(ranking, forward_slicing_path);
			}
			printf_sensa_rankings(ranking_output_prefix, aggregate_rankings);
		}
	}

	public void get_ranking_actual(String run_mode, String actualimpact_output) throws Exception {
		//import all execution history outfiles to memory for actual impact computing
		ActualImpactByExecHistDiff actualimpactCalculator;
		actualimpactCalculator = new ActualImpactByExecHistDiff(
				startid, actualexechist_outfiledir_original,
				actualexechist_outfiledir_modified, run_mode
				);
		
		//Calculating actual impacts
		actualimpactCalculator.compute_actualimpact(actualimpact_output, testinputsids);
	}

	private void append_pntsInStaticSlicing(
			Map<Integer, Double> ranking,
			String i_forward_slicing_path) {
		try {
			int pntscnt_inStaticSlicing = 0;
			{
				FileInputStream fstream = new FileInputStream(i_forward_slicing_path);
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				while ((br.readLine()) != null)   {
					pntscnt_inStaticSlicing = pntscnt_inStaticSlicing+1;
				}
				in.close();
				System.out.printf("static slicing size: " + pntscnt_inStaticSlicing + "\n");
			}
			
			{
				int original_pntscnt_inSensaRanking = ranking.size();
				System.out.printf("original ranking size: " + original_pntscnt_inSensaRanking + "\n");

				FileInputStream fstream = new FileInputStream(i_forward_slicing_path);
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;
				ArrayList<Integer> appendPntList = new ArrayList<Integer>();
				
				int same_pntscnt = 0;
				int duplicate_pntscnt = 0;
				while ((strLine = br.readLine()) != null)   {
					try{
						int pntid = Integer.parseInt(strLine);
						
						//check if pntid exists in the ranking. If not, add pntid into the ranking
						if(ranking.containsKey(new Integer(pntid))){
							same_pntscnt ++;
						}
						else{
							if(! appendPntList.contains(pntid))
								appendPntList.add(pntid);
							else{
								duplicate_pntscnt++;
								//System.out.printf("  duplicate:"+pntid+"\n");
							}
						}
						
					} catch (NumberFormatException e1){
						e1.printStackTrace();
					} 
				}
				for(Integer pntid : appendPntList){
					ranking.put(pntid, 
						(new Double(original_pntscnt_inSensaRanking+1+
								original_pntscnt_inSensaRanking+appendPntList.size()))
						 /2);
				}
				in.close();

				System.out.printf("same points cnt: " + same_pntscnt + "\n");
				System.out.printf("duplicate points cnt: " + duplicate_pntscnt + "\n");
				System.out.printf("final ranking points cnt: " + ranking.size() + "\n\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void inner_calculateSensaImpact(
			EstimatedImpactBySensA[] estimatedimpactCalculators, 
			int samplesize, List<HashMap<Integer, Double>> aggregate_rankings,
			int runcounter, int[] sampleids) throws Exception {
		//Calculating sensa impacts
		for(int strategyid = 0; strategyid < sensa_strategies.length; strategyid++){
			String sensa_strategy = sensa_strategies[strategyid];
			String Sensaimpact_output = outputroot_dir + "\\size" + samplesize
					+ "-run" + (runcounter+1) + "-sensaimpact-"+sensa_strategy+".txt";
			
			Map<Integer, Double> ranking = estimatedimpactCalculators[strategyid].compute_sensaimpact(Sensaimpact_output, sampleids);
			Map<Integer, Double> existing_ranking = aggregate_rankings.get(strategyid);
			aggregate_rankings.set(strategyid, mergeranking(existing_ranking, ranking));
		}
	}


	static public void printf_sensa_rankings(String ranking_output_prefix,
			List<HashMap<Integer, Double>> aggregate_rankings) {
		for(int strategyid=0;strategyid<sensa_strategies.length;strategyid++){
			String outputfilepath = ranking_output_prefix+"-"+sensa_strategies[strategyid]+".txt";
			HashMap<Integer, Double> ranking = aggregate_rankings.get(strategyid);
			printf_ranking(outputfilepath, ranking);
		}
	}

	static public void printf_ranking(String outputfilepath,
			Map<Integer, Double> ranking) {
		try {
			File ranking_output = new File(outputfilepath);
			if(!ranking_output.exists()){
				if(null != ranking_output.getParentFile() && !ranking_output.getParentFile().exists()){
					ranking_output.getParentFile().mkdirs();
				}
				ranking_output.createNewFile();
			}
			BufferedWriter out = new BufferedWriter(new FileWriter(ranking_output.getAbsolutePath()));
			
			Map<Double, List<Integer>> rankToPnts = reverseMap_rankToPoints(ranking);
			List<Double> ranklist=new ArrayList<Double>(rankToPnts.keySet());
			Comparator_double comp = new Comparator_double();
			Collections.sort(ranklist, comp);
			// sort the aggregate ranking
			for(Double rank : ranklist){
				List<Integer> pntsForRank = rankToPnts.get(rank);
				for(Integer pnt : pntsForRank){
					out.write(pnt + ":" + (rank/runnumber_persamplesize) + "\n");
				}
			}
			//
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	static public HashMap<Integer, Double> mergeranking(
			Map<Integer, Double> existing_ranking, Map<Integer, Double> newranking) {
		
		HashMap<Integer, Double> merged_ranking = new HashMap<Integer, Double>();
		
		Set<Integer> newranking_stmtids = newranking.keySet();
		for(Map.Entry<Integer, Double> entry : existing_ranking.entrySet()){
			Integer currentkey = entry.getKey();
			if( newranking_stmtids.contains(currentkey) ){
				Double new_rankingscore = newranking.get(currentkey);
				Double existing_rankingscore = entry.getValue();
				Double aggregate_rankingscore = new_rankingscore+existing_rankingscore; 
				merged_ranking.put(currentkey, aggregate_rankingscore);
			}
			else{
				merged_ranking.put(currentkey, entry.getValue());
			}
		}
		
		Set<Integer> existing_ranking_stmtids = existing_ranking.keySet();
		for(Map.Entry<Integer, Double> entry : newranking.entrySet()){
			Integer currentkey = entry.getKey();
			if( ! existing_ranking_stmtids.contains(currentkey)){
				merged_ranking.put(currentkey, entry.getValue());
			}
		}
		
		return merged_ranking;
	}

	@SuppressWarnings("unused")
	private static int[] getComplement(int[] completeset, int[] sample){
		if(sample.length >= completeset.length)
			innerError();
		
		ArrayList<Integer> samplelist = new ArrayList<Integer>();
		for (int index = 0; index < sample.length; index++)
	    {
			samplelist.add(sample[index]);
	    }
		
	    int[] answer = new int[completeset.length - sample.length];
	    int answerindex = 0;
		for(int index=0;index<completeset.length;index++){
			Integer id = index; // zero-based
			if(samplelist.contains(id)){
				;
			}else{
				answer[answerindex] = id;
				answerindex++;
			}
		}
	    return answer;
	}

	private static void innerError() {
		System.out.println("Get a sample size which is bigger than/equals to the whole set.(random pick sample inputs)");
		System.exit(0);
	}

	public static void argsError() {
		System.out.println("Arguments are not correct!");
		System.exit(0);
	}

	public static boolean isDirectory(String string) {
		File newfile = new File(string);
		boolean isDirectory = newfile.isDirectory();
		return isDirectory;
	}

	public static ArrayList<Integer> intarrayToList(int[] newsample) {
		ArrayList<Integer> newsamplelist = new ArrayList<Integer>(newsample.length);
	    for (int i : newsample)
	    	newsamplelist.add(i);
		return newsamplelist;
	}	

	public static ArrayList<Integer> integerarrayToList(Integer[] newsample) {
		ArrayList<Integer> newsamplelist = new ArrayList<Integer>(newsample.length);
	    for (Integer i : newsample)
	    	newsamplelist.add(i);
		return newsamplelist;
	}	
	
	public static int[] listToIntarray(List<Integer> integers) 
	{ 
	    int[] ret = new int[integers.size()]; 
	    for (int i=0; i < ret.length; i++) 
	    { 
	        ret[i] = integers.get(i).intValue(); 
	    } 
	    return ret; 
	} 
	
	public int[] pickNRandom(int[] array, int n){
		int []newsample = pickNRandom_inner(array,n);
		ArrayList<Integer> newsamplelist = intarrayToList(newsample);
		Comparator_int comp = new Comparator_int();
		Collections.sort(newsamplelist, comp);

		while ( picked_samples.contains(newsamplelist) ){
			newsample = pickNRandom_inner(array,n);
			newsamplelist = intarrayToList(newsample);
		}
		
		picked_samples.add(newsamplelist);
		return newsample;
	}
	
	private static int[] pickNRandom_inner(int[] array, int n) {

	    List<Integer> list = new ArrayList<Integer>(array.length);
	    for (int i : array)
	        list.add(i);
	    Collections.shuffle(list);

	    int[] answer = new int[n];
	    for (int i = 0; i < n; i++)
	        answer[i] = list.get(i);
	    Arrays.sort(answer);

	    return answer;
	}

	public void get_Effectiveness_accumulatedcurve() {
		for(int samplesize : samplesizes){
			for(int strategyid = 0; strategyid < sensa_strategies.length; strategyid++){
	
				String sensa_strategy = sensa_strategies[strategyid];
				String originalaccumulateddata = outputroot_dir 
						+ "\\_effecitiveness_accumulated_size" + samplesize + "-" + sensa_strategy
						+ ".txt";
				String outputfilepath = outputroot_dir 
						+ "\\_effecitiveness_accumulatedCurve_size" + samplesize + "-" + sensa_strategy
						+ ".txt";
				
				get_metric("accumulated");
				computeEffectiveness_accumulatedCurve(originalaccumulateddata,
						outputfilepath);
			}
		}
	}

	private static void computeEffectiveness_accumulatedCurve(
			String originalaccumulateddata, String outputfilepath) {
		computeEffectiveness_accumulatedCurve_withIntervalSetting(
				originalaccumulateddata, outputfilepath, (float)0.001);
	}
	
	private void get_Effectiveness_accumulatedcurve_wslice() {
		String originalaccumulateddata = outputroot_dir 
					+ "\\_effecitiveness_accumulated_wslice.txt";
		String outputfilepath = outputroot_dir 
					+ "\\_effecitiveness_accumulatedCurve_wslice.txt";
		
		get_metric_wslice("accumulated");
		computeEffectiveness_accumulatedCurve(originalaccumulateddata, outputfilepath);
	}
	
	private void get_Effectiveness_accumulatedcurve_ideal(){
		String originalaccumulateddata = outputroot_dir 
				+ "\\_effecitiveness_accumulated_ideal.txt";
		String outputfilepath = outputroot_dir 
				+ "\\_effecitiveness_accumulatedCurve_ideal.txt";
	
		get_metric_ideal("accumulated");
		computeEffectiveness_accumulatedCurve(originalaccumulateddata, outputfilepath);		
	}
	
	private void get_metric_wslice(String effectiveness_sort) {
		File ranking_folder = new File(outputroot_dir);
		if(! ranking_folder.exists()){
			System.out.println("experiment.get_ranking hasn't created results!");
			System.exit(0);
		}
		else{
			Map<Integer, Double> acutalRanking = readRankingFile(outputroot_dir + "\\actualimpact.txt");
			
			Map<Integer, Double> wsliceRanking = readRankingFile(outputroot_dir + "\\wsliceimpact.txt");
			if(effectiveness_sort.equals("accumulated")){
				String outputfilepath = outputroot_dir + "\\_effecitiveness_accumulated_wslice.txt";
				computeEffectiveness_accumulated(
						outputfilepath,
						acutalRanking, 
						wsliceRanking);
			}
			
			if(effectiveness_sort.equals("cost")){
				String outputfilepath = outputroot_dir + "\\_cost_size_wslice.txt";
				computeCost(outputfilepath,
						acutalRanking, 
						wsliceRanking);
			}
		}
	}

	private void get_metric_ideal(String effectiveness_sort) {
		File ranking_folder = new File(outputroot_dir);
		if(! ranking_folder.exists()){
			System.out.println("experiment.get_ranking hasn't created results!");
			System.exit(0);
		}
		else{
			Map<Integer, Double> acutalRanking = readRankingFile(outputroot_dir + "\\actualimpact.txt");
			
			Map<Integer, Double> idealRanking = readRankingFile(outputroot_dir + "\\idealranking.txt");
			if(effectiveness_sort.equals("accumulated")){
				String outputfilepath = outputroot_dir + "\\_effecitiveness_accumulated_ideal.txt";
				computeEffectiveness_accumulated(
						outputfilepath,
						acutalRanking, 
						idealRanking);
			}
			
			if(effectiveness_sort.equals("cost")){
				String outputfilepath = outputroot_dir + "\\_cost_size_ideal.txt";
				computeCost(outputfilepath,
						acutalRanking, 
						idealRanking);
			}
		}
	}
	
	private static void printf_Map_FloatToFloat(Map<Float, Float> map,
			String outputfilepath) {
		File output = new File(outputfilepath);
		try {
			if(!output.exists()){
				if(null != output.getParentFile() && !output.getParentFile().exists()){
					output.getParentFile().mkdirs();
				}
				output.createNewFile();
			}
			
			BufferedWriter out = new BufferedWriter(new FileWriter(output));
			
			SortedSet<Float> keys = new TreeSet<Float>(map.keySet());
			for (Float key : keys) { 
				Float value = map.get(key);
				out.write(key+":"+value+"\n");
			}
			
			out.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static float linearInterpolation(float last_x, float last_y, float current_x, float current_y, float target_x) {
		 //Pre conditions
		/* the following assertion was commented out by hcai, since this assertion is not necessarily 
		 * true, esp. with a small interval involved in the interpolation 
		 */
		//assert last_x<target_x;
		assert target_x<current_x;
		
		if((current_y-last_y) < 0.000001)
			return (current_y+last_y)/2;
		//Calculate slope from p1 to p2
		float m = (current_x-last_x)/(current_y-last_y);
		//Calculate y position of x
		float y = (target_x-last_x)*m+last_y;
		//create new point
		return y;
	}
	
	public void get_all_metrics(){
		get_Effectiveness_accumulatedcurve();
		get_Effectiveness_accumulatedcurve_wslice();
		get_Effectiveness_accumulatedcurve_ideal();
		get_metric("cost");
		get_metric_wslice("cost");
		get_metric_ideal("cost");
	}
	
	public static void get_Effectiveness_and_cost_from2rankings(String sensarankingfile, String actualrankingfile, 
			String accumulatedEffectiveness_outputfilepath, String cost_outputfilepath, String curveEffectiveness_outputfilepath) {
		get_Effectiveness_and_cost_from2rankings_withIntervalSetting(sensarankingfile, actualrankingfile, accumulatedEffectiveness_outputfilepath,
				cost_outputfilepath, curveEffectiveness_outputfilepath, (float) 0.001);
	}
	
	public static void get_Effectiveness_and_cost_from2rankings_withIntervalSetting(String sensarankingfile, String actualrankingfile, 
			String accumulatedEffectiveness_outputfilepath, String cost_outputfilepath, String curveEffectiveness_outputfilepath,
			float interval){
		
		Map<Integer, Double> acutalRanking = readRankingFile(actualrankingfile);
		Map<Integer, Double> sensaRanking  = readRankingFile(sensarankingfile);
		computeEffectiveness_accumulated(accumulatedEffectiveness_outputfilepath, acutalRanking, sensaRanking);
		computeCost(cost_outputfilepath, acutalRanking, sensaRanking);
		computeEffectiveness_accumulatedCurve_withInterval_revised(accumulatedEffectiveness_outputfilepath, curveEffectiveness_outputfilepath,
				interval);
	}
	
	private static void computeEffectiveness_accumulatedCurve_withIntervalSetting(
			String originalaccumulateddata, String outputfilepath, float interval) {
		try {
			String sCurrentLine;
			BufferedReader br;
 
			br = new BufferedReader(new FileReader(originalaccumulateddata));
		
			BigDecimal last_x = new BigDecimal("0");
			BigDecimal last_y = new BigDecimal("0");
			BigDecimal current_percentage = new BigDecimal(Float.toString(interval));
			BigDecimal span = new BigDecimal(Float.toString(interval));

			Map<Float, Float> effectivenesscurve = new HashMap<Float, Float>();
			while ((sCurrentLine = br.readLine()) != null) {
				String[] effectiveness = sCurrentLine.split(":");
				if(effectiveness.length == 2){
					BigDecimal x = new BigDecimal(effectiveness[0]);
					BigDecimal y = new BigDecimal(effectiveness[1]);

					if(x.compareTo(current_percentage) == -1)
					{
						;
					}
					
					if(x.compareTo(current_percentage) == 0)
					{
						effectivenesscurve.put(x.floatValue(), y.floatValue());
						current_percentage = current_percentage.add(span);
					}
					
					if(x.compareTo(current_percentage) == 1)
					{
						float current_percentage_y = linearInterpolation(last_x.floatValue(), last_y.floatValue(),
								x.floatValue(), y.floatValue(), current_percentage.floatValue());
						effectivenesscurve.put(current_percentage.floatValue(), current_percentage_y);
						current_percentage = current_percentage.add(span);
					}
					
					last_x = x;
					last_y = y;
					
					if(current_percentage.equals(1) || current_percentage.floatValue() > 1) //don't replace it with 1.00
						break;
				}
			}
			
			br.close();
			
			printf_Map_FloatToFloat(effectivenesscurve, outputfilepath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	// --- hcai begins
	/* -- added for assuring linear interpolation leading to all X-axis points in [0.0,1.0] with specified interval
	 * 
	 *   It is presumed that all tuples in the file originalaccumulateddata are already sorted by their element
	 *   in non-descending order
	 *   
	 *   In addition, current processing supports approximation of X:[0.0,1.0] only.
	*/
	private static void computeEffectiveness_accumulatedCurve_withInterval_revised(
			String originalaccumulateddata, String outputfilepath, float interval) 
	{
	
		class CHelper 
		{
			class compareBigDecimal implements Comparator<BigDecimal>
			{
				public int compare(BigDecimal o1, BigDecimal o2) {
					return o1.compareTo(o2);
				}
			}
			
			Map<BigDecimal, BigDecimal> dataset;
			List<BigDecimal> keys;
			CHelper(Map<BigDecimal,BigDecimal> _dataset) {
				this.dataset = _dataset;
				
				keys =new ArrayList<BigDecimal>(dataset.keySet());
				Collections.sort(keys, new compareBigDecimal());
			}
			// return value indicates if the the value to be approximated has already been in the original set
			public boolean searchBound(BigDecimal val, BigDecimal[] bounds) {
				BigDecimal preVal = null;
				for (BigDecimal curv:this.keys) {
					if (0 == curv.compareTo(val)) {
						bounds[2] = curv;
						return true;
					}
					// the first value happening to be larger than val is the upper bound to be found and, the 
					// last value to be smaller than val is the lower bound
					if (1 == curv.compareTo(val)) {
						bounds[1] = curv;
						if (null != preVal) {
							bounds[0] = preVal;
						}
						break;
					}
					else {
						preVal = curv;
					}
				}
				return false;
			}
		}
		
		try {
			String sCurrentLine;
			BufferedReader br;
			
			br = new BufferedReader(new FileReader(originalaccumulateddata));
		
			final BigDecimal lowerlimit = new BigDecimal("0.0");
			final BigDecimal upperlimit = new BigDecimal("1.0");
			
			BigDecimal current_percentage = new BigDecimal(Float.toString(interval));
			BigDecimal span = new BigDecimal(Float.toString(interval));
			
			Map<Float, Float> effectivenesscurve = new HashMap<Float, Float>();
			Map<BigDecimal, BigDecimal> orgDataset = new HashMap<BigDecimal, BigDecimal>(); 
			
			orgDataset.put(lowerlimit, lowerlimit);
			// read the raw accumulated effectiveness into orgDataset 
			while ((sCurrentLine = br.readLine()) != null) {
				String[] strXY = sCurrentLine.split(":");
				if(2 != strXY.length){
					continue;
				}
				orgDataset.put(new BigDecimal(strXY[0]), new BigDecimal(strXY[1]));
			}
			br.close();
			orgDataset.put(upperlimit, upperlimit);

			CHelper helper = new CHelper(orgDataset);
			float current_percentage_y = 0;
			while ( 1 != current_percentage.compareTo(upperlimit) ) {
					BigDecimal[] bounds = new BigDecimal[]{new BigDecimal("0.0"), new BigDecimal("1.0"), current_percentage};
					boolean bExist = helper.searchBound(current_percentage, bounds);
					BigDecimal lower = bounds[0];
					BigDecimal upper = bounds[1];

					if ( bExist ) {
						current_percentage_y = orgDataset.get(bounds[2]).floatValue();
					}
					else {
						assert (null != lower && null != upper);
						// approximate towards the Y value corresponding to current_percentage by linear interpolation				{
						current_percentage_y = linearInterpolation(
								lower.floatValue(), orgDataset.get(lower).floatValue(),
								upper.floatValue(), orgDataset.get(upper).floatValue(),
								current_percentage.floatValue());
					}
					effectivenesscurve.put(current_percentage.floatValue(), current_percentage_y);
					current_percentage = current_percentage.add(span);
			}
	
			printf_Map_FloatToFloat(effectivenesscurve, outputfilepath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	// ---- hcai ends

	/*
	 * Note that the accumulated curves should have SAME interval between two points in the curves 
	 * and the number of points in each curve should be same, too */
	public static void merge_EffectivenessCurves_sameInterval(ArrayList<String> accumulatedCurve_filelist, String outputfilepath){
		Map<BigDecimal, BigDecimal> accumulatedCurve = new HashMap<BigDecimal, BigDecimal>();
		
		for(String curvefile : accumulatedCurve_filelist){
			File file = new File(curvefile);
			assert(file.exists());
			assert(file.isFile());

			try {
				FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;
				while ((strLine = br.readLine()) != null)   {
					 String[] XandY = strLine.split(":");
					 if(XandY.length != 2)
						 continue;
					 BigDecimal x = new BigDecimal(XandY[0].trim());
					 BigDecimal y = new BigDecimal(XandY[1].trim());
					 if(accumulatedCurve.containsKey(x)){
						 BigDecimal aggregate_y = accumulatedCurve.get(x);
						 BigDecimal new_aggregate_y = aggregate_y.add(y);
						 accumulatedCurve.put(x, new_aggregate_y);
					 }else{
						 accumulatedCurve.put(x, y);
					 }
				}
				in.close();
								
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException ioe){
				ioe.printStackTrace();
			}
		}
		
		//compute the average of each y
		Map<Float, Float> accumulatedCurve_float = new HashMap<Float, Float>();
		BigDecimal totalNumber = new BigDecimal(accumulatedCurve_filelist.size());
		for(Map.Entry<BigDecimal, BigDecimal> ent : accumulatedCurve.entrySet()){
			BigDecimal aggY = ent.getValue();
			ent.setValue(aggY.divide(totalNumber, 6, BigDecimal.ROUND_FLOOR));
			accumulatedCurve_float.put(ent.getKey().floatValue(), ent.getValue().floatValue());
		}
		
		printf_Map_FloatToFloat(accumulatedCurve_float, outputfilepath);

	}
	
	// refactor from ExechistDiff.java
	// metric_sort: "accumulated"; "cost"
	private void get_metric(String effectiveness_sort) {
		File ranking_folder = new File(outputroot_dir);
		if(! ranking_folder.exists()){
			System.out.println("experiment.get_ranking hasn't created results!");
			System.exit(0);
		}
		else{
			for(int samplesize : samplesizes){
				
				Map<Integer, Double> acutalRanking = readRankingFile(outputroot_dir + "\\actualimpact.txt");

				for(int strategyid = 0; strategyid < sensa_strategies.length; strategyid++){
					String sensa_strategy = sensa_strategies[strategyid];
					Map<Integer, Double> sensaRanking = readRankingFile(outputroot_dir + "\\agg_size"
							+ samplesize + "-" + sensa_strategy +".txt");
					if(effectiveness_sort.equals("accumulated")){
						String outputfilepath = outputroot_dir + "\\_effecitiveness_accumulated_size" + samplesize + "-" + sensa_strategy
								+ ".txt";
						computeEffectiveness_accumulated(
								outputfilepath,
								acutalRanking, 
								sensaRanking);
					}
					
					if(effectiveness_sort.equals("cost")){
						String outputfilepath = outputroot_dir + "\\_cost_size" + samplesize + "-" + sensa_strategy
								+ ".txt";
						computeCost(outputfilepath,
								acutalRanking, 
								sensaRanking);
					}
				}
			}
		}
	}

	private static void computeCost(String outputfilepath,
			Map<Integer, Double> acutalRanking,
			Map<Integer, Double> sensaRanking){
		int numDiffs = acutalRanking.size();
		try {
			int sizeSensaRanking = sensaRanking.size();
			
			List<Integer> pntsDiffSorted = new ArrayList<Integer>(acutalRanking.keySet());
			Collections.sort(pntsDiffSorted);
			final float costFactor = 1.0f / (sizeSensaRanking * numDiffs);
			
			float sumOfSensaRankingForActualImpactedPnt = 0.0f;
			for (Integer pntDiff : pntsDiffSorted) {
				Double sensa_ranking = sensaRanking.get(pntDiff);
				if(sensa_ranking != null){
					sumOfSensaRankingForActualImpactedPnt = sumOfSensaRankingForActualImpactedPnt + (float)(double)sensa_ranking;
				}
			}
			float averageSensaRankingForActualImpacedtPnt = sumOfSensaRankingForActualImpactedPnt * costFactor;
			
			//write the result
			File cost_output;
			BufferedWriter cost_out;
			cost_output = new File(outputfilepath);
			if(!cost_output.exists()){
				if(null != cost_output.getParentFile() && !cost_output.getParentFile().exists()){
					cost_output.getParentFile().mkdirs();
				}
				cost_output.createNewFile();
			}
			cost_out = new BufferedWriter(new FileWriter(cost_output));
			cost_out.write((averageSensaRankingForActualImpacedtPnt * 100)+"");
			cost_out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void computeEffectiveness_accumulated(String outputfilepath,
			Map<Integer, Double> acutalRanking,
			Map<Integer, Double> sensaRanking) {
		
		
		try {
			int numDiffs = acutalRanking.size();
			float impactFactor = 1.0f / numDiffs;
			List<Integer> sortedSensaRanking = sortPointsByRank(sensaRanking);

			int sizeSensaRanking = sensaRanking.size();
			float pntFactor = 1.0f / sizeSensaRanking;
									
			int pntsSoFar = 0;
			int impactedSoFar = 0;
			
			// prepare the output file
			File accumulated_output;
			BufferedWriter accumulated_out;
			accumulated_output = new File(outputfilepath);
			if(!accumulated_output.exists()){
				if(null != accumulated_output.getParentFile() && !accumulated_output.getParentFile().exists()){
					accumulated_output.getParentFile().mkdirs();
				}
				accumulated_output.createNewFile();
			}
			accumulated_out = new BufferedWriter(new FileWriter(accumulated_output));
			//
			for (int pnt : sortedSensaRanking) {
				++pntsSoFar;
				if (acutalRanking.containsKey(pnt)){
					++impactedSoFar;
				}
				float x = pntsSoFar * pntFactor;
				float y = impactedSoFar * impactFactor;
				accumulated_out.write(x + ":" + y + "\n");
			}
			//
			accumulated_out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Map<Integer, Double> readRankingFile(String filename) {
		File file = new File(filename);
		assert(file.exists());
		assert(file.isFile());
		
		Map<Integer, Double> stmtranking = new HashMap<Integer, Double>();
		try {
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
				 String[] stmtranks = strLine.split(":");
				 if(stmtranks.length != 2)
					 continue;
				 Integer stmtid = Integer.parseInt(stmtranks[0].trim());
				 Double rank = new Double(stmtranks[1]);
				 stmtranking.put(stmtid, rank);
			}
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
		
		return stmtranking;
	}

	//refactor from ExechistDiff.java
	public static List<Integer> sortPointsByRank(Map<Integer,Double> ranking) {
		List<Integer> sortedPnts = new ArrayList<Integer>();
		// create map rank->pnts
		Map<Double, List<Integer>> rankToPnts = reverseMap_rankToPoints(ranking);
		
		List<Double> ranklist=new ArrayList<Double>(rankToPnts.keySet());
		Comparator_double comp = new Comparator_double();
		Collections.sort(ranklist, comp);

		for (Double r : ranklist) {
			List<Integer> pntsForRank = rankToPnts.get(r);
			if (pntsForRank == null)
				continue;
			Collections.sort(pntsForRank); // make it deterministic
			sortedPnts.addAll(pntsForRank);
		}
		
		return sortedPnts;
	}

	private static Map<Double, List<Integer>> reverseMap_rankToPoints(
			Map<Integer, Double> ranking) {
		Map<Double,List<Integer>> rankToPnts = new HashMap<Double, List<Integer>>();
		for (Integer pnt : ranking.keySet()) {
			Double rankOfPnt = ranking.get(pnt);
			// get create list of points for rank of current pnt
			List<Integer> pntsForRank = rankToPnts.get(rankOfPnt);
			if (pntsForRank == null) {
				pntsForRank = new ArrayList<Integer>();
				rankToPnts.put(rankOfPnt, pntsForRank);
			}
			pntsForRank.add(pnt);
		}
		return rankToPnts;
	}
}
