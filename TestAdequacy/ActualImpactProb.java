import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import options.Options;

/** Analyzes dynamic slices of changes in different versions to determine whether and which changes interacted. */
public class ActualImpactProb
{
	
	private final int startId;
	private final String pathBase;
	private final String version;
	private static String pathOrigOut, pathModOut;
	private static int totalTestRun, totalFile, expectedTests=100000; // by default, warning is restrained.
	private static Map<Integer,Integer>stmt;
	
	private static int MAXN = 1000, MAXM = 10000;
	
	class compare implements Comparator<Integer>{
		
		public int compare(Integer o1,Integer o2){
			Integer v1=stmt.get(o1), v2=stmt.get(o2);
			if (v1<v2)
				return 1;
			if (v1>v2)
				return -1;
			return 0;
		}
	}
	
	public static void main(String[] args) {
		
		ActualImpactProb analysis = new ActualImpactProb(args);
		
		String outFilePath = analysis.pathBase + "/ActualImpactProbRank-" + analysis.version + ".txt";
		// -- hcai start
		if ( null != System.getProperty("LoadMap") ) {
			if ( args.length > 4 ) {
				outFilePath = args[args.length-2];
				expectedTests = Integer.parseInt(args[args.length-1]);
			} else {
				outFilePath = args[args.length-1];
			}
		}
		// -- hcai end
		
		File outFile = new File(outFilePath);
		PrintStream out;

		try {
			out = new PrintStream(new FileOutputStream(outFile));
			System.setOut(out);
			
			ActualImpactProb.pathOrigOut = analysis.pathBase + "/outdyn-"+analysis.version+"-orig-"+analysis.startId+"/";
			ActualImpactProb.pathModOut  = analysis.pathBase + "/outdyn-"+analysis.version+"-"+analysis.startId+"/";
			// -- hcai start
			if ( null != System.getProperty("LoadMap") ) {
				analysis.runWithMaps(expectedTests);
			}
			// -- hcai end
			else {
				analysis.run();
			}

			List<Integer> key=new ArrayList<Integer>(stmt.keySet());
			compare comp=analysis.new compare();
			Collections.sort(key, comp);

			int stmtobj[] = new int[MAXM];
			int i = 1, tmpScore = 0, l = 0, h = 1;
			double no = -1;;
			for(Integer obj:key){
				int itmp = stmt.get(obj);
				if(tmpScore == itmp){
					h = i;
					stmtobj[i] = obj.intValue();
				}
				else {
					if(l != 0){
						no = (double)(h+l)/2;
						for(int j=l;j<=h;j++){
							System.out.println(stmtobj[j]+" : "+no);
						}
					}
					h = l = i;
					stmtobj[i] = obj.intValue();
					tmpScore = itmp;
				}
				
				i++;
			}
			no = (double)(h+l)/2;
			for(int j=l;(!key.isEmpty()) && j<=h;j++){
				System.out.println(stmtobj[j]+" : "+no);
			}
			
			out.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/** arg 0: starting_sId   (e.g., 2501)
	    arg 1: base path of files to be analyzed  */
	private ActualImpactProb(String[] args) {
		this.startId = Integer.valueOf(args[0]);
		this.pathBase = args[1];
		this.version = args[2];
		this.totalTestRun=0;
		this.stmt=new HashMap<Integer,Integer>();	
	}

	private void run() {
		Map<Integer, Integer> pntToDiffs = new HashMap<Integer, Integer>();
		Set<Integer> pntsCovered = new HashSet<Integer>();
		final int numTests = readAndDiffExecHistories(pntToDiffs, pntsCovered) - 1;
		assert numTests > 0;
		
		// DEBUG: covered points
		List<Integer> pntsCovSorted = new ArrayList<Integer>(pntsCovered);	
		Collections.sort(pntsCovSorted);
	}
	
	// -- added by hcai
	// an alternative to run() for occasions where the .out files to be loaded already contain the 
	// map:stmtid->#changes info. so we just need to reduce them together per stmtid across all tests in that case  
	private void runWithMaps(int expectedRuns)
	{
		int tId;
		boolean flag = true;
		int count_impacted = 0;
		for (tId = 1; true; ++tId) {
			flag = true;
			try {
				BufferedReader reader = new BufferedReader(new FileReader(pathBase + tId + "/1.out"));
				String s;
				while ((s = reader.readLine()) != null) {
					String pair[] = s.split(":");
					Integer sid = Integer.valueOf(pair[0]), scnt = Integer.valueOf(pair[1]);
					Integer count = stmt.get(sid);
					if (null == count) count = 0;
					stmt.put(sid, scnt + count);
					
					if(flag){
						flag = false;
						count_impacted ++;
					}
				}
			}
			catch (FileNotFoundException e) { 
				if ( tId < expectedTests + 1 ) {
					System.err.println(pathBase + tId + "/1.out" + " was not found.");
				}
				break;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
		}
		
		//System.err.println(tId + "tests have been processed.");
	}
	// ---
	
	/** For each test output on the orig and mod program, read and diff execution histories of reported points covered AFTER the start point. */
	private int readAndDiffExecHistories(Map<Integer, Integer> pntToDiffs, Set<Integer> pntsCovered) {
		int tId;
		// make sure the original file would not be counted.
		
		boolean flag = true;
		int count_impacted = 0;
		for (tId = 1; true; ++tId) {
			flag = true;
			int bak = totalTestRun;
			Map<Integer,Object> exHistOrig = readExHist(pathOrigOut + tId + ".out");
			totalTestRun = bak;
			Map<Integer,Object> exHistMod = readExHist(pathModOut + tId + ".out");
			if (exHistMod == null)
				break; // reached end of test outputs
			
			// determine diffs for each point and update point diff counter
			Set<Integer> pnts = new HashSet<Integer>(exHistOrig.keySet());
			if (exHistMod != null) pnts.addAll(exHistMod.keySet());
			pntsCovered.addAll(pnts);
			for (Integer pnt : pnts) {
				// one of the following lists/hashes might be null, but not both
				Object valsOrig = exHistOrig.get(pnt);
				Object valsMod = exHistMod.get(pnt);
				assert valsOrig != null || valsMod != null;
				Integer count = pntToDiffs.get(pnt);
				if (count == null)
					count = 0;
				Integer count2 = stmt.get(pnt);
				if (count2 == null)
					count2 = 0;
				if (valsOrig == null || !valsOrig.equals(valsMod)){
					pntToDiffs.put(pnt, ++count); // increment diff counter for this point
					stmt.put(pnt, ++count2);
					if(flag){
						flag = false;
						count_impacted ++;
					}
				}
			}
		}

		return tId;
	}
	
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	/** Ignore data occurring BEFORE start point. */
	private Map<Integer,Object> readExHist(String fpathname) {
		boolean reachedStart = false;
		try{
			Map<Integer,Object> exHist = new HashMap<Integer,Object>();
			
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));
				
				// store exec hist entry point->val
				if (Options.useHashing()) {
					Long prevHash = (Long) exHist.get(signedPoint);
					long hash = (prevHash == null)? 0 : prevHash;
					exHist.put(signedPoint, hash + (long)val.hashCode());
				}
				else {
					List<String> valsForPnt = (List<String>) exHist.get(signedPoint);
					if (valsForPnt == null) {
						valsForPnt = new ArrayList<String>();
						exHist.put(signedPoint, valsForPnt);
					}
					valsForPnt.add(val);
				}
			}
			if (reachedStart)
			{
				totalTestRun++;
			}
			return exHist;
		}
		catch (FileNotFoundException e) { 
			//e.printStackTrace();
			return null; }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr)
				s = s.substring(0, addrPos + 1);
		}
		return s;
	}
	
	
}
