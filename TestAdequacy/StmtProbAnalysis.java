

import java.io.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class StmtProbAnalysis {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String dirPath = null;
		dirPath = args[0];

		errorCalculate(0.95f, dirPath, args[1], args[2]);
	}

	
	private static void errorCalculate(float pct, String fdir, String sdir, String s2dir){ //pct = percentage, 0f-1.0f
		System.out.println("===Start calculating error for the first " + (int)(pct*100) +"% of stmts... ...");
		
		String dirPath = fdir;
		float avrError = 0.0f;
		float totalDist = 0.0f;
		float totalStmtCnt = 0.0f; // to store the number of stmt
		float actualRank = 0.0f;
		float min = 1.0f;
		float max = 0f;
		
		List<Float> absDistList = new ArrayList<Float>();
		List<Float> avrAbsDistList = new ArrayList<Float>();
		List<Integer> stmtList = new ArrayList<Integer>();
		List<Float>sensaRankList = new ArrayList<Float>();
		List<Float>actualRankList = new ArrayList<Float>();
		
		// -- changed by hcai to adapt to the Linux file/directory naming 
		/*
		// Read SortedDynstmtRankMap file and SortedStastmtRankMap file and store them back into two maps
//		Map<Integer, Float>sensastmtRankMap = readFileToMap(dirPath + "\\DynImpactProbRank-326-rand_100.txt");
		Map<Integer, Float>sensastmtRankMap = readFileToMap(dirPath + "\\" + sdir);
//		Map<Integer, Float>actualstmtRankMap = readFileToMap(dirPath + "\\ActualImpactProbRank-v8.txt");
		Map<Integer, Float>actualstmtRankMap = readFileToMap(dirPath + "\\" + s2dir);
		*/
		Map<Integer, Float> sensastmtRankMap = null, actualstmtRankMap = null;
		String osname = System.getProperty("os.name");
		if ( osname.equalsIgnoreCase("Linux") ) {
			sensastmtRankMap = readFileToMap(dirPath + "/" + sdir);
			actualstmtRankMap = readFileToMap(dirPath + "/" + s2dir);
		}
		else if ( osname.equalsIgnoreCase("Windows") ) {
			sensastmtRankMap = readFileToMap(dirPath + "\\" + sdir);
			actualstmtRankMap = readFileToMap(dirPath + "\\" + s2dir);
		}
		else {
			System.out.println("Unhandled operating system environment: " + osname);
			return;
		}

		// Calculate ALL asbDist for each stmt and store in a list
		Set<Entry<Integer, Float>> sensaSet = sensastmtRankMap.entrySet();
		Iterator<Entry<Integer, Float>> iter = sensaSet.iterator();
		while(iter.hasNext()){
			Map.Entry<Integer, Float> entry = (Map.Entry<Integer, Float>)iter.next(); 		
			int stmtId = (Integer) entry.getKey();
			float sensaRank = (Float) entry.getValue();
			
			if (actualstmtRankMap.containsKey(stmtId)){ // This stmt (in static ranking) is also in dynamic ranking
				actualRank = actualstmtRankMap.get(stmtId);
			}
			else{
				actualRank = (sensastmtRankMap.size() + (actualstmtRankMap.size() + 1)) * 0.5f;
			}
			float absDist = Math.abs(sensaRank-actualRank);
			
			absDistList.add(absDist); // All the absDist is stored in this list
			stmtList.add(stmtId);  
			sensaRankList.add(sensaRank);
			actualRankList.add(actualRank);
			
			totalStmtCnt++; // The number of stmts/ also the size of asbDistList
		}
		
		// those in Actual impact ranking but not in sensa ranking
		Set<Entry<Integer, Float>> actualSet = actualstmtRankMap.entrySet();
		iter = actualSet.iterator();
		int tot=0;
		while(iter.hasNext()){
			Map.Entry<Integer, Float> entry = (Map.Entry<Integer, Float>)iter.next(); 		
			int stmtId = (Integer) entry.getKey();
			if (!sensastmtRankMap.containsKey(stmtId))
				tot++;
		}
		iter = actualSet.iterator();
		int k = 0;
		while(iter.hasNext())
		{
			Map.Entry<Integer, Float> entry = (Map.Entry<Integer, Float>)iter.next(); 		
			int stmtId = (Integer) entry.getKey();
			actualRank = (Float) entry.getValue();
			k++;
			if (!sensastmtRankMap.containsKey(stmtId)){
				float sensaRank;
				sensaRank = sensastmtRankMap.size() + (tot+1) * 0.5f;
				float absDist = Math.abs(sensaRank-actualRank);
				
				absDistList.add(absDist); 
				stmtList.add(stmtId);  
				sensaRankList.add(sensaRank);
				actualRankList.add(actualRank);
				
				totalStmtCnt++; 
			}
		}
		// Calculate error for part of the stmts (first 5%, 10%, ..., 100%)
		int i;
		int partstmtCnt = 0;
		List<Float> partAbsDistList = new ArrayList<Float>();
		for (i=0; i < (totalStmtCnt*pct) ; i++){
			float absDist = absDistList.get(i);
		
			// Min and Max for absDist. Need to /(stmtCnt * stmtCnt) after stmtCnt is finally calculated
			if (absDist<min)
				min = absDist;
			if (absDist>max)
				max = absDist;
			
			totalDist += absDist;
			partstmtCnt ++;
			partAbsDistList.add(absDist);
		}

		// Average 
		avrError = totalDist / (totalStmtCnt * partstmtCnt); 
		// Min and Max
		min = min/totalStmtCnt;
		max = max/totalStmtCnt;
		// Standard Deviation
		// asbDist/partstmtCnt should be the "dataset". Iterate absDist List and Devide each element by stmtCnt and store them into avrAbsDist List
		Iterator iterList = partAbsDistList.iterator();
		while (iterList.hasNext()){
			float absDist = (Float) iterList.next();
			float avrAbsDist = absDist / (float) totalStmtCnt;
			avrAbsDistList.add(avrAbsDist);
		}
		float stdDev = getStdDeviation(avrAbsDistList);
		
		// Output the result
		System.out.println("Avrage Error = "+ avrError);
		System.out.println("Min    Error = "+ min);
		System.out.println("Max    Error = "+ max);
		System.out.println("Standard Deviation = "+ stdDev);
	}
	
	public static Map<Integer, Float> readFileToMap(String fPath){
		String filePath = fPath;
		File file = new File(filePath);
		
		BufferedReader lineReader = null;
		String lineString = null;
		
		String intStr = new String();
		String floatStr = new String();
		
		Map<Integer, Float> fileMap = new HashMap<Integer, Float>();
		
		int i,j;
		
		try {
			lineReader = new BufferedReader(new FileReader(file));
			while((lineString = lineReader.readLine()) != null){ 
				StringBuffer intSb = new StringBuffer();
				StringBuffer floatSb = new StringBuffer();
				
				for(i=0;i<lineString.length();i++){
					char x = lineString.charAt(i);
					if (x != ' '){
						intSb.append(x);
					}
					else
						break;
				}
				for(j=i+3 ; j<lineString.length() ; j++){
					char y = lineString.charAt(j);
					floatSb.append(y);
				}
				intStr = intSb.toString();
				floatStr = floatSb.toString();
				
				int intNum = Integer.parseInt(intStr);
				float floatNum = Float.parseFloat(floatStr);
				
				fileMap.put(intNum, floatNum);
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileMap;
	}
	
	public static Map<String, Integer> readStrFileToMap(String fPath){
		String filePath = fPath;
		File file = new File(filePath);
		
		BufferedReader lineReader = null;
		String lineString = null;
		
		String nameStr = new String();
		String intStr = new String();
		
		Map<String, Integer> fileMap = new HashMap<String, Integer>();
		
		int i,j;
		
		try {
			lineReader = new BufferedReader(new FileReader(file));
			while((lineString = lineReader.readLine()) != null){ 
				StringBuffer intSb = new StringBuffer();
				StringBuffer floatSb = new StringBuffer();
				
				for(i=0;i<lineString.length();i++){
					char x = lineString.charAt(i);
					if (x != ':'){
						intSb.append(x);
					}
					else
						break;
				}
				for(j=i+2 ; j<lineString.length() ; j++){
					char y = lineString.charAt(j);
					floatSb.append(y);
				}
				nameStr = intSb.toString();
				intStr = floatSb.toString();
				
				int intNum = Integer.parseInt(intStr);
				
				fileMap.put(nameStr, intNum);
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fileMap;
	}
	
	public static float getStdDeviation(List<Float> dataset){
		float stdDev = 0f;
		float sum = 0f;
		
		// Calculate avr first
		Iterator iter1 = dataset.iterator();
		while (iter1.hasNext()){
			sum = sum + (Float)iter1.next();
		}
		float avr = sum / (float) dataset.size();
		
		// stdDev = SUM[(xi - avr)^2] * 1/N (or 1/N-1)
		Iterator iter2 = dataset.iterator();
		while (iter2.hasNext()){
			sum = sum + (float) Math.pow((Float)iter2.next()-avr, 2);
		}
		stdDev = sum / (float) (dataset.size() - 1);
		
		return stdDev;
	}
}
