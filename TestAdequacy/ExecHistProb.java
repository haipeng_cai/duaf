/**
 * Produce SensA ranking using the DEA method - execution history differencing 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import options.Options;

/** Analyzes dynamic slices of changes in different versions to determine whether and which changes interacted. */
public class ExecHistProb {
	
	private final int startId;
	private final String pathBase;
	private static String pathOrigOut, pathModOut;
	private static int totalTestRun, totalFile, expectedRuns=21;
	private static Map<Integer,Integer>stmt;
	private static HashSet<Integer> fwdslice;
	
	private static int MAXN = 1000, MAXM = 10000;
	
	class changeCntComparator implements Comparator<Integer>{
		
		public int compare(Integer o1,Integer o2){
			Integer v1=stmt.get(o1), v2=stmt.get(o2);
			if (v1<v2)
				return 1;
			if (v1>v2)
				return -1;
			return 0;
		}
	}
	
	/*
	 * args expected in order: totalFile, startId/changeLoc, pathBase, outputFile [,expectedRuns_under_each_test]
	 */
	public static void main(String[] args) {
		boolean bLoadMaps = null != System.getProperty("LoadMap");
		//int offset = bLoadMaps && (args.length>4) ? 1:0;
		int offset = args.length - 4;
		totalFile = Integer.parseInt(args[0]);
		String[] args2 = new String[args.length-2-offset];		
		String outFilePath = args[args.length-1-offset];
		
		String fwdsliceFile = "";
		if (1 == offset ) {
			expectedRuns = Integer.parseInt(args[args.length-1]);
		}
		else if (2 == offset) {
			expectedRuns = Integer.parseInt(args[args.length-2]);
			fwdsliceFile = args[args.length-1];
		}
		
		System.arraycopy(args, 1, args2, 0, args.length-2-offset);
		
		ExecHistProb analysis = new ExecHistProb(args2);
		File outFile = new File(outFilePath);
		PrintStream out;
		try {
			for(int i=1;i<=totalFile;i++){

				ExecHistProb.pathOrigOut = analysis.pathBase + i + "/run";
				ExecHistProb.pathModOut  = analysis.pathBase + i + "/run";
				//System.out.println(ExecHistProb.pathModOut);
				//System.out.println("now processing test No. " + i);
				if ( bLoadMaps ) {
					analysis.runWithMaps(i, expectedRuns);
				}
				else {
					analysis.run();
				}
			}
			
			analysis.dumpSliceQuantities(fwdsliceFile, true, true);
			
//			System.out.println("Total Valid_Test*Run: "+totalTestRun);
			List<Integer> key=new ArrayList<Integer>(stmt.keySet());
			changeCntComparator comp=analysis.new changeCntComparator();
			Collections.sort(key, comp);
			
			out = new PrintStream(new FileOutputStream(outFile));
			System.setOut(out);
			
			int stmtobj[] = new int[MAXM];
			int i = 1, tmpScore = 0, l = 0, h = 1;
			double no = -1;;
			for(Integer obj:key){
//				System.out.println(obj+": "+stmt.get(obj)+"   "+(double)stmt.get(obj)*100/totalTestRun+"%");
				int itmp = stmt.get(obj);
				if(tmpScore == itmp){
					h = i;
					stmtobj[i] = obj.intValue();
				}
				else {
					if(l != 0){
						no = (double)(h+l)/2;
						for(int j=l;j<=h;j++){
							System.out.println(stmtobj[j] + " : " + no);
//							System.out.println(no + "  " + stmtobj[j] + "  " + (double)tmpScore*100/totalTestRun+"%");
						}
					}
					h = l = i;
					stmtobj[i] = obj.intValue();
					tmpScore = itmp;
				}
				
				i++;
			}
			no = (double)(h+l)/2;
			for(int j=l;(!key.isEmpty()) && j<=h;j++){
				System.out.println(stmtobj[j] + " : " + no);
//				System.out.println(no + "  " + stmtobj[j] + "  " + (double)tmpScore*100/totalTestRun+"%");
			}
			out.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	/**
	 * dump the content of unranked statements in "this.stmt" with either the raw change count or probability
	 * @param bProbability - dump statements with probability when being true
	 * @param bSort - if sorting before dumping
	 */
	private void dumpSliceQuantities(String fnfwdslice, boolean bProbability, boolean bSort) {
		List<Integer> keys=new ArrayList<Integer>(stmt.keySet());
		if (bSort) {
			changeCntComparator comp= new changeCntComparator();
			Collections.sort(keys, comp);
		}
		
		for (Integer sid : keys) {
			if (bProbability) {
				System.out.println(sid + " : " + stmt.get(sid) * 1.0f / totalTestRun);
			}
			else {
				System.out.println(sid + " : " + stmt.get(sid));
			}
		}
		
		if (!fnfwdslice.equalsIgnoreCase("")) {
			loadFwdslice(fnfwdslice, fwdslice);
			
			if (keys.size() < fwdslice.size()) {
				for (Integer pntId : fwdslice) {
					if (!keys.contains(pntId)) {
						stmt.put(pntId, 0);
						System.out.println(pntId + " : " + (bProbability?0.0f:0));
					}
				}
			}
		}
	}
	
	/** arg 0: starting_sId   (e.g., 2501)
	    arg 1: base path of files to be analyzed  */
	private ExecHistProb(String[] args) {

		this.startId = Integer.valueOf(args[0]);
		this.pathBase = args[1];
		this.totalTestRun=0;
		this.stmt=new HashMap<Integer,Integer>();
		this.fwdslice = new LinkedHashSet<Integer>();
	}

	private void run() {
		Map<Integer, Integer> pntToDiffs = new HashMap<Integer, Integer>();
		Set<Integer> pntsCovered = new HashSet<Integer>();
		final int numTests = readAndDiffExecHistories(pntToDiffs, pntsCovered) - 1;
		assert numTests > 0;
	//	System.out.println("Read " + numTests + " test-output files");
		
		// DEBUG: covered points
		List<Integer> pntsCovSorted = new ArrayList<Integer>(pntsCovered);	
		Collections.sort(pntsCovSorted);
//		System.out.println("Covered: " + pntsCovSorted.size());
	}
	
	// -- added by hcai
	// an alternative to run() for occasions where the .out files to be loaded already contain the 
	// map:stmtid->#changes info. so we just need to reduce them together per stmtid across all tests in that case  
	private void runWithMaps(int tid, int expectedRuns)
	{
		int rId;
		boolean flag = true;
		int count_impacted = 0;
		for (rId = 2; true; ++rId) {
			flag = true;
			try {
				FileReader fr = new FileReader(pathBase + tid + "/" + rId + ".out");
				BufferedReader reader = new BufferedReader(fr);
				String s;
				while ((s = reader.readLine()) != null) {
					String pair[] = s.split(":");
					Integer sid = Integer.valueOf(pair[0]), scnt = Integer.valueOf(pair[1]);
					Integer count = stmt.get(sid);
					if (null == count) count = 0;
					stmt.put(sid, scnt + count);
					
					if(flag){
						flag = false;
						count_impacted ++;
					}
				}
				fr.close();
				reader.close();
			}
			catch (FileNotFoundException e) { 
				if ( rId < expectedRuns+1 ) {
					System.err.println(pathBase + tid + "/" + rId + ".out" + " was not found.");
				}
				break;
			}
			catch (IOException e) {
				throw new RuntimeException(e); 
			}
		}
		
		//System.err.println((rId-1) + " runs have been processed for test No." + tid);
	}
	// ---
		
	/** For each test output on the orig and mod program, read and diff execution histories of reported points covered AFTER the start point. */
	private int readAndDiffExecHistories(Map<Integer, Integer> pntToDiffs, Set<Integer> pntsCovered) {
		
		int tId;
		// make sure the original file would not be counted.
		int bak = totalTestRun;
		Map<Integer,Object> exHistOrig = readExHist(pathOrigOut + 1 + ".out");
		totalTestRun = bak;
		
		boolean flag = true;
		int count_impacted = 0;
		for (tId = 2; true; ++tId) {
			flag = true;
			Map<Integer,Object> exHistMod = readExHist(pathModOut + tId + ".out");
			if (exHistMod == null)
				break; // reached end of test outputs
			
			// determine diffs for each point and update point diff counter
			Set<Integer> pnts = new HashSet<Integer>(exHistOrig.keySet());
			if (exHistMod != null) pnts.addAll(exHistMod.keySet());
			pntsCovered.addAll(pnts);
			for (Integer pnt : pnts) {
				// one of the following lists/hashes might be null, but not both
				Object valsOrig = exHistOrig.get(pnt);
				Object valsMod = exHistMod.get(pnt);
				assert valsOrig != null || valsMod != null;
				Integer count = pntToDiffs.get(pnt);
				if (count == null)
					count = 0;
				Integer count2 = stmt.get(pnt);
				if (count2 == null)
					count2 = 0;
				if (valsOrig == null || !valsOrig.equals(valsMod)){
					pntToDiffs.put(pnt, ++count); // increment diff counter for this point
					stmt.put(pnt, ++count2);
					if(flag){
						flag = false;
						count_impacted ++;
					}
				}
			}
		}

		return tId;
	}
	
	private final static String DATA_PREFIX = "|||";
	private final static int DATA_PREFIX_LEN = DATA_PREFIX.length();
	/** Ignore data occurring BEFORE start point. */
	private Map<Integer,Object> readExHist(String fpathname) {
		boolean reachedStart = false;
		try{
			Map<Integer,Object> exHist = new HashMap<Integer,Object>();
			
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fpathname));
			String s;
			while ((s = reader.readLine()) != null) {
				if((!s.startsWith(DATA_PREFIX)) && (s.indexOf(DATA_PREFIX) != -1)){
					s = s.substring(s.indexOf(DATA_PREFIX));
				}
				// check if line is exec hist output line
				if (!s.startsWith(DATA_PREFIX))
					continue;
				
				// parse point as integer value that is positive if followed by [1] and negative if followed by [0]
				final int bracketPos = s.indexOf('[', DATA_PREFIX_LEN);
				final int point = Integer.valueOf(s.substring(DATA_PREFIX_LEN, bracketPos));
				if (!reachedStart) {
					if (point == startId)
						reachedStart = true;
					else
						continue; // ignore data occurring BEFORE start point
				}
				final char sign = s.charAt(bracketPos + 1);
				assert sign == '0' || sign == '1';
				final int signedPoint = sign == '1'? point : -point;
				assert s.charAt(bracketPos + 2) == ']' && s.charAt(bracketPos + 3) == '=';
				
				// parse value
				final String val = removeFinalAddress(s.substring(bracketPos + 4));
				
				// store exec hist entry point->val
				if (Options.useHashing()) {
					Long prevHash = (Long) exHist.get(signedPoint);
					long hash = (prevHash == null)? 0 : prevHash;
					exHist.put(signedPoint, hash + (long)val.hashCode());
				}
				else {
					List<String> valsForPnt = (List<String>) exHist.get(signedPoint);
					if (valsForPnt == null) {
						valsForPnt = new ArrayList<String>();
						exHist.put(signedPoint, valsForPnt);
					}
					valsForPnt.add(val);
				}
			}
			if (reachedStart)
			{
				totalTestRun++;
			}
			return exHist;
		}
		catch (FileNotFoundException e) { 
			//e.printStackTrace();
			return null; }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	private static String removeFinalAddress(String s) {
		// remove object address, if it exists
		final int addrPos = s.lastIndexOf('@');
		if (addrPos != -1) {
			// now, ensure there is an hexadecimal number after the @
			boolean removeAddr = true;
			for (int i = addrPos + 1; i < s.length(); ++i) {
				final char c = s.charAt(i);
				if (!((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'))) {
					removeAddr = false;
					break;
				}
			}
			if (removeAddr)
				s = s.substring(0, addrPos + 1);
		}
		return s;
	}
	
	/**
	 * Load static forward slice either from a statement list like fwdslice.out produced by SensaInst (each line giving statement id only) 
	 * or a complete SensA Ranking (each line giving "statement id : rank")
	 * @param fnSrc
	 * @param fwdslice
	 * @return
	 */
	private static int loadFwdslice(String fnSrc, HashSet<Integer> fwdslice) {
		int size = 0;
		try{
			// process lines in file, one by one
			BufferedReader reader = new BufferedReader(new FileReader(fnSrc));
			String s;
			while ((s = reader.readLine()) != null) {
				// parse statement ids as node point - rhspos is reflected by either being negative or positive number
				final int separatorPos = s.indexOf(':', 0);
				
				// skip invalid lines
				try {
					int point = 0;
					if (-1 != separatorPos) {
						point = Integer.valueOf(s.substring(0, separatorPos).trim());
					}
					else {
						point = Integer.valueOf(s.trim());
					}
					
					fwdslice.add(point);
					size ++;
				}
				catch (NumberFormatException e) {
					continue;
				}
			}
			
			return size;
		}
		catch (FileNotFoundException e) { return 0; }
		catch (IOException e) { throw new RuntimeException(e); }
	}
	
}
