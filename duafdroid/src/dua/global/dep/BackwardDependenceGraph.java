package dua.global.dep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Local;
import soot.SootMethod;
import soot.Value;
import soot.ValueBox;
import soot.jimple.*;
import dua.Options;
import dua.global.ProgramFlowGraph;
import dua.global.ReachabilityAnalysis;
import dua.global.ReqBranchAnalysis;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.Dependence.DepType;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG;
import dua.method.CFGDefUses;
import dua.method.CFGDefUses.Def;
import dua.method.CallSite;
import dua.method.MethodTag;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.Branch;
import dua.method.CFGDefUses.CSArgVar;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.ReturnVar;
import dua.method.CFGDefUses.StdVariable;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.unit.Location;
import dua.util.Pair;

/** Provides a graph of all dependences backward from a CFG node. Corresponds to the static backward slice from that node.
 *
 *  Ignores target nodes, for now -- Excludes dependencies not reachable from source or that do not reach target. */
public class BackwardDependenceGraph {
	private static final boolean debugOut = false;

	/** Start point. */
	private final NodePoint pointStart;
	/** End points. */
	private Set<NodePoint> pointEnds;
	/** Map node->{out-deps} */
	private Map<NodePoint, Set<Dependence>> pntToOutDeps = new HashMap<NodePoint, Set<Dependence>>();
	/** Map node->{in-deps} */
	private Map<NodePoint, Set<Dependence>> pntToInDeps = new HashMap<NodePoint, Set<Dependence>>();
	/** Used during each of the computation phases of the dependence graph to avoid re-visiting points. */
	private Set<NodePoint> visitedPoints = new HashSet<NodePoint>();
	/** Caches all dependences (edges) in graph. */
	private Set<Dependence> deps = new HashSet<Dependence>();

	public NodePoint getStart() { return pointStart; }
	public Set<NodePoint> getEnds() { return pointEnds; }
	public Set<Dependence> getOutDeps(NodePoint p) { return pntToOutDeps.get(p); }
	public Set<Dependence> getInDeps(NodePoint p) { return pntToInDeps.get(p); }
	public Set<Dependence> getDeps() { return deps; }

	public BackwardDependenceGraph(NodePoint pStart, int dist) {
		this(pStart, new ArrayList<NodePoint>(), dist);
	}

	/** Builds dep graph by finding all transitive dependencies from start point to target.
	 *  Excludes dependencies not reachable from source or that do not reach target.
	 *
	 *  TODO: Fix imprecision due to data dependence to pre-rhs param use linking to other param defs or str-const defs.
	 */
	public BackwardDependenceGraph(NodePoint pStart, List<NodePoint> pTargets, int dist) {
		this.pointStart = pStart;
		this.pointEnds = new HashSet<NodePoint>(pTargets);

		// Get all dependencies (data and control) from start point that can affect target
		Set<Pair<NodePoint,Integer>> worklist = new HashSet<Pair<NodePoint,Integer>>(); // nodes from which originate dependences that have to be added
		Set<Pair<NodePoint,Integer>> worklistHist = new HashSet<Pair<NodePoint,Integer>>(); // keeps track of all processed elements for the next phase
		worklist.add(new Pair<NodePoint,Integer>(pStart, dist));

		doDepSearchPhase(worklist, worklistHist, false);

		if (debugOut) {
			// DEBUG
	//		System.out.println("Fwd slice output: # deps " + deps.size() + " # points " + pntToDeps.keySet().size());
			System.out.print('s'); // indicates start point
			printFwdSlice(pointStart, "", new HashSet<NodePoint>());
			System.out.println();
		}
	}
	private void printFwdSlice(NodePoint p, String prefix, Set<NodePoint> visited) {
		if (!visited.add(p)) {
			System.out.print("@" + p);
			return;
		}

		final String sP = p + "->";
		System.out.print(sP);
		Set<Dependence> outDeps = pntToOutDeps.get(p);
		Set<Dependence> inDeps = pntToInDeps.get(p);
		if (outDeps.isEmpty())
			System.out.print("END");
		else {
			List<NodePoint> pSuccs = new ArrayList<NodePoint>();
			Map<NodePoint,Integer> pSuccToDepClass = new HashMap<NodePoint, Integer>(); // 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
			for (Dependence dep : outDeps) {
				NodePoint pTgt = dep.getTgt();
				pSuccs.add(pTgt);
				final boolean isDataDep = dep instanceof DataDependence;

				// decide dep type -- 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
				Integer clsOld = pSuccToDepClass.get(pTgt);
				Integer clsNew;
				if (clsOld == null)
					clsNew = isDataDep? 1 : 0;
				else if (clsOld == 0)
					clsNew = isDataDep? 2 : 0;
				else if (clsOld == 1)
					clsNew = isDataDep? 1 : 2;
				else {
					assert clsOld == 2;
					clsNew = 2;
				}
				pSuccToDepClass.put(pTgt, clsNew);
			}
			Collections.sort(pSuccs, NodePointComparator.inst);

			// create next prefix with empty spaces matching length of last arrow printed
			char[] nextPreffixChars = new char[prefix.length() + sP.length() + 1]; // extra 1 for c/d/b dep class indicator
			Arrays.fill(nextPreffixChars, ' ');
			String nextPrefix = new String(nextPreffixChars);
			boolean first = true;
			for (NodePoint pSucc : pSuccs) {
				if (first)
					first = false;
				else
					System.out.print("\n" + nextPrefix);
				final int depCls = pSuccToDepClass.get(pSucc);
				System.out.print((depCls == 0)? 'c' : (depCls == 1)? 'd' : 'b');
				printFwdSlice(pSucc, nextPrefix, visited);
			}
		}
	}

	private void doDepSearchPhase(Set<Pair<NodePoint, Integer>> worklist, Set<Pair<NodePoint, Integer>> worklistHist, boolean fwdOnly) {
		while (!worklist.isEmpty()) {
			// extract next dependence-source node
			Pair<NodePoint,Integer> pntAndDist = worklist.iterator().next();
			worklist.remove(pntAndDist);
			if (worklistHist != null)
				worklistHist.add(pntAndDist);
			
			final int currDist = pntAndDist.second();
			NodePoint pTgt = pntAndDist.first();
			
			CFGNode nTgt = pTgt.getN(); // starting from the failure point
			
			final int posTgt = pTgt.getRhsPos();
			CallSite csTgt = nTgt.getCallSite();
			CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getContainingCFG(nTgt);
			Stmt tgtStmt = nTgt.getStmt();
			
			// create set of in-deps for this node;   NOT ANYMORE --- node should be visited only once!
			if (!pntToInDeps.containsKey(pTgt))
				pntToInDeps.put(pTgt, new HashSet<Dependence>()); // starts empty
			visitedPoints.add(pTgt);
			
			// 1. Data dependencies from n
			// 1.1 Regular variable dependencies
			//		1) get all uses at this point;
			//		2) for each use, get it's def (srcNode, which is actually the "target" in this backward case)
//			for (Use u: cfgDU.getUses()){ // Get all the uses at this point 
//			Variable vUse = u.getVar();
			for(Variable vUse : ((NodeDefUses)nTgt).getUsedVars()){
				List<Pair<Def, Integer>> defsForUse = DependenceFinder.getAllDefsForUse(vUse, nTgt);
				
				Iterator iter = defsForUse.iterator();
				while(iter.hasNext()){ // iterate through each def
					Pair p = (Pair) iter.next();
					Def def = (Def) p.first();
					CFGNode nDef = def.getN();
					Variable vDef = def.getVar();
						
					if(vDef.isConstant())
						break;
					else{
						// def can't be actual param, so it is "post-rhs", except for string-const-params which are "pre-rhs"
						final int rhsPosDef = vDef.isStrConstObj()? NodePoint.PRE_RHS : NodePoint.POST_RHS;
						
						DepType depType=null; 
						if (vDef.isLocal() && !(tgtStmt instanceof IdentityStmt) && !(tgtStmt instanceof AssignStmt) )
							depType = DepType.INTRA;
						else if( !(tgtStmt instanceof IdentityStmt) && !(tgtStmt instanceof AssignStmt) ){
							depType = DepType.INTER; // regular (non-link) data dep type
						}
						else
							break; // not regular dd
						createDataDep(new NodePoint(nDef, rhsPosDef), new NodePoint(nTgt, posTgt), vDef, depType, worklist, currDist);
					}	
				}
			}
			
			// 1.2 Data dependencies linking actual-formal parameters
			int formalParaId = 0;
			// create data dependence <actual para, formal para>
			if(tgtStmt instanceof IdentityStmt){
				// find actual para
				SootMethod mTgt = ProgramFlowGraph.inst().getContainingMethod(tgtStmt);
				MethodTag mTag = (MethodTag) mTgt.getTag(MethodTag.TAG_NAME);
				Local[] formalParas = mTag.getFormalParams();
				ReachableUsesDefs ruCFGCallee = (ReachableUsesDefs) ProgramFlowGraph.inst().getCFG(mTgt);
				// find the id of formal para (in order to get actual para later)
				for (int i = 0; i < ruCFGCallee.getNumFormalParams(); ++i) {
					// NOTE: look at actual/formal params lists, because constants are not considered as uses by NodeDefUses.getUsedVars
					if (tgtStmt == ruCFGCallee.getFormalParam(i)){
						Stmt sIdFormal = ruCFGCallee.getFormalParam(i).getIdStmt();
						formalParaId = i;
					}
				}
				// find actual para
				ArrayList<CallSite> srcCallers = (ArrayList<CallSite>) cfgDU.getCallerSites();
				// might be multiple callers
				for (CallSite csCaller : srcCallers){
					Stmt sCaller = csCaller.getLoc().getStmt();
					Value val = csCaller.getActualParam(formalParaId);
					Variable argVar = (val instanceof Constant)? new CSArgVar((Constant)val, csCaller, formalParaId) : new StdVariable(val);
					
					createDataDep(new NodePoint(ProgramFlowGraph.inst().getNode(sCaller), NodePoint.PRE_RHS),                  // def occurs "before" call
							     // new NodePoint(ProgramFlowGraph.inst().getNode(ruCFGCallee.getFormalParam(formalParaId).getIdStmt()), NodePoint.POST_RHS), // use is in Identity stmt
							      pTgt,
							      argVar, DepType.FWD_LINK, worklist, currDist);
				}
			}
			
			// 1.3 Data dependencies linking return-callsite values.
			if(posTgt == NodePoint.POST_RHS && csTgt != null && tgtStmt instanceof AssignStmt){
				// find all the return stmt in this method
				// create data dependence <retValue, tgtValue>
//				SootMethod mFrom = ProgramFlowGraph.inst().getContainingMethod(tgtStmt);
//				MethodTag mTag = (MethodTag) mFrom.getTag(MethodTag.TAG_NAME);
				for(SootMethod mCallee : csTgt.getAppCallees()){
					MethodTag mTag = (MethodTag) mCallee.getTag(MethodTag.TAG_NAME);
					List<Location> retLocs = mTag.getExits();
					for(Location retLoc : retLocs){
						Stmt sRet = retLoc.getStmt();
						if(sRet instanceof ReturnStmt){
							Value vRet = ((ValueBox)sRet.getUseBoxes().iterator().next()).getValue();
							assert vRet instanceof Constant || vRet instanceof Local;
							Variable varToReturn = new ReturnVar(vRet, ProgramFlowGraph.inst().getNode(sRet));
							assert varToReturn.isLocalOrConst();
							
							createDataDep(new NodePoint(ProgramFlowGraph.inst().getNode(sRet), NodePoint.POST_RHS), // return stmt has no call inside
									  new NodePoint(nTgt, NodePoint.POST_RHS), 
									  varToReturn, DepType.BACK_LINK, worklist, currDist);

						}
					}	
				}
			}
			
			// 2. Control dependencies from n
			if(nTgt.isInCatchBlock()|| nTgt.toString().equals("EX"))
					continue; // ignore catch blocks (FOR NOW) and exit nodes, which don't contribute anything)
			
			SootMethod mTgt = ProgramFlowGraph.inst().getContainingMethod(tgtStmt);
			CFG cfgTgt= ProgramFlowGraph.inst().getCFG(mTgt);
			Branch entryBr = cfgTgt.getEntryBranch();
			
			Set<Branch> brsForPossibleTgt = ReqBranchAnalysis.inst().getCDBranches(nTgt);
			for (Branch br : brsForPossibleTgt) {
//				// Debug
//				System.out.println("Branch:" + br);
				
				// 2.1 Intraproc:  
				if (br != entryBr){
					CFGNode nBrSrc = br.getSrc();
					CallSite csBrSrc = nBrSrc.getCallSite();
					Stmt srcStmt = nBrSrc.getStmt();
					SootMethod mSrc = ProgramFlowGraph.inst().getContainingMethod(srcStmt);
					
					final int brId = nBrSrc.getOutBranches().indexOf(br);
					assert brId >= 0;
					
					// default (regular stmt) is POST
					createControlDep(new NodePoint(nBrSrc, NodePoint.POST_RHS), brId, pTgt, worklist, currDist);
					
				}
				
				// 2.2 Interproc via multi-target CS:  
				else{
					int brId = -1;
					if (nTgt.isInCatchBlock() || nTgt.isSpecial())
						continue;

					addInterProcCtrlDeps(pTgt, mTgt, brId, new HashSet<SootMethod>(), worklist, currDist);
				}
			}
		}
	}

	private void addInterProcCtrlDeps(NodePoint pTgt, SootMethod m, int brId, Set<SootMethod> visited, Set<Pair<NodePoint, Integer>> worklist, int currDist) {
		if(!visited.add(m))
			return;

		MethodTag mTag = (MethodTag) m.getTag(MethodTag.TAG_NAME);
		for (CallSite csCaller : mTag.getCallerSites()){
			Stmt sCaller = csCaller.getLoc().getStmt();
			CFGNode nCaller = ProgramFlowGraph.inst().getNode(sCaller);
			CFG cfgCaller = ProgramFlowGraph.inst().getContainingCFG(nCaller);
			Branch entryBr = cfgCaller.getEntryBranch();

			if (nCaller.isInCatchBlock() || nCaller.isSpecial())
				continue;

			++brId;
			if(csCaller.getAppCallees().size() > 1 || csCaller.getLibCallees().size() > 0){
				createControlDep(new NodePoint(nCaller,NodePoint.PRE_RHS), brId, pTgt, worklist, currDist);
			}
			else{
				Set<Branch> brsForCallerSite = ReqBranchAnalysis.inst().getCDBranches(nCaller);
				for (Branch br : brsForCallerSite){
					if(br==entryBr){
						addInterProcCtrlDeps(pTgt, cfgCaller.getMethod(), brId, new HashSet<SootMethod>(), worklist, currDist);
					}
					else{
						CFGNode nBrSrc = br.getSrc();
						createControlDep(new NodePoint (nBrSrc, NodePoint.POST_RHS), brId, pTgt, worklist, currDist);
					}
				}
			}
		}
	}

	/** Helper method that creates and stores data dependencies in corresponding set and map, updating given worklist.
	 *  @param var Identifies the defined variable in the dependence; for actual-formal links, it's the argument var. */
	private void createDataDep(NodePoint pntDef, NodePoint pntUse, Variable var, DepType depType, Set<Pair<NodePoint,Integer>> worklist, int dist) {
		Dependence dep = new DataDependence(pntDef, pntUse, var, depType);
		deps.add(dep);
		// add new data dep to dep list for tgt
		Set<Dependence> setDeps = pntToInDeps.get(pntUse);
		pntToInDeps.get(pntUse).add(dep);

		if (dist > 0 || (dist == -1 && !visitedPoints.contains(pntDef))){
			worklist.add(new Pair<NodePoint,Integer>(pntDef, dist>0? dist-1 : dist)); // add nDef to worklist if not processed before in current phase
			visitedPoints.add(pntDef);
			// Debug
			System.out.println("DD: " + pntDef + "->" + pntUse);
		}

	}
	/** Helper method that creates and stores control dependencies in corresponding set and map, updating given worklist. */
	private void createControlDep(NodePoint pntSrc, int brId, NodePoint pntTgt, Set<Pair<NodePoint,Integer>> worklist, int dist) {
		Dependence dep = new ControlDependence(pntSrc, brId, pntTgt);
		deps.add(dep);
		// add new ctrl dep to dep list for tgt
		pntToInDeps.get(pntTgt).add(dep);
		if (dist > 0 || (dist == -1 && !visitedPoints.contains(pntSrc))){
			worklist.add(new Pair<NodePoint,Integer>(pntSrc, dist>0? dist-1 : dist)); // add nTgt to worklist if not processed before in current phase
			visitedPoints.add(pntSrc);
			// Debug
			System.out.println("CD: " + pntSrc + "->" + pntTgt);
		}
	}

	/** REACHABILITY analysis should be used instead. */
	private boolean isFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);

		// first, look intraprocedurally; they might be in same cfg, but n2 must be reachable from n1 within that cfg
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;

		// then, look interprocedurally in (transitive) callees ONLY from callsites at cfg1 intraproc-reachable from n1
		Set<CFG> cfgsFwd = new HashSet<CFG>();
		for (CallSite cs : cfg1.getCallSites()) {
			if (!cs.hasAppCallees())
				continue;
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			if (ReachabilityAnalysis.reachesFromBottom(n1, nCS, false))
				for (SootMethod mTgt : cs.getAppCallees())
					cfgsFwd.add(ProgramFlowGraph.inst().getCFG(mTgt));
		}

		Set<CFG> workset = new HashSet<CFG>(cfgsFwd);
		while (!workset.isEmpty()) {
			CFG cfg = workset.iterator().next();
			workset.remove(cfg);
			cfgsFwd.add(cfg);

			for (CFG cfgSucc : cfg.getCallgraphSuccs())
				if (!cfgsFwd.contains(cfgSucc))
					workset.add(cfgSucc);
		}

		return cfgsFwd.contains(cfg2);
	}
	/** REACHABILITY analysis should be used instead. */
	private boolean isBackFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);

		// look intraprocedurally only in cfg1, NOT forward from cfg1!
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;

		Set<CallSite> backCSs = new HashSet<CallSite>();
		Set<CallSite> workset = new HashSet<CallSite>(cfg1.getCallerSites());
		while (!workset.isEmpty()) {
			CallSite cs = workset.iterator().next();
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			workset.remove(cs);
			backCSs.add(cs);

			if (isFwdReachable(nCS, n2))
				return true;

			CFG cfg = ProgramFlowGraph.inst().getContainingCFG(nCS);
			for (CallSite csBack : cfg.getCallerSites())
				if (!backCSs.contains(csBack))
					workset.add(csBack);
		}

		return false;
	}

	/** Whether forward slices have any node in common. */
	public boolean intersects(BackwardDependenceGraph depGraph2) {
		// gather collection of affected nodes for this graph
		Set<CFGNode> pointsHere = new HashSet<CFGNode>();
		for (Dependence dep : deps)
			pointsHere.add(dep.getTgt().getN());

		for (Dependence depOther : depGraph2.deps)
			if (pointsHere.contains(depOther.getTgt().getN()))
				return true;
		return false;
	}

	public Set<NodePoint> getPointsInSlice() {
		// start with src pnts of deps in slice
		Set<NodePoint> pointsInSlice = new HashSet<NodePoint>(pntToOutDeps.keySet());
		// add tgt pnts of deps out of those src pnts
		for (Set<Dependence> outDeps : pntToOutDeps.values())
			for (Dependence outDep : outDeps)
				pointsInSlice.add(outDep.getTgt());
		return pointsInSlice;
	}

}
