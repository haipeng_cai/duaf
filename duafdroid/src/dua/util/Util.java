package dua.util;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.AnySubType;
import soot.ArrayType;
import soot.Body;
import soot.BooleanType;
import soot.ByteType;
import soot.CharType;
import soot.DoubleType;
import soot.FastHierarchy;
import soot.FloatType;
import soot.IntType;
import soot.Local;
import soot.LongType;
import soot.PatchingChain;
import soot.PointsToSet;
import soot.PrimType;
import soot.RefLikeType;
import soot.RefType;
import soot.Scene;
import soot.ShortType;
import soot.SootClass;
import soot.SootFieldRef;
import soot.SootMethod;
import soot.SootMethodRef;
import soot.Type;
import soot.Value;
import soot.jimple.ArrayRef;
import soot.jimple.ClassConstant;
import soot.jimple.Constant;
import soot.jimple.FieldRef;
import soot.jimple.IdentityStmt;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InterfaceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.VirtualInvokeExpr;
import soot.jimple.internal.JRetStmt;
import soot.jimple.internal.JReturnStmt;
import soot.jimple.internal.JReturnVoidStmt;
import soot.toolkits.graph.Block;
import soot.util.Chain;
import soot.util.NumberedString;
import dua.Options;
import dua.cls.ClassTag;
import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.MethodTag;
import dua.method.ReachableUsesDefs;
import dua.unit.StmtTag;

public class Util {
	/** Comparator based on the value returned by the {@link Object#hashCode()} method. Null references are considered the lowest possible value. */
	public static class HashCodeComparator<T> implements Comparator<T> {
		/** Returns a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second, respectively. */
		public int compare(T o1, T o2) {
			if (o1 == null)
				return (o2 == null)? 0 : -1;
			if (o2 == null)
				return 1;
			
			final int h1 = o1.hashCode();
			final int h2 = o2.hashCode();
			return (h1 < h2)? -1 : ((h1 == h2)? 0 : 1);
		}
	}
	
	/** A value represents a var if it's a constant, local, field ref (for field itself), or array elem ref (any element in it).
	 *  Takes 'must' param to distinguish between 'just possibly equal' and 'definitely equal' values (a distinction for fields and array elem refs). 
	 *  Objects are *not* considered here. */
	public static boolean valuesEqual(Value v1, Value v2, boolean must) {
		// case 1: both null refs or constants
		if (v1 == null || v2 == null)
			return v1 == v2;
		if (v1 instanceof Constant) {
			if (v2 instanceof Constant)
				return v1.equals(v2);
			return false;
		}
		
		// case 2: locals
		if (v1 instanceof Local)
			return v1 == v2;
		// case 3: field refs (if 'must' is false, base is ignored)
		if (v1 instanceof FieldRef) {
			SootFieldRef sfr1 = ((FieldRef) v1).getFieldRef();
			if (!(v2 instanceof FieldRef))
				return false;
			SootFieldRef sfr2 = ((FieldRef) v2).getFieldRef();
			
			if (sfr1.declaringClass() == sfr2.declaringClass() && sfr1.name().equals(sfr2.name())) {
				if (!must)
					return true;
				// additional 'must' requirements
				return !(v1 instanceof InstanceFieldRef) || ((InstanceFieldRef)v1).getBase() == ((InstanceFieldRef)v2).getBase();
			}
			else
				return false;
		}
		// case 4: array elems; if 'must' is false, base is ignored; FOR NOW, index is never compared, even if constant (because of excess of instrumentation?) 
		//         they are the same if elem types are equal and index values are not definitely different
		assert (v1 instanceof ArrayRef);
		
		ArrayRef ae1 = (ArrayRef)v1;
		if (!(v2 instanceof ArrayRef))
			return false;
		ArrayRef ae2 = (ArrayRef)v2;
		if (!ae1.getBase().getType().equals(ae2.getBase().getType()))
			return false;
		// OLD -- *** FOR NOW, we avoid distinguishing array elems with constant indices as different variables,
		// OLD --  because it leads to too much instrumentation at defs and uses of array elem with unknown index.
		if (!must)
			return true;
		return ae1.getBase().equals(ae2.getBase()) && ae1.getIndex().equals(ae2.getIndex());
	}
	/** Hash code for Value representing a variable if it's a constant, local, field ref (for field itself), or array elem ref (for all elements of array of that type).
	 *  Hash codes are equal for two values iff the two values must be equal, as per {@link #valuesEqual(Value, Value, boolean)} with must=true. */
	public static int valueHashCode(Value v) {
		// case 1: null or constant
		if (v == null)
			return 0;
		if (v instanceof Constant)
			return v.hashCode();
		
		// case 2: local
		if (v instanceof Local)
			return v.hashCode();
		// case 3: field ref (base is ignored)
		if (v instanceof FieldRef) {
			SootFieldRef sfr = ((FieldRef) v).getFieldRef();
			return sfr.resolve().hashCode();
		}
		// case 4: array elems
		assert (v instanceof ArrayRef);
		ArrayRef ae1 = (ArrayRef)v;
		// *** FOR NOW, we avoid distinguishing array elems with constant indices as different variables,
		//  because it leads to too much instrumentation at defs and uses of array elem with unknown index.
		return ae1.getBase().getType().hashCode();// + ((ae1.getIndex() instanceof Constant)? ae1.getIndex().hashCode() : 0);
	}
	
	// TODO: make consistent with 'must' object equality check
	/** Uses intersection of possible runtime types referenced by values.
	 *  These types of Value can represent an object:
	 *    1. NewExpr/NewArrayExpr (instance)
	 *    2. InvokeExpr           (static or instance)
	 *    3. Local/statfield ref var  (instance)
	 *    4. StringConstant       (instance)
	 *    5. ClassConstant       (instance)
	 */
	public static boolean objValuesMayEqual(Value v1, Value v2) {
		// case 5: (lib) object, either static or instance
		//         objects are (possibly) the same if they are both static or both instance,
		//         and there is at least one object type that v1 and v2 can refer to in their call.
		// NOTE: includes automatically constructed Strings, represented by StringConstant values, or Class constants (java.lang.Class)
		
		// first, handle special case with more precision: both values are string constants
		if (v1 instanceof StringConstant && v2 instanceof StringConstant)
			return ((StringConstant)v1).value.equals(((StringConstant)v2).value);
		// second, handle special case with more precision: both values are class constants
		if (v1 instanceof ClassConstant && v2 instanceof ClassConstant)
			return ((ClassConstant)v1).value.equals(((ClassConstant)v2).value);
		
		// get list of ref types of instances to which each value can refer
		// (null if val refers to class itself, not instances)
		List<RefLikeType> clsTargetsSorted1 = getAllPossibleRuntimeRefTypes(v1);
		List<RefLikeType> clsTargetsSorted2 = getAllPossibleRuntimeRefTypes(v2);
		
		// second, handle special case of values referring to class object (static)
		if (clsTargetsSorted1 == null) {
			if (clsTargetsSorted2 != null)
				return false;
			SootClass cls1 = ((StaticInvokeExpr)v1).getMethodRef().declaringClass(); // not the same as getMethod().getDeclaringClas() !!!
			SootClass cls2 = ((StaticInvokeExpr)v2).getMethodRef().declaringClass(); // not the same as getMethod().getDeclaringClas() !!!
			return cls1.equals(cls2);
		}
		else if (clsTargetsSorted2 == null)
			return false;
		
		// now, just check for intersection of returned lists of possible target classes
		return intersectSortedAscending(clsTargetsSorted1, clsTargetsSorted2);
	}
	
	// TODO: make consistent with 'may' object equality check
	/** Uses EXACT of possible runtime types referenced by values.
	 *  These types of Value can represent an object:
	 *    1. NewExpr/NewArrayExpr (instance)
	 *    2. InvokeExpr           (static or instance)
	 *    3. Local/statfield ref var  (instance)
	 *    4. StringConstant       (instance)
	 *    5. ClassConstant       (instance)
	 */
	public static boolean objValuesMustEqual(Value v1, Value v2, boolean sameLocation) {
		// class set that objects represent is NOT A SUFFICIENT condition for equality
		if (v1 instanceof NewExpr || v1 instanceof NewArrayExpr)
			return v1 == v2; //this.n == vOther.n; // location distinguishes new-object expressions
		else if (v1 instanceof StaticInvokeExpr)
			return v2 instanceof StaticInvokeExpr &&
				   ((StaticInvokeExpr)v1).getMethodRef().declaringClass() == ((StaticInvokeExpr)v2).getMethodRef().declaringClass(); // not the same as getMethod().getDeclaringClas() !!!
		else if (v1 instanceof InstanceInvokeExpr) {
			if (!(v2 instanceof InstanceInvokeExpr))
				return false;
			// RS Dec '13: in the end, just comparing bases is safer to ensure that 'must equal' holds if 'may equal' holds too (which was violated by all other solutions I could think of) 
			// *** removed *** only way to ensure they are equal is that bases are the same local and can point to no more than 1 concrete class
			// *** removed *** WHY: variables might be disjoint even if bases are equal. Consider base of type A that can point to sibling obj variables of types B<-A and C<-A.
			// *** removed *** We must avoid declaring 'cousin' classes as equals, which the old code allowed inadvertently by just checking base equality
			Local lBase = (Local) ((InstanceInvokeExpr) v1).getBase();
			if (lBase != ((InstanceInvokeExpr) v2).getBase())
				return false;
			
			// RS Dec '13: in the end, just comparing bases is safer to ensure that 'must equal' holds if 'may equal' holds too (which was violated by all other solutions I could think of) 
			return true; // removed for causing more trouble than helping ==> P2Analysis.inst().getP2Set(lBase).cardinality() <= 1; // more than 1 possible obj var, even of same time, is safely treated as 'not must' (may not) equal
		}
		else if (v1 instanceof Local)
			return v1 == v2; // discard equality if locals are not the same
		else if (v1 instanceof StaticFieldRef)
			return v2 instanceof StaticFieldRef &&
				   ((StaticFieldRef)v1).getField() == ((StaticFieldRef)v2).getField(); // discard equality if static fields are not the same
		else if (v1 instanceof ClassConstant)
			return v2 instanceof ClassConstant &&
					((ClassConstant)v1).getType().equals(((ClassConstant)v1).getType());
		else {
			assert v1 instanceof StringConstant;
			return sameLocation && v1.equals(v2);
		}
	}
	
	/** Cache to avoid re-computing potentially large sets of types possibly referenced by a value. */
	private static final Map<Value,List<RefLikeType>> valToRefTypes = new HashMap<Value, List<RefLikeType>>();
	private static final HashCodeComparator<RefLikeType> hashCodeComparator = new HashCodeComparator<RefLikeType>();
	/** Returns list of all possible ref/array types of instances that Value can represent as variables. Ref types are in ascending order based on their hashcodes.
	 *  Returns null if no instance obj can be represented by Value (e.g., static method call). */
	private static List<RefLikeType> getAllPossibleRuntimeRefTypes(Value val) {
		// first, try to extract from cache to avoid re-computation
		List<RefLikeType> typeTargets = valToRefTypes.get(val);
		if (typeTargets != null)
			return typeTargets;
		typeTargets = new ArrayList<RefLikeType>();
		
		// 1.a) NewExpr        (instance)
		if (val instanceof NewExpr)
			typeTargets.add((RefLikeType)((NewExpr)val).getType());
		// 1.b) NewArrayExpr   (instance)
		else if (val instanceof NewArrayExpr)
			typeTargets.add((RefLikeType)((NewArrayExpr)val).getType());
		// 2. InvokeExpr     (static or instance)
		else if (val instanceof InvokeExpr) {
			if (val instanceof StaticInvokeExpr)
				typeTargets = null; // special case
			else if (val instanceof SpecialInvokeExpr)
				typeTargets.add((RefType)((SpecialInvokeExpr)val).getBase().getType());
			else {
				assert val instanceof InstanceInvokeExpr;
				SootClass declCls = ((InvokeExpr)val).getMethod().getDeclaringClass();
				typeTargets.add(declCls.getType());
				
				for (SootClass clsSub : getAllSubtypes(declCls))
					typeTargets.add(clsSub.getType());
			}
		}
		// 3. Local/statfield ref var  (instance)
		else if (val instanceof Local || val instanceof StaticFieldRef) {
			RefLikeType lType = (RefLikeType) val.getType();
			typeTargets.add(lType);
			if (lType instanceof RefType) {
				// add all possible subtypes of ref type
				for (SootClass clsSub : getAllSubtypes(((RefType)lType).getSootClass()))
					typeTargets.add(clsSub.getType());
			}
			else
				assert lType instanceof ArrayType; // array type has no subtypes
		}
		// 4. StringConstant (instance)
		else {
			assert val instanceof StringConstant || val instanceof ClassConstant;
			typeTargets.add((RefType)val.getType()); //Scene.v().getRefType("java.lang.String"));
		}
		
		// sort list before returning, to facilitate intersection-checking of lists
		if (typeTargets != null)
			Collections.sort(typeTargets, hashCodeComparator);
		valToRefTypes.put(val, typeTargets);
		return typeTargets;
	}
	
	/** Checks for intersection of lists, exploiting that the lists are (must be!) sorted in ascending order of the hashcodes of their elements. */
	private static boolean intersectSortedAscending(List<? extends Object> l1, List<? extends Object> l2) {
		// use iterators
		Iterator<? extends Object> it1 = l1.iterator();
		Iterator<? extends Object> it2 = l2.iterator();
		if (!it1.hasNext() || !it2.hasNext())
			return false; // one of the list is empty; no possible intersection
		// start iterating over l1 or l2, based on lowest hash code
		Object o1 = it1.next();
		Object o2 = it2.next();
		int h1 = o1.hashCode();
		int h2 = o2.hashCode();
		while (true) {
			// compare objects only if hash code is the same
			if (h1 == h2 && o1.equals(o2))
				return true;
			
			// objects are different; determine whether to advance o1 or o2 to next element
			if (h1 <= h2) {
				if (it1.hasNext()) {
					o1 = it1.next();
					h1 = o1.hashCode();
				}
				else
					return false;
			}
			else {
				if (it2.hasNext()) {
					o2 = it2.next();
					h2 = o2.hashCode();
				}
				else
					return false;
			}
		}
	}
	
	/** Hash code for Value representing an object (static or instance) */
	public static int objValueHashCode(Value v) {
		// case 5: (lib) object, either static or instance
		if (v instanceof StaticInvokeExpr)
			return ((StaticInvokeExpr)v).getMethodRef().declaringClass().hashCode(); // not the same as getMethod().getDeclaringClas() !!!
		else {
			// no good solution that guarantees that equal (may-point-to-same-instance) refs will get same hash code,
			// so just use constant value
			return 1;
		}
	}
	
	/** Returns true if value is "statically-definite", that is, it can represent only
	 *  one memory location at a time during an execution. */
	public static boolean isValueDefinite(Value v) {
		// TODO: determine if local is in a recursive method (in which case, it is NOT definite)
		// case 1 or 2: constant or local
		if (v instanceof Constant || v instanceof Local)
			return true;
		
		// case 3: field is definite only if it's static
		if (v instanceof FieldRef)
			return ((FieldRef) v).getFieldRef().isStatic();
		
		// case 4: array elems are all treated, for now, as not definite (conservative assumption)
		// case 5: only class objects (due to static invoke expr) are definite
		assert v instanceof ArrayRef || v instanceof InvokeExpr;
		return v instanceof StaticInvokeExpr;
	}
	
	public static boolean isReturnStmt(Stmt s) {
		return s instanceof JRetStmt || s instanceof JReturnStmt || s instanceof JReturnVoidStmt;
	}
	public static boolean isCtorCall(Stmt s) {
		return s.containsInvokeExpr() &&
			(s.getInvokeExpr() instanceof SpecialInvokeExpr) && s.getInvokeExpr().getMethod().getName().equals("<init>");
	}
	
	public static Collection<Type> getP2Nodes(Value v) {
		PointsToSet p2sTypes;
		if (v instanceof Local)
			p2sTypes = Scene.v().getPointsToAnalysis().reachingObjects((Local)v);
		else
			p2sTypes = Scene.v().getPointsToAnalysis().reachingObjects(((FieldRef)v).getField());
		
		return (Collection<Type>) p2sTypes.possibleTypes();
	}
	
	/** 
	 * Finds in class hierarchy and returns all app and lib concrete methods possibly referenced by method ref.
	 * Method is assumed to be virtual (not special or static).
	 * Returns true if there are library methods among targets found.
	 */
	public static boolean getConcreteCallTargets(InvokeExpr instInvExpr, /*OUT*/ Set<SootMethod> appTargets, /*OUT*/ Set<SootMethod> libTargets) {
		// get class of method ref; we start searching from this class
		SootMethodRef mref = instInvExpr.getMethodRef();
		SootClass cls = mref.declaringClass(); // starting class
		final NumberedString subsignature = mref.getSubSignature(); // signature to search for
		
		// CASE 1: object is of declared class type or inherited from some superclass
		//         find first superclass, starting from current cls, that declares method; there HAS to be such a class
		// note that if cls is interface, superclass if java.lang.Object
		// note that we don't check if there is indeed an interface declaring the method; we assume this is the case if no superclass declares it
		while (!cls.declaresMethod(subsignature) && cls.hasSuperclass())
			cls = cls.getSuperclass(); // never an interface
		// now, method might not be in superclass, or might be abstract; in that case, it's not a target
		SootMethod m;
		if (cls.declaresMethod(subsignature)) {
			m = cls.getMethod(subsignature);
			//if (!m.isAbstract()) {
			if (!m.isAbstract() && m.isConcrete()) { // changed by hcai
				//if (cls.hasTag(ClassTag.TAG_NAME))
				if (cls.hasTag(ClassTag.TAG_NAME) && m.hasTag(MethodTag.TAG_NAME))  // changed by hcai @ 12/09/2015
					appTargets.add(m); // add app method
				else
					libTargets.add(m); // add lib method
			}
		}
		
		// (only for virtual/interface calls)
		// CASE 2: object's actual type is a subclass; any subclass declaring the method is a possible target
		//         we have to check all superclasses of implementers, because starting cls might be interface
		if (instInvExpr instanceof VirtualInvokeExpr || instInvExpr instanceof InterfaceInvokeExpr) {
			cls = mref.declaringClass(); // start again from declaring class
			List<SootClass> allSubclasses = getAllSubtypes(cls);
			for (SootClass subCls : allSubclasses) {
				m = getMethodInClassOrSuperclass(subCls, subsignature);
				if (m == null) continue; // added by hcai
				//if (m != null && !m.isAbstract()) {
				if (!m.isAbstract() && m.isConcrete()) { // changed by hcai
					//if (m.getDeclaringClass().hasTag(ClassTag.TAG_NAME))
					if (m.getDeclaringClass().hasTag(ClassTag.TAG_NAME) && m.hasTag(MethodTag.TAG_NAME)) // hcai@12/09/2015
						appTargets.add(m); // add app method
					else
						libTargets.add(m); // add lib method
				}
			}
		}
		
		return !libTargets.isEmpty();
	}
	
	/** Returns the transitive closure of subinterfaces and subclasses of given class or interface (excluding given class). */
	public static List<SootClass> getAllSubtypes(SootClass cls) {
		// TODO store (cache) all subclasses in Class Tag
		List<SootClass> subclasses = new ArrayList<SootClass>();
		FastHierarchy hierarchy = Scene.v().getOrMakeFastHierarchy();
		for (Iterator itSubCls = hierarchy.getSubclassesOf(cls).iterator(); itSubCls.hasNext(); ) {
			SootClass subCls = (SootClass) itSubCls.next();
			subclasses.add(subCls);
			subclasses.addAll(getAllSubtypes(subCls));
		}
		for (Iterator itSubCls = hierarchy.getAllImplementersOfInterface(cls).iterator(); itSubCls.hasNext(); ) {
			SootClass subCls = (SootClass) itSubCls.next();
			subclasses.add(subCls);
			subclasses.addAll(getAllSubtypes(subCls));
		}
		return subclasses;
	}
	/** Returns all transitive subinterfaces and subclasses of given class/interface, including that class/interface. */
	public static List<SootClass> getTypeAndSubtypes(SootClass cls) {
		List<SootClass> allClasses = getAllSubtypes(cls);
		allClasses.add(cls);
		return allClasses;
	}
	
	/** Returns all transitive super-types (interfaces and classes, up to java.lang.Object of given class/interface. */
	public static void getAllSupertypes(SootClass cls, List<Type> superTypes) {
		if (cls.hasSuperclass()) {
			SootClass supCls = cls.getSuperclass();
			superTypes.add(supCls.getType());
			getAllSupertypes(supCls, superTypes);
		}
	}
	/** Returns method in given class or first upwards superclass,
	 *  or null if not found in any class (no interface checked) */
	private static SootMethod getMethodInClassOrSuperclass(SootClass cls, NumberedString subsignature) {
		if (cls.declaresMethod(subsignature))
			return cls.getMethod(subsignature);
		if (cls.hasSuperclass())
			return getMethodInClassOrSuperclass(cls.getSuperclass(), subsignature);
		return null;
	}
	
	/** For instance invokes */
	public static ArrayList<SootMethod> resolveAppCall(Type tgtType, SootMethodRef methodRef) {
		final NumberedString mSubsignature = methodRef.getSubSignature();
		if (tgtType instanceof RefType) {
			// find first class upwards in hierarchy, starting from cls, that implements method (i.e., *concrete* method)
			SootClass cls = ((RefType) tgtType).getSootClass();
			while (!cls.declaresMethod(mSubsignature))
				cls = cls.getSuperclass(); // if method not in this class, it HAS to be in a superclass, so a superclass must exist
			
			if (!cls.hasTag(ClassTag.TAG_NAME))
				return null; // not an app method
			
			// finally, store resolved app method
			SootMethod m = cls.getMethod(mSubsignature);
			assert m.hasTag(MethodTag.TAG_NAME);
			
			ArrayList<SootMethod> methods = new ArrayList<SootMethod>();
			methods.add(m); // just one element, directly resolved
			return methods;
		}
		
		if (tgtType instanceof AnySubType) {
			// return set of all app subtypes that implement referenced method
			SootClass baseCls = ((AnySubType)tgtType).getBase().getSootClass();
			List subClasses = baseCls.isInterface()?
					Scene.v().getActiveHierarchy().getImplementersOf(baseCls) :
					Scene.v().getActiveHierarchy().getSubclassesOf(baseCls);
			ArrayList<SootMethod> methods = new ArrayList<SootMethod>();
			for (Object oSubCls : subClasses) {
				SootClass subCls = (SootClass) oSubCls;
				if (subCls.hasTag(ClassTag.TAG_NAME)) {
					try {
						SootMethod m = subCls.getMethod(mSubsignature);
						assert m.hasTag(MethodTag.TAG_NAME);
						if (!m.isAbstract())
							methods.add(m);
					}
					catch (RuntimeException e) {}
				}
			}
			
			return methods;
		}
		
		assert tgtType instanceof ArrayType; // only other case observed so far
		return new ArrayList(); // no array class/method is in app
	}
	
	/** Returns basic block that contains the statement, or null. If id stmt, it looks in tag of first non-id stmt. */
	public static Block getBB(Stmt s) {
		// move to first non-id stmt
		Stmt sNonId = s;
		if (sNonId instanceof IdentityStmt) {
			PatchingChain pchain = ProgramFlowGraph.inst().getContainingMethod(sNonId).retrieveActiveBody().getUnits();
			do {
				sNonId = (Stmt) pchain.getSuccOf(sNonId);
			} while (sNonId instanceof IdentityStmt);
		}
		
		// retrieve basic block for non-id stmt
		StmtTag sTag = (StmtTag) sNonId.getTag(StmtTag.TAG_NAME);
		Block bb = sTag.getBasicBlock();
		
		return bb;
	}
	
	/** Helper to convert map value type */
	public static Map<SootMethod, ReachableUsesDefs> convertToRUMap(Map<SootMethod, CFG> mToCFGs) {
		Map<SootMethod, ReachableUsesDefs> mToRUs = new HashMap<SootMethod, ReachableUsesDefs>();
		
		for (SootMethod m : mToCFGs.keySet())
			mToRUs.put(m, (ReachableUsesDefs) mToCFGs.get(m));
		
		return mToRUs;
	}
	
	/** Returns base path, ending in '\', creating path if it didn't exist */
	public static String getCreateBaseOutPath() {
		String baseOutPath = Options.getOutPath();
		final char sepChar = File.separatorChar;
		if (!baseOutPath.isEmpty() && baseOutPath.charAt(baseOutPath.length()-1) != sepChar)
			baseOutPath += sepChar;
		(new File(baseOutPath)).mkdir(); // ensure directory is created
		
		return baseOutPath;
	}
	
	/** Parses a comma-separated list of strings into a list of strings. Trims leading/trailing whitespace. */
	public static List<String> parseStringList(String s) {
		return parseStringList(s, ',');
	}
	/** Parses a list of strings separated by given char into a list of strings. Trims leading/trailing whitespace. */
	public static List<String> parseStringList(String s, char separator) {
		List<String> strList = new ArrayList<String>();
		
		if (s.isEmpty())
			return strList;
		
		int i = 0;
		while (true) {
			final int end = s.indexOf(separator, i);
			if (end == -1) { // last string in list
				strList.add(s.substring(i).trim());
				break;
			}
			strList.add(s.substring(i, end).trim());
			i = end + 1; // past comma
		}
		
		return strList;
	}
	
	/** Parses a comma-separated list of integers into a list of integers. List might optionally by surrounded by square brackets. */
	public static List<Integer> parseIntList(String s) {
		// remove brackets, if present
		if (s.charAt(0) == '[') {
			assert s.charAt(s.length()-1) == ']';
			s = s.substring(1, s.length()-1);
		}
		
		List<String> strList = parseStringList(s);
		
		List<Integer> intList = new ArrayList<Integer>();
		for (String sInt : strList)
			intList.add(Integer.valueOf(sInt));
		
		return intList;
	}
	
	/** Returns index of local in body, or negative value if not found. */
	public static int getIndexOfLocal(Local l, Body b) {
		int i = 0;
		Chain locals = b.getLocals();
		for (Iterator itLoc = locals.iterator(); itLoc.hasNext(); ) {
			if (l == (Local)itLoc.next())
				return i;
			++i;
		}
		return Integer.MIN_VALUE;
	}
	
	/** Makes sure that if a class has no equals, that it is properly compared. */
	public static boolean areEqual(Object o1, Object o2) {
		if (o1 == null)
			return o2 == null;
		if (o1 == o2)
			return true;
		
		Class c1 = o1.getClass();
		if (c1 != o2.getClass())
			return false;
		
		// handle special cases
		if (o1 instanceof StringBuffer) {
			final String s1 = new String((StringBuffer)o1);
			final String s2 = new String((StringBuffer)o2);
			return s1.equals(s2);
		}
		
		// get equals method
		try {
			Method mEq = c1.getDeclaredMethod("equals", new Class[] { Object.class } );
			return (Boolean) mEq.invoke(o1, new Object[] { o2 } );
		}
		catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static Pair<RefType,SootMethod> getBoxingTypeAndCtor(PrimType t) {
		if (t instanceof BooleanType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Boolean");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(boolean)"));
		}
		else if (t instanceof ByteType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Byte");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(byte)"));
		}
		else if (t instanceof CharType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Character");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(char)"));
		}
		else if (t instanceof DoubleType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Double");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(double)"));
		}
		else if (t instanceof FloatType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Float");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(float)"));
		}
		else if (t instanceof IntType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Integer");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(int)"));
		}
		else if (t instanceof LongType) {
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Long");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(long)"));
		}
		else {
			assert t instanceof ShortType;
			SootClass clsBoxing = Scene.v().getSootClass("java.lang.Short");
			return new Pair<RefType,SootMethod>(clsBoxing.getType(), clsBoxing.getMethod("void <init>(short)"));
		}
	}
	
	/** Returns boxing class for given class, if it's primitive; returns same cls otherwise */
	public static Class getBoxingClass(Class cls) {
		try {
			if (cls.equals( boolean.class ))
				return Class.forName("java.lang.Boolean");
			else if (cls.equals( byte.class ))
				return Class.forName("java.lang.Byte");
			else if (cls.equals( char.class ))
				return Class.forName("java.lang.Character");
			else if (cls.equals( double.class ))
				return Class.forName("java.lang.Double");
			else if (cls.equals( float.class ))
				return Class.forName("java.lang.Float");
			else if (cls.equals( int.class ))
				return Class.forName("java.lang.Integer");
			else if (cls.equals( long.class ))
				return Class.forName("java.lang.Long");
			else if (cls.equals( short.class ))
				return Class.forName("java.lang.Short");
		}
		catch (ClassNotFoundException e) { e.printStackTrace(); return null; }
		
		// non-primitive class
		return cls;
	}
	
	/** Returns type corresponding to type name in string; null if not understood. */
	public static Type nameToType(String strT) {
		// check for primitive types, first
		if (strT.equals("boolean"))
			return BooleanType.v();
		if (strT.equals("byte"))
			return ByteType.v();
		if (strT.equals("char"))
			return CharType.v();
		if (strT.equals("double"))
			return DoubleType.v();
		if (strT.equals("float"))
			return FloatType.v();
		if (strT.equals("int"))
			return IntType.v();
		if (strT.equals("long"))
			return LongType.v();
		if (strT.equals("short"))
			return ShortType.v();
		
		// check for array
		if (strT.charAt(strT.length() - 1) == ']') {
			assert strT.charAt(strT.length() - 2) == '[';
			return ArrayType.v(nameToType(strT.substring(0, strT.length() - 2)), 1);
		}
		
		// finally, look for ref type
		return Scene.v().getRefType(strT);
	}
	
	/** Returns java.lang.Class corresponding to type name in string; null if not found. */
	public static Class nameToClass(String strT) throws ClassNotFoundException {
		// check for primitive types, first
		if (strT.equals("boolean"))
			return boolean.class;
		if (strT.equals("byte"))
			return byte.class;
		if (strT.equals("char"))
			return char.class;
		if (strT.equals("double"))
			return double.class;
		if (strT.equals("float"))
			return float.class;
		if (strT.equals("int"))
			return int.class;
		if (strT.equals("long"))
			return long.class;
		if (strT.equals("short"))
			return short.class;
		
		// non-primitive type: use reflection
		return Class.forName(strT);
	}
	
	/** @return All subtypes of refT, including refT itself, that are concrete */
	public static Set<RefLikeType> getConcreteRefLikeTypes(RefLikeType refLikeT) {
		Set<RefLikeType> concreteTypes = new HashSet<RefLikeType>();
		if (refLikeT instanceof RefType) {
			// add ref type itself, if concrete
			RefType refT = (RefType) refLikeT;
			if (refT.getSootClass().isConcrete())
				concreteTypes.add(refT);
			// add all concrete ref subtypes
			for (Object oSubClass : getAllSubtypes(refT.getSootClass())) {
				RefType refSubT = (RefType) ((SootClass)oSubClass).getType();
				concreteTypes.add(refSubT);
			}
		}
		else {
			ArrayType arrT = (ArrayType) refLikeT;
			Type elemT = arrT.getArrayElementType();
			if (elemT instanceof RefLikeType) {
				// get all possible concrete ref types of objects that the array can contain
				Set<RefLikeType> containedTypes = getConcreteRefLikeTypes((RefType) elemT);
				for (RefLikeType containedT : containedTypes) {
					ArrayType concArrT = ArrayType.v(containedT, 1);
					concreteTypes.add(concArrT);
				}
			}
			else {
				// for contained non-ref types, just create SE for that type
				concreteTypes.add(ArrayType.v(elemT, 1));
			}
		}
		
		return concreteTypes;
	}
	
	public static SootMethod getMethodInClsOrSuperCls(RefType t, String mSubSig) {
		SootClass cls = t.getSootClass();
		while (!cls.declaresMethod(mSubSig))
			cls = cls.getSuperclass();
		
		return cls.getMethod(mSubSig);
	}
	
	/** Takes a boxed int/long/short/byte/char and returns the value as boxed int. Returns null if not any of these types. */
	public static Integer convertToInteger(Object o) {
		if (o instanceof Integer)
			return (Integer)o;
		if (o instanceof Long)
			return (int)(long)(Long)o;
		if (o instanceof Short)
			return (int)(short)(Short)o;
		if (o instanceof Byte)
			return (int)(byte)(Byte)o;
		if (o instanceof Character)
			return (int)(char)(Character)o;
		
		return null;
	}
	
	/** @param clsConcreteB Must be a concrete, instantiatable class of type B or a subtype of B. */
	public static <A,B> B getCreateMapValue(Map<A,B> map, A a, Class clsConcreteB) {
		B b = map.get(a);
		if (b == null) {
			try { b = (B) clsConcreteB.newInstance(); }
			catch (Exception e) { throw new RuntimeException(e); }
			map.put(a, b);
		}
		return b;
	}
	
	public static int[] getIntArray(List<Integer> list) {
		int[] res = new int[list.size()];
		for (int i = 0; i < res.length; ++i)
			res[i] = list.get(i);
		return res;
	}
	
}
