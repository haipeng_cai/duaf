package profile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import soot.Body;
import soot.Local;
import soot.PatchingChain;
import soot.PrimType;
import soot.RefLikeType;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.FieldRef;
import soot.jimple.IdentityStmt;
import soot.jimple.IfStmt;
import soot.jimple.IntConstant;
import soot.jimple.Jimple;
import soot.jimple.LookupSwitchStmt;
import soot.jimple.NewArrayExpr;
import soot.jimple.NewExpr;
import soot.jimple.RetStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.SpecialInvokeExpr;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.TableSwitchStmt;
import dua.global.ProgramFlowGraph;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.ObjVariable;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.CallSite;
import dua.method.ReachableUsesDefs.NodeReachDefsUses;
import dua.util.Pair;
import dua.util.Util;
import fault.StmtMapper;

/**
 * Instruments statements to track their augmented execution histories (i.e.,
 * exec histories with computed values).
 */
public class ExecHistInstrumenter {
	private SootClass clsObject;
	private SootClass clsReporter;
	private SootMethod mReportStmt;
	
	//for XMLsec
	private final List<String> blacklist_classes;
	
	public ExecHistInstrumenter() {
		this(new String[0]);
	}
	
	public ExecHistInstrumenter(String classes_to_be_blocked[]){
		clsObject = Scene.v().getSootClass("java.lang.Object");
		clsReporter = Scene.v().getSootClass("profile.ExecHistReporter");
		// params: sId, pre/post [0|1], objDefVal -- NOTE: if more than 1 def,
		// there will be 1 reporter call per def
		mReportStmt = clsReporter.getMethod("void reportDef(int,boolean,java.lang.Object)");
		
		//for XMLsec:
		//blacklist_Classes = Arrays.asList(new String[] {"org.apache.xpath.objects.XObject"});
		blacklist_classes = Arrays.asList(classes_to_be_blocked);
	}

	/**
	 * Use fixed method to instrument all the points of subject
	 */
	public List<NodePoint> instrument() {
		return instrument(true);
	}

	/**
	 * Instrument all the points of subject
	 * 
	 * @param useFixedMethod
	 *            whether to use the fixed instrumentation method
	 */
	//private final static String A = "<org.argouml.uml.ui.ActionAddMessage: ru.novosoft.uml.behavior.collaborations.MMessage addMessage(ru.novosoft.uml.behavior.collaborations.MAssociationRole)>";
	//private final static String B = "<org.argouml.uml.ui.TabConstraints$ConstraintModel$CR$1: void <init>(org.argouml.uml.ui.TabConstraints$ConstraintModel$CR,java.lang.String)>";
	public List<NodePoint> instrument(boolean useFixedMethod) {
		List<NodePoint> instNodes = new ArrayList<NodePoint>();

		// get NodePoints to be instrumented
		List<CFG> CFGs = ProgramFlowGraph.inst().getCFGs();
		for (CFG cfg : CFGs) {
			List<CFGNode> cfgNodes = cfg.getNodes();
			/*
			if (cfg.getMethod().getSignature().contains("createAndRelease")) {
				System.out.println("Method skipped for instrumentation... " + cfg.getMethod().getSignature());
				continue;
			}
			*/
			/*
			if (cfg.getMethod().getSignature().contains(A) ||	cfg.getMethod().getSignature().contains(B)) {
				System.out.println("Method skipped for instrumentation... " + cfg.getMethod().getSignature());
				continue;
			}
			*/
			//Variable vskip = null;
			for (CFGNode cfgNode : cfgNodes) {
				// skip special CFGNodes and CFGNodes in catch block
				if (cfgNode.isSpecial() || cfgNode.isInCatchBlock())
					continue;
				
				/*
				if (cfgNode.getStmt().toString().contains("java.lang.System: void gc()")) {
					System.out.println("Stop monitoring after garbage collection in " + cfg.getMethod().getSignature());
					break;
				}
				*/
				//// the following would be useful for debugging some Verify Errors 
				/*
				NodeDefUses udNode = ((NodeDefUses)cfgNode);
				if (cfg.getMethod().getSignature().contains(B)) {
					boolean bskip = false;
					for (Variable d : udNode.getDefinedVars()) {
						if (d.isFieldRef()) { bskip = true; break; }
					}
					if (bskip) {
						System.out.println("One node skipped for instrumentation: " + cfgNode.getStmt());
	 					continue;
					}
				}
				*/
				/*
				if (cfg.getMethod().getSignature().contains(B)) {
					Stmt _s = cfgNode.getStmt();
					if (_s instanceof IdentityStmt) {
						IdentityStmt s = (IdentityStmt)_s;
						if ( s.getRightOp().toString().contains("this")) {
							vskip = udNode.getDefinedVars().get(0);
							System.out.println("one node for skipping obtained: " + s);
							//continue;
						}
					}
				}
				if (vskip != null) {
					boolean bskip = false;
					if (udNode.getStmt().toString().contains("tudresden.ocl.parser.analysis.DepthFirstAdapter")) {
						bskip = true;
					}
					for (Variable d : udNode.getDefinedVars()) {
						if (d.isFieldRef()) { bskip = true; break; }
						if (vskip.mayEqual(d) || vskip.toString().equalsIgnoreCase(d.getValue().toString())) { bskip = true; break; }
					}
	 				if (bskip) {
	 					System.out.println("One node skipped for instrumentation: " + cfgNode.getStmt());
	 					continue;
	 				}
				}
				*/
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				/*
				CallSite cs = cfgNode.getCallSite();
				if	(cs != null && cs.hasAppCallees()) {
					instNodes.add(new NodePoint(cfgNode, NodePoint.PRE_RHS));
				}
				else {
					instNodes.add(new NodePoint(cfgNode, NodePoint.POST_RHS));
				}
				*/
				/** hcai: changed the above commented block to the following for including all node points 
				 *  namely fixing the way pre/post-execution use was chosen as commented out above
				 */ 
				// -
				CallSite cs = cfgNode.getCallSite(); 
				boolean hasPre = false;
				List<Variable> defs = ((NodeDefUses)cfgNode).getDefinedVars();
				if (cfgNode.getSuccs().size()<=1 && !defs.isEmpty()) {
					for (Variable vdef : defs) {
						if (vdef.isStrConstObj()) { hasPre = true; break; }
						
						// this test is expensive - we assume there are always uses of the def
						//if (!DependenceFinder.getAllUsesForDef(vdef, cfgNode).isEmpty())
						{
							if	(cs != null && cs.hasAppCallees()) {
								hasPre = true; break;
							}
						}
					}
				}
				if (hasPre) {
					instNodes.add(new NodePoint(cfgNode, NodePoint.PRE_RHS));
				}
				
				/*
				if (cfgNode.getSuccs().size()>1 || !defs.isEmpty() || 
						(cfgNode.getStmt() instanceof ReturnStmt) && !(cfg.getMethod().getReturnType() instanceof soot.VoidType)
						) {
					instNodes.add(new NodePoint(cfgNode, NodePoint.POST_RHS));
				}
				*/
				// there is no easy way to exclude postRhs nodepoint for this cfg node without computing dependencies, thus we choose
				// a conservative inclusion of it anyway
				instNodes.add(new NodePoint(cfgNode, NodePoint.POST_RHS));
				// -
			}
		}

		System.out.println("Inst points count: " + instNodes.size());

		// [DEBUG]
		// for (NodePoint np : instNodes) {
		// if (np.getN() == null) {
		// System.err.println("getN() == null: " + np);
		// continue;
		// }
		// System.err.print(np.getN().getStmt() + "\t");
		// System.err.println("GlobalNodeId: "
		// + StmtMapper.getGlobalNodeId(np.getN()));
		// }

		// instrument them
		instrument(instNodes, useFixedMethod);

		return instNodes;
	}

	public void instrument(Collection<NodePoint> pnts, boolean useFixedMethod) {
		List<NodePoint> pntsSorted = new ArrayList<NodePoint>(pnts);
		Collections.sort(pntsSorted, NodePoint.NodePointComparator.inst);

		// DEBUG
		// Set<Integer> bads = new HashSet<Integer>(); //Arrays.asList(
		// 3741, 3744));
		System.out.println("Instrumenting points: " + pntsSorted.size());

		for (NodePoint pntDef : pntsSorted) {
			// instrument defined values for this point
			final boolean preRhs = pntDef.getRhsPos() == NodePoint.PRE_RHS;
			NodeDefUses nDef = (NodeDefUses) pntDef.getN();
			Stmt sDef = nDef.getStmt();
			List<Variable> defVars = nDef.getDefinedVars();

			final int sId = StmtMapper.getGlobalNodeId(nDef);

			boolean isInstrumented = false;
			// DEBUG
			// if (bads.contains(sId))
			// continue;

			SootMethod mDef = ProgramFlowGraph.inst().getContainingMethod(sDef);
			Body bDef = mDef.retrieveActiveBody();
			PatchingChain<Unit> pchain = bDef.getUnits();

			if (nDef.getSuccs().size() > 1) { // predicate case -- `defines'
												// value of program counter
				assert !preRhs;

				if (sDef instanceof IfStmt) {
					// instrument each outgoing branch with sId and outcome's
					// integer value (e.g., 0=false, 1=true)
					assert nDef.getOutBranches().size() == 2;

					// false branch (0)
					List<Object> probeF = new ArrayList<Object>();
					List<Object> argsF = new ArrayList<Object>();
					argsF.add(IntConstant.v(sId)); // param 1: node id
					argsF.add(IntConstant.v(preRhs ? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxedF = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probeF, lBoxedF, IntConstant.v(0));
					argsF.add(lBoxedF); // param 3: var value -- 0 for false branch
					Stmt sReportCallF = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), argsF));
					probeF.add(sReportCallF);
					InstrumManager.v().insertProbeAt(nDef.getOutBranches().get(0), probeF);
					isInstrumented = true;

					// true branch (1)
					List<Object> probeT = new ArrayList<Object>();
					List<Object> argsT = new ArrayList<Object>();
					argsT.add(IntConstant.v(sId)); // param 1: node id
					argsT.add(IntConstant.v(preRhs ? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxedT = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probeT, lBoxedT, IntConstant.v(1));
					argsT.add(lBoxedT); // param 3: var value -- 1 for true branch
					Stmt sReportCallT = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(	mReportStmt.makeRef(), argsT));
					probeT.add(sReportCallT);
					InstrumManager.v().insertProbeAt(nDef.getOutBranches().get(1), probeT);
					isInstrumented = true;
				} else { // switch (table or lookup): insert report of deciding value before switch					
					// get value that decides switch-branch to take
					Value valDeciding;
					if (sDef instanceof LookupSwitchStmt)
						valDeciding = ((LookupSwitchStmt) sDef).getKey();
					else {
						assert sDef instanceof TableSwitchStmt;
						valDeciding = ((TableSwitchStmt) sDef).getKey();
					}

					// insert probe before switch stmt with boxed deciding value
					// as argument
					List<Object> probe = new ArrayList<Object>();
					List<Object> args = new ArrayList<Object>();
					args.add(IntConstant.v(sId)); // param 1: node id
					args.add(IntConstant.v(preRhs ? 0 : 1)); // param 2: pre position at node -- should be always post for conditional statements
					Local lBoxed = UtilInstrum.getCreateLocal(bDef, LOCAL_OBJ, clsObject.getType());
					addCopyBoxOrCastStmt(bDef, probe, lBoxed, valDeciding);
					args.add(lBoxed); // param 3: var value -- should be a boxed local primitive (integer-like)
					Stmt sReportCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), args));
					probe.add(sReportCall);
					InstrumManager.v().insertAtProbeBottom(pchain, probe, sDef);
					isInstrumented = true;
				}
			} else { // variable definition(s) case
				for (Variable vDef : defVars) {
					// spare special cases
					if (vDef.isObject()	&& vDef.getValue() instanceof StaticInvokeExpr) {
						// isInstrumented = true;
						continue;
					}

					if (vDef.isArrayRef() && sDef instanceof AssignStmt && ((AssignStmt) sDef).getRightOp() instanceof NewArrayExpr) {
						// isInstrumented = true;
						continue;
					}
					// Case 3: the statement defines a variable whose type is not suitable for instrumentation.
					if (blacklist_classes.contains(vDef.getValue().getType().toString())){
						continue;
					}
					// Case 4: the statement uses blacklist_classes type variables
					boolean continueflag = false;
					Iterator<Variable> iterator = nDef.getUsedVars().iterator();
					while (iterator.hasNext()) {
						Variable usedVar = iterator.next();
						if (blacklist_classes.contains(usedVar.getValue().getType().toString())){
							continueflag = true;
							break;
						}
					}
					if (continueflag == true){
						continue;
					}
					
					//if (vDef.getValue().getType() instanceof soot.IntegerType)  { isInstrumented = true; continue; }
					//if (vDef.isFieldRef()) { isInstrumented = true; continue; }
					
					List<Object> probe = new ArrayList<Object>();
					List<Object> args = new ArrayList<Object>();
					args.add(IntConstant.v(sId)); // param 1: node id
					args.add(IntConstant.v(preRhs ? 0 : 1)); // param 2: pre position at node
					args.add(boxVar(mDef, vDef, probe)); // param 3: var value modifies probe with boxing code while getting local ref pointing to boxed value
					Stmt sReportCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(), args));
					probe.add(sReportCall);
					if (preRhs) {
						// any variable defined that is no the lhs (to which the return value is assigned)
						assert !(sDef instanceof IdentityStmt);
						if (!(sDef instanceof AssignStmt) || ((Local) ((AssignStmt) sDef).getLeftOp()) != vDef.getValue()) {
							InstrumManager.v().insertAtProbeBottom(pchain, probe, getAfterSpecialInvokeStmt(pchain, sDef));
							isInstrumented = true;
						}
					} else {
						if (sDef instanceof IdentityStmt) {
							// if (defVars.size() != 1) {
							// System.err.println("Assertion gonna fail!\n\t"
							// + sDef);
							// }

							// TODO this assertion is commented because
							// instrument() and instrument(boolean) cannot pass
							// it when instrumenting RollCall
							// assert defVars.size() == 1;

							if (!isParUsed((IdentityStmt) sDef,	(NodeReachDefsUses) nDef, mDef)) {
								isInstrumented = true; // if uncommented, will throw an exception
														// "@param-assignment statements should precede all non-identity statements"
								continue;
							}
							// just before first non-id stmt (first valid  insertion location)
							InstrumManager.v().insertBeforeNoRedirect(pchain, probe, getFirstSafeNonIdStmt(vDef.getValue(), mDef));
							/*
							Stmt firstuse = isParUsed((IdentityStmt) sDef, 	(NodeReachDefsUses) nDef, mDef);
							if (firstuse == null) { isInstrumented = true; continue; }
							Stmt firstvalid = getFirstSafeNonIdStmt(vDef.getValue(), mDef);
							Stmt finalLoc = firstuse;
							if (firstvalid == (Stmt)pchain.getSuccOf(firstuse)) {
								finalLoc = firstvalid;
								InstrumManager.v().insertBeforeNoRedirect(pchain, probe, finalLoc);
							}
							*/
							isInstrumented = true;
						} else {
							if (sDef instanceof ReturnStmt) {
								InstrumManager.v().insertAtProbeBottom(pchain, probe, sDef); // return stmt
								isInstrumented = true;
							} else {
								assert !(sDef instanceof RetStmt);

								// *** TODO: *** this definition should still be taken into account!!
								// special case: writing of reference to parent (arg 1) in nested class ctor
								if (mDef.getName().equals("<init>")	&& vDef.isFieldRef()) {
									final String fldName = ((FieldRef) vDef.getValue()).getFieldRef().name();
									if (fldName.equals("this$0")) {
										// isInstrumented = true;
										continue; // skip
									}
									/** 
									 * hcai: the following fix, which looks reasonable and did fix a JavaVerify Error "expecting to find object/array on stack" for
									 * ArgoUML, has not yet been generalized to other subjects for now;
									 * I leave it here because it would be useful for debugging similar issues that may happen to other classes later on
									 *
									if (((FieldRef)vDef.getValue()).getField().getDeclaringClass().getName().equalsIgnoreCase(mDef.getDeclaringClass().getName())) {
										Stmt sinit = getFirstSafeNonIdStmt(bDef.getLocals().getFirst(), mDef);
										
										if (sId <= StmtMapper.getGlobalStmtId((Stmt)pchain.getPredOf(sinit))) {
											// reference to a field of "this" object before "this" is initialized cann't be monitored
											isInstrumented = true;
											//System.out.println("One node skipped for monitoring: " + sDef);
											continue;
										}
									}
									*/
								}
								// special case: writing of static 'class' field in <clinit>
								if (mDef.getName().equals("<clinit>") && vDef.isFieldRef()) {
									final String fldName = ((FieldRef) vDef.getValue()).getFieldRef().name();
									if (fldName.equals("class$0")) {
										// isInstrumented = true;
										continue; // skip
									}
								}
								InstrumManager.v().insertBeforeNoRedirect(pchain, probe, getSuccAfterNextSpecialInvokeStmt(pchain, sDef)); // after definition of lhs
								isInstrumented = true;
							}
						}
					}
				}
			}

			if (isInstrumented == false && useFixedMethod == true) {
				List<Object> probe = new ArrayList<Object>();
				List<Value> args = new ArrayList<Value>();
				args.add(IntConstant.v(sId)); // param 1: node id
				args.add(IntConstant.v(preRhs ? 0 : 1)); // param 2: pre position at node
				// args.add(boxVar(mDef, NullConstant.v(), probe)); // param 3:	var value -- modifies probe with boxing code while getting	local ref pointing to boxed value
				args.add(boxVar(mDef, IntConstant.v(0), probe)); // param 3: var  value -- modifies probe with boxing code while getting local ref pointing to boxed value
				Stmt sReportCall = Jimple.v().newInvokeStmt(Jimple.v().newStaticInvokeExpr(mReportStmt.makeRef(),	args));
				probe.add(sReportCall);
				
				/** hcai: the statement involving a type in the black-list can be an ID statement, then we must insert the probe before the
				 * first non-ID statement as we do for monitoring any value defined at an ID statement
				 */
				if (sDef instanceof IdentityStmt) {
					// just before first non-id stmt (first valid  insertion location)
					InstrumManager.v().insertBeforeNoRedirect(pchain, probe, getFirstSafeNonIdStmt(IntConstant.v(0), mDef));
				} // -
				else {
					InstrumManager.v().insertAtProbeBottom(pchain, probe, sDef);
				}
				isInstrumented = true;
			}
		}
	}

	public static Stmt getAfterSpecialInvokeStmt(PatchingChain<Unit> pchain, Stmt s) {
		return (s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr) ? (Stmt) pchain.getSuccOf(s) : s;
	}

	public static Stmt getSuccAfterNextSpecialInvokeStmt(PatchingChain<Unit> pchain, Stmt s) {
		// if s is new expr, then get past <init> specialinvoke
		if (s instanceof AssignStmt && ((AssignStmt) s).getRightOp() instanceof NewExpr) { // exclude newarray case
			Local lNew = (Local) ((AssignStmt) s).getLeftOp();
			do {
				s = (Stmt) pchain.getSuccOf(s);
			} // get past new expr
			while (!(s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr 
					&& ((SpecialInvokeExpr) s.getInvokeExpr()).getBase() == lNew));
		}
		return (Stmt) pchain.getSuccOf(s); // get successor, whether s is <init> specialinvoke or not
	}

	public static Stmt getFirstSafeNonIdStmt(Value val, SootMethod m) {
		Body b = m.retrieveActiveBody();
		PatchingChain<Unit> pchain = b.getUnits();
		Stmt s = UtilInstrum.getFirstNonIdStmt(pchain);
		if (m.getName().equals("<init>") && val == b.getLocals().getFirst()) {
			while (!(s.containsInvokeExpr() && s.getInvokeExpr() instanceof SpecialInvokeExpr
					&& ((SpecialInvokeExpr) s.getInvokeExpr()).getMethod().getName().equals("<init>") 
					&& ((SpecialInvokeExpr) s.getInvokeExpr()).getBase() == val)) {
				s = (Stmt) pchain.getSuccOf(s);
			}
			s = (Stmt) pchain.getSuccOf(s);
		}
		return s;
	}
	
	// return the first use statement of the l-value of statement s if any, otherwise null 
	//public static Stmt isParUsed(IdentityStmt s, NodeReachDefsUses nRU, SootMethod m) {
	public static boolean isParUsed(IdentityStmt s, NodeReachDefsUses nRU, SootMethod m) {
		Local lPar = (Local) ((IdentityStmt) s).getLeftOp();
		// ensure that formal param is used
		BitSet bsUIn = nRU.getUBackOut();
		CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getCFG(m);
		List<Use> uses = cfgDU.getUses();
		final int numUses = uses.size();
		for (int i = 0; i < numUses; ++i) {
			if (bsUIn.get(i) && uses.get(i).getValue() == lPar) {
				return true;
				//return uses.get(i).getSrcNode().getStmt();
			}
		}
		//return null;
		return false;
	}

	private static final String LOCAL_OBJ = "<loc_obj>";

	public static Value boxVar(SootMethod m, Variable v, List<Object> probe, SootClass cls){
		Body b = m.retrieveActiveBody();
		Value vFrom;
		if (v.isObject() && (vFrom = ((ObjVariable) v).getBaseLocal()) != null)
			;
		else
			vFrom = v.getValue();

		// const, local, field, array elem, obj w/o base local (i.e., static field or str const)
		Local lObj = UtilInstrum.getCreateLocal(b, LOCAL_OBJ,	cls.getType());
		addCopyBoxOrCastStmt(b, probe, lObj, vFrom);
		return lObj;		
	}

	private Value boxVar(SootMethod m, Variable v, List<Object> probe) {
		return boxVar(m, v, probe, clsObject);
	}

	public static Value boxVar(SootMethod m, Constant v, List<Object> probe, SootClass cls) {
		Body b = m.retrieveActiveBody();
		//Value vFrom;

		// const, local, field, array elem, obj w/o base local (i.e., static field or str const)
		Local lObj = UtilInstrum.getCreateLocal(b, LOCAL_OBJ, cls.getType());
		addCopyBoxOrCastStmt(b, probe, lObj, v);
		return lObj;		
	}
	
	protected Value boxVar(SootMethod m, Constant v, List<Object> probe) {
		return boxVar(m, v, probe, clsObject);
	}

	private static final String LOCAL_BOX_PREFIX = "<loc_box_";
	private static final String LOCAL_BOX_SUFFIX = ">";

	private static void addCopyBoxOrCastStmt(Body b, List<Object> probe, Local lObj, Value vFrom) {
		// box local into obj local, if primitive; just copy to obj local otherwise
		Type t = vFrom.getType();

		// for use in boxing ctor call or cast stmt, non-const non-local value must be copied to local first
		Value vFinalFrom;
		if (!(vFrom instanceof Constant || vFrom instanceof Local)) {
			// get/create local of appropriate type
			Local lValCopy = UtilInstrum.getCreateLocal(b, LOCAL_BOX_PREFIX + t 	+ LOCAL_BOX_SUFFIX, t);
			Stmt sCopyToLocal = Jimple.v().newAssignStmt(lValCopy, vFrom);
			probe.add(sCopyToLocal);
			vFinalFrom = lValCopy; // replace vFrom
		} else
			vFinalFrom = vFrom;

		if (t instanceof PrimType) {
			Pair<RefType, SootMethod> refTypeAndCtor = Util.getBoxingTypeAndCtor((PrimType) t);
			Stmt sNewBox = Jimple.v().newAssignStmt(lObj,	Jimple.v().newNewExpr(refTypeAndCtor.first()));
			Stmt sInitBox = Jimple.v().newInvokeStmt(Jimple.v().newSpecialInvokeExpr(lObj,refTypeAndCtor.second().makeRef(), vFinalFrom));
			probe.add(sNewBox);
			probe.add(sInitBox);
		} else {
			assert t instanceof RefLikeType;
			Stmt sCopyRef = Jimple.v().newAssignStmt(lObj, vFinalFrom);// Jimple.v().newCastExpr(vFinalFrom, clsObject.getType()));
			probe.add(sCopyRef);
		}
	}
}
