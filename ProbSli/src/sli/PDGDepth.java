package sli;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;

public class PDGDepth {
	
	private static Map<NodePoint,Integer> wsliceDepthFromStart = new HashMap<NodePoint,Integer>();
	private static LinkedHashMap<NodePoint,Float> pwsliceScoreFromStart = new LinkedHashMap<NodePoint,Float>();
	private static Map<Object,Float> psliceProbFromStart;
	
	public static Map<NodePoint, Integer> genWSliceDepth(DependenceGraph depGraph){
		Map<Dependence, Integer> depDepths = new HashMap<Dependence, Integer>();
		
		System.out.println("Calculting W-slice depth.........");
		// Dependence graph has been constructed in depGraph
		// BFS, starting from start point
		depDepths = DataDepDepth.findDataDepDepths(depGraph);
		// Debug
		DuaProb.writeMapToFile(depDepths, dua.util.Util.getCreateBaseOutPath() + "DepDepth.txt");

		// Iterate through depDepths map(<Dependence,Integer>), find target NodePoint for each Dependence, and assign the depth to each NodePont
		Iterator iter = depDepths.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			Dependence dependence = (Dependence) entry.getKey();
			Integer depth = (Integer) entry.getValue();
			NodePoint tgt = dependence.getTgt();
			//If the same tgt has been stored before, then compare the current depth and the stored depth and store the smaller one.
			if( wsliceDepthFromStart.containsKey(tgt)){
				Integer oldDepth =  wsliceDepthFromStart.get(tgt);
				if(oldDepth < depth)
					depth = oldDepth;
			}
			 wsliceDepthFromStart.put(tgt, depth);
		}
		return  wsliceDepthFromStart;
	}
	
	public void genWSliceFileFromStart(DependenceGraph depGraph) {
		File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "wslice.out");
		Map<NodePoint,Integer> wsliceFromStart = new HashMap<NodePoint,Integer>();
		try {
			Writer writer = new FileWriter(fOut);
			wsliceFromStart = genWSliceDepth(depGraph);
			
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(wsliceFromStart.keySet());
			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted)
				writer.write(pTgt + "=" + wsliceFromStart.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();	
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	public static LinkedHashMap<NodePoint, Float> genPWSliceOrder(DependenceGraph depGraph){
		Queue depQueue = new LinkedList();
		float currentScore = 0f;
		
		System.out.println("Calculting Prio-Wslice Order.........");
		//Get all the probabilities for each dependence.
		Map depTgtPrios = new HashMap();   //depPrios means psliceProb in this case
		depTgtPrios = getPsliceProb(depGraph); // This is a <NodePoint, Float> map

		// prio-BFS, starting from start point
		depQueue = DataDepDepth.findDataDepPrioDepth(depGraph, depTgtPrios);

		// Iterate through the queue, 
		// 1) find target NodePoint for each Dependence, and assign the score to each NodePont
		// 2) store the <NodePoint, score> IN ORDER (in a LinkedHashMap)
		Iterator iter = depQueue.iterator();
		while (iter.hasNext()){
			Dependence dep = (Dependence) iter.next();
			NodePoint tgt = dep.getTgt();
//			//If the same tgt has been stored before, then compare the current depth and the stored depth and store the smaller one.
//			if( pwsliceScoreFromStart.containsKey(tgt)){
//				float oldScore =  pwsliceScoreFromStart.get(tgt);
//				if (oldScore!=0){ // if oldScore=0, it means that prio=0 for that dependence. This kind of dependences are NOT in depTgtPrios map.
//					currentScore = (Float) depTgtPrios.get(tgt);
//					if(oldScore < currentScore)
//						currentScore = oldScore;
//				}
//				else currentScore = oldScore;
//			}
			if (depTgtPrios.containsKey(tgt))
				currentScore = (Float) depTgtPrios.get(tgt);
			pwsliceScoreFromStart.put(tgt, currentScore);
		}
		return  pwsliceScoreFromStart;
	}
	
	public void genPWSliceFileFromStart(DependenceGraph depGraph) {
		File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "priowslice.out");
		LinkedHashMap<NodePoint,Float> pwsliceFromStart = new LinkedHashMap<NodePoint,Float>();
		try {
			Writer writer = new FileWriter(fOut);
			pwsliceFromStart = genPWSliceOrder(depGraph);
			
			List<NodePoint> pntsOrdered = new ArrayList<NodePoint>(pwsliceFromStart.keySet());
			for (NodePoint pTgt : pntsOrdered)
				writer.write(pTgt + "=" + pwsliceFromStart.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();	
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	//get prob p(s) for each dependence using pslice method
	private static Map getPsliceProb(DependenceGraph depGraph){
		PDGProb pdgProb = new PDGProb(depGraph);
		psliceProbFromStart =pdgProb.findFwdProbsFromStart();
		return psliceProbFromStart;
	}
	
}
