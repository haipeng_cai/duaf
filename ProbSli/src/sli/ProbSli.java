package sli;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import profile.DynSliceInstrumenter;
import profile.ExecHistInstrumenter;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

public class ProbSli implements Extension {
	
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		ProbSli probSlicer = new ProbSli();
		Forensics.registerExtension(probSlicer);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = ProbSliOptions.process(args);
		
		// add option to consider formal parameters as defs and return values as uses, for reaching def/use analysis
		//     and option to not remove repeated branches in switch nodes
		String[] args2 = new String[args.length + 2];
		System.arraycopy(args, 0, args2, 0, args.length);
		args2[args.length]     = "-paramdefuses";
		args2[args.length + 1] = "-keeprepbrs";
		
		args = args2;
		return args;
	}
	
	@Override public void run() {
		System.out.println("Running Q-slicer extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// for now, assume exactly one start point is provided
		CFGNode nStart = StmtMapper.getNodeFromGlobalId( ProbSliOptions.getStartStmtIds().get(0) );
		DependenceGraph depGraph = new DependenceGraph(new NodePoint(nStart, NodePoint.POST_RHS), -1);
		
//		findDistPoints(depGraph);
		
		
		
		
		// Debug: find path between two points in the slice
		//List<NodePoint> pntList = DataDepProb.findAllPntsInSlice(depGraph);
		//NodePoint pathStartPnt = null;
		//NodePoint pathEndPnt = null;
		//for (NodePoint pnt : pntList){
		//	String startPntStr = "2444[1]";
		//	String endPntStr = "4227[1]";
			
		//	if (pnt.toString().equals(startPntStr)){
		//		pathStartPnt = pnt;
		//	}
		//	if (pnt.toString().equals(endPntStr)){
		//		pathEndPnt = pnt;
		//	}
		//}
		//Set<List<NodePoint>> pathSet = BackDepAnalysis.findPath(depGraph, pathStartPnt, pathEndPnt);
		//System.out.println("Path Finding...");
		//System.out.println(pathSet);
		
		
		
		
		if (!ProbSliOptions.noPSlice()) {
			PDGProb pdgProb = new PDGProb(depGraph);
			//
			// add a option here to gen/not gen a file that contains the map depProbs from PDGProb class
			//
			pdgProb.findFwdProbsFromStart();
			pdgProb.genPSliceFileFromStart();
			if (ProbSliOptions.genAllToAll())
				pdgProb.genPSliceFileAllToAll();
		}
		
		if (ProbSliOptions.fwdDynSliceInstr()) {
			DynSliceInstrumenter dynSliceInstrum = new DynSliceInstrumenter(false);
			dynSliceInstrum.instrument(depGraph);
		}
		// Perform W-Sli
		if (ProbSliOptions.wSlice()){
			PDGDepth pdgDepth = new PDGDepth();
			pdgDepth.genWSliceFileFromStart(depGraph);
		}
		
		// (Plan A:) Perform prioritized-BFS order <"priowslice">
		if (ProbSliOptions.prioWSlice()){
			PDGDepth pdgDepth = new PDGDepth();
			pdgDepth.genPWSliceFileFromStart(depGraph);
		}
		
		// (Plan B:) Perform slice using modified probability p(s) <"modpslice">
		if (ProbSliOptions.modPSlice()){
			ModPSlice modPSlice = new ModPSlice();
			modPSlice.genScoreFile(depGraph);	
		}
				
		
//		else {
		// instrument subject to output augmented execution histories of sliced statements for dynamic change-impact assessment
		ExecHistInstrumenter instr = new ExecHistInstrumenter();
		//the old, buggy way: 		instr.instrument(depGraph.getPointsInSlice(), false);
		instr.instrument(depGraph.getPointsInSlice(), true);
//		}
	}
	
	/** Looks for all points at distance d (given by setting). */
	private void findDistPoints(DependenceGraph depGraph) {
		final int d = ProbSliOptions.dist();
		
		Set<NodePoint> pntsAtDist = findNextDistPoints(depGraph, depGraph.getStart(), d);
		List<NodePoint> sortedPntsAtD = new ArrayList<NodePoint>(pntsAtDist);
		Collections.sort(sortedPntsAtD, NodePointComparator.inst);
		
		// DEBUG: print all points at dep distance d
		System.out.println("DISTANCE " + d + " points from " + StmtMapper.getGlobalNodeId(depGraph.getStart().getN()) + ": total " + sortedPntsAtD.size());
		for (NodePoint pntAtD : sortedPntsAtD)
			System.out.println(" " + StmtMapper.getGlobalNodeId(pntAtD.getN()));
	}
	/** Recursive helper for findDistPoints */
	private Set<NodePoint> findNextDistPoints(DependenceGraph depGraph, NodePoint pnt, int d) {
		assert d >= 1;
		
		Set<NodePoint> pntsAtDist = new HashSet<NodePoint>();
		if (d == 1) {
			// get immediate successors in dep graph
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.add(dep.getTgt());
		}
		else {
			// for each successor in dep graph, recursively advance with d-1
			for (Dependence dep : depGraph.getOutDeps(pnt))
				pntsAtDist.addAll( findNextDistPoints(depGraph, dep.getTgt(), d - 1) );
		}
		
		// DEBUG
		Set<NodePoint> dbgPntsSet = new HashSet<NodePoint>();
		for (Dependence dep : depGraph.getOutDeps(pnt))
			dbgPntsSet.add(dep.getTgt());
		List<NodePoint> dbgSortedPnts = new ArrayList<NodePoint>(dbgPntsSet);
		Collections.sort(dbgSortedPnts, NodePointComparator.inst);
		System.out.println(" d " + d + ": " + pnt + " -> " + dbgSortedPnts);
		
		return pntsAtDist;
	}
	
}
