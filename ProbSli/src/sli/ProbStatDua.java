package sli;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import profile.DynSliceInstrumenter;
import profile.ExecHistInstrumenter;
import dua.DUA;
import dua.DUAAnalysis;
import dua.DUAssocSet;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceFinder.Dependence;
import fault.StmtMapper;

public class ProbStatDua implements Extension {
	
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		ProbStatDua probDua = new ProbStatDua();
		Forensics.registerExtension(probDua);
		Forensics.main(args);
	}
	
	private static String[] preProcessArgs(String[] args) {
		args = ProbStatDuaOptions.process(args);
		return args;
	}
	
	private Map<Integer, Float> duaIdProbs;
	private Map<Dependence, Float> ctrlDepProbs;
	
	@Override public void run(){
		System.out.println("=========Running ProbStatDua:=========");	
		StmtMapper.getCreateInverseMap(); 
		
		//To get all duas. Copied from Forensics.java
		DUAAnalysis duaAnalysis = DUAAnalysis.inst();
		DUAssocSet duaSet = duaAnalysis.getDUASet();
		List<DUA> duas = duaSet.getAllDUAs();
		
		if(ProbStatDuaOptions.simpleProb()){  // This strategy corresponds to IWPD12 PrioDU paper
			duaIdProbs = DuaProb.findSimpleDuaProbs(duas);
			Map<Integer, Float> sortedDuaIdProbsMap = DuaProb.rankMapRev(duaIdProbs);
			Map<Integer, Float> duaIdProbsRankMap = DuaProb.giveRanktoDua(sortedDuaIdProbsMap);
			Map<Integer, Float> sortedDuaIdProbsRankMap = DuaProb.rankMap(duaIdProbsRankMap);
			
			DuaProb.writeMapToFile(duaIdProbs,dua.util.Util.getCreateBaseOutPath() + "SimpleDuaProb.txt");
			DuaProb.writeMapToFile(sortedDuaIdProbsMap,dua.util.Util.getCreateBaseOutPath() + "SortedSimpleDuaProb.txt");
			DuaProb.writeMapToFile(sortedDuaIdProbsRankMap,dua.util.Util.getCreateBaseOutPath() + "SortedSimpleDuaProbRank.txt");
		}
		
		if(ProbStatDuaOptions.condProb())
		{
			duaIdProbs = DuaProb.findCondDuaProbs(duas);
//			ctrlDepProbs = DuaProb.addControlDepProbs();
			
			Map<Integer, Float> sortedDuaIdProbsMap = DuaProb.rankMapRev(duaIdProbs);
			Map<Integer, Float> duaIdProbsRankMap = DuaProb.giveRanktoDua(sortedDuaIdProbsMap);
			Map<Integer, Float> sortedDuaIdProbsRankMap = DuaProb.rankMap(duaIdProbsRankMap);
			
			DuaProb.writeMapToFile(duaIdProbs,dua.util.Util.getCreateBaseOutPath() + "CondDuaProb.txt");
			DuaProb.writeMapToFile(sortedDuaIdProbsMap,dua.util.Util.getCreateBaseOutPath() + "SortedCondDuaProb.txt");
			DuaProb.writeMapToFile(sortedDuaIdProbsRankMap,dua.util.Util.getCreateBaseOutPath() + "SortedCondDuaProbRank.txt");
		}
	}
}
