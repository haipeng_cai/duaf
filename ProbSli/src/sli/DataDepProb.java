package sli;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Local;
import soot.SootMethod;
import soot.jimple.Stmt;
import dua.global.ProgramFlowGraph;
import dua.global.dep.BackwardDependenceGraph;
import dua.global.dep.DependenceFinder;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG;
import dua.method.CallSite;
import dua.method.MethodTag;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFG.CFGNodeSpecial;
import dua.method.CFGDefUses.Def;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.ReachableUsesDefs.NodeReachDefsUses;
import dua.unit.Location;
import fault.StmtMapper;

public class DataDepProb {
	/// *** THIS is probably old and rather obsolete
	/// Coverage prob:
	///  For each cfg node n, rd d and ru u at n, prob_rd(d,n->u) =
	///       { 1 if n = u;
	///         0 if !reach_from(u,m);
	///         sum(m in succ(n)) { prob_cov(n->m) x prob_reach_bottom(d,m) x prob_rd(d,m->u) }  otherwise;
	///       }
	/// *** THIS is probably old and rather obsolete
	
	/******** DAG NODES *********/
	/** Node of search dag for some graph (CFG, call-site-graph) */
	public static abstract class ReachNode {
		protected Set<ReachNode> ancestors = new HashSet<ReachNode>(); // used during dag construction to check for cycles
		protected List<ReachNode> dagChildren = new ArrayList<ReachNode>();
		protected List<ReachNode> backChildren = new ArrayList<ReachNode>(); // targets of backedges whose source is this node
		protected Collection<ReachNode> loopExChildren = new HashSet<ReachNode>(); // artificial edges to transfer probs from outside loops to backedge source nodes
		
		/** Probabilities of reaching objects (use ids, nodes, call sites) from the BOTTOM of current node. */
		protected Map<Object,Float> probReachFromBottom = new HashMap<Object, Float>();
		
		/** Retrieves the object (use id, cfg node, call site) represented by this DAG node. */
		protected abstract Object getObjNode();
		
		protected ReachNode(ReachNode parent) { }
		@Override public String toString() { Object o = getObjNode(); return (o == null)? "null" : o.toString(); }
		
		private static final List<ReachNode> FIXED_EMPTY_NODE_LIST = new ArrayList<ReachNode>();
		protected void postBuildCleanupRecursive(Set<ReachNode> visited, boolean removeProbsMap) {
			final boolean notVisitedBefore = visited.add(this);
			assert notVisitedBefore;
			
			// cleanup children first
			assert !this.dagChildren.contains(this);
			for (ReachNode succ : this.dagChildren)
				if (succ.ancestors != null)
					succ.postBuildCleanupRecursive(visited, removeProbsMap);
			// cleanup this node
			this.ancestors.clear(); // ancestors not needed anymore
			this.ancestors = null;
			if (dagChildren.isEmpty()) this.dagChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			if (backChildren.isEmpty()) this.backChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			if (loopExChildren.isEmpty())
				this.loopExChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			else
				this.loopExChildren = new ArrayList<ReachNode>(this.loopExChildren); // transform set into array list to save space
			// cleanup probs map only if required
			if (removeProbsMap)
				this.probReachFromBottom = null;
		}
	}
	/** Node of search dag for CFG (def-use, intraproc reach). */
	public static class ReachCFGNode extends ReachNode {
		/** The object that this DAG node represents. */
		protected final CFGNode n;
		/// *** METHODS *** ///
		protected ReachCFGNode(CFGNode n, ReachNode parent) { super(parent); this.n = n; }
		protected Object getObjNode() { return n; }
	}
	public static class ReachEdge {
		public final ReachNode src;
		public final ReachNode tgt;
		public ReachEdge(ReachNode src, ReachNode tgt) { this.src = src; this.tgt = tgt; }
	}
	
	/******** FACTORIES *********/
	public static interface ReachDagFactory { ReachDag newInst(Object oInfo); ReachNode newNode(Object oNode, ReachNode parent); }
	public static class ReachCFGDagFactory implements ReachDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachCFGDefUseDag((CFG)oInfo); }
		@Override public ReachNode newNode(Object oNode, ReachNode parent) { return new ReachCFGNode((CFGNode)oNode, parent); }
		
		public static ReachCFGDagFactory inst = new ReachCFGDagFactory();
		private ReachCFGDagFactory() {}
	}
	
	/******** DAGs *********/
	/** Search dag for some graph (CFG, call-site-graph). Stores backedges in list as found in DFS order, so innermost backedges are listed first. */
	public static abstract class ReachDag {
//		public static final int BACKEDGE_FREQ = 4; // for now, same for all backedges
		
		protected final ReachDagFactory fact;
		protected final ReachNode root;
		protected final List<ReachNode> leaves = new ArrayList<ReachNode>();
		protected final List<ReachEdge> backedges = new ArrayList<ReachEdge>();
		/** May be filled by subclass after DAG is built. */
		protected final Map<Object,ReachNode> objToDAGNode = new HashMap<Object, ReachNode>();
		
		protected ReachDag(ReachNode root, ReachDagFactory fact) { this.root = root; this.fact = fact; }
		
		// DEBUG -- meant to be overridden, if desired
		protected void printStatsPreLoopExits() {}
		protected void printStatsDuringLoopExits(int numBEProcessed) {}
		protected void printStatsAfterLoopExits(int numLoopExits) {}
		/** DFS search to create spanning dag and identify backedges. */
		protected static ReachDag buildDag(Object oInfo, ReachDagFactory fact) {
			ReachDag dag = fact.newInst(oInfo);
			
			// build dag, identifying backedges along the way
			Map<Object,ReachNode> visited = new HashMap<Object,ReachNode>(); // keeps map to already created reachNodes
			dag.buildSubDag(dag.root, visited);
			
			dag.fillNodeMap();
			
			dag.printStatsPreLoopExits();
			
			// after dag is completed (in particular, the ancestor relation), find the exits for each backedge's loop
			dag.findLoopExits();
			
			dag.postBuildCleanup(false); // free some memory
			
			return dag;
		}
		/** Recursive helper to find the dag and backedges in DFS search of CFG. */
		private final void buildSubDag(ReachNode node, Map<Object,ReachNode> visited) {
			final Object oNode = node.getObjNode();
			
			// update visited map
			Object oPrev = visited.put(oNode, node);
			assert oPrev == null;
			
			// handle "leaf" case
			List<Object> oSuccs = getSuccessors(oNode); //node.n.getSuccs();
			if (oSuccs.isEmpty()) {
				leaves.add(node);
				return;
			}
			
			// add non-visited successors to dag recursively; connect to visited successors; detect backedges
			for (Object oSucc : oSuccs) {
//				if (oSucc.isInCatchBlock())
//					continue; // don't support exception flow, for now
				
				ReachNode succ = visited.get(oSucc);
				if (succ == null) {  // newly found node
					// create successor reachNode, whose ancestors are current node's ancestor plus current node
					succ = fact.newNode(oSucc, node); // current node is a parent
					node.dagChildren.add(succ); // link to child
					succ.ancestors.addAll(node.ancestors); // share ancestor with successor
					succ.ancestors.add(node); // this node is also an ancestor of successor
					// continue recursion on new successor node
					buildSubDag((ReachNode)succ, visited);
				}
				else {  // already visited, so succ is either a join point in dag or the tgt of a backedge
					// successor is a backedge iff it's an ancestor of current node or the current node itself
					if (oSucc.equals(oNode) || node.ancestors.contains(succ)) {
						backedges.add( new ReachEdge(node, succ) );
						node.backChildren.add(succ);
					}
					else {
						// just update succ's parents and ancestors with current node and its own ancestors
						node.dagChildren.add(succ); // link to child
						succ.ancestors.addAll(node.ancestors);
						succ.ancestors.add(node);
					}
				}
			}
		}
		protected abstract List getSuccessors(Object oNode);
		
		/** Returns the nodes considered for loop-exit edge analysis. This default implementation returns all nodes in loop. */
		protected Set<ReachNode> getLoopNodes(ReachEdge be) {
			// L = ancestors(be-src) + be-src - ancestors(be-tgt)
			Set<ReachNode> loopNodes = new HashSet<ReachNode>(be.src.ancestors);
			loopNodes.add(be.src);
			loopNodes.removeAll(be.tgt.ancestors);
			return loopNodes;
		}
		/** Part of the DAG building algorithm. */
		private final void findLoopExits() {
			int numLoopExits = 0;
			int numBE = 0;
			for (ReachEdge be : backedges) {
				Set<ReachNode> loopNodes = getLoopNodes(be);
				
				// look for dag edges out of these nodes that also leave loop
				for (ReachNode lnode : loopNodes) {
					for (ReachNode succ : lnode.dagChildren) {
						if (!loopNodes.contains(succ) && !be.src.dagChildren.contains(succ)) {
							if (be.src.loopExChildren.add(succ))
								++numLoopExits;
						}
					}
				}
				
				if (++numBE % 100 == 0)
					printStatsDuringLoopExits(numBE);
			}
			
			printStatsAfterLoopExits(numLoopExits);
		}
		
		// DEBUG -- meant to be overridden, if desired
		protected void propagationIterationPreStep(int i) {}
		/** Propagates probabilities from leaves backwards (DFS post-order). */
		public void propagateProbs() {
			// do DFS post-order traversal of nodes in dag
			propagationIterationPreStep(0);
			Set<ReachNode> visited = new HashSet<ReachNode>();
			propProbsDFSFromSuccs(root, visited, null); // no backedge -- visit the whole dag
			
			// do alternating activation of backedges and loop-exit edges, re-propagating each time
			if (!this.backedges.isEmpty()) {
				for (int i = 0; i < ProbSliOptions.backEdgeFreq(); ++i) {
					propagationIterationPreStep(i+1);
					
					visited.clear();
					propProbsDFSFromSuccs(root, visited, this.backedges.get(0));
					visited.clear();
					propProbsDFSFromSuccs(root, visited, null);
				}
			}
		}
		/** Template method for recursive DFS post-order visitor of dag to propagate probs from successors to bottom of current node.
		 *  Possibly receives a non-null backedge tgt. If so, propagates from that successor too. If not, propagates through artificial loop-exit edges. */
		protected void propProbsDFSFromSuccs(ReachNode node, Set<ReachNode> visited, ReachEdge backedge) {
			// mark current node as visited
			final boolean notVisitedBefore = visited.add(node);
			assert notVisitedBefore;
			
			// DEBUG
			final int numVisited = visited.size();
			
			// get list of successors to visit before this node
			List<ReachNode> childrenToPropagateFrom = (List<ReachNode>) ((ArrayList<ReachNode>) node.dagChildren).clone();
			if (backedge != null)
				childrenToPropagateFrom.addAll(node.backChildren);
			else
				childrenToPropagateFrom.addAll(node.loopExChildren);
			
			// first, visit successors, which ensures that probs will be there to propagate to here
			for (ReachNode succ : childrenToPropagateFrom) {
				if (!visited.contains(succ))
					propProbsDFSFromSuccs(succ, visited, backedge);
			}
			
			// then, process this node -- propagate probs from successors, including backedge's target if specified or loop-exit edges otherwise
			final int numChildren = childrenToPropagateFrom.size();
			Map<Object,float[]> accumProbs = new HashMap<Object,float[]>();
			for (int childIdx = 0; childIdx < numChildren; ++childIdx)
				propProbsFromChild(node, childrenToPropagateFrom.get(childIdx), childIdx, childrenToPropagateFrom, accumProbs);
			
			// consolidate lists of probs into one prob for each of node's bottom's reachable objects (e.g., reachable uses)
			for (Object oReachable : accumProbs.keySet())
				consolidateProbs(node, oReachable, accumProbs);
			
			// DEBUG
			if (numVisited % 100 == 0) {
				System.out.println("DBG: probsli visited " + numVisited + " used/total/max mem " +
						(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
			}
		}
		
		/** Performs the transfer of probabilities from a particular child (successor) into a prob-accumulating map. */
		protected abstract void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs);
		/** Performs the final merging of the different probabilities propagated from all children into one probability for given node. */
		protected abstract void consolidateProbs(ReachNode node, Object oReachable, Map<Object,float[]> accumProbs);
		
		/** Utility to get/create float array from obj->float[] map. */
		protected static float[] getCreateFloatsArrayFor(Map<Object, float[]> oToFloatArrs, Object oKey, int arrSize) {
			float[] floatsArrForObj = oToFloatArrs.get(oKey);
			if (floatsArrForObj == null) {
				floatsArrForObj = new float[arrSize];
				oToFloatArrs.put(oKey, floatsArrForObj);
			}
			return floatsArrForObj;
		}
		/** Utility to get/create float list from obj->floatlist map. */
		protected static List<Float> getCreateFloatsList(Map<Object,List<Float>> oToFloatLists, Object key) {
			List<Float> flist = oToFloatLists.get(key);
			if (flist == null) {
				flist = new ArrayList<Float>();
				oToFloatLists.put(key, flist);
			}
			return flist;
		}
		
		/** Simple helper that averages float values (e.g., probs) in vector. */
		protected static float avg(float[] values) {
			assert values.length > 0;
			float sum = 0f;
			for (float v : values)
				sum += v;
			return sum / values.length;
		}
		/** Simple helper that averages float values (e.g., probs) in list. */
		protected static float avg(List<Float> values) {
			final int size = values.size();
			assert size > 0;
			float sum = 0f;
			for (float v : values)
				sum += v;
			return sum / size;
		}
		
		/** Computes the prob of the disjunction of a list of events given that their degree of (in)dependence is unknown. */
		protected static float disjunctiveProbMerge(float[] probs) {
			// Goal: - all probs should add something to the final prob
			//       - the final prob must be greater than any individual prob (if there is more than 1 event)
			// Algorithm:
			//    Sort probs from greatest to lowest. Each event overlaps evenly with the previous events and the rest of the universe.
			//  Example: [0.6, 0.5, 0.3] -> 0.6 + (1-0.6)x0.5/2 + (1-...)x0.3/3 = 0.6 + 0.1 + (1-0.7)x0.1 = 0.73
			//  Note: the divisor for the i^th prob is i, indicating that only 1/i of the rest of the universe overlaps with the i^th event
			
			// create sorted probs list in descending order
			final int numProbs = probs.length;
//			List<Float> probsSorted = new ArrayList<Float>(numProbs);
//			for (float p : probs)
//				probsSorted.add(p);
//			Collections.sort(probsSorted, Collections.reverseOrder());
			
			List<Float> probsList = new ArrayList<Float>(numProbs);
			for (float p : probs){
				probsList.add(p);
			}
			
			// apply prob-merging algorithm outlined above
			float result = 0f;
			for (int i = 0; i < numProbs; ++i)
				result += (1-result) * probsList.get(i); // / (i+1);
			result = Math.min(result, 1.0f); //avoid float-approximation error
			result = Math.max(result, 0f); //avoid float-approximation error
			return result;
		}
		/** Wrapper for disjunctiveProbMerge that first transforms prob list into array. */
		protected static float disjunctiveProbMerge(List<Float> probs) {
			float[] probsArr = new float[probs.size()];
			int i = 0;
			for (Float p : probs)
				probsArr[i++] = p;
			return disjunctiveProbMerge(probsArr);
		}
		
		// DEBUG -- printing
		public abstract void printProbs();
		
		protected void fillNodeMap() {
			fillNodeMapRecursive(root, objToDAGNode);
		}
		protected static void fillNodeMapRecursive(ReachNode node, Map<Object,ReachNode> oToNode) {
			if (oToNode.put(node.getObjNode(), node) != null)
				return;
			for (ReachNode succ : node.dagChildren)
				fillNodeMapRecursive(succ, oToNode);
		}
		
		/** Cleanup as much memory as possible. */
		protected void postBuildCleanup(boolean removeProbsMap) {
			Set<ReachNode> visited = new HashSet<ReachNode>();
			this.root.postBuildCleanupRecursive(visited, removeProbsMap);
		}
	}
	
	public static abstract class ReachCFGDag extends ReachDag {
		protected final CFG cfg;
		
		protected ReachCFGDag(CFG cfg, ReachDagFactory fact) { super(fact.newNode(cfg.getNodes().get(0), null), fact); this.cfg = cfg; }
		
		@Override protected final List getSuccessors(Object oNode) {
			// the successors are those NOT in catch glo
			List succs = new ArrayList();
			for (CFGNode nSucc : ((CFGNode)oNode).getSuccs()) {
				if (!nSucc.isInCatchBlock()) // don't support exception flow, for now
					succs.add(nSucc);
			}
			return succs;
		}
		
		@Override protected final void consolidateProbs(ReachNode node, Object oReachable, Map<Object,float[]> accumProbs) {
			// now, only ensure that existing prob to descendant is not greater (if so, a backedge was used previously to increase it)
			final float accumProbDescendant = avg(accumProbs.get(oReachable));
			final Float prevProb = node.probReachFromBottom.get(oReachable);
			if (prevProb == null || prevProb < accumProbDescendant)
				node.probReachFromBottom.put(oReachable, accumProbDescendant);
		}
		
		// DEBUG -- printing
		protected final void printProbsHelper(String msgPerDAG) {
			System.out.println(msgPerDAG + cfg.getMethod());
			for (CFGNode n : cfg.getNodes()) {
				if (n.isInCatchBlock())
					continue; // don't support exception flow, for now
				ReachNode node = objToDAGNode.get(n);
				if (node == null)
					System.out.println(" " + n + ": <unreachable in DAG>");
				else {
					System.out.print(" " + getNodeId(n) + " " + n + ":");
					for (Object oReach : node.probReachFromBottom.keySet()) {
						final float prob = node.probReachFromBottom.get(oReach);
						System.out.print(" " + getReachObjId(oReach) + "=" + prob);
					}
					System.out.println();
				}
			}
		}
		protected static String getNodeId(CFGNode n) {
			assert n instanceof CFGNode;
			if (n instanceof CFGNodeSpecial)
				return n.toString();
			return Integer.toString( StmtMapper.getGlobalNodeId(n) );
		}
		protected abstract String getReachObjId(Object oReach);
	}
	
	/** Search dag for def-uses in specific graph: CFG */
	public static final class ReachCFGDefUseDag extends ReachCFGDag {
		public ReachCFGDefUseDag(CFG cfg) { super(cfg, ReachCFGDagFactory.inst); }
		
		public static ReachCFGDag buildCFGDefUseDag(CFG cfg) {
			return (ReachCFGDag) buildDag(cfg, ReachCFGDagFactory.inst);
		}
		
		/** Specific propagation algorithm for reachable uses from child */
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs) {
			final ReachCFGNode childCFGNode = (ReachCFGNode) child;
			final int numChildren = children.size();
			if (!(childCFGNode.n instanceof NodeReachDefsUses))
				return; // skip if node does not have RU info (e.g., EN, EX)
			// look for GEN ru's at successor
			NodeReachDefsUses nRUSucc = (NodeReachDefsUses) childCFGNode.n;
			BitSet bsUGen = nRUSucc.getUGen();
			for (int i = bsUGen.nextSetBit(0); i >= 0; i = bsUGen.nextSetBit(i + 1)) {
				// get/create list of exclusive probabilities for RU
				final float[] probsForRU = getCreateFloatsArrayFor(accumProbs, i, numChildren);
				// add prob to accum list of probs for RU
				probsForRU[childIdx] = 1.0f; // prob of prop to use IN succ from here (coverage prob is implicit in size of this vector)
			}
			
			// look for other non-killed RU's at successor's top
			BitSet bsUBackIn = nRUSucc.getUBackIn();
			for (int i = bsUBackIn.nextSetBit(0); i >= 0; i = bsUBackIn.nextSetBit(i + 1)) {
				if (bsUGen.get(i))
					continue; // only consider uses reachable from bottom of successor in this step
				
				Float probSuccBottom = child.probReachFromBottom.get(i);
				if (probSuccBottom == null)
					continue; // use flows through a backedge -- not in dag
				
				// get/create list of exclusive probabilities for RU
				final float[] probsForRU = getCreateFloatsArrayFor(accumProbs, i, numChildren);
				// add prob to accum list of probs for RU
				assert probsForRU[childIdx] == 0f;
				probsForRU[childIdx] = probSuccBottom; // prob of prop to use THROUGH succ from here (coverage prob is implicit in size of this vector)
			}
		}
		
		// DEBUG
		@Override public void printProbs() {
			printProbsHelper("NEW Prob reachable uses: ");
		}
		@Override protected String getReachObjId(Object oReach) {
			return ((Integer)oReach).toString();
		}
		
	}
	
	public static Map<Dependence, Float> findDataDepProbs(DependenceGraph depGraph) {
		findIntraDataDepProbs();
		findInterDataDepProbs();
		Map<Dependence, Float> depProbs = assignDataDepProbs(depGraph);
		
		System.out.println("Used/total/max memory after data-dep prob analysis finished: " +
				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		freeMemory();
		System.out.println("Used/total/max memory after freeing up data-dep prob analysis memory: " +
				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		
		return depProbs;
	}
	
	
	// This method contains two pre-computations, which are needed in BOTH calculating EnToPnt probs and DataDepCov probs in PrioSlice (the backward case). 
	public static void findIntraAndInterDataDepProbs(BackwardDependenceGraph depGraph){
		findIntraDataDepProbs();
		findInterDataDepProbs();
	}
	
	public static Map<Dependence, Float> findDataDepProbs(BackwardDependenceGraph depGraph) {
		// These two are not needed anymore because they are in pre-computation phase now.
//		findIntraDataDepProbs();
//		findInterDataDepProbs();
		Map<Dependence, Float> depProbs = assignDataDepProbs(depGraph);
		
/*		System.out.println("Used/total/max memory after data-dep prob analysis finished: " +
				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		freeMemory();
		System.out.println("Used/total/max memory after freeing up data-dep prob analysis memory: " +
				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());*/
		
		return depProbs;
	}
	
	/**
	 * This method is to calculate the coverage probability reaching from the entry of the program to each NodePoint in the slice
	 * */
	public static Map<NodePoint, Float> findEnToPointCovProb(BackwardDependenceGraph depGraph){
		// These two are not needed anymore because they are in pre-computation phase now.
//		findIntraDataDepProbs();
//		findInterDataDepProbs();
		
		// 1. Obtain entry cfg node
		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
		CFGNode nEntry = cfgMain.ENTRY;
		
		List<NodePoint> nodePointList = new ArrayList<NodePoint>();
		Map<NodePoint, Float> enToPntCovProbs = new HashMap<NodePoint, Float>();
		float probEnToPnt = 0f;
		
		List<Dependence> allDeps = (List<Dependence>) depGraph.getDeps();
		
		for( Dependence dep : allDeps){		
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			
			if(!nodePointList.contains(src)){
				nodePointList.add(src);
				if(src.toString().equals("-1[1]"))
					probEnToPnt = 1.0f;
				else
					probEnToPnt = ReachingProb.getInterprocCovProb(nEntry, true, src.getN());			
//				probEnToPnt = (float)Math.round(probEnToPnt * 10000) / 10000;		
				enToPntCovProbs.put(src, probEnToPnt);	
			}

			if(!nodePointList.contains(tgt)){
				nodePointList.add(tgt);
				if(tgt.toString().equals("-1[1]"))
					probEnToPnt = 1.0f;
				else
					probEnToPnt = ReachingProb.getInterprocCovProb(nEntry, true, tgt.getN());
//				probEnToPnt = (float)Math.round(probEnToPnt * 10000) / 10000;
				enToPntCovProbs.put(tgt, probEnToPnt);		
			}
		}
		
		return enToPntCovProbs;
	}
	
	public static List<NodePoint> findAllPntsInSlice(BackwardDependenceGraph depGraph){
//		findIntraDataDepProbs();
//		findInterDataDepProbs();
//		
//		// 1. Obtain entry cfg node
//		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
//		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
//		CFGNode nEntry = cfgMain.ENTRY;
		
		List<Dependence> candidateDeps = new ArrayList<Dependence>();
		List<NodePoint> nodePointList = new ArrayList<NodePoint>();
		// 2. Get all the NodePoints by first getting all the dependences in the slice and then storing the src and tgt of the dependence.
		// Output of this step: nodePoingList
		NodePoint startPnt = depGraph.getStart();
		Set<Dependence> startDeps = depGraph.getInDeps(startPnt);
		for (Dependence dep : startDeps) 
			candidateDeps.add(dep);
		
		for(int i=0; i<candidateDeps.size(); i++)    {   
			//1. 
			Dependence dep = candidateDeps.get(i);   
			//2. Get its src and tgt
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			//3. Add the src and tgt to nodePointList
			if(!nodePointList.contains(src))
				nodePointList.add(src);
			if(!nodePointList.contains(tgt))
				nodePointList.add(tgt);
			//4. add indeps of the dep
			for (Dependence inDep : depGraph.getInDeps(src)) {
				if(!candidateDeps.contains(inDep))	
					candidateDeps.add(inDep);
			}
		 }  
	
		return nodePointList;
	}
	
	public static List<NodePoint> findAllPntsInSlice(DependenceGraph depGraph){
		findIntraDataDepProbs();
		findInterDataDepProbs();
		
		// 1. Obtain entry cfg node
		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
		CFGNode nEntry = cfgMain.ENTRY;
		
		List<Dependence> candidateDeps = new ArrayList<Dependence>();
		List<NodePoint> nodePointList = new ArrayList<NodePoint>();
		// 2. Get all the NodePoints by first getting all the dependences in the slice and then storing the src and tgt of the dependence.
		// Output of this step: nodePoingList
		NodePoint startPnt = depGraph.getStart();
		Set<Dependence> startDeps = depGraph.getOutDeps(startPnt);
		for (Dependence dep : startDeps) 
			candidateDeps.add(dep);
		
		for(int i=0; i<candidateDeps.size(); i++)    {   
			//1. 
			Dependence dep = candidateDeps.get(i);   
			//2. Get its src and tgt
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			//3. Add the src and tgt to nodePointList
			if(!nodePointList.contains(src))
				nodePointList.add(src);
			if(!nodePointList.contains(tgt))
				nodePointList.add(tgt);
			//4. add outdeps of the dep
			for (Dependence outDep : depGraph.getOutDeps(tgt)) {
				if(!candidateDeps.contains(outDep))	
					candidateDeps.add(outDep);
			}
		 }  
	
		return nodePointList;
	}
	
	private static Map<CFG,ReachCFGDag> cfgToIntraDefUse = new HashMap<CFG, ReachCFGDag>();
	/** cfg->node->def->use->prob */
	private static void findIntraDataDepProbs() {
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			// do DFS of CFG to create dag and identify backedges
			ReachCFGDag dag = ReachCFGDefUseDag.buildCFGDefUseDag(cfg);
			dag.propagateProbs();
			
			cfgToIntraDefUse.put(cfg, dag);
			
			// DEBUG
			((ReachCFGDefUseDag) dag).printProbs();
		}
	}
	
	/** For each def and each use for that def, associates probability of coverage (given def is covered). */
	private static Map<Def, Map<Use, Float>> interDefUseProb = new HashMap<Def, Map<Use,Float>>();
	/** For each d, look for u intraprocedurally, then only forward through call sites, and then from d's containing method's EX (backward/forward in call graph).
	 *  The backward/forward way has two sub-cases: (1) u is located after a returning call site from EX or (2) u is forward-reachable from some CS after that returning CS.
	 *  These four ways are NOT exclusive -- d can reach u through all of them if there are intra, forward, and backward/forward paths. */
	private static void findInterDataDepProbs() {
		// prepare components of the probabilities for inter-procedural-DUA coverage
		ReachingProb.findReachingProbs();
		
		// match defs to uses and compute cov prob for each pair
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			if (ProbSliOptions.debugOut())
				System.out.println("NEW prob interproc def-use: " + cfg.getMethod());
			
			ReachableUsesDefs ruCFG = (ReachableUsesDefs) cfg;
			
			List<Def> allHeapDefs = new ArrayList<Def>(ruCFG.getFieldDefs());
			allHeapDefs.addAll(ruCFG.getArrayElemDefs());
			allHeapDefs.addAll(ruCFG.getLibObjDefs());
			
			for (Def def : allHeapDefs) {
				if (def.isInCatchBlock())
					continue;
				
				Map<Use, Float> useToProb = new HashMap<Use, Float>();
				interDefUseProb.put(def, useToProb);
				final boolean fromDefBottom = !def.getVar().isStrConstObj() || def.getN().getCallSite() == null || !def.getN().getCallSite().hasAppCallees();
				
				if (ProbSliOptions.debugOut())
					System.out.print(" Def " + def);
				for (Use use : DependenceFinder.getAllUsesForDef(def.getVar(), def.getN())) {
					final float probReachUse = ReachingProb.getInterprocCovProb(def.getN(), fromDefBottom, use.getSrcNode());
					final float probAliasDefUse = getAliasProb(def.getVar(), use.getVar());
//					assert probAliasDefUse > 0.0f;
					final float combinedProb = probAliasDefUse * probReachUse;
					if (ProbSliOptions.debugOut())
						System.out.print(" u " + use + ": " + combinedProb);
					
					if (combinedProb > 0.0f)
						
						useToProb.put(use, combinedProb);
				}
				if (ProbSliOptions.debugOut())
					System.out.println();
			}
		}
	}
	/** Computes probability that two objects with these p2 sets are the same at runtime. */
	private static float getAliasProb(Variable var1, Variable var2) {
		final BitSet p2Set1 = var1.getP2Set();
		final BitSet p2Set2 = var2.getP2Set();
		
		final int p2Card1 = p2Set1.cardinality();
		final int p2Card2 = p2Set2.cardinality();
		
		// handle cases where one or both p2 sets are empty
		if (p2Card1 == 0) {
			if (p2Card2 == 0) {
				Local lbase1 = var1.getBaseLocal();
				return (lbase1 != null && lbase1 == var2.getBaseLocal())? 1.0f : 0.0f;
			}
			return 1.0f + (p2Card2 - 1.0f)/p2Card2; // the greater card2, the greater the chance that var2 aliases var1
		}
		if (p2Card2 == 0)
			return 1.0f + (p2Card1 - 1.0f)/p2Card1; // the greater card1, the greater the chance that var1 aliases var2
		
		// use typical formula: size of p2 intersection / size of p2 union
		final BitSet bsInters = (BitSet) p2Set1.clone();
		bsInters.and(p2Set2);
		final BitSet bsUnion = (BitSet) p2Set1.clone();
		bsUnion.or(p2Set2);
		
		return ((float)bsInters.cardinality()) / bsUnion.cardinality();
	}
	
	/** Assigns to each data dependence in graph its coverage probability. */
	private static Map<Dependence, Float> assignDataDepProbs(DependenceGraph depGraph) {
		// assign to each data dependence in graph its coverage probability (given that def is executed)
		Map<Dependence, Float> dataDepProbs = new HashMap<Dependence, Float>();
		for (Dependence dep : depGraph.getDeps()) {
			if (dep instanceof DataDependence) {
				DataDependence dataDep = (DataDependence)dep;
				Variable v = dataDep.getVar();
				
				switch (dataDep.getType()) {
				case INTRA: {
					assert v.isLocal();
					
					NodeDefUses nDepDef = (NodeDefUses) dataDep.getSrc().getN();
					NodeDefUses nDepUse = (NodeDefUses) dataDep.getTgt().getN();
					ReachableUsesDefs ruCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDepDef);
					
					// find def id
					assert nDepDef.getLocalDefsIds().length == 1;
					final int dId = nDepDef.getLocalDefsIds()[0];
					
					// find use id
					List<Use> uses = ruCFG.getUses();
					int uId = 0;
					for (Use u : uses) {
						if (u.getSrcNode() == nDepUse && u.getVar().equals(v)) {  // local vars
							assert u.getVar().isLocal();
							break;
						}
						++uId;
					}
					assert uId < uses.size();
					
					// get intra def-use reachability prob from (bottom of) def node to use-id "object"
					final float prob = cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDepDef).probReachFromBottom.get(uId);
					dataDepProbs.put(dep, prob);
				} break;
				case INTER: {
					// interprocedural case
					CFGNode nDef = dataDep.getSrc().getN();
					CFGNode nUse = dataDep.getTgt().getN();
					ReachableUsesDefs rCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDef);
					
					// search for heap def
					Def defFound = null;
					List<Def> defList = v.isArrayRef()? rCFG.getArrayElemDefs() : v.isFieldRef()? rCFG.getFieldDefs() : v.isObject()? rCFG.getLibObjDefs() : null;
					for (Def def : defList) {
						if (def.getN() == nDef && def.getVar().mayEqualAndAlias(v)) {
							defFound = def;
							break;
						}
					}
					assert defFound != null;
					
					// search for heap use
					Map<Use,Float> useToProb = interDefUseProb.get(defFound); // get interprocedural du-coverage probability
					Float prob = null;
					for (Use u : useToProb.keySet()) {
						if (u.getSrcNode() == nUse) {
							prob = useToProb.get(u);
							break;
						}
					}
					if (prob != null)  // ignore deps with 0 prob (i.e., those for which a use wasn't added to def's list)
						dataDepProbs.put(dep, prob);
				} break;
				case FWD_LINK: {
					CallSite cs = dataDep.getSrc().getN().getAppCallSite();
					dataDepProbs.put(dep, 1.0f / cs.getAllCallees().size());
				} break;
				case BACK_LINK: {
					CFG cfgSrc = ProgramFlowGraph.inst().getContainingCFG(dep.getSrc().getN());
//					dataDepProbs.put(dep, 1.0f / cfgSrc.getCallerSites().size());
					NodePoint srcPnt = dep.getSrc();
					
					// obtain entry cfg node
					SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
					CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
					CFGNode nEntry = cfgMain.ENTRY;
					
					// instead, use this equation to calcualte prob for dep(retVar, callersite): P(En->CSi|VCSk) = P(CSi ^ VCSk) / P(VCSk) = P(CSi)/P(VCSk)
					// the prob of reaching the caller site from the Entry of the program. the devidend in the equation.
					float enToCallerSiteProb = ReachingProb.getInterprocCovProb(nEntry, true, srcPnt.getN());
					// there might be more than one caller site for this retVar. this is the prob that at least one of these callersite has been reached. the divisor in the euqation.
					float anyCallerSiteProb = 0f; 
					
					// To calculate the prob of at least one of the callersite has been reached
					//	1. Get all callersite
					List<CallSite> csList = cfgSrc.getCallerSites();
					List<Float> csProbList = new ArrayList<Float>();  // to store the probs for each cs in the same order as in csList
					//	2. Calculate enToPointProb for each callersite
					float max = 0f;
					float sum = 1.0f;
					for (CallSite cs : csList){
						Location csLoc = cs.getLoc();
						Stmt csStmt = csLoc.getStmt();
						CFGNode csCfgNode = ProgramFlowGraph.inst().getNode(csStmt);
						float enToCSProb = ReachingProb.getInterprocCovProb(nEntry, true, csCfgNode);
						
						csProbList.add(enToCSProb); // prob(En->CS)
						
						//	3. Use Fréchet inequalities: max(P(A1), P(A2), ..., P(An)) ≤ P(A1 ∨ A2 ∨ ... ∨ An) ≤ min(1, P(A1) + P(A2) + ... + P(An)),
						//		3.1 to calculate lower bound
						if(enToCSProb >= max){
							max = enToCSProb;
						}	
						//		3.2 to calculate upper bound
						sum += enToCSProb;
					}
					// 		3.2 to calculate upper bound
					if(sum > 1.0f){
						sum = 1.0f;
					}
					//		3.3 Calcualte midpoint value, and assign to anyCallerSiteProb
					anyCallerSiteProb = (max + sum) / 2;
					
					// Finally, calcualte the prob for this CS, using P(En->CSi|VCSk) = P(CSi)/P(VCSk)
					float depProb = enToCallerSiteProb / anyCallerSiteProb ;
					dataDepProbs.put(dep, depProb);
				} break;
				}
			}
		}
		return dataDepProbs;
	}
	
	/** Assigns to each data dependence in graph its coverage probability. */
	private static Map<Dependence, Float> assignDataDepProbs(BackwardDependenceGraph depGraph) {
		// assign to each data dependence in graph its coverage probability (given that def is executed)
		Map<Dependence, Float> dataDepProbs = new HashMap<Dependence, Float>();
		for (Dependence dep : depGraph.getDeps()) {
			if (dep instanceof DataDependence) {
				DataDependence dataDep = (DataDependence)dep;
				Variable v = dataDep.getVar();
				
				switch (dataDep.getType()) {
				case INTRA: {
					assert v.isLocal();
					
					NodeDefUses nDepDef = (NodeDefUses) dataDep.getSrc().getN();
					NodeDefUses nDepUse = (NodeDefUses) dataDep.getTgt().getN();
					ReachableUsesDefs ruCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDepDef);
					
					// find def id
					assert nDepDef.getLocalDefsIds().length == 1;
					final int dId = nDepDef.getLocalDefsIds()[0];
					
					// find use id
					List<Use> uses = ruCFG.getUses();
					int uId = 0;
					for (Use u : uses) {
						if (u.getSrcNode() == nDepUse && u.getVar().equals(v)) {  // local vars
							assert u.getVar().isLocal();
							break;
						}
						++uId;
					}
					assert uId < uses.size();
					
					// get intra def-use reachability prob from (bottom of) def node to use-id "object"
//					final float prob = cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDepDef).probReachFromBottom.get(uId);
					float prob = 0f;
					if(cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDepDef).probReachFromBottom.get(uId)!=null){
						prob = cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDepDef).probReachFromBottom.get(uId);
					}
					else prob = 0f;
//					prob = (float)Math.round(prob * 10000) / 10000;
					dataDepProbs.put(dep, prob);
				} break;
				case INTER: {
					// interprocedural case
					CFGNode nDef = dataDep.getSrc().getN();
					CFGNode nUse = dataDep.getTgt().getN();
					ReachableUsesDefs rCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDef);
					
					// search for heap def
					Def defFound = null;
					List<Def> defList = v.isArrayRef()? rCFG.getArrayElemDefs() : v.isFieldRef()? rCFG.getFieldDefs() : v.isObject()? rCFG.getLibObjDefs() : null;
					for (Def def : defList) {
						if (def.getN() == nDef && def.getVar().mayEqualAndAlias(v)) {
							defFound = def;
							break;
						}
					}
					assert defFound != null;
					
					// search for heap use
					Map<Use,Float> useToProb = interDefUseProb.get(defFound); // get interprocedural du-coverage probability
					Float prob = null;
					for (Use u : useToProb.keySet()) {
						if (u.getSrcNode() == nUse) {
							prob = useToProb.get(u);
							break;
						}
					}
					if (prob != null){  // ignore deps with 0 prob (i.e., those for which a use wasn't added to def's list)
//						prob = (float)Math.round(prob * 10000) / 10000;
						dataDepProbs.put(dep, prob);
					}
				} break;
				case FWD_LINK: {
					CallSite cs = dataDep.getSrc().getN().getAppCallSite();
					float prob =  1.0f / cs.getAllCallees().size();
//					prob = (float)Math.round(prob * 10000) / 10000;
					dataDepProbs.put(dep, prob);
				} break;
				case BACK_LINK: {			
					CFG cfgSrc = ProgramFlowGraph.inst().getContainingCFG(dep.getSrc().getN());
//					dataDepProbs.put(dep, 1.0f / cfgSrc.getCallerSites().size()); // old method
					NodePoint srcPnt = dep.getSrc();
					NodePoint tgtPnt = dep.getTgt();
					
					// obtain entry cfg node
					SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
					CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
					CFGNode nEntry = cfgMain.ENTRY;
					
					// Use this equation to calcualte prob for back_link dep(retVar, callersite): P(En->CSi|VCSk) = P(CSi ^ VCSk) / P(VCSk) = P(CSi)/P(VCSk)
					// the prob of reaching the caller site from the Entry of the program. i.e. the devidend in the equation.
					float enToCallerSiteProb = ReachingProb.getInterprocCovProb(nEntry, true, tgtPnt.getN());
					// there might be more than one caller sites for this retVar. this is the prob that at least one of these callersite has been reached. i.e. the divisor in the euqation.
					float anyCallerSiteProb = 0f; 
					
					// To calculate the prob of at least one of the callersite has been reached
					//	1. Get all callersite
					List<CallSite> csList = cfgSrc.getCallerSites();
					List<Float> csProbList = new ArrayList<Float>();  // to store the probs for each cs in the same order as in csList
					//	2. Calculate enToPointProb for each callersite
					float max = 0f;
					float sum = 0f;
					for (CallSite cs : csList){
						Location csLoc = cs.getLoc();
						Stmt csStmt = csLoc.getStmt();
						CFGNode csCfgNode = ProgramFlowGraph.inst().getNode(csStmt);
						float enToCSProb = ReachingProb.getInterprocCovProb(nEntry, true, csCfgNode);
						
						csProbList.add(enToCSProb); // prob(En->CS)
						
						//	3. To calculate the divisor: use Fréchet inequalities max(P(A1), P(A2), ..., P(An)) ≤ P(A1 ∨ A2 ∨ ... ∨ An) ≤ min(1, P(A1) + P(A2) + ... + P(An)),
						//		3.1 to calculate lower bound
						if(enToCSProb >= max){
							max = enToCSProb;
						}	
						//		3.2.1 to calculate upper bound
						sum += enToCSProb;
					}
					// 		3.2.2 to calculate upper bound
					if(sum > 1.0f){
						sum = 1.0f;
					}
					//		3.3 Calcualte midpoint value, and assign to anyCallerSiteProb
					anyCallerSiteProb = (max + sum) / 2;
					
					// Finally, calcualte the prob for this (ret,CS), using P(En->CSi|VCSk) = P(CSi)/P(VCSk)
					float depProb = 0f;
					if(anyCallerSiteProb!=0)
						depProb = enToCallerSiteProb / anyCallerSiteProb ;
					
//					depProb = (float)Math.round(depProb * 10000) / 10000;
					dataDepProbs.put(dep, depProb);
				} break;
				}
			}
		}
		return dataDepProbs;
	}
	
	/** To be used when dags and maps in this class are no longer needed. */
	private static void freeMemory() {
		ReachingProb.freeMemory();
		
		DataDepProb.cfgToIntraDefUse.clear();
		DataDepProb.cfgToIntraDefUse = null;
		
		DataDepProb.interDefUseProb.clear();
		DataDepProb.interDefUseProb = null;
		
		System.gc();
	}
	
}
