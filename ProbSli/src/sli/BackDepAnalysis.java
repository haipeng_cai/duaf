package sli;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import soot.Local;
import soot.RefType;
import soot.Value;
import soot.jimple.InstanceInvokeExpr;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Stmt;

import dua.global.dep.BackwardDependenceGraph;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceGraph;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.StdVariable;
import dua.method.CFGDefUses.Variable;
import dua.util.Pair;
import fault.StmtMapper;

/**
 * In general, this class is responsible for taking the backwardDepGraph as input, and doing some analysis.
 * 
 * Now, this class is responsible for accumulating probabilities and doing backward wBFS
 * */
public class BackDepAnalysis {
	// Calculate the conditional probability P(d->u->F|F) of each dependence du
	public static Map<Dependence, Float> findCondDataDepProb(Map<Dependence, Float> dataDepCovProbs, Map<NodePoint, Float> enToPointCovProbs){
		
		Map<Dependence, Float>condDataDepProbs = new HashMap<Dependence,Float>();
		
		Iterator iter = dataDepCovProbs.entrySet().iterator();
		while(iter.hasNext()){
			float condDataDepProb = 0f;

			Map.Entry entry = (Map.Entry) iter.next();
			Dependence dep = (Dependence) entry.getKey();
			float dataDepCovProb = (Float) entry.getValue();
			
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			
			float enToSrcProb = enToPointCovProbs.get(src);
			float enToTgtProb = enToPointCovProbs.get(tgt);
			
			// Debug
			if(enToSrcProb!=0 && dataDepCovProb!=0){
				assert enToTgtProb != 0;
			}
			
			if(enToTgtProb != 0){
				condDataDepProb = enToSrcProb * dataDepCovProb / enToTgtProb ; // else condDataDepProb=0
				if(ProbBackSliOptions.priosliRound())
					condDataDepProb = (float)Math.round(condDataDepProb * 1000) / 1000;	
				// Debug
				if(condDataDepProb > 1){
					System.out.println("dep " + dep 
									+ " || En->Src " + enToSrcProb 
									+ " || dep(Src,Tgt) " + dataDepCovProb
									+ " || En->Tgt " + enToTgtProb
									+ " || prob: " + condDataDepProb);
					
					condDataDepProb = 1.0f; // Temp solution!!!
				}
			}
			condDataDepProbs.put(dep, condDataDepProb);
		}
		return condDataDepProbs;
	}
	
	public static Map<Dependence, Float> findCondCtrlDepProb(Map<Dependence, Float> ctrlDepCovProbs, Map<NodePoint, Float> enToPointCovProbs){
		
		Map<Dependence, Float> condCtrlDepProbs = new HashMap<Dependence, Float>();
		
		Iterator iter = ctrlDepCovProbs.entrySet().iterator();
		while(iter.hasNext()){
			float condCtrlDepProb = 0f;

			Map.Entry entry = (Map.Entry) iter.next();
			Dependence dep = (Dependence) entry.getKey();
			float ctrlDepCovProb = (Float) entry.getValue();
			
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			
			float enToSrcProb = enToPointCovProbs.get(src);
			float enToTgtProb = enToPointCovProbs.get(tgt);
					
			if(enToTgtProb != 0){
				condCtrlDepProb = enToSrcProb * ctrlDepCovProb / enToTgtProb;
				if(ProbBackSliOptions.priosliRound())
					condCtrlDepProb = (float)Math.round(condCtrlDepProb * 1000) / 1000;	
				
				// Debug
				if(condCtrlDepProb > 1){
//					System.out.println("GREATER THAN 1 PROB!!!");
					
					System.out.println("dep " + dep 
							+ " || En->Src " + enToSrcProb 
							+ " || dep(Src,Tgt) " + ctrlDepCovProb
							+ " || En->Tgt " + enToTgtProb
							+ " || prob: " + condCtrlDepProb);
			
					condCtrlDepProb = 1.0f; //Temp Solution!!!
				}
			}
			condCtrlDepProbs.put(dep, condCtrlDepProb);
		}
		return condCtrlDepProbs;
	}
	
	// findPrioSliOrder  Version 1: using PriorityQueue (PQ)
	// The prio of each pnt is fixed once it is calculated FOR comparison; The prio is NOT updated when it is rediscovered;
	// -- The prio in the queue is fixed; the prio in the map is fixed.
	// DO take into account the prio of the pnts which are NOT visited.
	public static HashMap<NodePoint,Float> findPrioSliOrderPQSimple(BackwardDependenceGraph backDepGraph, Map<Dependence,Float> condDataDepProbs, Map<Dependence,Float> condCtrlDepProbs){
		// The output. A LinkedHashMap to store a ordered list of <Pnt, Prio> entries.
		HashMap<NodePoint, Float> rsltPntPrioMap = new LinkedHashMap<NodePoint, Float>();
		
		// Priority queue which contains all the candidate NodePoints that could be picked up next. The priority is its PrioSlice prob.
		Comparator<PntPrio<NodePoint, Float>> comparator = new PntPrioComparator();
		PriorityQueue<PntPrio<NodePoint,Float>> candidatePntPrio = new PriorityQueue<PntPrio<NodePoint,Float>>(11,comparator); // 11 is the default value
		
		// The data structure that might need to be updated EVERY STEP. 
		Set<NodePoint> visitedPnts = new HashSet<NodePoint>();
		Map<NodePoint, Float>pntCurrentPrioMap = new HashMap<NodePoint, Float>();
		
		Comparator depComparator = new Comparator<Dependence>(){
			@Override
			public int compare(Dependence dep1, Dependence dep2) {
				// TODO Auto-generated method stub
				NodePoint src1 = dep1.getSrc();
				NodePoint src2 = dep2.getSrc();

				int stmtId1 = StmtMapper.getGlobalStmtId(src1.getN().getStmt());
				int stmtId2 = StmtMapper.getGlobalStmtId(src2.getN().getStmt());
				
				if (stmtId1 !=stmtId2){
					return stmtId1 - stmtId2;
				}
				else { // (stmtId1 == stmtId2){
					String depStr1 = dep1.toString();
					String depStr2 = dep2.toString();
					
					// assert depStr1.compareTo(depStr2)!=0; // This assertion is not true!!! But when depStr1=depStr2
					
					// Debug
//					if(depStr1.equals(depStr2)){
//						System.out.println("dep1: " + dep1 + "; dep2: " + dep2);
//					}
					
					return depStr1.compareTo(depStr2);						

					
				}	

//				return stmtId1-stmtId2;  // return in a order with stmt id from low to high
			}
		};
		
		
		// 1. wBFS starts from startPnt.
		// 1.1 Find start point
		// 1.2 Update the containers for startPnt
		// 1.3 Update and store prio for the src of inDeps of startPnt 
		NodePoint startPnt = backDepGraph.getStart();
		pntCurrentPrioMap.put(startPnt, 1f);
		rsltPntPrioMap.put(startPnt, 1f);
		visitedPnts.add(startPnt);

		Set<Dependence> startDeps = backDepGraph.getInDeps(startPnt);
		List<Dependence> startDepsList = new ArrayList<Dependence>(startDeps);
		
		Collections.sort(startDepsList, depComparator);
		
		// Debug
//		for (Dependence dep : startDepsList){
//			System.out.println(dep + ";");
//		}
		
		// Not necessary to sort the startDeps here because the prios are directly retrieved from the maps but not possiblt depend on any other deps/pnts
		for (Dependence dep : startDepsList){
			NodePoint src = dep.getSrc();

			if(!visitedPnts.contains(src)){
				float prob;
				// Possibly be ctrl or Data Deps
				if(condDataDepProbs.containsKey(dep)){
					prob = condDataDepProbs.get(dep);
				}
				else if (condCtrlDepProbs.containsKey(dep)){
					prob = condCtrlDepProbs.get(dep);
				}
				else // This is necessary because 0-prob deps are not included in the maps from the previous computation.
					prob = 0f;

				candidatePntPrio.add(new PntPrio<NodePoint, Float>(src, (Float) prob));
				visitedPnts.add(src);
				if(ProbBackSliOptions.priosliRound())
					prob = (float)Math.round(prob * 1000) / 1000;	
				pntCurrentPrioMap.put(src, prob);
				
				// Debug
//				System.out.println("StartDeps: " + dep + "; Prio: " + prob);
			}
		}

		
		// 2. wBFS continues
		while (candidatePntPrio.size() != 0){
			// Step 2.1: find max-prio pnt among all candidates
			// Step 2.2: Remove the max-prio pnt from candidate set
			// Step 2.3: Add max-prio pnt into final queue; update pntWBfsPrioLinkedMap
			PntPrio<NodePoint, Float> maxPntPrioPair = candidatePntPrio.poll();
			rsltPntPrioMap.put(maxPntPrioPair.getPnt(), maxPntPrioPair.getPrio());
//			visitedPnts.add(maxPntPrioPair.getPnt()); // remove this line because this point should already been in this set when it was added to the PrioQ

			// Step 2.4: Update candidate set: including the new candidate pnt, and the prio of this new candidate pnt;
			Set<Dependence> inDepsSet = backDepGraph.getInDeps(maxPntPrioPair.getPnt());
			List<Dependence> inDepsList = new ArrayList<Dependence>(inDepsSet);
			// sort depsList, with src's stmtId from low to high
			// This sorting is NECESSARY!
			
			Collections.sort(inDepsList, depComparator);

			for (Dependence dep : inDepsList){ 
				NodePoint src = dep.getSrc(); // src is the new candidate pnt to be added to candidatePnts set, if it does not exist in candidatePnts yet
								
//				// debug
//				if(src.toString().equals("3810[1]")){
//					System.out.println("Break point!");
//				}
				
				// If the prioQueue does not contain this point, then add. other wise, do nothing				
				if (!visitedPnts.contains(src)){	
					// Calculate its prio, then add the object

					// Calculate the prio of src, by
					// 2.4.1 accumulating the prob to calculate  srcToEndViaSucci = Pcond(s,succi)*P(succi)  for each succi (abbv for: successor i)
					// 2.4.2 and approximating the prio of src using Fréchet inequalities: max(P(A1), P(A2), ..., P(An)) ≤ P(A1 disj A2 ... An) ≤ min(1, P(A1) + P(A2) + ... + P(An))

					float srcToEndViaTgtProb = 0f; //
					float max = 0f; // for 2.4.2
					float sum = 0f; // for 2.4.2

					Set<Dependence> outDepsSet = backDepGraph.getOutDeps(src);
					List<Dependence> outDepsList = new ArrayList<Dependence>(outDepsSet);
					Collections.sort(outDepsList, depComparator);


					for (Dependence outDep : outDepsList){  // get all successors. Not necessary to sort the outDeps here because the order doesn't matter.
						NodePoint tgt = outDep.getTgt();  // for each successor
						srcToEndViaTgtProb = 0f; // need to reset this var again because using current algorithm, it is possible that this value will not be assigned to any new value in this round so it will mistakenly keep the value from last round, which makes sum incorrect.
						
						// 2.4.1
						if(condDataDepProbs.containsKey(outDep)){
							if(visitedPnts.contains(tgt)) // means that the successor has been processed before and has a prio at the point
								srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condDataDepProbs.get(outDep);	
							// else, it means that the successor has not been enqueued so far. do nothing.
						}
						else if (condCtrlDepProbs.containsKey(outDep)){
							if(visitedPnts.contains(tgt)) 
								srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condCtrlDepProbs.get(outDep);
						}
						else{
							srcToEndViaTgtProb = 0f;
						}

						// 2.4.2
						//	2.4.2.1 lower bound
						if (srcToEndViaTgtProb >= max){
							max = srcToEndViaTgtProb;
						} 
						// 	2.4.2.2 upper bound
						sum += srcToEndViaTgtProb;
						if(sum >= 1){
							sum = 1;
						}
					}
					//2.4.2.3 mid-value, which is the prio of src
					float prob = (max + sum)/2;
					if(ProbBackSliOptions.priosliRound())
						prob = (float)Math.round(prob * 1000) / 1000;	
					pntCurrentPrioMap.put(src, prob);	


					// Add the object to PrioQueue, and other ds		
					assert !visitedPnts.contains(src);
					candidatePntPrio.add(new PntPrio<NodePoint, Float>(src, (Float) prob));
					visitedPnts.add(src);
					
					assert visitedPnts.size()==candidatePntPrio.size()+rsltPntPrioMap.size();
				}
			}
			// 3. Recursively updating: DISABLED FOR NOW.
		}

		return rsltPntPrioMap;
	}

	
	
	// findPrioSliOrder Version 2: using PriorityQueue (PQ), soft version to balance the trade off between the completeness of the prio and the cost.
	// The prio of each pnt is fixed once it is calculated FOR comparison; The prio IS updated when it is rediscovered;
	// -- The prio in the queue is fixed; the prio in the map is NOT fixed.
	// DO take into account the prio of the pnts which are NOT visited.
	public static HashMap<NodePoint,Float> findPrioSliOrderPQSoft(BackwardDependenceGraph backDepGraph, Map<Dependence,Float> condDataDepProbs, Map<Dependence,Float> condCtrlDepProbs){
		// The output. A LinkedHashMap to store a ordered list of <Pnt, Prio> entries.
		HashMap<NodePoint, Float> rsltPntPrioMap = new LinkedHashMap<NodePoint, Float>();
		
		// Priority queue which contains all the candidate NodePoints that could be picked up next. The priority is its PrioSlice prob.
		Comparator<PntPrio<NodePoint, Float>> comparator = new PntPrioComparator();
		PriorityQueue<PntPrio<NodePoint,Float>> candidatePntPrio = new PriorityQueue<PntPrio<NodePoint,Float>>(11,comparator); // 11 is the default value
		
		// The data structure that might need to be updated EVERY STEP. 
		Set<NodePoint> visitedPnts = new HashSet<NodePoint>(); // visited = in rslt
		Set<NodePoint> discoveredPnts = new HashSet<NodePoint>(); // discovered = in rslt + in queue
		Map<NodePoint, Float>pntCurrentPrioMap = new HashMap<NodePoint, Float>();
		
		Comparator depComparator = new Comparator<Dependence>(){
			@Override
			public int compare(Dependence dep1, Dependence dep2) {
				// TODO Auto-generated method stub
				NodePoint src1 = dep1.getSrc();
				NodePoint src2 = dep2.getSrc();

				int stmtId1 = StmtMapper.getGlobalStmtId(src1.getN().getStmt());
				int stmtId2 = StmtMapper.getGlobalStmtId(src2.getN().getStmt());
				
				if (stmtId1 !=stmtId2){
					return stmtId1 - stmtId2;
				}
				else { // (stmtId1 == stmtId2){
					String depStr1 = dep1.toString();
					String depStr2 = dep2.toString();
					
					// Debug
//					if(depStr1.equals(depStr2)){
//						System.out.println("dep1: " + dep1 + "; dep2: " + dep2);
//					}
					
					return depStr1.compareTo(depStr2);								
				}	
			}
		};
		
		
		// 1. wBFS starts from startPnt.
		// 1.1 Find start point
		// 1.2 Update the containers for startPnt
		// 1.3 Update and store prio for the src of inDeps of startPnt 
		NodePoint startPnt = backDepGraph.getStart();
		pntCurrentPrioMap.put(startPnt, 1f);
		rsltPntPrioMap.put(startPnt, 1f);
		visitedPnts.add(startPnt);
		discoveredPnts.add(startPnt);

		Set<Dependence> startDeps = backDepGraph.getInDeps(startPnt);
		List<Dependence> startDepsList = new ArrayList<Dependence>(startDeps);
		
		Collections.sort(startDepsList, depComparator);
		
		
		// Not necessary to sort the startDeps here because the prios are directly retrieved from the maps but not possiblt depend on any other deps/pnts
		for (Dependence dep : startDepsList){
			NodePoint src = dep.getSrc();
			discoveredPnts.add(src);

			if(!visitedPnts.contains(src)){
				float prob;
				// Possibly be ctrl or Data Deps
				if(condDataDepProbs.containsKey(dep)){
					prob = condDataDepProbs.get(dep);
				}
				else if (condCtrlDepProbs.containsKey(dep)){
					prob = condCtrlDepProbs.get(dep);
				}
				else // This is necessary because 0-prob deps are not included in the maps from the previous computation.
					prob = 0f;

				candidatePntPrio.add(new PntPrio<NodePoint, Float>(src, (Float) prob));
//				visitedPnts.add(src);
				if(ProbBackSliOptions.priosliRound())
					prob = (float)Math.round(prob * 1000) / 1000;	
				pntCurrentPrioMap.put(src, prob);
				
				// Debug
//				System.out.println("StartDeps: " + dep + "; Prio: " + prob);
			}
		}

		
		// 2. wBFS continues
		while (candidatePntPrio.size() != 0){
			// Debug
			System.out.println("Q size: " + candidatePntPrio.size());
			
			// Step 2.1: find max-prio pnt among all candidates
			// Step 2.2: Remove the max-prio pnt from candidate set
			// Step 2.3: Add max-prio pnt into final queue; update pntWBfsPrioLinkedMap
			PntPrio<NodePoint, Float> maxPntPrioPair = candidatePntPrio.poll();
			rsltPntPrioMap.put(maxPntPrioPair.getPnt(), maxPntPrioPair.getPrio());
			visitedPnts.add(maxPntPrioPair.getPnt());

			// Step 2.4: Update candidate set: including the new candidate pnt, and the prio of this new candidate pnt;
			Set<Dependence> inDepsSet = backDepGraph.getInDeps(maxPntPrioPair.getPnt());
			List<Dependence> inDepsList = new ArrayList<Dependence>(inDepsSet);
			// sort depsList, with src's stmtId from low to high
			// This sorting is NECESSARY!
			
			Collections.sort(inDepsList, depComparator);

			for (Dependence dep : inDepsList){ 
				NodePoint src = dep.getSrc(); // src is the new candidate pnt to be added to candidatePnts set, if it does not exist in candidatePnts yet

//				// debug
/*				if(src.toString().equals("154[1]")){
					System.out.println("Break point!");
				}
				
				if(src.toString().equals("153[1]")){
					System.out.println("Break point!");
				}*/
				
				//// Calculate or recalculate this src node's prio no matter what
				// Calculate the prio of src, by
				// 2.4.1 accumulating the prob to calculate  srcToEndViaSucci = Pcond(s,succi)*P(succi)  for each succi (abbv for: successor i)
				// 2.4.2 and approximating the prio of src using Fréchet inequalities: max(P(A1), P(A2), ..., P(An)) ≤ P(A1 disj A2 ... An) ≤ min(1, P(A1) + P(A2) + ... + P(An))

				float srcToEndViaTgtProb = 0f; //
				float max = 0f; // for 2.4.2
				float sum = 0f; // for 2.4.2

				Set<Dependence> outDepsSet = backDepGraph.getOutDeps(src);
				List<Dependence> outDepsList = new ArrayList<Dependence>(outDepsSet);
				Collections.sort(outDepsList, depComparator);


				for (Dependence outDep : outDepsList){  // get all successors. Not necessary to sort the outDeps here because the order doesn't matter.
					NodePoint tgt = outDep.getTgt();  // for each successor
					srcToEndViaTgtProb = 0f; // need to reset this var again because using current algorithm, it is possible that this value will not be assigned to any new value in this round so it will mistakenly keep the value from last round, which makes sum incorrect.
					
					// 2.4.1
					if(condDataDepProbs.containsKey(outDep)){
						if(visitedPnts.contains(tgt)) // means that the successor has been processed before and has a prio at the point
							srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condDataDepProbs.get(outDep);	
						// else, it means that the successor has not been enqueued so far. do nothing.
					}
					else if (condCtrlDepProbs.containsKey(outDep)){
						if(visitedPnts.contains(tgt)) 
							srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condCtrlDepProbs.get(outDep);
					}
					else{
						srcToEndViaTgtProb = 0f;
					}

					// 2.4.2
					//	2.4.2.1 lower bound
					if (srcToEndViaTgtProb >= max){
						max = srcToEndViaTgtProb;
					} 
					// 	2.4.2.2 upper bound
					sum += srcToEndViaTgtProb;
					if(sum >= 1){
						sum = 1;
					}
				}
				//2.4.2.3 mid-value, which is the prio of src
				float prob = (max + sum)/2;
				if(ProbBackSliOptions.priosliRound())
					prob = (float)Math.round(prob * 1000) / 1000;	
				pntCurrentPrioMap.put(src, prob);	
				
				
				if(!discoveredPnts.contains(src)){
					candidatePntPrio.add(new PntPrio<NodePoint, Float>(src, (Float) prob));
					discoveredPnts.add(src);
				}

			}
			// 3. Recursively updating: DISABLED FOR NOW.
		}

		return rsltPntPrioMap;
	}
	
	

	// findPrioSliOrder Version 3: using PriorityQueue (PQ)
	// The prio of each pnt is NOT fixed once -- it gets updated by being removed and re-inserted; The prio IS updated when it is rediscovered;
	// -- The prio in the queue is NOT fixed; the prio in the map is NOT fixed.
	// DO take into account the prio of the pnts which are NOT visited.
	public static HashMap<NodePoint,Float> findPrioSliOrderPQHard(BackwardDependenceGraph backDepGraph, Map<Dependence,Float> condDataDepProbs, Map<Dependence,Float> condCtrlDepProbs){
		// The output. A LinkedHashMap to store a ordered list of <Pnt, Prio> entries.
		HashMap<NodePoint, Float> rsltPntPrioMap = new LinkedHashMap<NodePoint, Float>();

		// TO DO...
		return rsltPntPrioMap;
	}

	
	// Accumulate the probs, and do wBFS on backwardDepGraph from the failure point (which is the "start point" in this case)
	// findPrioSliOrder not using PriorityQueue
	// The prio of each pnt is NOT fixed until the end; The prio IS updated when it is rediscovered;
	// -- the prio in the map is NOT fixed.
	// DO NOT take into account the prio of the pnts which are NOT visited.
	public static Map<NodePoint, Float> findPrioSliOrderOld(BackwardDependenceGraph backDepGraph, Map<Dependence,Float> condDataDepProbs, Map<Dependence,Float> condCtrlDepProbs) {
		
		// The data structure that needs to be updated EVERY TIME a new NodePoint is PICKED.
		Set<NodePoint> visitedPntSet = new HashSet<NodePoint>(); // Store all visited points, no probs.
		Map<NodePoint,Float> pntWBfsPrioLinkedMap = new LinkedHashMap<NodePoint,Float>(); // Store <NodePoint, wBfsProb> pairs in wBFS order. wBfsProb is the prob of the NodePoint at the time the NodePoint is selected to add to the final order
		
		// The following data structure will only be updated ONCE at the very end, when all the computation is done.
		Map<NodePoint,Float> pntFullPrioLinkedMap = new LinkedHashMap<NodePoint,Float>(); // Store <NodePoint, fullProb> pairs in wBFS order. 
		
		// The data structure that might need to be updated every STEP. 
		Map<NodePoint,Float> pntCurrentPrioMap = new LinkedHashMap<NodePoint,Float>(); // Store <NodePoint, updatedProb>, which is kept updating to store the current prio of all DISCOVERED pnt. At the very end, it will be the fullProb
		Set<NodePoint> candidatePnts = new HashSet<NodePoint>(); // Candidate NodePoint Set, which contains all the possible NodePoints to visit next
		Map<NodePoint, Set<Dependence>> pntToOutDeps = new HashMap<NodePoint, Set<Dependence>>(); // Track pntToOutDeps, helper structure for calculating the Fréchet inequalities
	
		// 1. wBFS starts from startPnt.
		// 1.1 Find start point
		NodePoint startPnt = backDepGraph.getStart();
		// 1.2 Update the containers for startPnt
		visitedPntSet.add(startPnt);
		pntCurrentPrioMap.put(startPnt, 1.0f);
		pntWBfsPrioLinkedMap.put(startPnt, 1.0f);
		pntToOutDeps.put(startPnt, new HashSet<Dependence>()); // startPnt has no outDeps
		
		Set<Dependence> startDeps = backDepGraph.getInDeps(startPnt);

		// Debug
		System.out.println("DBG: Tracking Point Selection and InDeps......");
		System.out.println("<startPnt>: "+ startPnt);
		System.out.println("<startDeps>: "+ startDeps);
		
		// 1.3 Update and store prio for the src of inDeps of startPnt 
		for (Dependence dep : startDeps){
			NodePoint src = dep.getSrc();
			candidatePnts.add(src);
			
			float prob;
			// Ctrl or Data Deps
			if(condDataDepProbs.containsKey(dep)){
				prob = condDataDepProbs.get(dep);
			}
			else if (condCtrlDepProbs.containsKey(dep)){
				prob = condCtrlDepProbs.get(dep);
			}
			else
				prob = 0f;
			pntCurrentPrioMap.put(src, prob);
			
			if (!pntToOutDeps.containsKey(src))
				pntToOutDeps.put(src, new HashSet<Dependence>()); //starts empty
			Set<Dependence> setDeps = pntToOutDeps.get(src);
			pntToOutDeps.get(src).add(dep);
		}

		
		// 2. wBFS continues on "candidatePnts" set.
		// Term: candidatePnts == "discovered" pnts; orderedPntQueue == "visited" pnts == final queue
		// Debug
		int round = 1;

		while (!candidatePnts.isEmpty()){
			// Debug
			long roundStart = System.currentTimeMillis();
			System.out.println("######New Round picking from candidatePnts start! Round: " + round);
			// Debug
			System.out.println(candidatePnts.size()+"<candidatePnts,prio>:");
			for(NodePoint pnt : candidatePnts){		
				System.out.println("<" + pnt + ", " + pntCurrentPrioMap.get(pnt) + ">");
			}
			
			// Step 2.1: find max-prio pnt among all candidates
//			long startFindMaxPrioTime = System.currentTimeMillis();
			NodePoint maxPrioPnt = findMaxPrioPnt(candidatePnts, pntCurrentPrioMap);
//			long stopFindMaxPrioTime = System.currentTimeMillis();
//			long findMaxPrioTime = stopFindMaxPrioTime - startFindMaxPrioTime;
//			System.out.println("#Total time used for FindMaxPrioPnt: " + findMaxPrioTime);
			// Step 2.2: Remove the max-prio pnt from candidate set
			candidatePnts.remove(maxPrioPnt);
			
			if(!visitedPntSet.contains(maxPrioPnt)){ // this point has not been picked/visited before 
				// Step 2.3: Add max-prio pnt into final queue; update pntWBfsPrioLinkedMap
				visitedPntSet.add(maxPrioPnt);
				pntWBfsPrioLinkedMap.put(maxPrioPnt, pntCurrentPrioMap.get(maxPrioPnt));
				
				// Debug
				System.out.println("----Picked Pnt <maxPrioPnt, maxPrio>: " + "<" + maxPrioPnt + ", " + pntCurrentPrioMap.get(maxPrioPnt) + ">");
				
				// Debug
//				if(maxPrioPnt.toString().equals("3718[1]")){
//					System.out.println("Break Point 3718[1]!");
//				}
				
				// Step 2.4: Update candidate set: including the new candidate pnt, and the prio of this new candidate pnt;
				// always pick the smallest stmt id from candidate set
				Set<Dependence> inDepsSet = backDepGraph.getInDeps(maxPrioPnt);
				List<Dependence> inDepsList = new ArrayList<Dependence>(inDepsSet);
				// sort depsList, with src's stmtId from low to high
				// This sorting is NECESSARY!
				Comparator depComparator = new Comparator<Dependence>(){
					@Override
					public int compare(Dependence dep1, Dependence dep2) {
						// TODO Auto-generated method stub
						NodePoint src1 = dep1.getSrc();
						NodePoint src2 = dep2.getSrc();
						
						int stmtId1 = StmtMapper.getGlobalStmtId(src1.getN().getStmt());
						int stmtId2 = StmtMapper.getGlobalStmtId(src2.getN().getStmt());
						
						return stmtId1-stmtId2;  // return in a order with stmt id from low to high
					}
				};
				Collections.sort(inDepsList, depComparator);
				
				for (Dependence dep : inDepsList){ 
					NodePoint src = dep.getSrc(); // srcPnt is the new candidate pnt to be added to candidatePnts set, if it does not exist in candidatePnts yet
					
//					// Debug
//					if(src.toString().equals("2485[1]")){
//						System.out.println("Break point!");
//					}
						
					
					if (!candidatePnts.contains(src)){						
						candidatePnts.add(src);
					}

					// Update pntToOutDeps, for later calculating disjunction approximation
					if (!pntToOutDeps.containsKey(src))
						pntToOutDeps.put(src, new HashSet<Dependence>()); //starts empty
					Set<Dependence> setDeps = pntToOutDeps.get(src);
					pntToOutDeps.get(src).add(dep);
					
					
					// Calculate (update) the prio of src, by
					// 2.4.1 accumulating the prob to calculate  srcToEndViaSucci = Pcond(s,succi)*P(succi)  for each succi (abbv for: successor i)
					// 2.4.2 and approximating the prio of src using Fréchet inequalities: max(P(A1), P(A2), ..., P(An)) ≤ P(A1 disj A2 ... An) ≤ min(1, P(A1) + P(A2) + ... + P(An))
					float srcToEndViaTgtProb = 0f;
					float max = 0f; // for 2.4.2
					float sum = 0f; // for 2.4.2
					// sort deps in the order with stmtId from low to high
					Set<Dependence> outDepsSet = pntToOutDeps.get(src);
					// The sorting below is not necessary 
//					List<Dependence> outDepsList = new ArrayList<Dependence>(outDepsSet);
//					Collections.sort(outDepsList, depComparator);
					
					for (Dependence outDep : outDepsSet){  // get all successors
						NodePoint tgt = outDep.getTgt();  // for each successor
						
						// 2.4.1
						if(condDataDepProbs.containsKey(outDep)){
							srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condDataDepProbs.get(outDep);	
						}
						else if (condCtrlDepProbs.containsKey(outDep)){
							srcToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condCtrlDepProbs.get(outDep);
						}
						else{
							srcToEndViaTgtProb = 0f;
						}

						// 2.4.2
						//	2.4.2.1 lower bound
						if (srcToEndViaTgtProb >= max){
							max = srcToEndViaTgtProb;
						} 
						// 	2.4.2.2 upper bound
						sum += srcToEndViaTgtProb;
						if(sum >= 1){
							sum = 1;
						}
					}
					//2.4.2.3 mid-value, which is the prio of src
					pntCurrentPrioMap.put(src, (max+sum)/2);
					
					
					long recStart = System.currentTimeMillis();
					// 3. Recursively updating: if 
					// 1) this src node has been VISITED, 
					// then recursively update the prio of all the points that have been either DISCOVERED or VISITED and had dependency relationship with this src node
					// and 2) if the maxPrioPnt has prio==0, which means all future candidate points have prio 0, then there's no need to recursively update all prios 
					// because the "newly discovered" 0-prio will not have effect on the prios of other visited or discovered points
					// ***note: any point or dep set won't change. only the prio will change, if necessary.
					if(!(ProbBackSliOptions.noRecUpdate()) && visitedPntSet.contains(src) && pntCurrentPrioMap.get(maxPrioPnt)!=0 ){ // if not, done
						Map<Dependence,Integer> depCountMap = new HashMap<Dependence,Integer>(); // Used to count the # of times a dep has been used to update the probs. in case of loops.
						
						// 3.1 Remove this point from candidatePnts because it has been VISITED so there is no need to pick it again
						candidatePnts.remove(src);
						
						// 3.2 Add all src of this point ("this pnt" = "src" so now we have a new notation for "src of src") into reCalcCandidatePnts set 
						// Initialization of reCalcCandidatePnts
						Set<NodePoint> reCalcCandidatePnts = new HashSet<NodePoint>();
						for (Dependence inDep : backDepGraph.getInDeps(src)){
							reCalcCandidatePnts.add(inDep.getSrc());
						}
		
						// 3.3 
						while (!reCalcCandidatePnts.isEmpty()){
							// Sort all points with stmtId from low to high
							// This sorting is NECESSARY!
							List<NodePoint> reCalcCandidatePntsList = new ArrayList<NodePoint>(reCalcCandidatePnts);
							Comparator pntComparator = new Comparator<NodePoint>(){
								@Override
								public int compare(NodePoint pnt1, NodePoint pnt2) {
									// TODO Auto-generated method stub
									int stmtId1 = StmtMapper.getGlobalStmtId(pnt1.getN().getStmt());
									int stmtId2 = StmtMapper.getGlobalStmtId(pnt2.getN().getStmt());
									
									return stmtId1-stmtId2; // return in a order with stmt id from low to high
								}
							};
							Collections.sort(reCalcCandidatePntsList, pntComparator);
							
							// 3.3.1 Pick the first element in the list
							NodePoint nextPnt = reCalcCandidatePntsList.get(0);
							
							// 3.3.2 if this pnt ("nextPnt") is in either DISCOVERED or VISITED set
							if(pntCurrentPrioMap.containsKey(nextPnt) || pntWBfsPrioLinkedMap.containsKey(nextPnt)){
								// 3.3.2.1 Update prio of this point
								float maxRec = 0f; // Rec means "Recursive", which means the recursively updating prio process
								float sumRec = 0f;

								// sort deps in the order with stmtId from low to high
								Set<Dependence> nextPntOutDepsSet = pntToOutDeps.get(nextPnt);
								//List<Dependence> nextPntOutDepsList = new ArrayList<Dependence>(nextPntOutDepsSet);
								//Collections.sort(nextPntOutDepsList, depComparator);

								for ( Dependence outDep : nextPntOutDepsSet){
									if(!depCountMap.containsKey(outDep)){ // This dep has not been visited. okay.
										depCountMap.put(outDep, 1);

										// update prio
										NodePoint tgt = outDep.getTgt();  // for each successor

										float pntToEndViaTgtProb = 0f;
										// Fréchet inequalities (again)
										if(condDataDepProbs.containsKey(outDep)){
											pntToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condDataDepProbs.get(outDep);	
										}
										else if (condCtrlDepProbs.containsKey(outDep)){
											//											assert condCtrlDepProbs.containsKey(outDep);
											pntToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condCtrlDepProbs.get(outDep);
										}
										else{
											pntToEndViaTgtProb = 0f;
										}

										//	Fréchet inequalities: lower bound
										if (pntToEndViaTgtProb >= maxRec){
											maxRec = pntToEndViaTgtProb;
										} 
										// 	Fréchet inequalities: upper bound
										sumRec += pntToEndViaTgtProb;

										if(sumRec >= 1){
											sumRec = 1;
										}
									}
									else {// This dep has been visited before in recalculating prios
										assert depCountMap.get(outDep) >= 1;
										if( depCountMap.get(outDep) < 1){  ////////////////////////////NOTE: This is the FIRST place to determine where to control the # of edge iteration (loop). There's a second one below that needs to be changed too!
											// increase dep count by 1
											int currentCount = depCountMap.get(outDep);
											currentCount += 1;
											depCountMap.put(outDep, currentCount);

											// update prio
											NodePoint tgt = outDep.getTgt();  // for each successor

											float pntToEndViaTgtProb = 0f;
											// Fréchet inequalities (again)
											if(condDataDepProbs.containsKey(outDep)){
												pntToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condDataDepProbs.get(outDep);	
											}
											else{
												assert condCtrlDepProbs.containsKey(outDep);
												pntToEndViaTgtProb = pntCurrentPrioMap.get(tgt) * condCtrlDepProbs.get(outDep);
											}
											// Fréchet inequalities: lower bound
											if (pntToEndViaTgtProb >= maxRec){
												maxRec = pntToEndViaTgtProb;
											} 
											// Fréchet inequalities: upper bound
											sumRec += pntToEndViaTgtProb;

											if(sumRec >= 1){
												sumRec = 1;
											}
										}
									}
								}
								// Finally update the prio value
								pntCurrentPrioMap.put(nextPnt, (maxRec + sumRec) / 2);
								// Debug
								System.out.println("------Recalculated prio of point: " + nextPnt);
								
								// prio update done, but need to continue checking -- if this point is in VISITED set
								if(pntWBfsPrioLinkedMap.containsKey(nextPnt)){
									for(Dependence inNextDep : backDepGraph.getInDeps(nextPnt)){
										if(!depCountMap.containsKey(inNextDep) || depCountMap.get(inNextDep)<1){ //!!! The second place to change if we want to set another loop limit
											reCalcCandidatePnts.add(inNextDep.getSrc());
											// Debug
											System.out.println("--------Adding more points to be recalculated: " + inNextDep.getSrc());
										}
									}
								}
							}
//							long recStop = System.currentTimeMillis();
//							long recTime = recStop - recStart;
//							System.out.println("###Total time used for recursive update: " + recTime);
							
							// 3.3.3 
							reCalcCandidatePnts.remove(nextPnt);
						}			
					}
				}
			}
			long roundEnd = System.currentTimeMillis();
			long roundTime = roundEnd - roundStart;
			System.out.println("######Round time used: " + roundTime);
			round++;
		}

		
		Iterator<Map.Entry<NodePoint, Float>> iter = pntWBfsPrioLinkedMap.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry<NodePoint,Float> entry = (Entry<NodePoint, Float>) iter.next();
			NodePoint pnt = entry.getKey();
			float prio = pntCurrentPrioMap.get(pnt);
			
			pntFullPrioLinkedMap.put(pnt, prio);
		}
		

//		return pntFullPrioLinkedMap;
		
		// Debug
		return pntWBfsPrioLinkedMap;
	}
	

	// The function below is the OLD one, which does NOT give a deterministic order
//	// find max-prio pnt among all candidates. Just simply picking the max one. If tie, randomly pick one. Do not need to compare depth or any other values.
//	private static NodePoint findMaxPrioPnt(Set<NodePoint> candidatePnts, Map<NodePoint,Float> pntCurrentPrioMap){
//		float max = -1.0f;
//		float prio = -1.0f;
//		NodePoint maxPrioPnt = null;
//		
//		for (NodePoint pnt : candidatePnts){
//			prio = pntCurrentPrioMap.get(pnt);
//			
//			if(prio > max){
//				max = prio;
//				maxPrioPnt = pnt;
//			}
//		}
//		return maxPrioPnt;
//	}
	
	
	// find max-prio pnt among all candidates. Just simply picking the max one. If tie, randomly pick one. Do not need to compare depth or any other values.
	private static NodePoint findMaxPrioPnt(Set<NodePoint> candidatePnts, Map<NodePoint,Float> pntCurrentPrioMap){
		float max = -1.0f;
		float prio = -1.0f;
		int stmtId = -1;
		NodePoint maxPrioPnt = null;
		
		for (NodePoint pnt : candidatePnts){
			prio = pntCurrentPrioMap.get(pnt);
			
			if (prio > max){
				stmtId = StmtMapper.getGlobalStmtId(pnt.getN().getStmt());
				max = prio;
				maxPrioPnt = pnt;
			}
			else if (prio == max){ // if tie, pick the one which has smaller stmtId
				int currentStmtId = StmtMapper.getGlobalStmtId(pnt.getN().getStmt());
				if(currentStmtId < stmtId){
					stmtId = currentStmtId; // stmtId should be replaced by a smaller number
					max = prio; // should be the same though
					maxPrioPnt = pnt;
				}
			}
		}
		return maxPrioPnt;
	}
	
	// find max-prio dependence among all candidates. When calculating the max-prio, need to accumulate the prob first.
	private static Dependence findMaxPrioDep(Set<Dependence> candidateDeps, Map<Dependence,Float> dataDepProbs, Map<Dependence,Float> ctrlDepProbs, Map<Dependence, Integer>depDepths){
		float max = 0f;
		float prio = -1.0f; // to avoid the case that max == prio happens for the very first dependence
		Dependence maxPrioDep = null;
		
		for (Dependence dep : candidateDeps){
			// There is a case that the dep is not in the map, and it happens when its prio=0. 
			if (dataDepProbs.containsKey(dep))
				prio = dataDepProbs.get(dep); 
			else if (ctrlDepProbs.containsKey(dep))
				prio = ctrlDepProbs.get(dep);
			else prio = 0;

			if (prio > max){
				max = prio;
				maxPrioDep = dep;
			}
			else if (prio == max){
				int currentDepDepth = depDepths.get(dep);
				if (maxPrioDep!=null){
					int oldDepDepth = depDepths.get(maxPrioDep);
					if (currentDepDepth < oldDepDepth){ // new dep is on a higher level than old existing dep, so consider the current new dep first and replace the old dep
						maxPrioDep = dep;
					}
				}
				else maxPrioDep = dep;	
			}
		}
		return maxPrioDep;
	}
	
	// assign score (depth) to each dependence using pure BFS strategy, i.e w-slice
	public static Map<NodePoint, Float> findWeiserOrder (BackwardDependenceGraph backDepGraph){
		Map<NodePoint, Float> pntDepths = new LinkedHashMap<NodePoint, Float>(); // <NodePoint, depth>	
		Set<NodePoint> visitedPnts = new HashSet<NodePoint>();
		
		float startDepth = 1f;
		float parentDepth = 1f;
		float childDepth = 0f;

		// Do BFS on depGraph to find depth of each dep
		Queue<NodePoint> queue = new LinkedList<NodePoint>(); // Store NodePoint in BFS order
		// Find start point
		NodePoint startPnt = backDepGraph.getStart();
		queue.add(startPnt);
		pntDepths.put(startPnt, startDepth);
		visitedPnts.add(startPnt);
		
		while(!queue.isEmpty()){
			NodePoint pnt = (NodePoint) queue.remove();
			
			parentDepth = pntDepths.get(pnt);
			
			for(Dependence inDep : backDepGraph.getInDeps(pnt)){	
				
				NodePoint src = inDep.getSrc();
				if(!visitedPnts.contains(src)){
					if(src.toString().equals("2485[1]"))	
						System.out.println("Break Point!");
					
					queue.add(src);
					visitedPnts.add(src);
					childDepth = parentDepth + 1;
					pntDepths.put(src, childDepth);
				}
			}
		}		
		return pntDepths;
	}
	
	
	// do bfs on graph and only consider "producer" statements (i.e no base var dd, and no cd)
	public static Map<NodePoint, Float> findThinOrder (BackwardDependenceGraph backDepGraph){
		Map<NodePoint, Float> pntDepths = new HashMap<NodePoint, Float>(); //full map, which contains all points (including base dd and cd src and tgt)
		Map<NodePoint, Float> rsltPntDepths = new HashMap<NodePoint, Float>();
		Map<NodePoint, Float> sortedRsltMap = new LinkedHashMap<NodePoint, Float>();
		Set<NodePoint> storedPnts = new HashSet<NodePoint>();
		Set<Dependence> visitedDeps = new HashSet<Dependence>();

		float startDepth = 1f;
		float tgtDepth = 1f;
		float srcDepth = 0f;

		// Do BFS on depGraph to find depth of each dep
		Queue<NodePoint> queue = new LinkedList<NodePoint>(); // Store NodePoint in BFS order
		// Find start point
		NodePoint startPnt = backDepGraph.getStart();
		queue.add(startPnt);
		pntDepths.put(startPnt, startDepth); 
		rsltPntDepths.put(startPnt, startDepth);
		storedPnts.add(startPnt);

		while(!queue.isEmpty()){
			NodePoint tgt = (NodePoint) queue.remove();
			tgtDepth = pntDepths.get(tgt);
			

			for(Dependence inDep : backDepGraph.getInDeps(tgt)){
				NodePoint src = inDep.getSrc();
				// debug
				if(src.toString().equals("2502[1]"))
					System.out.println("Break Point!!");

				if(!storedPnts.contains(src) && !visitedDeps.contains(inDep)){
					// all points will be added to the queue anyways but only point that Thin Slicing considers will be added to pntDepthMap, 
					// because the traversal doesn't stop even all the indeps are ctrl deps. They just don't include them in the slice.
					queue.add(src); 
					
					// to make sure no infinite loop because of not stored pnts but same dep
					visitedDeps.add(inDep);

					// filter dependences
					// if cd, we record its depth as its srcDepth, because this tgt's indep's src will be treated as if the src is on the same level of this tgt
					// *this depth is meaningless to cd's tgt, though
					if(inDep instanceof ControlDependence){
						srcDepth = tgtDepth;
						pntDepths.put(src, srcDepth);
						continue;
					}
					
					else{//(inDep instanceof DataDependence){
						// check whether it is a base var
						Variable v = ((DataDependence) inDep).getVar();
						Value depVar = v.getValue();

						Stmt s = tgt.getN().getStmt();
						if(s.containsInvokeExpr()){
							if(!(s instanceof InvokeStmt)){ // Thin Slicing does not consider InvokeStmt
								InvokeExpr invExpr = s.getInvokeExpr();
								if (invExpr instanceof InstanceInvokeExpr) {
									Local lBaseVar = (Local)((InstanceInvokeExpr)invExpr).getBase();
									if(lBaseVar.equals(depVar)){
										// update the depth like this for the same reason for cd
										srcDepth = tgtDepth;
										pntDepths.put(src, srcDepth);
										continue; // all conditions above satisfied means this is a base var
									}
									// Debug
//									System.out.println("Statement:  " + s);
//									System.out.println("InvokeExpr:  " +
//											"inDep: " + inDep +
//											"  depVar: " + depVar +
//											"  lBase: " + lBaseVar);
								}
							}
						}
						// if it IS a producer stmt, we then increase its depth and store it.
						srcDepth = tgtDepth + 1;
						pntDepths.put(src, srcDepth); 
						// only store producers
						rsltPntDepths.put(src, srcDepth);
						// Visited means it has been stored 
						storedPnts.add(src);
					}
				}
			}
		}		
		
		// sort by depth, from low to high
		List<Entry<NodePoint,Float>> entryList = new ArrayList<Entry<NodePoint,Float>>();
		Iterator<Entry<NodePoint,Float>> iter = rsltPntDepths.entrySet().iterator();
		while(iter.hasNext()){
			entryList.add((Entry<NodePoint, Float>) iter.next());
		}
		
		Comparator<Entry<NodePoint,Float>> pntFloatEntryComparator = new Comparator<Entry<NodePoint,Float>>(){
			@Override
			public int compare(Entry<NodePoint, Float> entry1,
					Entry<NodePoint, Float> entry2) {
				float f1 = entry1.getValue();
				float f2 = entry2.getValue();
				
				return (int) (f1-f2);
			}
		};
		Collections.sort(entryList, pntFloatEntryComparator);
		
		// put Entries into linkedhashmap
		for(Entry<NodePoint, Float> entry : entryList){
			sortedRsltMap.put(entry.getKey(), entry.getValue());
			// debug
			System.out.println(entry.getValue());
		}
		return sortedRsltMap;
	}

	/**
	 * This function is used in Thin and Weiser. 
	 *  */
	public static Map<NodePoint, Float> findBFSOrder (BackwardDependenceGraph backDepGraph, Map<Dependence, Float> ctrlDepProbs, Map<Dependence, Float> dataDepProbs){
		Map<NodePoint, Float> rsltMap = new LinkedHashMap<NodePoint, Float>();
		Set<Dependence> visitedDeps = new HashSet<Dependence>();

		// Do BFS on depGraph
		Queue<Dependence> depQueue = new LinkedList<Dependence>(); // Store dependence in BFS order
		List<NodePoint> pntList = new LinkedList<NodePoint>(); // Store NodePoints in order
		// Find start point
		NodePoint startPnt = backDepGraph.getStart();
		pntList.add(startPnt);
		Set<Dependence> startDeps = backDepGraph.getInDeps(startPnt);
		for( Dependence dep : startDeps){
			depQueue.add(dep);
			if(!pntList.contains(dep.getSrc())){
				pntList.add(dep.getSrc());
				
				// get the prob value of this dep, store (pnt, prob) as a pair in the rslt map
				if(ctrlDepProbs.containsKey(dep)){
					if(ctrlDepProbs.get(dep)!=0){ // This check is for ThinSlice, because ThinSlice has ctrl deps with 0 probs and they are not included in the slice
						rsltMap.put(dep.getSrc(), ctrlDepProbs.get(dep));
					}
				}
				else if(dataDepProbs.containsKey(dep)){
					rsltMap.put(dep.getSrc(), dataDepProbs.get(dep));
				}
			}
			visitedDeps.add(dep);
		}
		while(!depQueue.isEmpty()){
			Dependence dep = (Dependence)depQueue.remove();
			
			for(Dependence inDep : backDepGraph.getInDeps(dep.getSrc())){
				if( inDep != null && !(visitedDeps.contains(inDep))){
					 depQueue.add(inDep);
					 visitedDeps.add(inDep);
					 if(!pntList.contains(inDep.getSrc())){
						 pntList.add(inDep.getSrc());	 
						 
						// get the prob value of this dep, store (pnt, prob) as a pair in the rslt map				
						if(ctrlDepProbs.containsKey(inDep)){
							if(ctrlDepProbs.get(inDep)!=0){ // This check is for ThinSlice, because ThinSlice has ctrl deps with 0 probs and they are not included in the slice
								rsltMap.put(inDep.getSrc(), ctrlDepProbs.get(inDep));
							}
						}
						else if(dataDepProbs.containsKey(inDep)){
							rsltMap.put(inDep.getSrc(), dataDepProbs.get(inDep));
						}
					 }
				}
				else
					break;
			}
		}			
		return rsltMap;
	}
	
	public static Map<NodePoint, Float> findBFSOrder (BackwardDependenceGraph backDepGraph, Map<Dependence, Float> dataDepProbs){
		Map<NodePoint, Float> rsltMap = new LinkedHashMap<NodePoint, Float>();
		Set<Dependence> visitedDeps = new HashSet<Dependence>();

		// Do BFS on depGraph
		Queue<Dependence> depQueue = new LinkedList<Dependence>(); // Store dependence in BFS order
		List<NodePoint> pntList = new LinkedList<NodePoint>(); // Store NodePoints in order
		// Find start point
		NodePoint startPnt = backDepGraph.getStart();
		pntList.add(startPnt);
		Set<Dependence> startDeps = backDepGraph.getInDeps(startPnt);
		for( Dependence dep : startDeps){
			depQueue.add(dep);
			if(!pntList.contains(dep.getSrc())){
				pntList.add(dep.getSrc());
				
				// get the prob value of this dep, store (pnt, prob) as a pair in the rslt map
				if(dataDepProbs.containsKey(dep)){
					rsltMap.put(dep.getSrc(), dataDepProbs.get(dep));
				}
			}
			visitedDeps.add(dep);
		}
		while(!depQueue.isEmpty()){
			Dependence dep = (Dependence)depQueue.remove();
			
			for(Dependence inDep : backDepGraph.getInDeps(dep.getSrc())){
				if( inDep != null && !(visitedDeps.contains(inDep))){
					 depQueue.add(inDep);
					 visitedDeps.add(inDep);
					 if(!pntList.contains(inDep.getSrc())){
						 pntList.add(inDep.getSrc());	 
						 
						// get the prob value of this dep, store (pnt, prob) as a pair in the rslt map				
						if(dataDepProbs.containsKey(inDep)){
							rsltMap.put(inDep.getSrc(), dataDepProbs.get(inDep));
						}
					 }
				}
				else
					break;
			}
		}			
		return rsltMap;
	}
	
	// Do not include self loop
	public static Set<List<NodePoint>> findPath(BackwardDependenceGraph depGraph, NodePoint startPnt, NodePoint endPnt){
		Set<List<NodePoint>> pathSet = new HashSet<List<NodePoint>>();
		Set<NodePoint> visitedSet = new HashSet<NodePoint>();
		
		// Preperation step
		// *** WARNING: This sorting can make things quite INEFFICIENT
		List<Dependence> deps = new ArrayList<Dependence>( depGraph.getDeps() );
		Collections.sort(deps, new Dependence.DepComp((Map<Dependence, Integer>) deps) );
		List<NodePoint> srcs = new ArrayList<NodePoint>();
		List<NodePoint> tgts = new ArrayList<NodePoint>();
		
		for(Dependence dep : deps){
			srcs.add(dep.getSrc());
			tgts.add(dep.getTgt());
		}
		
		if(srcs.contains(startPnt) && tgts.contains(endPnt)){
			// do DFS, and find the path 
			Stack<NodePoint> stack = new Stack<NodePoint>();
			stack.push(startPnt);
			
			while(!stack.isEmpty()){
				NodePoint firstPnt = (NodePoint) stack.peek();
				// get the first unvisited child of firstPnt
				NodePoint child = getUnvisitedFirstChild(deps, firstPnt, visitedSet);	
				// if child!=null
				if( child != null){
					// check whether the child is endPnt
					// if yes
					if(endPnt.equals(child)){
						// store stack into list
						List<NodePoint> path = storeStackIntoList(stack);
						// add list to pathSet
						pathSet.add(path);
					}
					else{// if no
						visitedSet.add(child);// child.visited = true;
						stack.push(child);// push
					}
				}
				else{
					stack.pop();
				}	

			}
		}
		else if (!srcs.contains(startPnt) && tgts.contains(endPnt)){
			System.out.println("Error: Start point does not exist in this slice!");
		}
		else if(srcs.contains(startPnt) && !tgts.contains(endPnt)){
			System.out.println("Error: End point does not exist in this slice!");
		}
		else{
			System.out.println("Error: Start point and end point do not exist in this slice!");
		}
		
		return pathSet;
	}
	
	// Do not include self loop
	public static Set<List<NodePoint>> findPath(DependenceGraph depGraph, NodePoint startPnt, NodePoint endPnt){
		Set<List<NodePoint>> pathSet = new HashSet<List<NodePoint>>();
		Set<NodePoint> visitedSet = new HashSet<NodePoint>();

		// Preperation step
		// *** WARNING: This sorting can make things quite INEFFICIENT
		List<Dependence> deps = new ArrayList<Dependence>( depGraph.getDeps() );
		Collections.sort(deps, new Dependence.DepComp((Map<Dependence, Integer>) deps) );
		List<NodePoint> srcs = new ArrayList<NodePoint>();
		List<NodePoint> tgts = new ArrayList<NodePoint>();

		for(Dependence dep : deps){
			srcs.add(dep.getSrc());
			tgts.add(dep.getTgt());
		}

		if(srcs.contains(startPnt) && tgts.contains(endPnt)){
			// do DFS, and find the path 
			Stack<NodePoint> stack = new Stack<NodePoint>();
			stack.push(startPnt);

			while(!stack.isEmpty()){
				NodePoint firstPnt = (NodePoint) stack.peek();
				// get the first unvisited child of firstPnt
				NodePoint child = getUnvisitedFirstChild(deps, firstPnt, visitedSet);	
				// if child!=null
				if( child != null){
					// check whether the child is endPnt
					// if yes
					if(endPnt.equals(child)){
						// store stack into list
						List<NodePoint> path = storeStackIntoList(stack);
						// add list to pathSet
						pathSet.add(path);
					}
					else{// if no
						visitedSet.add(child);// child.visited = true;
						stack.push(child);// push
					}
				}
				else{
					stack.pop();
				}	

			}
		}
		else if (!srcs.contains(startPnt) && tgts.contains(endPnt)){
			System.out.println("Error: Start point does not exist in this slice!");
		}
		else if(srcs.contains(startPnt) && !tgts.contains(endPnt)){
			System.out.println("Error: End point does not exist in this slice!");
		}
		else{
			System.out.println("Error: Start point and end point do not exist in this slice!");
		}

		return pathSet;
	}
	
	private static NodePoint getUnvisitedFirstChild(List<Dependence> deps, NodePoint parent, Set<NodePoint> visitedSet){
		NodePoint child = null;
		
		for (Dependence dep : deps){
			if (parent.equals(dep.getSrc())){
				if(!visitedSet.contains(dep.getTgt())){
					child = dep.getTgt();
				}
			}
		}
		
		return child;
	}
	
	private static List<NodePoint> storeStackIntoList(Stack<NodePoint> s){
		List<NodePoint> rsltList = new ArrayList<NodePoint>();
		
		while(!s.isEmpty()){
			rsltList.add(s.pop());
		}
		
		Collections.reverse(rsltList);
		
		return rsltList;
	}
}



















