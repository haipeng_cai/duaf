package sli;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sli.DataDepProb.ReachDag;
import sli.DataDepProb.ReachDagFactory;
import sli.DataDepProb.ReachEdge;
import sli.DataDepProb.ReachNode;
import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG.CFGNode;
import dua.util.Pair;

public class PDGProb {
	/******** DAG NODES *********/
	/** Node of search dag for CFG (def-use, intraproc reach). */
	public static class ReachDepNode extends ReachNode {
		/** The object that this DAG node represents. */
		protected final NodePoint pnt;
		/** Unique id (and index in arrays) of this node. */
		protected final int idx;
		protected static int nextIdxToAssign = 0;
		/// *** METHODS *** ///
		protected ReachDepNode(NodePoint pnt, ReachNode parent) { super(parent); this.pnt = pnt; this.idx = nextIdxToAssign++; }
		protected Object getObjNode() { return pnt; }
	}
	/******** FACTORIES *********/
	/** Factory for dependence DAG and nodes. */
	public static class ReachDepDagFactory implements ReachDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachDepDag((DependenceGraph)((Pair)oInfo).first(), this, (Map)((Pair)oInfo).second()); }
		@Override public ReachNode newNode(Object oNode, ReachNode parent) { return new ReachDepNode((NodePoint)oNode, parent); }
		public static ReachDepDagFactory inst = new ReachDepDagFactory();
		private ReachDepDagFactory() {}
	}
	/******** DAGs *********/
	/** DAG for a specific dependence graph. Nodes are dependencies (root is null). Reachable objects are NodePoints. */
	public static class ReachDepDag extends ReachDag {
		private final DependenceGraph depGraph;
		private final Map<Dependence, Float> depProbs;
		/** Number of nodes. Length of each row in probs 2D array. */
		protected static int numNodes;
		/** Map nodeIdx -> node */
		protected static ReachDepNode[] idxToNode;
		/** Replacement for probs maps of all nodes. */
		protected static float[] probsPntToPnt;
		
		protected ReachDepDag(DependenceGraph depGraph, ReachDagFactory fact, Map<Dependence, Float> depProbs) {
			super(fact.newNode(depGraph.getStart(), null), fact);
			this.depGraph = depGraph;
			this.depProbs = depProbs;
		}
		public static ReachDag buildDepDag(DependenceGraph depGraph, Map<Dependence,Float> depProbs) {
			ReachDepDag dag = (ReachDepDag) buildDag(new Pair(depGraph, depProbs), ReachDepDagFactory.inst);
			
			// initialize map idx->node and probs 2D array
			numNodes = dag.objToDAGNode.size();
			idxToNode = new ReachDepNode[numNodes];
			for (ReachNode node : dag.objToDAGNode.values())
				idxToNode[((ReachDepNode)node).idx] = (ReachDepNode) node;
			probsPntToPnt = new float[numNodes * numNodes];
			
			return dag;
		}
		
		@Override protected List getSuccessors(Object oNodePnt) {
			// get list of non-zero-prob dependencies coming out of given dependence node (which is either null or a dep object)
			final NodePoint pntSrc = (NodePoint) oNodePnt; //(oNodePnt == null)? depGraph.getStart() : ((Dependence)oNodePnt).getTgt();
			List<NodePoint> succPointsSorted = new ArrayList<NodePoint>();
			
			for (Dependence outDep : depGraph.getOutDeps(pntSrc))
				if (depProbs.containsKey(outDep))  // only add out dep's tgt (succ) if dep's prob exists (i.e., it's > 0)
					succPointsSorted.add(outDep.getTgt());
			Collections.sort(succPointsSorted, NodePointComparator.inst);
			return succPointsSorted;
		}
		
		/** To reduce the cost, returns only the src and tgt of the backedge. */
		@Override protected Set<ReachNode> getLoopNodes(ReachEdge be) {
			if (ProbSliOptions.simpleLoops()) {
				Set<ReachNode> loopNodes = new HashSet<ReachNode>();
				loopNodes.add(be.tgt);
				loopNodes.add(be.src); // needed to avoid loop-exit edges from src to itself 
				loopNodes.removeAll(be.tgt.ancestors);
				return loopNodes;
			}
			return super.getLoopNodes(be);
		}
		
		@Override protected void propagationIterationPreStep(int iterNum) {
			System.out.println("Starting dep-prob propagation iteration " + (iterNum+1));
		}
		
		/** Specific propagation algorithm for reachable points from child. */
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs) {
			final int numChildren = children.size();
			
			// immediately reachable point: child pnt
			final NodePoint pntParent = (NodePoint)node.getObjNode();
			final NodePoint pntChild = (NodePoint)child.getObjNode();
			final int childGlobalIdx = ((ReachDepNode)child).idx;
			
			// merge probs of all deps from parent to child
			List<Float> probsParentChild = new ArrayList<Float>();
			for (Dependence dep : depGraph.getOutDeps(pntParent)) {
				if (!dep.getTgt().equals(pntChild))
					continue; // only consider edges (deps) to child
				final Float probDep = depProbs.get(dep); // dep probs already include the cov prob of dep  //1.0f / numChildren;
				if (probDep == null)
					continue;
				probsParentChild.add(probDep);
			}
			// handle special case of no dep: parent->child is a loop exit edge, so parent is a backedge src
			if (probsParentChild.isEmpty()) {
				for (ReachNode beTgt : node.backChildren) {  // be's tgt is the loop head
					final NodePoint pntBETgt = (NodePoint) beTgt.getObjNode();
					for (Dependence depFromBETgt : depGraph.getOutDeps(pntBETgt)) {
						if (!depFromBETgt.getTgt().equals(pntChild))
							continue;
						final Float probDep = depProbs.get(depFromBETgt); // dep probs already include the cov prob of dep
						if (probDep == null)
							continue;
						probsParentChild.add(probDep);
					}
				}
			}
			final float probCovAndPropChild = disjunctiveProbMerge(probsParentChild);
			
			if (probCovAndPropChild == 0.0f)
				return; // no point in transferring probs from child
			
			float[] probsToTgt = getCreateFloatsArrayFor(accumProbs, childGlobalIdx, numChildren);
			probsToTgt[childIdx] = probCovAndPropChild;
			
			// propagate probs for other target points reachable from child
			final int firstArrIdx = childGlobalIdx * numNodes;
			final int lastArrIdx = firstArrIdx + numNodes;
			int reachableGlobalIdx = -1;
			for (int idxArrChildToReachable = firstArrIdx; idxArrChildToReachable < lastArrIdx; ++idxArrChildToReachable) {
				++reachableGlobalIdx;
				if (reachableGlobalIdx == childGlobalIdx)
					continue; // child is already reached here and its prob was set in the first step of this method
				final float probChildToReachable = probsPntToPnt[idxArrChildToReachable];
				if (probChildToReachable != 0.0f) {
					float[] probsToReachTgt = getCreateFloatsArrayFor(accumProbs, reachableGlobalIdx, numChildren);
					probsToReachTgt[childIdx] = probCovAndPropChild * probChildToReachable; // explicitly includes cov&prop prob of this child
				}
			}
		}
		
		/** Reachable object is an integer representing the global index of the reachable node. */
		@Override protected void consolidateProbs(ReachNode node, Object oReachable, Map<Object, float[]> accumProbs) {
			// for descendant or reachable obj, get prob of disjunction of events (ways to reach obj) whose independence degree is too hard to determine
			final float accumProbReachableObj = disjunctiveProbMerge(accumProbs.get(oReachable));
			
			// ensure that existing prob is not greater (if it is, a backedge was used previously to increase it)
			final int arrProbIdx = (((ReachDepNode)node).idx * numNodes) + ((Integer)oReachable);
			final float prevProb = probsPntToPnt[arrProbIdx];
			if (prevProb < accumProbReachableObj)
				probsPntToPnt[arrProbIdx] = accumProbReachableObj;
		}
		
		// DEBUG
		@Override public void printProbs() {
			System.out.println("P-slice from start " + depGraph.getStart() + ":");
			final int firstIdx = ((ReachDepNode)root).idx * numNodes;
			final int lastIdx = firstIdx + numNodes;
			for (int i = firstIdx; i < lastIdx; ++i) {
				final NodePoint pntTgt = idxToNode[i-firstIdx].pnt;
				System.out.println("  " + pntTgt + " = " + probsPntToPnt[i]);
			}
		}
		
		// DEBUG -- meant to be overridden, if desired
		@Override protected void printStatsPreLoopExits() {
			System.out.println("DEP DAG -- PRE LOOP-EXIT-COMPUTATION STATS");
			final int numNodes = objToDAGNode.size();
			System.out.println(" Nodes: " + numNodes);
			System.out.println(" Backedges: " + backedges.size());
			
			// count DAG edges and other metrics per node
			int dagEdges = 0;
//			int parents = 0;
//			int ancestors = 0;
			Set<ReachNode> beSources = new HashSet<ReachNode>();
			int beSrcNoChild = 0; // how many be srcs have no other children
			float besToNormalChildren = 0.0f; // ratio be:dagedges (for those with at least 1 dag child)
			for (ReachNode node : objToDAGNode.values()) {
				dagEdges += node.dagChildren.size();
				if (!node.backChildren.isEmpty()) {
					beSources.add(node);
					if (node.dagChildren.isEmpty())
						++beSrcNoChild;
					else
						besToNormalChildren += ((float)node.backChildren.size()) / node.dagChildren.size();
				}
//				parents += node.parents.size();
//				ancestors += node.ancestors.size();
			}
			
			System.out.println(" Avg dag children: " + (dagEdges / (float)numNodes));
//			System.out.println(" Avg parents: " + (parents / (float)numNodes));
//			System.out.println(" Avg ancestors: " + (ancestors / (float)numNodes));
			System.out.println(" Num backedge sources: " + beSources.size());
			System.out.println(" Num backedge sources w/o dag children: " + beSrcNoChild);
			System.out.println(" Ratio backedges:dagchildren for be srcs with at least 1 dag child: " + (besToNormalChildren / (beSources.size()-beSrcNoChild)));
			
			System.out.println("Used/total/max memory before loop exit analysis: " +
					(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		}
		@Override protected void printStatsDuringLoopExits(int numBEProcessed) {
			System.out.print(".");
			if (numBEProcessed % 8000 == 0)
				System.out.println();
		}
		@Override protected void printStatsAfterLoopExits(int numLoopExits) {
			System.out.println(); // after dot sequence
			
			System.out.println("Total/avg-per-child number of loop-exit edges: " + numLoopExits + " / " + (((float)numLoopExits)/objToDAGNode.size()));
			
			System.out.println("Used/total/max memory after loop exit analysis: " +
					(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		}
		
		/** Cleanup as much memory as possible. */
		@Override protected void postBuildCleanup(boolean removeProbsMap) {
			System.out.println("Used/total/max memory before DEP DAG cleanup before GC: " +
					(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
			System.gc();
			System.out.println("Used/total/max memory before DEP DAG cleanup after GC: " +
					(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
			super.postBuildCleanup(true); // override original flag to force cleanup of probs map
			System.gc();
			System.out.println("Used/total/max memory after DEP DAG cleanup: " +
					(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		}
	}
	
	private static DependenceGraph depGraph;
	private static Map<Dependence, Float> depProbs;
//	private Map<NodePoint, Map<NodePoint, Float>> pslice = new HashMap<NodePoint, Map<NodePoint,Float>>();
	private static ReachDepDag dagDepReach;
	private static Map<Object,Float> startToPntProbs;
	
	public PDGProb(DependenceGraph depGraph) {
		this.depGraph = depGraph;
		this.depProbs = DataDepProb.findDataDepProbs(depGraph);
		addControlDepProbs();
	}
	/** Helper for ctor to fill the remainder of dep-prob map with ctrl deps probs. */
	private void addControlDepProbs() {
		for (Dependence dep : depGraph.getDeps()) {
			if (dep instanceof ControlDependence)
				depProbs.put(dep, getControlDepProb((ControlDependence) dep));
		}
	}
	/** Helper to compute propagation probability through a ctrl dep, including its coverage prob. */
	private static float getControlDepProb(ControlDependence dep) {
		CFGNode nSrc = dep.getSrc().getN();
		final int numSuccs = nSrc.getSuccs().size();
		if (nSrc.hasAppCallees()) {
			assert numSuccs <= 1;
			final int numCallees = nSrc.getAppCallSite().getAllCallees().size();
			assert numCallees > 1;
			// probability covering target times prob of randomly switching to any of the other branch outcomes
			return (1.0f / numCallees) * ((float)numCallees - 1.0f) / numCallees;
		}
		
		assert numSuccs > 1;
		// probability of covering successors times prob of randomly switching to any of the other branch outcomes
		return (1.0f / numSuccs) * ((float)numSuccs - 1.0f) / numSuccs;
	}
	
	/** Do a forward p-slice: all probs from start to each affected point. */
	public Map findFwdProbsFromStart() {
		dagDepReach = (ReachDepDag) ReachDepDag.buildDepDag(depGraph, depProbs);
		dagDepReach.propagateProbs();
		// DEBUG
		dagDepReach.printProbs();
		
		// fill map pnt->prob for start pnt
		startToPntProbs = new HashMap<Object,Float>();
		final int firstIdx = ((ReachDepNode)dagDepReach.root).idx * ReachDepDag.numNodes;
		final int lastIdx = firstIdx + ReachDepDag.numNodes;
		for (int i = firstIdx; i < lastIdx; ++i)
			startToPntProbs.put(ReachDepDag.idxToNode[i-firstIdx].getObjNode(), ReachDepDag.probsPntToPnt[i]);
		return startToPntProbs;
	}
	
	public void genPSliceFileFromStart() {
		File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "pslice.out");
		try {
			Writer writer = new FileWriter(fOut);
			
			Map<NodePoint,Float> psliceFromStart = (Map)startToPntProbs;
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(psliceFromStart.keySet());
			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted)
				writer.write(pTgt + "=" + psliceFromStart.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	public void genPSliceFileAllToAll() {
		File fOut = new File(dua.util.Util.getCreateBaseOutPath() + "pslice-all.out");
		try {
			Writer writer = new FileWriter(fOut);
			
			// get sorted list of points FROM which we have a fwd p-slice
			List<NodePoint> pntsFromSorted = new ArrayList(dagDepReach.objToDAGNode.keySet()); //pslice.keySet());
			Collections.sort(pntsFromSorted, NodePointComparator.inst);
			for (NodePoint pntFrom : pntsFromSorted) {
				writer.write(pntFrom + ":");
				final int firstIdx = ((ReachDepNode)dagDepReach.objToDAGNode.get(pntFrom)).idx * ReachDepDag.numNodes;
				final int lastIdx = firstIdx + ReachDepDag.numNodes;
//				List<NodePoint> pntsTgtSorted = new ArrayList<NodePoint>(tgtPntProbs.keySet());
//				Collections.sort(pntsTgtSorted, NodePointComparator.inst);
//				for (NodePoint pTgt : pntsTgtSorted)
				// we are not sorting tgt points here -- doesn't seem necessary
				for (int i = firstIdx; i < lastIdx; ++i) {
					NodePoint pntTgt = ReachDepDag.idxToNode[i-firstIdx].pnt;
					writer.write(" " + pntTgt + "=" + ReachDepDag.probsPntToPnt[i]);
				}
				writer.write('\n');
			}
			
			writer.flush();
			writer.close();
		} catch (IOException e) { e.printStackTrace(); }
	}
	
}
