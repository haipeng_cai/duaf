package sli;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import dua.global.dep.DependenceGraph;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;

public class ModPSlice {
	
	private static Map<Object,Float> psliceProbFromStart;
	private static Map<NodePoint,Integer> wsliceDepthFromStart;
	private static Map<NodePoint,Map<Float,Integer>> pntProbDepth;
	
	// four strategies for modified prob
	public void genScoreFile(DependenceGraph depGraph){
		Map psliceProb = new HashMap();
		Map<NodePoint, Integer> wsliceDepth = new HashMap<NodePoint, Integer>();
		
		psliceProb = getPsliceProb(depGraph);
		wsliceDepth = getWsliceDepth(depGraph);
		
		int maxD = getMaxDistance(depGraph);
		
		// Calculate the score under each strategy and store <pnt, score> in corresponding map
		Map<NodePoint, Float> combinationMultScore = new HashMap<NodePoint, Float>();
		Map<NodePoint, Float> combinationAddScore = new HashMap<NodePoint, Float>();
		Map<NodePoint, Float> worstCaseScore = new HashMap<NodePoint, Float>();
		Map<NodePoint, Float> bestCaseScore = new HashMap<NodePoint, Float>();
		
		Iterator iter = psliceProb.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			NodePoint pnt = (NodePoint) entry.getKey();
			Float prob = (Float) entry.getValue();
			assert wsliceDepth.containsKey(pnt);
			Integer depth = (Integer)wsliceDepth.get(pnt);
			
			float score1, score2, score3, score4;
			
			// Strategy 1 (Option 4a): simple combination, p = p(s) � (D-d(s))/D
			score1 = prob * ((float)(maxD-depth) / maxD);
			combinationMultScore.put(pnt, score1);
			// Strategy 2 (Option 4b): simple combination, p = p(s) + (D-d(s))/D
			score2 = prob + ((float)(maxD-depth) / maxD);
			combinationAddScore.put(pnt, score2);
			// Strategy 3 (Option 5): worst case, p = p(s) � ((D-d(s))/D)
			score3 = prob * ((float)(maxD-depth) / maxD);
			worstCaseScore.put(pnt, score3);
			// Strategy 4 (Option 6): best case, p = p(s) + (d(s)/D) � (1-p(s))
			score4 = prob + ((depth/maxD) * (1-prob));
			bestCaseScore.put(pnt, score4);
		}
		
		// Generate the file for each strategy
		genOutFile("1.combinationMult.out", combinationMultScore);
		genOutFile("2.combinationAdd.out", combinationAddScore);
		genOutFile("3.worstCase.out", worstCaseScore);
		genOutFile("4.bestCase.out", bestCaseScore);
	}
	
	//get prob p(s) for each dependence using pslice method
	private Map getPsliceProb(DependenceGraph depGraph){
		PDGProb pdgProb = new PDGProb(depGraph);
		psliceProbFromStart =pdgProb.findFwdProbsFromStart();
		return psliceProbFromStart;
	}
	
	//get depth d(s) for each dependence using wslice method
	private Map<NodePoint, Integer> getWsliceDepth(DependenceGraph depGraph){	
		wsliceDepthFromStart = PDGDepth.genWSliceDepth(depGraph);
		return wsliceDepthFromStart;
	}
	
	private Map<NodePoint,Map<Float,Integer>> getPntPorbDepth(DependenceGraph depGraph){
		Map psliceProb = new HashMap();
		Map<NodePoint, Integer> wsliceDepth = new HashMap<NodePoint, Integer>();
		
		pntProbDepth = new HashMap<NodePoint, Map<Float,Integer>>();
		psliceProb = getPsliceProb(depGraph);
		wsliceDepth = getWsliceDepth(depGraph);
		Map <Float,Integer> probDepth = new HashMap<Float,Integer>();
		
		Iterator iter = psliceProb.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			NodePoint pnt = (NodePoint) entry.getKey();
			Float prob = (Float) entry.getValue();
			assert wsliceDepth.containsKey(pnt);
			Integer depth = (Integer)wsliceDepth.get(pnt);
			probDepth.put(prob, depth);
			pntProbDepth.put(pnt,probDepth);
		}
		return pntProbDepth;
	}
	
	private int getMaxDistance(DependenceGraph depGraph){
		int maxD = 0;
		//Map<NodePoint, Integer>wsliceDepth = getWsliceDepth(depGraph);
		Iterator iter = wsliceDepthFromStart.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			Integer depth = (Integer) entry.getValue();
			if (depth>maxD)
				maxD = depth;
		}
		return maxD;
	}
	
	private void genOutFile(String fileName, Map pntScoreMap){
		File fOut = new File(dua.util.Util.getCreateBaseOutPath() + fileName);
		try {
			Writer writer = new FileWriter(fOut);
			
			List<NodePoint> pntsSorted = new ArrayList<NodePoint>(pntScoreMap.keySet());
			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted)
				writer.write(pTgt + "=" + pntScoreMap.get(pTgt) + '\n');
			
			writer.flush();
			writer.close();	
		} catch (IOException e) { e.printStackTrace(); }
	}
	
}