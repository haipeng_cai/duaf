package sli;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import options.ReachProbOptions;

import sli.DataDepProb.ReachCFGDag;
import sli.DataDepProb.ReachCFGNode;
import sli.DataDepProb.ReachDag;
import sli.DataDepProb.ReachDagFactory;
import sli.DataDepProb.ReachNode;
import soot.SootMethod;
import dua.Extension;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CallSite;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

/** Computes intra and inter procedural reachability information to aid def-use reachability. */
public class ReachingProb implements dua.Extension {
	
	/******** DAG NODES *********/
	public static class ReachCSNode extends ReachNode {
		/** The object that this DAG node represents. */
		protected final CallSite cs;
		/// *** METHODS *** ///
		protected ReachCSNode(CallSite cs, ReachNode parent) { super(parent); this.cs = cs; }
		@Override protected final Object getObjNode() { return cs; }
	}
//	public static final class ReachCSBackNode extends ReachCSNode {
//		/** Special additional probs of returning to callers's bottoms (directly or transitively). */
//		private final Map<CallSite,Float> probReachCallers = new HashMap<CallSite, Float>();
//		protected ReachCSBackNode(CallSite cs, ReachNode parent) { super(cs, parent); }
//	}
	/** Represents a CFG's EN or EX node. */
	public static class ReachEnExNode extends ReachNode {
		protected final CFG cfg;
		/// *** METHODS *** ///
		protected ReachEnExNode(CFG cfg, ReachNode parent) { super(parent); this.cfg = cfg; }
		@Override protected final Object getObjNode() { return cfg; }
	}
	
	/******** FACTORIES *********/
	/** Factory for simple intraproc (CFG) reachability. */
	public static class ReachCFGSimpleDagFactory implements ReachDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachCFGSimpleDag((CFG)oInfo); }
		@Override public ReachNode newNode(Object oNode, ReachNode parent) { return new ReachCFGNode((CFGNode) oNode, parent); }
		
		public static ReachCFGSimpleDagFactory inst = new ReachCFGSimpleDagFactory();
		private ReachCFGSimpleDagFactory() {}
	}
	/** Intermediate class between base and the two types of interproc DAG subclasses. */
	public static abstract class ReachCSDagFactory implements ReachDagFactory {
		/** Root (null obj) is a CS node. */
		@Override public ReachNode newNode(Object oNode, ReachNode parent) {
			return (oNode == null || oNode instanceof CallSite)? new ReachCSNode((CallSite)oNode, parent) : new ReachEnExNode((CFG)oNode, parent);
		}
	}
	/** Factory for fwd interproc CS reachability. */
	public static class ReachFwdCSDagFactory extends ReachCSDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachFwdCSDag(this); }
		public static ReachFwdCSDagFactory inst = new ReachFwdCSDagFactory();
		private ReachFwdCSDagFactory() {}
	}
	/** Factory for back/fwd interproc CS reachability. Node created can represent a CS or a CFG's EX. */
	public static class ReachBackCSDagFactory extends ReachCSDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachBackCSDag(this); }
		public static ReachBackCSDagFactory inst = new ReachBackCSDagFactory();
		private ReachBackCSDagFactory() {}
	}
	
	/*********** DAGs ************/
	/** DAG for reachability in specific graph: CFG */
	public static final class ReachCFGSimpleDag extends ReachCFGDag {
		public ReachCFGSimpleDag(CFG cfg) { super(cfg, ReachCFGSimpleDagFactory.inst); }
		
		public static ReachDag buildCFGSimpleDag(CFG cfg) {
			return (ReachCFGSimpleDag) buildDag(cfg, ReachCFGSimpleDagFactory.inst);
		}
		
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs) {
			// case 1: successor is directly reachable
			//  get/create list of exclusive probabilities for succ
			final int numChildren = children.size();
			final float[] probsForSucc = getCreateFloatsArrayFor(accumProbs, ((ReachCFGNode)child).n, numChildren);
			// add prob to accum list of probs for succ
			probsForSucc[childIdx] = 1.0f; // prob of prop to succ from here (coverage prob is implicit in size of this vector)
			
			// 2. Look for other reachable nodes from successor's bottom
			for (Object oReachable : child.probReachFromBottom.keySet()) {
				assert oReachable instanceof CFGNode;
				if (oReachable == ((ReachCFGNode)child).n)
					continue; // only consider reachable nodes OTHER than successor (already processed in previous step)
				
				//    get/create list of exclusive probabilities for reachable node
				final float[] probsForReachableNode = getCreateFloatsArrayFor(accumProbs, oReachable, numChildren);
				probsForReachableNode[childIdx] = child.probReachFromBottom.get(oReachable);
			}
		}
		
		// DEBUG -- printing
		@Override public void printProbs() {
			printProbsHelper("NEW Prob intraproc reachable nodes: ");
		}
		/** Little helper that distinguishes EN/EX nodes from the rest. */
		@Override protected String getReachObjId(Object oReach) {
			return getNodeId((CFGNode) oReach);
		}
	}
	
	/** DAG for interprocedural reachability CS[fwd|bottom]->[CS|CFG]_top, forward or backward. Forward is from top to top, backward is from bottom to top.
	 *  Nodes in DAG as only CSs. Reachable objects are CSs and CFGs. */
	public static abstract class ReachCSDag extends ReachDag {
		/** Root contains null CS to indicate pseudo-root whose successors are all CFGs (their TOP for both fwd and bwd trees). */
		public ReachCSDag(ReachCSDagFactory fact) { super(fact.newNode(null, null), fact); }
		
		/** Info object is null */
		public static ReachCSDag buildCSDag(ReachCSDagFactory fact) { return (ReachCSDag) buildDag(null, fact); }
		
		/** The successors of a CS are the CSs in the target CFG(s). The 'null' CS is the DAG's root, whose successors are all CSs. */
		@Override protected final List getSuccessors(Object oNode) {
			// get list of target (successor) app method CS/EN/EX for node, which can be all CFGs if it's null (root)
			return (oNode == null)? ProgramFlowGraph.inst().getCFGs() : getInterprocSuccs(oNode);
		}
		
		/** Lists those CS successors in the DAG (either forward of backward). */
		protected abstract List getInterprocSuccs(Object oNode);
		
		/** Convenient reusable method that lists all app-mtd-calling CSs inside the given CFG. */
		protected static List<CallSite> getAppCSs(CFG cfg) {
			List<CallSite> callsites = new ArrayList<CallSite>();
			// create this list, if it doesn't exist yet
			for (CallSite cs : cfg.getCallSites()) {  // these are the call sites NOT in catch blocks
				if (cs.hasAppCallees())
					callsites.add(cs);
			}
			return callsites;
		}
		/** Convenient reusable method that transforms list of app mtds into a list of CFGs. */
		protected static List<CFG> appMtdsToCFGs(List<SootMethod> appMtds) {
			List<CFG> cfgs = new ArrayList<CFG>(appMtds.size());
			for (SootMethod m : appMtds)
				cfgs.add(ProgramFlowGraph.inst().getCFG(m));
			return cfgs;
		}
		
		/** Utility to get/create float list from cs->floatlist map. */
		protected static List<Float> getCreateFloatsList(Map<CallSite,List<Float>> csToFloatLists, CallSite key) {
			List<Float> flist = csToFloatLists.get(key);
			if (flist == null) {
				flist = new ArrayList<Float>();
				csToFloatLists.put(key, flist);
			}
			return flist;
		}
		
		/** Reachable object can be a CS or a CFG. */
		@Override protected final void consolidateProbs(ReachNode node, Object oReachable, Map<Object, float[]> accumProbs) {
			// for descendant or reachable obj, get prob of disjunction of events (ways to reach obj) whose independence degree is too hard to determine
			final float accumProbReachableObj = disjunctiveProbMerge(accumProbs.get(oReachable));
			
			// ensure that existing prob is not greater (if it is, a backedge was used previously to increase it)
			final Float prevProb = node.probReachFromBottom.get(oReachable);
			if (prevProb == null || prevProb < accumProbReachableObj)
				node.probReachFromBottom.put(oReachable, accumProbReachableObj);
		}
		
		// DEBUG
		protected void printProbsHelper(String msgPerCFG) {
			for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
				System.out.println(msgPerCFG + cfg.getMethod());
				
				// cfg reachability probs first
				printProbsCSorCFGHelper(cfg);
				
				// cs reachability probs second
				for (CallSite cs : cfg.getCallSites()) {
					if (!cs.hasAppCallees())
						continue;
					printProbsCSorCFGHelper(cs);
				}
			}
		}
		private void printProbsCSorCFGHelper(Object oCSorCFG) {
			ReachNode nodeCSorCFG = objToDAGNode.get(oCSorCFG);
			if (nodeCSorCFG == null)
				System.out.println(" " + getCSOrCFGId(oCSorCFG) + " " + oCSorCFG + ": <unreachable in DAG>");
			else {
				System.out.print(" " + getCSOrCFGId(oCSorCFG) + " " + oCSorCFG + ":");
				for (Object oReach : nodeCSorCFG.probReachFromBottom.keySet()) {
					final float prob = nodeCSorCFG.probReachFromBottom.get(oReach);
					System.out.print(" " + getCSOrCFGId(oReach) + "=" + prob);
				}
				System.out.println();
			}
		}
		
		/** Returns short string description of CS or CFG object. */
		protected static String getCSOrCFGId(Object oCSorCFG) {
			// case 1: call site
			if (oCSorCFG instanceof CallSite) {
				CallSite cs = (CallSite) oCSorCFG;
				return Integer.toString( StmtMapper.getGlobalStmtId(cs.getLoc().getStmt()) );
			}
			// case 2: cfg
			CFG cfg = (CFG)oCSorCFG;
			CFGNode nFirstReal = cfg.getFirstRealNode();
			if (nFirstReal == cfg.EXIT)
				return "EX_(NO_NODES)";
			return Integer.toString( StmtMapper.getGlobalNodeId(nFirstReal) );
		}
	}
	/** DAG for forward interprocedural reachability from each CS forward (callees) to CS tops. */
	public static final class ReachFwdCSDag extends ReachCSDag {
		/** Root contains null CS to indicate pseudo-root whose successors are all CFGs (their TOP for both fwd and bwd trees). */
		public ReachFwdCSDag(ReachCSDagFactory fact) { super(fact); }
		
		public static ReachFwdCSDag buildFwdCSDag() {
			return (ReachFwdCSDag) buildCSDag(ReachFwdCSDagFactory.inst);
		}
		
		@Override protected List getInterprocSuccs(Object oNode) {
			return ((oNode instanceof CallSite)? appMtdsToCFGs(((CallSite)oNode).getAppCallees()) : getAppCSs((CFG)oNode));
		}
		
		/** Prob to any child from DAG root (null CS) is simply 0. */
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs) {
			// First, child is a direct successor
			//  get/create list of probabilities for child
			final Object oChild = child.getObjNode();
			final int numChildren = children.size();
			final float[] probsForChild = getCreateFloatsArrayFor(accumProbs, oChild, numChildren);
			//  add prob of directly reachable child obj
			final Object oNode = node.getObjNode();
			if (oNode == null)
				probsForChild[childIdx] = 0f; // prob to any child from DAG root (null CS) is simply 0
			else {
				float probNodeToChild;
				if (oNode instanceof CallSite) {
					// CS -> EN or loopexitCS
					probNodeToChild = 1.0f / numChildren; // this is correct in our model: all target methods are "equally likely"
				}
				else {
					// EN -> CS
					CallSite csChild = (CallSite) oChild;
					final CFGNode nCSChild = ProgramFlowGraph.inst().getNode( csChild.getLoc().getStmt() );
					ReachCFGDag dagIntraReachChild = ReachingProb.cfgToIntraReach.get((CFG)node.getObjNode());
					probNodeToChild = dagIntraReachChild.root.probReachFromBottom.get(nCSChild); // prob of reaching child from here (incl. cov prob explicitly)
				}
				
				probsForChild[childIdx] = probNodeToChild;
				
				// Second, child has probs propagated from ALL its descendants
				for (Object oReachable : child.probReachFromBottom.keySet()) {
					if (oReachable == oChild)
						continue; // only consider reachable objects OTHER than successor (already processed in previous step)
					
					// get/create list of probabilities for reachable obj
					final float[] probsForReachableObj = getCreateFloatsArrayFor(accumProbs, oReachable, numChildren);
					probsForReachableObj[childIdx] = probNodeToChild * child.probReachFromBottom.get(oReachable);
				}
			}
		}
		
		// DEBUG
		@Override public void printProbs() {
			printProbsHelper("NEW Prob interproc CS fwd: ");
		}
	}
	/** DAG for backward interprocedural reachability from each CS's bottom to CS tops (nodes are always CS bottoms -- they don't count for probability). */
	public static final class ReachBackCSDag extends ReachCSDag {
		/** Root contains null CS to indicate pseudo-root whose successors are all CFGs (their TOP for both fwd and bwd trees). */
		public ReachBackCSDag(ReachCSDagFactory fact) { super(fact); }
		
		public static ReachBackCSDag buildBackCSDag() {
			return (ReachBackCSDag) buildCSDag(ReachBackCSDagFactory.inst);
		}
		
		/** Backwards, the successor nodes of a CFG EX are simply the caller sites (their bottoms). For a CS, the only successor is its CFG EX. */
		@Override protected List getInterprocSuccs(Object oNode) {
			// case 1: node is CS, only succ is containing CFG's EX
			if (oNode instanceof CallSite) {
				List<CFG> succs = new ArrayList<CFG>();
				CallSite cs = (CallSite) oNode;
				succs.add( ProgramFlowGraph.inst().getContainingCFG(cs.getLoc().getStmt()) );
				return succs;
			}
			// case 2: node is CFG EX, succs are CS callers
			return ((CFG)oNode).getCallerSites();
		}
		
		/** Computes/propagates probs EX->CS (using CSs reachable from child/caller) and CS->EX. Reachable objects are CS bottoms and CFG ENs. */
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, List<ReachNode> children, Map<Object, float[]> accumProbs) {
			// ignore root
			if (node == root)
				return;
			
			final int numChildren = children.size();
			final Object oChild = child.getObjNode();
			if (oChild instanceof CallSite) {
				// probs at CSs intra-reachable from caller CS (child) are propagated to that CS
				CFGNode nCSCaller = ProgramFlowGraph.inst().getNode( ((ReachCSNode)child).cs.getLoc().getStmt() );
				CFG cfgCaller = ProgramFlowGraph.inst().getContainingCFG(nCSCaller);
				ReachNode nodeCallerIntra = ((ReachCFGSimpleDag) ReachingProb.cfgToIntraReach.get(cfgCaller)).objToDAGNode.get(nCSCaller);
				final float probEXToCaller = getProbCallerSiteFromProgramEntry(children, childIdx); // 1.0f / numChildren;
				Map<Object,List<Float>> probsToBackFwdObjs = new HashMap<Object,List<Float>>(); // will store probs from caller (child) for each intra or fwd reachable cs/cfg
				
				// 1. propagate fwd probs from CSs intra-reachable from caller (child) CS
				for (CallSite csIntraReachable : cfgCaller.getCallSites()) {
					// process cs only if it is has app callees
					if (!csIntraReachable.hasAppCallees())
						continue;
					
					// get prob to each cs intra-reachable from caller (child) cs
					CFGNode nIntraReachable = ProgramFlowGraph.inst().getNode( csIntraReachable.getLoc().getStmt() );
					final Float probCallerToCSIntra = nodeCallerIntra.probReachFromBottom.get(nIntraReachable);
					if (probCallerToCSIntra == null)
						continue; // cs not reachable from returning cs
					final float probEXToCallerCSIntra = probEXToCaller * probCallerToCSIntra;
					//assert probEXToCallerCSIntra > 0f;
					
					// propagate interproc fwd probs to CFG-ENs only
					ReachNode nodeReachableCSFwd = ReachingProb.dagInterprocCSFwd.objToDAGNode.get(csIntraReachable);
					for (Object oReachFwd : nodeReachableCSFwd.probReachFromBottom.keySet()) {
						if (!(oReachFwd instanceof CFG))
							continue; // we are only interested in CFG-ENs, in addition to CS bottoms (propagated in the next steps)
						// prob to fwd CFG-ENs MUST INCLUDE prob of reaching caller (child) from parent (node) AND intra cs from caller, after which fwd obj is reached
						final Float probFwdToObj = probEXToCallerCSIntra * nodeReachableCSFwd.probReachFromBottom.get(oReachFwd);
						List<Float> probsToCSFwd = getCreateFloatsList(probsToBackFwdObjs, oReachFwd);
						probsToCSFwd.add(probFwdToObj);
					}
				}
				
				// 2. propagate back/fwd probs from caller (child) cs, which are nothing else than caller's EX probs
				for (Object oReachBackFromCaller : child.probReachFromBottom.keySet()) {
					if (oReachBackFromCaller == oChild)
						continue; // child itself is taken care of in step 4
					// prob of reaching caller from parent must be included
					final Float probBackFwd = probEXToCaller * child.probReachFromBottom.get(oReachBackFromCaller);
					List<Float> probsBackFwd = getCreateFloatsList(probsToBackFwdObjs, oReachBackFromCaller);
					probsBackFwd.add(probBackFwd);
				}
				
				// 3. merge probs obtained from CS tops intra-reachable from child CS's bottom
				for (Object oReachable : probsToBackFwdObjs.keySet()) {
					final float probToReachObj = disjunctiveProbMerge(probsToBackFwdObjs.get(oReachable)); // probs already include parent-child reach prob
					float[] probsToReachObj = getCreateFloatsArrayFor(accumProbs, oReachable, numChildren);
					probsToReachObj[childIdx] = probToReachObj;
				}
				
				// 4. directly set prob from EX to caller (child) CS's bottom
				float[] probsToCallerBottom = getCreateFloatsArrayFor(accumProbs, oChild, numChildren);
				probsToCallerBottom[childIdx] = probEXToCaller;
			}
			else {
				// just propagate back/fwd probs from EX to CS's bottom
				assert oChild instanceof CFG; // child obj represents EX
				assert numChildren == 1;
				for (Object oReachable : child.probReachFromBottom.keySet()) {
					float[] probsToReachObj = getCreateFloatsArrayFor(accumProbs, oReachable, numChildren);
					probsToReachObj[childIdx] = child.probReachFromBottom.get(oReachable);
				}
			}
		}
		
		/** Given that the EX of a called method is reached, computes the probability of returning to the caller site given by index in given list. */
		private float getProbCallerSiteFromProgramEntry(List<ReachNode> callerSites, int callerIdx) {
			assert callerIdx >= 0 && callerIdx < callerSites.size();
			
			// get entry of the program, from which we'll obtain reachability probabilities to all caller sites
			assert ProgramFlowGraph.inst().getEntryMethods().size() == 1;
			CFG cfgEntry = ProgramFlowGraph.inst().getCFG( ProgramFlowGraph.inst().getEntryMethods().get(0) );
			ReachNode nodeProgramEntry = ReachingProb.dagInterprocCSFwd.objToDAGNode.get(cfgEntry); // represents ENTRY node of ENTRY method
			assert nodeProgramEntry instanceof ReachEnExNode; // for safety and to improve understanding of this code
			
			// use fwd interproc reach probs for all callers
			List<Float> reachProbsCallers = new ArrayList<Float>();
			for (ReachNode caller : callerSites) {
				Float prob = nodeProgramEntry.probReachFromBottom.get(caller.getObjNode());
				reachProbsCallers.add(prob == null? 0f : prob);
			}
			final float probAnyCaller = disjunctiveProbMerge(reachProbsCallers);
			if (probAnyCaller == 0f)
				return 0f;
			final Float probSpecificCaller = nodeProgramEntry.probReachFromBottom.get(callerSites.get(callerIdx).getObjNode());
			
			return probSpecificCaller == null? 0f : probSpecificCaller / probAnyCaller; // probability of specific caller given that some caller is reached
		}

		// DEBUG
		@Override public void printProbs() {
			printProbsHelper("NEW Prob interproc CS back/fwd: ");
		}
	}
	
	public final static float FLOAT_EPSILON = 0.000001f;
	
	public static void findReachingProbs() {
		findIntraReachProbs();
		findInterFwdOnlyReachProbs();
		findInterBackOnlyReachProbs();
	}
	
	private static Map<CFG,ReachCFGDag> cfgToIntraReach = new HashMap<CFG, ReachCFGDag>();
	/** For each CFG, finds intra-proc probability of reaching each node's top from every other node's bottom (to suit def-use reachability). */
	private static void findIntraReachProbs() {
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			// do DFS of CFG to create dag and identify backedges
			ReachDag tree = ReachCFGSimpleDag.buildCFGSimpleDag(cfg);
			tree.propagateProbs();
			
			cfgToIntraReach.put(cfg, (ReachCFGDag) tree); // store for use by interproc CS dag propagation
			
			// DEBUG
			((ReachCFGSimpleDag) tree).printProbs();
		}
	}
	
	private static ReachCSDag dagInterprocCSFwd;
	/** For each CS (call site), finds inter-proc probability of reaching each CFG. For each CS, only considers call edges coming out of it, not intra-CFG successors. */
	private static void findInterFwdOnlyReachProbs() {
		dagInterprocCSFwd = ReachFwdCSDag.buildFwdCSDag();
		dagInterprocCSFwd.propagateProbs();
		// DEBUG
		dagInterprocCSFwd.printProbs();
	}
	
	private static ReachCSDag dagInterprocCSBackFwd;
	private static void findInterBackOnlyReachProbs() {
		dagInterprocCSBackFwd = ReachBackCSDag.buildBackCSDag();
		dagInterprocCSBackFwd.propagateProbs();
		// DEBUG
		dagInterprocCSBackFwd.printProbs();
	}
	
	/** Computes interprocedural probability of reaching tgt from src's bottom or top. */
	public static float getInterprocCovProb(CFGNode nSrc, boolean bottomSrc, CFGNode nTgt) {
		// For each d, look for u intraprocedurally, then only forward through call sites, and then from d's containing method's EX (backward/forward in call graph).
		// The backward/forward way has two sub-cases: (1) u is located after a returning call site from EX or (2) u is forward-reachable from some CS after that returning CS.
		// These four ways are NOT exclusive -- d can reach u through all of them if there are intra, forward, and backward/forward paths.
		
		assert !nSrc.isInCatchBlock();
		assert !nTgt.isInCatchBlock();
		assert nSrc.getSuccs().size() <= 1; // no heap def has multiple successors
		
		List<Float> probs = new ArrayList<Float>();
		
		// 1. Intraprocedural case
		//   applies if containing cfg is the same for src and tgt
		CFG cfgSrc = ProgramFlowGraph.inst().getContainingCFG(nSrc);
		CFG cfgTgt = ProgramFlowGraph.inst().getContainingCFG(nTgt);
		if (cfgSrc == cfgTgt) {
			CFGNode nSrcSucc = nSrc.getSuccs().get(0);
			Float prob = cfgToIntraReach.get(cfgSrc).objToDAGNode.get(nSrcSucc).probReachFromBottom.get(nTgt);
			if (prob != null)
				probs.add(prob);
		}
		
		// 2. Fwd-only interprocedural case
		//   look for fwd prob to tgt's CFG from CSs intra-reachable from src's bottom or top (according to bottomSrc flag)
		ReachNode nodeIntraSrc = cfgToIntraReach.get(cfgSrc).objToDAGNode.get(nSrc);
		ReachNode nodeCFGTgtEntry = cfgToIntraReach.get(cfgTgt).root;
		final float probIntraENToTgt = nodeCFGTgtEntry.probReachFromBottom.get(nTgt);
		for (CallSite cs : cfgSrc.getCallSites()) {
			if (!cs.hasAppCallees())
				continue;
			// get intra prob to cs from src's bottom or top
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			final Float probIntraToCS = (!bottomSrc && nCS == nSrc)? Float.valueOf(1.0f) : nodeIntraSrc.probReachFromBottom.get(nCS);
			if (probIntraToCS == null)
				continue; // cs not intra-reachable from src
			// get fwd prob from cs to tgt's cfg
			ReachNode nodeCSFwd = dagInterprocCSFwd.objToDAGNode.get(cs);
			final Float probCSToTgtCFG = nodeCSFwd.probReachFromBottom.get(cfgTgt);
			if (probCSToTgtCFG == null)
				continue; // tgt's cfg not fwd reachable from cs
			// get intra prob from tgt's cfg entry to tgt
			// finally, compose the three probabilities
			probs.add(probIntraToCS * probCSToTgtCFG * probIntraENToTgt);
		}
		
		// 3. Back/fwd interprocedural case
		//    case 1: back/fwd prob to tgt's CFG from src's CFG's EX
		//    case 2: intra prob to tgt from CSs back-reachable from src's CFG's EX
		
		//    case 1: back/fwd prob to tgt's CFG from src's EX
		ReachCFGDag dagTgtCFG = cfgToIntraReach.get(cfgTgt);
		ReachNode nodeBackFwdCSCaller = dagInterprocCSBackFwd.objToDAGNode.get(cfgSrc);
		final Float probFwdToTgtCFG = nodeBackFwdCSCaller.probReachFromBottom.get(cfgTgt);
		if (probFwdToTgtCFG != null)
			probs.add(probFwdToTgtCFG * probIntraENToTgt); // compute and add prob if tgt's CFG is back/fwd reachable from caller's bottom
		
		//    case 2: src has transitive caller in tgt's CFG
		for (CallSite csInTgtCFG : cfgTgt.getCallSites()) {
			if (!csInTgtCFG.hasAppCallees())
				continue;
			final Float probBackToCSInTgtCFG = nodeBackFwdCSCaller.probReachFromBottom.get(csInTgtCFG);
			if (probBackToCSInTgtCFG == null)
				continue;
			ReachNode nodeIntraCSInTgtCFG = dagTgtCFG.objToDAGNode.get( ProgramFlowGraph.inst().getNode(csInTgtCFG.getLoc().getStmt()) );
			final Float probIntraCSToTgt = nodeIntraCSInTgtCFG.probReachFromBottom.get(nTgt);
			if (probIntraCSToTgt == null)
				continue;
			probs.add(probBackToCSInTgtCFG * probIntraCSToTgt);
		}
		
		return ReachCSDag.disjunctiveProbMerge(probs);
	}
	
	/** To be used when dags and maps in this class are no longer needed. */
	public static void freeMemory() {
		ReachingProb.cfgToIntraReach.clear();
		ReachingProb.cfgToIntraReach = null;
		
		ReachingProb.dagInterprocCSFwd = null;
		
		ReachingProb.dagInterprocCSBackFwd = null;
	}
	
	/////////////////////////////////////////////////////////////////////////
	/// Support for running ReachingProb directly, without a client analysis
	/** Entry point for convenient testing of reaching probabilities, without a client analysis. */
	public static void main(String[] args) {
		args = preProcessArgs(args);
		
		ReachingProb reachProbInstance = new ReachingProb();
		Forensics.registerExtension(reachProbInstance);
		Forensics.main(args); // pass control to Forensics, which will then pass it back to us
	}
	/** No options class, at least for now. */
	private static String[] preProcessArgs(String[] args) {
		args = ReachProbOptions.process(args);
		return args;
	}
	/** Used when testing reaching probabilities, without a client analysis. */
	@Override public void run() {
		System.out.println("=========Running ReachingProb=========");	
		StmtMapper.getCreateInverseMap();
		
		ReachingProb.findReachingProbs();
	}
}
