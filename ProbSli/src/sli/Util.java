package sli;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Map.Entry;

import dua.global.dep.BackwardDependenceGraph;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;


public class Util {
	
	public static float orProb(List<Float> probs) {
		float orProb = 0.0f;
		for (float prob : probs)
			orProb += (1.0f - orProb) * prob;
		return orProb;
	}
	
	public static Map<String, Float> parseFileIntoStrFloatMap(File file){
		Map<String, Float>stringFloatMap = new HashMap<String, Float>();
		
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            String str = null;
            String floatStr = null;
            float floatNum; //used to denote score when value is given for a line
            // Read one line each time, until read null, which means the end of file
            while ((tempString = reader.readLine()) != null) {
            	for(int c=0;c<tempString.length();c++)
            	{
            		if (tempString.charAt(c)==':'){
            			
            				str = tempString.substring(0, c-1);
            				floatStr = tempString.substring(c+1,tempString.length());
            		}
            	}
            	
            	floatNum = Float.parseFloat(floatStr);
            	
            	stringFloatMap.put(str, floatNum);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
		
		return stringFloatMap;
	}
	
	public static void writeMapToFile(Map map, String filepath) {  
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(filepath, false); 
			Set set = map.entrySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeMapToFileWithMethod(Map rsltMap, String method){		
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(dua.util.Util.getCreateBaseOutPath() +  "OrderedMap" + method + ".txt", false);

			Set set = rsltMap.entrySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeQueueToFileWithMethod(Queue rsltQueue, String method){		
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(dua.util.Util.getCreateBaseOutPath() +  "OrderedQueue" + method + ".txt", false);

			Iterator iter = rsltQueue.iterator();
			while(iter.hasNext()){
				NodePoint nPoint = (NodePoint)iter.next(); 
				str.append(nPoint).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * printBackSli is used to print all the dependences of a slice to a file
	 * */
	public static void printBackSli(BackwardDependenceGraph depGraph, String fPath){
		try{
			FileWriter fw = new FileWriter(fPath);
			String line = System.getProperty("line.separator");

			for( Dependence dep : depGraph.getDeps()){
				fw.write(dep.toString());
				fw.write(line);
			}
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public static Map<NodePoint, Float> giveRanktoPnt(Map<NodePoint, Float> rsltMap){
		Map<NodePoint, Float> pntRankMap = new LinkedHashMap<NodePoint, Float>();
		
		List<NodePoint> pntList = new ArrayList<NodePoint>();
		List<Float> probList = new ArrayList<Float>(); 
		List<Float> rankList = new ArrayList<Float>();
		
		List<Entry<NodePoint, Float>> list = new ArrayList<Entry<NodePoint, Float>>(rsltMap.entrySet());
		Iterator iter = list.iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			NodePoint pnt = (NodePoint) entry.getKey();
			Float prob = (Float) entry.getValue();
			
			pntList.add(pnt);
			probList.add(prob);
		}
		
		Iterator probIter = probList.iterator();
		Float curProb = (Float) probIter.next();
		
		float rank = 0f;
		int i;
		int tieCnt = 1;
		int firstTiePos = 1; // First position for a tie case
		int curPos = 1; // Current Position. Rank can be got from #Position
		float nextProb = 0f;
		
		// Start from the second element
		while(probIter.hasNext()){
			nextProb = (Float) probIter.next();

			if(nextProb == curProb){
				tieCnt += 1;
				curPos += 1; // Update the current position. Not update the first tie position
			}
			else{
				rank = (float)(firstTiePos + (firstTiePos + tieCnt - 1.0f)) * 0.5f;
				for(i=0; i<tieCnt; i++)
					rankList.add(rank);
				// Update curPos and firtiePos for the next step
				curPos += 1;
				firstTiePos = curPos;
				tieCnt = 1; // reset
				curProb = nextProb;
			}
		}
		
		
		// If the last value in the probList is different from the second last value, the rank is not calculated above
		if (rankList.size() != probList.size()){
			rank = (float)(firstTiePos + (firstTiePos + tieCnt - 1.0f)) * 0.5f;
			for(i=0; i<tieCnt; i++)
				rankList.add(rank);
		}
		
		assert rankList.size() == probList.size();
		
		for (i=0; i<pntList.size(); i++){
			NodePoint pnt = pntList.get(i);
			rank = rankList.get(i);
			pntRankMap.put(pnt, rank);
		}
		
		return pntRankMap;
	}
	
	// Get one set for each type of dependences
	public static void getTwoDepSets(BackwardDependenceGraph backDepGraph){
		Set<Dependence> ctrlDependenceSet = new HashSet<Dependence>();
		Set<Dependence> dataDependenceSet = new HashSet<Dependence>();
		
		for(Dependence dep :  backDepGraph.getDeps()){
			if (dep instanceof DataDependence){
				dataDependenceSet.add(dep);
			}
			else{
				ctrlDependenceSet.add(dep);
			}
		}
	}
}
