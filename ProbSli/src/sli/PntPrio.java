package sli;

/* Class Description of PntPrio:
 * This class represents a NodePoint and its priority.
 * This is an abstract structure and is not specific to any models.
 * */

public class PntPrio<NodePoint, Float> {
	private final NodePoint pnt;
	private final Float prio;
	
	public NodePoint getPnt() {return pnt;}
	public Float getPrio() {return prio;}
	
	public PntPrio<NodePoint,Float> get(NodePoint pnt) {return this;}
	
	public PntPrio(NodePoint pnt, Float prio) {this.pnt = pnt; this.prio = prio;}
	
	@Override
	public String toString() { return "<" + ((pnt == null)? "null" : pnt.toString()) + "," + ((prio == null)? "null" : prio.toString()) + ">"; }
	
	@Override
	public int hashCode() { return ((pnt == null)? 0 : pnt.hashCode()) + ((prio == null)? 0 : prio.hashCode()); }
	
}
