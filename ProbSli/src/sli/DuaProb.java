package sli;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import soot.Local;
import soot.SootMethod;
import dua.global.ProgramFlowGraph;
import dua.global.dep.BackwardDependenceGraph;
import dua.global.dep.DependenceFinder;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.method.CFG;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFG.CFGNodeSpecial;
import dua.method.CFGDefUses.Def;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.ReachableUsesDefs.NodeReachDefsUses;
import fault.StmtMapper;

import dua.*; //need to be MORE PRESICE --this line is to successfully use DUA class;

public class DuaProb{
	
	/******** DAG NODES *********/
	/** Node of search dag for some graph (CFG, call-site-graph) */
	public static abstract class ReachNode {
		protected Set<ReachNode> ancestors = new HashSet<ReachNode>(); // used during dag construction to check for cycles
		protected List<ReachNode> dagChildren = new ArrayList<ReachNode>();
		protected List<ReachNode> backChildren = new ArrayList<ReachNode>(); // targets of backedges whose source is this node
		protected Collection<ReachNode> loopExChildren = new HashSet<ReachNode>(); // artificial edges to transfer probs from outside loops to backedge source nodes
		
		/** Probabilities of reaching objects (use ids, nodes, call sites) from the BOTTOM of current node. */
		protected Map<Object,Float> probReachFromBottom = new HashMap<Object, Float>();
		
		/** Retrieves the object (use id, cfg node, call site) represented by this DAG node. */
		protected abstract Object getObjNode();
		
		protected ReachNode(ReachNode parent) { }
		@Override public String toString() { Object o = getObjNode(); return (o == null)? "null" : o.toString(); }
		
		private static final List<ReachNode> FIXED_EMPTY_NODE_LIST = new ArrayList<ReachNode>();
		protected void postBuildCleanupRecursive(Set<ReachNode> visited, boolean removeProbsMap) {
			final boolean notVisitedBefore = visited.add(this);
			assert notVisitedBefore;
			
			// cleanup children first
			assert !this.dagChildren.contains(this);
			for (ReachNode succ : this.dagChildren)
				if (succ.ancestors != null)
					succ.postBuildCleanupRecursive(visited, removeProbsMap);
			// cleanup this node
			this.ancestors.clear(); // ancestors not needed anymore
			this.ancestors = null;
			if (dagChildren.isEmpty()) this.dagChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			if (backChildren.isEmpty()) this.backChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			if (loopExChildren.isEmpty())
				this.loopExChildren = FIXED_EMPTY_NODE_LIST; // replace with shared empty list
			else
				this.loopExChildren = new ArrayList<ReachNode>(this.loopExChildren); // transform set into array list to save space
			// cleanup probs map only if required
			if (removeProbsMap)
				this.probReachFromBottom = null;
		}
	}
	/** Node of search dag for CFG (def-use, intraproc reach). */
	public static class ReachCFGNode extends ReachNode {
		/** The object that this DAG node represents. */
		protected final CFGNode n;
		/// *** METHODS *** ///
		protected ReachCFGNode(CFGNode n, ReachNode parent) { super(parent); this.n = n; }
		protected Object getObjNode() { return n; }
	}
	public static class ReachEdge {
		public final ReachNode src;
		public final ReachNode tgt;
		public ReachEdge(ReachNode src, ReachNode tgt) { this.src = src; this.tgt = tgt; }
	}
	
	/******** FACTORIES *********/
	public static interface ReachDagFactory { ReachDag newInst(Object oInfo); ReachNode newNode(Object oNode, ReachNode parent); }
	public static class ReachCFGDagFactory implements ReachDagFactory {
		@Override public ReachDag newInst(Object oInfo) { return new ReachCFGDefUseDag((CFG)oInfo); }
		@Override public ReachNode newNode(Object oNode, ReachNode parent) { return new ReachCFGNode((CFGNode)oNode, parent); }
		
		public static ReachCFGDagFactory inst = new ReachCFGDagFactory();
		private ReachCFGDagFactory() {}
	}
	
	/******** DAGs *********/
	/** Search dag for some graph (CFG, call-site-graph). Stores backedges in list as found in DFS order, so innermost backedges are listed first. */
	public static abstract class ReachDag {
//		public static final int BACKEDGE_FREQ = 4; // for now, same for all backedges
		
		protected final ReachDagFactory fact;
		protected final ReachNode root;
		protected final List<ReachNode> leaves = new ArrayList<ReachNode>();
		protected final List<ReachEdge> backedges = new ArrayList<ReachEdge>();
		/** May be filled by subclass after DAG is built. */
		protected final Map<Object,ReachNode> objToDAGNode = new HashMap<Object, ReachNode>();
		
		protected ReachDag(ReachNode root, ReachDagFactory fact) { this.root = root; this.fact = fact; }
		
		// DEBUG -- meant to be overridden, if desired
		protected void printStatsPreLoopExits() {}
		protected void printStatsDuringLoopExits(int numBEProcessed) {}
		protected void printStatsAfterLoopExits(int numLoopExits) {}
		/** DFS search to create spanning dag and identify backedges. */
		protected static ReachDag buildDag(Object oInfo, ReachDagFactory fact) {
			ReachDag dag = fact.newInst(oInfo);
			
			// build dag, identifying backedges along the way
			Map<Object,ReachNode> visited = new HashMap<Object,ReachNode>(); // keeps map to already created reachNodes
			dag.buildSubDag(dag.root, visited);
			dag.fillNodeMap();
			
			dag.printStatsPreLoopExits();
			
			// after dag is completed (in particular, the ancestor relation), find the exits for each backedge's loop
			dag.findLoopExits();
			
			dag.postBuildCleanup(false); // free some memory
			
			return dag;
		}
		/** Recursive helper to find the dag and backedges in DFS search of CFG. */
		private final void buildSubDag(ReachNode node, Map<Object,ReachNode> visited) {
			final Object oNode = node.getObjNode();
			
			// update visited map
			Object oPrev = visited.put(oNode, node);
			assert oPrev == null;
			
			// handle "leaf" case
			List<Object> oSuccs = getSuccessors(oNode); //node.n.getSuccs();
			if (oSuccs.isEmpty()) {
				leaves.add(node);
				return;
			}
			
			// add non-visited successors to dag recursively; connect to visited successors; detect backedges
			for (Object oSucc : oSuccs) {
//				if (oSucc.isInCatchBlock())
//					continue; // don't support exception flow, for now
				
				ReachNode succ = visited.get(oSucc);
				if (succ == null) {  // newly found node
					// create successor reachNode, whose ancestors are current node's ancestor plus current node
					succ = fact.newNode(oSucc, node); // current node is a parent
					node.dagChildren.add(succ); // link to child
					succ.ancestors.addAll(node.ancestors); // share ancestor with successor
					succ.ancestors.add(node); // this node is also an ancestor of successor
					// continue recursion on new successor node
					buildSubDag((ReachNode)succ, visited);
				}
				else {  // already visited, so succ is either a join point in dag or the tgt of a backedge
					// successor is a backedge iff it's an ancestor of current node or the current node itself
					if (oSucc.equals(oNode) || node.ancestors.contains(succ)) {
						backedges.add( new ReachEdge(node, succ) );
						node.backChildren.add(succ);
					}
					else {
						// just update succ's parents and ancestors with current node and its own ancestors
						node.dagChildren.add(succ); // link to child
						succ.ancestors.addAll(node.ancestors);
						succ.ancestors.add(node);
					}
				}
			}
		}
		protected abstract List getSuccessors(Object oNode);
		
		/** Returns the nodes considered for loop-exit edge analysis. This default implementation returns all nodes in loop. */
		protected Set<ReachNode> getLoopNodes(ReachEdge be) {
			// L = ancestors(be-src) + be-src - ancestors(be-tgt)
			Set<ReachNode> loopNodes = new HashSet<ReachNode>(be.src.ancestors);
			loopNodes.add(be.src);
			loopNodes.removeAll(be.tgt.ancestors);
			return loopNodes;
		}
		/** Part of the DAG building algorithm. */
		private final void findLoopExits() {
			int numLoopExits = 0;
			int numBE = 0;
			for (ReachEdge be : backedges) {
				Set<ReachNode> loopNodes = getLoopNodes(be);
				
				// look for dag edges out of these nodes that also leave loop
				for (ReachNode lnode : loopNodes) {
					for (ReachNode succ : lnode.dagChildren) {
						if (!loopNodes.contains(succ) && !be.src.dagChildren.contains(succ)) {
							if (be.src.loopExChildren.add(succ))
								++numLoopExits;
						}
					}
				}
				
				if (++numBE % 100 == 0)
					printStatsDuringLoopExits(numBE);
			}
			
			printStatsAfterLoopExits(numLoopExits);
		}
		
		// DEBUG -- meant to be overridden, if desired
		protected void propagationIterationPreStep(int i) {}
		/** Propagates probabilities from leaves backwards (DFS post-order). */
		public void propagateProbs() {
			// do DFS post-order traversal of nodes in dag
			propagationIterationPreStep(0);
			Set<ReachNode> visited = new HashSet<ReachNode>();
			propProbsDFSFromSuccs(root, visited, null); // no backedge -- visit the whole dag
			
			// do alternating activation of backedges and loop-exit edges, re-propagating each time
			if (!this.backedges.isEmpty()) {
				for (int i = 0; i < ProbSliOptions.backEdgeFreq(); ++i) {
					propagationIterationPreStep(i+1);
					
					visited.clear();
					propProbsDFSFromSuccs(root, visited, this.backedges.get(0));
					visited.clear();
					propProbsDFSFromSuccs(root, visited, null);
				}
			}
		}
		/** Template method for recursive DFS post-order visitor of dag to propagate probs from successors to bottom of current node.
		 *  Possibly receives a non-null backedge tgt. If so, propagates from that successor too. If not, propagates through artificial loop-exit edges. */
		protected void propProbsDFSFromSuccs(ReachNode node, Set<ReachNode> visited, ReachEdge backedge) {
			// mark current node as visited
			final boolean notVisitedBefore = visited.add(node);
			assert notVisitedBefore;
			
			// DEBUG
			final int numVisited = visited.size();
			
			// get list of successors to visit before this node
			List<ReachNode> childrenToPropagateFrom = (List<ReachNode>) ((ArrayList<ReachNode>) node.dagChildren).clone();
			if (backedge != null)
				childrenToPropagateFrom.addAll(node.backChildren);
			else
				childrenToPropagateFrom.addAll(node.loopExChildren);
			
			// first, visit successors, which ensures that probs will be there to propagate to here
			for (ReachNode succ : childrenToPropagateFrom) {
				if (!visited.contains(succ))
					propProbsDFSFromSuccs(succ, visited, backedge);
			}
			
			// then, process this node -- propagate probs from successors, including backedge's target if specified or loop-exit edges otherwise
			final int numChildren = childrenToPropagateFrom.size();
			Map<Object,float[]> accumProbs = new HashMap<Object,float[]>();
			for (int childIdx = 0; childIdx < numChildren; ++childIdx)
				propProbsFromChild(node, childrenToPropagateFrom.get(childIdx), childIdx, numChildren, accumProbs);
			
			// consolidate lists of probs into one prob for each of node's bottom's reachable objects (e.g., reachable uses)
			for (Object oReachable : accumProbs.keySet())
				consolidateProbs(node, oReachable, accumProbs);
			
			// DEBUG
			if (numVisited % 100 == 0) {
				System.out.println("DBG: probsli visited " + numVisited + " used/total/max mem " +
						(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
			}
		}
		
		/** Performs the transfer of probabilities from a particular child (successor) into a prob-accumulating map. */
		protected abstract void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, int numChildren, Map<Object,float[]> accumProbs);
		/** Performs the final merging of the different probabilities propagated from all children into one probability for given node. */
		protected abstract void consolidateProbs(ReachNode node, Object oReachable, Map<Object,float[]> accumProbs);
		
		/** Utility to get/create float array from obj->float[] map. */
		protected static float[] getCreateFloatsArrayFor(Map<Object, float[]> oToFloatArrs, Object oKey, int arrSize) {
			float[] floatsArrForObj = oToFloatArrs.get(oKey);
			if (floatsArrForObj == null) {
				floatsArrForObj = new float[arrSize];
				oToFloatArrs.put(oKey, floatsArrForObj);
			}
			return floatsArrForObj;
		}
		/** Utility to get/create float list from obj->floatlist map. */
		protected static List<Float> getCreateFloatsList(Map<Object,List<Float>> oToFloatLists, Object key) {
			List<Float> flist = oToFloatLists.get(key);
			if (flist == null) {
				flist = new ArrayList<Float>();
				oToFloatLists.put(key, flist);
			}
			return flist;
		}
		
		/** Simple helper that averages float values (e.g., probs) in vector. */
		protected static float avg(float[] values) {
			assert values.length > 0;
			float sum = 0f;
			for (float v : values)
				sum += v;
			return sum / values.length;
		}
		/** Simple helper that averages float values (e.g., probs) in list. */
		protected static float avg(List<Float> values) {
			final int size = values.size();
			assert size > 0;
			float sum = 0f;
			for (float v : values)
				sum += v;
			return sum / size;
		}
		
		/** Computes the prob of the disjunction of a list of events given that their degree of (in)dependence is unknown. */
		protected static float disjunctiveProbMerge(float[] probs) {
			// Goal: - all probs should add something to the final prob
			//       - the final prob must be greater than any individual prob (if there is more than 1 event)
			// Algorithm:
			//    Sort probs from greatest to lowest. Each event overlaps evenly with the previous events and the rest of the universe.
			//  Example: [0.6, 0.5, 0.3] -> 0.6 + (1-0.6)x0.5/2 + (1-...)x0.3/3 = 0.6 + 0.1 + (1-0.7)x0.1 = 0.73
			//  Note: the divisor for the i^th prob is i, indicating that only 1/i of the rest of the universe overlaps with the i^th event
			
			// create sorted probs list in descending order
			final int numProbs = probs.length;
			List<Float> probsSorted = new ArrayList<Float>(numProbs);
			for (float p : probs)
				probsSorted.add(p);
			Collections.sort(probsSorted, Collections.reverseOrder());
			
			// apply prob-merging algorithm outlined above
			float result = 0f;
			for (int i = 0; i < numProbs; ++i)
				result += (1-result) * probsSorted.get(i) / (i+1);
			
			return result;
		}
		/** Wrapper for disjunctiveProbMerge that first transforms prob list into array. */
		protected static float disjunctiveProbMerge(List<Float> probs) {
			float[] probsArr = new float[probs.size()];
			int i = 0;
			for (Float p : probs)
				probsArr[i++] = p;
			return disjunctiveProbMerge(probsArr);
		}
		
		// DEBUG -- printing
		public abstract void printProbs();
		
		protected void fillNodeMap() {
			fillNodeMapRecursive(root, objToDAGNode);
		}
		protected static void fillNodeMapRecursive(ReachNode node, Map<Object,ReachNode> oToNode) {
			if (oToNode.put(node.getObjNode(), node) != null)
				return;
			for (ReachNode succ : node.dagChildren)
				fillNodeMapRecursive(succ, oToNode);
		}
		
		/** Cleanup as much memory as possible. */
		protected void postBuildCleanup(boolean removeProbsMap) {
			Set<ReachNode> visited = new HashSet<ReachNode>();
			this.root.postBuildCleanupRecursive(visited, removeProbsMap);
		}
	}
	
	public static abstract class ReachCFGDag extends ReachDag {
		protected final CFG cfg;
		
		protected ReachCFGDag(CFG cfg, ReachDagFactory fact) { super(fact.newNode(cfg.getNodes().get(0), null), fact); this.cfg = cfg; }
		
		@Override protected final List getSuccessors(Object oNode) {
			// the successors are those NOT in catch glo
			List succs = new ArrayList();
			for (CFGNode nSucc : ((CFGNode)oNode).getSuccs()) {
				if (!nSucc.isInCatchBlock()) // don't support exception flow, for now
					succs.add(nSucc);
			}
			return succs;
		}
		
		@Override protected final void consolidateProbs(ReachNode node, Object oReachable, Map<Object,float[]> accumProbs) {
			// now, only ensure that existing prob to descendant is not greater (if so, a backedge was used previously to increase it)
			final float accumProbDescendant = avg(accumProbs.get(oReachable));
			final Float prevProb = node.probReachFromBottom.get(oReachable);
			if (prevProb == null || prevProb < accumProbDescendant)
				node.probReachFromBottom.put(oReachable, accumProbDescendant);
		}
		
		// DEBUG -- printing
		protected final void printProbsHelper(String msgPerDAG) {
			System.out.println(msgPerDAG + cfg.getMethod());
			for (CFGNode n : cfg.getNodes()) {
				if (n.isInCatchBlock())
					continue; // don't support exception flow, for now
				ReachNode node = objToDAGNode.get(n);
				if (node == null)
					System.out.println(" " + n + ": <unreachable in DAG>");
				else {
					System.out.print(" " + getNodeId(n) + " " + n + ":");
					for (Object oReach : node.probReachFromBottom.keySet()) {
						final float prob = node.probReachFromBottom.get(oReach);
						System.out.print(" " + getReachObjId(oReach) + "=" + prob);
					}
					System.out.println();
				}
			}
		}
		protected static String getNodeId(CFGNode n) {
			assert n instanceof CFGNode;
			if (n instanceof CFGNodeSpecial)
				return n.toString();
			return Integer.toString( StmtMapper.getGlobalNodeId(n) );
		}
		protected abstract String getReachObjId(Object oReach);
	}
	
	/** Search dag for def-uses in specific graph: CFG */
	public static final class ReachCFGDefUseDag extends ReachCFGDag {
		public ReachCFGDefUseDag(CFG cfg) { super(cfg, ReachCFGDagFactory.inst); }
		
		public static ReachCFGDag buildCFGDefUseDag(CFG cfg) {
			return (ReachCFGDag) buildDag(cfg, ReachCFGDagFactory.inst);
		}
		
		/** Specific propagation algorithm for reachable uses from child */
		@Override protected void propProbsFromChild(ReachNode node, ReachNode child, int childIdx, int numChildren, Map<Object,float[]> accumProbs) {
			final ReachCFGNode childCFGNode = (ReachCFGNode) child;
			if (!(childCFGNode.n instanceof NodeReachDefsUses))
				return; // skip if node does not have RU info (e.g., EN, EX)
			// look for GEN ru's at successor
			NodeReachDefsUses nRUSucc = (NodeReachDefsUses) childCFGNode.n;
			BitSet bsUGen = nRUSucc.getUGen();
			for (int i = bsUGen.nextSetBit(0); i >= 0; i = bsUGen.nextSetBit(i + 1)) {
				// get/create list of exclusive probabilities for RU
				final float[] probsForRU = getCreateFloatsArrayFor(accumProbs, i, numChildren);
				// add prob to accum list of probs for RU
				probsForRU[childIdx] = 1.0f; // prob of prop to use IN succ from here (coverage prob is implicit in size of this vector)
			}
			
			// look for other non-killed RU's at successor's top
			BitSet bsUBackIn = nRUSucc.getUBackIn();
			for (int i = bsUBackIn.nextSetBit(0); i >= 0; i = bsUBackIn.nextSetBit(i + 1)) {
				if (bsUGen.get(i))
					continue; // only consider uses reachable from bottom of successor in this step
				
				Float probSuccBottom = child.probReachFromBottom.get(i);
				if (probSuccBottom == null)
					continue; // use flows through a backedge -- not in dag
				
				// get/create list of exclusive probabilities for RU
				final float[] probsForRU = getCreateFloatsArrayFor(accumProbs, i, numChildren);
				// add prob to accum list of probs for RU
				assert probsForRU[childIdx] == 0f;
				probsForRU[childIdx] = probSuccBottom; // prob of prop to use THROUGH succ from here (coverage prob is implicit in size of this vector)
			}
		}
		
		// DEBUG
		@Override public void printProbs() {
			printProbsHelper("NEW Prob reachable uses: ");
		}
		@Override protected String getReachObjId(Object oReach) {
			return ((Integer)oReach).toString();
		}
	}
	
	public static Map<Integer, Float> findSimpleDuaProbs(List<DUA> duas) {
		//The following two functions remains the same because they don't take any input but only calculate some info.
		findIntraDataDepProbs();
		findInterDataDepProbs();
		//The following function is changed to take DUA as input.
		//Map<DUA, Float> duaProbs = assignduaProbs(duas);
		Map<Integer, Float> duaProbs = assignEnToDefToUseProbs(duas);
			
//		System.out.println("Used/total/max memory after data-dep prob analysis finished: " +
//				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
//		freeMemory();
//		System.out.println("Used/total/max memory after freeing up data-dep prob analysis memory: " +
//				(Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory()) + " / " + Runtime.getRuntime().totalMemory() + " / " + Runtime.getRuntime().maxMemory());
		
		return duaProbs;
	}
	
	static Map<Integer, Float> condDuaProbs = new HashMap<Integer, Float>();
	
	public static Map<NodePoint, Float> findEnToPointCovProb(BackwardDependenceGraph depGraph){
		findIntraDataDepProbs();
		findInterDataDepProbs();
		
		// obtain entry cfg node
		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
		CFGNode nEntry = cfgMain.ENTRY;
		
		List<Dependence> candidateDeps = new ArrayList<Dependence>();
		List<NodePoint> nodePointList = new ArrayList<NodePoint>();
		// Get all the NodePoints
		NodePoint startPnt = depGraph.getStart();
		Set<Dependence> startDeps = depGraph.getInDeps(startPnt);
		for (Dependence dep : startDeps) 
			candidateDeps.add(dep);
		
		for(int i=0; i<candidateDeps.size(); i++)    {   
			//1. 
			Dependence dep = candidateDeps.get(i);   
			//2. Get its src and tgt
			NodePoint src = dep.getSrc();
			NodePoint tgt = dep.getTgt();
			//3. Add the src and tgt to nodePointList
			if(!nodePointList.contains(src))
				nodePointList.add(src);
			if(!nodePointList.contains(tgt))
				nodePointList.add(tgt);
			//4. add indeps of the dep
			for (Dependence inDep : depGraph.getInDeps(src)) {
				if(!candidateDeps.contains(inDep))	
					candidateDeps.add(inDep);
			}
		 }  
		
		Map<NodePoint, Float> enToPntCovProbs = new HashMap<NodePoint, Float>();
		float probEnToPnt = 0f;
		// Iterate through all the NodePoints and calculate the coverage probability of each.
		Iterator iter = nodePointList.iterator();
		while(iter.hasNext()){
			NodePoint pnt = (NodePoint) iter.next();
			if(pnt.getN() == nEntry)
				probEnToPnt = 1.0f;
			else
				probEnToPnt = ReachingProb.getInterprocCovProb(nEntry, true, pnt.getN());
			enToPntCovProbs.put(pnt, probEnToPnt);
		}
		return enToPntCovProbs;
	}
				
	public static Map<Integer, Float> findCondDuaProbs(List<DUA> duas) {
		//The following two functions remains the same because they don't take any input but only calculate some info.
		findIntraDataDepProbs();
		findInterDataDepProbs();

		Map<Integer, Float> enToDefToUseDuaIdProbs = assignEnToDefToUseProbs(duas);
		Map<Use, Float> enToUseProbs = assignEnToUseProbs(duas);

		// Iterate through enToDefToUseProbs map. 
		// For each duaId, search the EnToUseProb for its use, 
		// and calculate the final prob as enToDefToUseDuaIdProb/enToUseProb.
		// TO DO...
		Iterator iter = enToDefToUseDuaIdProbs.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry duaIdProbs = (Map.Entry)iter.next();
			Integer duaId = (Integer) duaIdProbs.getKey();
			Float prob = (Float) duaIdProbs.getValue();
			
			DUA dua = idDuas.get(duaId);
			Use use = dua.getUse();
			
			float enToDefToUseDuaIdCovProb = enToDefToUseDuaIdProbs.get(duaId);
			float enToDefToUseDuaIdProb = enToDefToUseDuaIdCovProb * (1-enToDefToUseDuaIdCovProb);
			float enToUseProb = enToUseProbs.get(use);
			
			if(enToUseProb!=0)
				assert enToDefToUseDuaIdProb <= enToUseProb;
			
			float condProb;
			if(enToUseProb!=0){
				condProb = enToDefToUseDuaIdProb / enToUseProb;
			}
			else
				condProb = 0f;

			condDuaProbs.put(duaId, condProb);
		}
		
		return condDuaProbs;
	}
	
	private static Map<CFG,ReachCFGDag> cfgToIntraDefUse = new HashMap<CFG, ReachCFGDag>();
	
	/** cfg->node->def->use->prob */
	private static void findIntraDataDepProbs() {
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			// do DFS of CFG to create dag and identify backedges
			ReachCFGDag dag = ReachCFGDefUseDag.buildCFGDefUseDag(cfg);
			dag.propagateProbs();
			
			cfgToIntraDefUse.put(cfg, dag);
			
			// DEBUG
			((ReachCFGDefUseDag) dag).printProbs();
		}
	}
	
	/** For each def and each use for that def, associates probability of coverage (given def is covered). */
	private static Map<Def, Map<Use, Float>> interDefUseProb = new HashMap<Def, Map<Use,Float>>();
	/** For each d, look for u intraprocedurally, then only forward through call sites, and then from d's containing method's EX (backward/forward in call graph).
	 *  The backward/forward way has two sub-cases: (1) u is located after a returning call site from EX or (2) u is forward-reachable from some CS after that returning CS.
	 *  These four ways are NOT exclusive -- d can reach u through all of them if there are intra, forward, and backward/forward paths. */
	private static void findInterDataDepProbs() {
		// prepare components of the probabilities for inter-procedural-DUA coverage
		ReachingProb.findReachingProbs();
		
		// match defs to uses and compute cov prob for each pair
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			if (ProbSliOptions.debugOut())
				System.out.println("NEW prob interproc def-use: " + cfg.getMethod());
			
			ReachableUsesDefs ruCFG = (ReachableUsesDefs) cfg;
			
			List<Def> allHeapDefs = new ArrayList<Def>(ruCFG.getFieldDefs());
			allHeapDefs.addAll(ruCFG.getArrayElemDefs());
			allHeapDefs.addAll(ruCFG.getLibObjDefs());
			
			for (Def def : allHeapDefs) {
				if (def.isInCatchBlock())
					continue;
				
				Map<Use, Float> useToProb = new HashMap<Use, Float>();
				interDefUseProb.put(def, useToProb);
				final boolean fromDefBottom = !def.getVar().isStrConstObj() || def.getN().getCallSite() == null || !def.getN().getCallSite().hasAppCallees();
				
				if (ProbSliOptions.debugOut())
					System.out.print(" Def " + def);
				for (Use use : DependenceFinder.getAllUsesForDef(def.getVar(), def.getN())) {
					final float probReachUse = ReachingProb.getInterprocCovProb(def.getN(), fromDefBottom, use.getSrcNode());
					final float probAliasDefUse = getAliasProb(def.getVar(), use.getVar());
//					assert probAliasDefUse > 0.0f;
					final float combinedProb = probAliasDefUse * probReachUse;
					if (ProbSliOptions.debugOut())
						System.out.print(" u " + use + ": " + combinedProb);
					
					if (combinedProb > 0.0f)
						useToProb.put(use, combinedProb);
				}
				if (ProbSliOptions.debugOut())
					System.out.println();
			}
		}
	}
	
	private static Map<DependenceFinder.Dependence, Float> ctrlDepProbs = new HashMap<DependenceFinder.Dependence, Float>();
	//private static BackwardDependenceGraph depGraph;
	/** Helper for ctor to fill the remainder of dep-prob map with ctrl deps probs. */
	public static Map<Dependence, Float> addControlDepProbs(BackwardDependenceGraph depGraph) {
		for (Dependence dep : depGraph.getDeps()) {
			if (dep instanceof ControlDependence){
				float ctrlProb = getControlDepProb((ControlDependence) dep);
//				ctrlProb = (float)Math.round(ctrlProb * 10000) / 10000;	
				ctrlDepProbs.put(dep, ctrlProb);
			}
		}
		return ctrlDepProbs;
	}
	
	/** Helper to compute propagation probability through a p-use(ctrl dep) in backward case */
	private static float getControlDepProb(ControlDependence dep) {
		NodePoint npUse = dep.getSrc();
		CFGNode nSrc = npUse.getN();
		final int numSuccs = nSrc.getSuccs().size();
		if (nSrc.hasAppCallees()) {
			assert numSuccs <= 1;
			final int numCallees = nSrc.getAppCallSite().getAllCallees().size();
			assert numCallees > 0;
			// probability covering target times prob of randomly switching to any of the other branch outcomes
			return (1.0f / numCallees) * ((float)numCallees - 1.0f) / numCallees;
		}
		
		assert numSuccs > 1; 
		// probability of covering successors times prob of randomly switching to any of the other branch outcomes
		return (1.0f / numSuccs) * ((float)numSuccs - 1.0f) / numSuccs;
		
		// Branch Prediction...
		// if u calls exit()
		//		for each successor prob(b->s)=0 
		// else if there are m back edges, and m < n, which is # of successors
		//		for each back edge prob(b->s) = taken_prob(LBH)/m
		//		for each exit edge successor prob(b->s) = not_taken_prob(LBH)/(n-m)
		// else if m>0 or n!=2
		//		for each successor prob(b->s)=1/n
		// else (2-way branch)
		//		prob(b->s1) = prob(b->s2) = 0.5
		//		for each heuristic H
			//		if (PH: a comparison of a pointer against null or of two pointer)
			//		else if (OH: a comparison of an integer for less than zero, less than or equal to zero, or equal to a constant)
			//		else if (GH: a register is an operand, the register is used before being defined in a successor block, and the successor block does not post-dominate will reach the successor block.)
			//		else if (LEH: a comparison in a loop in which no successor is a loop head)
			//		else if (LHH: a successor that is a loop header or a loop pre-header and does not post-dominate)
			//		else if (CH: a successor that contains a call and does not post-dominate)
			//		else if (SH: a successor that contains a store instruction and does not post-dominate)
			//		else if (RH: a successor that contains a return)
		//		H predicts s1 taken, and s2 not taken
		//		d = prob(b->s1) * taken_prob(H) + prob(b->s2) * not_taken_prob(H)
		//		prob(b->s1) = prob(b->s1) * taken_prob(H)/d
		//		prob(b->s2) = prob(b->s2) * not_taken_prob(H)/d
	}
	
	/** Computes probability that two objects with these p2 sets are the same at runtime. */
	private static float getAliasProb(Variable var1, Variable var2) {
		final BitSet p2Set1 = var1.getP2Set();
		final BitSet p2Set2 = var2.getP2Set();
		
		final int p2Card1 = p2Set1.cardinality();
		final int p2Card2 = p2Set2.cardinality();
		
		// handle cases where one or both p2 sets are empty
		if (p2Card1 == 0) {
			if (p2Card2 == 0) {
				Local lbase1 = var1.getBaseLocal();
				return (lbase1 != null && lbase1 == var2.getBaseLocal())? 1.0f : 0.0f;
			}
			return 1.0f + (p2Card2 - 1.0f)/p2Card2; // the greater card2, the greater the chance that var2 aliases var1
		}
		if (p2Card2 == 0)
			return 1.0f + (p2Card1 - 1.0f)/p2Card1; // the greater card1, the greater the chance that var1 aliases var2
		
		// use typical formula: size of p2 intersection / size of p2 union
		final BitSet bsInters = (BitSet) p2Set1.clone();
		bsInters.and(p2Set2);
		final BitSet bsUnion = (BitSet) p2Set1.clone();
		bsUnion.or(p2Set2);
		
		return ((float)bsInters.cardinality()) / bsUnion.cardinality();
	}
	
	public static Map<DUA, Integer> findduaIds(){
		//To get all duas. Copy from Forensics.java
		DUAAnalysis duaAnalysis = DUAAnalysis.inst();
		DUAssocSet duaSet = duaAnalysis.getDUASet();
		List<DUA> duas = duaSet.getAllDUAs();
		
		Map<DUA, Integer>duaIds = new HashMap<DUA, Integer>(); 
		int idCnt = 1; //duaId counter
		int duaId = 0;
		
		for (DUA dua : duas ){
			//To biuld the map for <dua,ID>
			duaIds.put(dua, idCnt);
			idCnt++;
			duaId = duaIds.get(dua);
		}
		return duaIds;
	}
	
	static Map<DUA, Integer> duaIds = new HashMap<DUA, Integer>(); 
	static Map<Integer, DUA> idDuas = new HashMap<Integer, DUA>();
	
	/** Assign to each dua in graph its coverage probability, and return a <duaID, prob> map.*/
	private static Map<Integer, Float> assignEnToDefToUseProbs(List<DUA> duas) { 	
		Map<DUA, Float> duaProbs = new HashMap<DUA, Float>();	//text version dua--prob
		Map<Integer, Float> duaIdProbs = new HashMap<Integer, Float>(); //index version dua--prob
		//Map<DUA, Integer>duaIds = new HashMap<DUA, Integer>(); 
		int idCnt = 0; //duaId counter
		
		// obtain entry cfg node
		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
		CFGNode nEntry = cfgMain.ENTRY;
		
		float probEnToDef;
		
		try {
		    FileWriter duaIdFile = new FileWriter(dua.util.Util.getCreateBaseOutPath() + "duaIdMap.txt", false);
		    
			for (DUA dua : duas ) {
				//To biuld the map for <dua,ID>
				duaIds.put(dua, idCnt);
				idDuas.put(idCnt, dua);
				int duaId = idCnt;
				idCnt++;
				
				Def def = dua.getDef();
				Use use = dua.getUse();
				
				Variable v = def.getVar();
				
				//Calculate prob from Entry to def
				//If the flag probEnToDef is ON then probEnToDef = 1.0f
				if (ProbStatDuaOptions.probEnToDef())
					probEnToDef = 1.0f;
				else				
					if(nEntry == def.getN())
						probEnToDef = 1.0f;
					else
						probEnToDef = ReachingProb.getInterprocCovProb(nEntry, true, def.getN());
				
				if (v.isLocal()) {  //IntraDua case
					//Node number in cfg.  
					NodeDefUses nDef = (NodeDefUses) def.getN();
					NodeDefUses nUse = (NodeDefUses) use.getSrcNode(); // null if p-use
					ReachableUsesDefs ruCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDef);
						
					// find def id  
					assert nDef.getLocalDefsIds().length == 1;
						
					// find use id 
					List<Use> uses = ruCFG.getUses();
					int uId = 0;
					// Debug
					System.out.println("uses:" + uses);
					for (Use u : uses) {
						if (u.getSrcNode() == nUse && u.getVar().equals(v)) {  // local vars
							assert u.getVar().isLocal();
							// Debug
							System.out.println("use:" + u);
							break;
						}
						++uId;
						// Debug
						System.out.println("uID:" + uId);
					}
					assert uId < uses.size();  
					
					float defToUseprob = 0f;
					float duaprob = 0f;
					
					// get intra def-use reachability prob from (bottom of) def node to use-id "object"
					if (use.getBranch() != null){ // if the use is p-use, multiply the prob by 0.5
						defToUseprob = cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDef).probReachFromBottom.get(uId) * 0.5f;
					}
					else 
						defToUseprob = cfgToIntraDefUse.get(ruCFG).objToDAGNode.get(nDef).probReachFromBottom.get(uId);
					
					duaprob = probEnToDef * defToUseprob;
				
					//defToUseProbs.put(dua, defToUseprob);
					duaProbs.put(dua, duaprob);
					duaIdProbs.put(duaId, duaprob);
					
					
				}
				else{ //InterDua case
					CFGNode nDef = def.getN();
					CFGNode nUse = use.getN();
					ReachableUsesDefs rCFG = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nDef);
						
					// search for heap def
					Def defFound = null;
					List<Def> defList = v.isArrayRef()? rCFG.getArrayElemDefs() : v.isFieldRef()? rCFG.getFieldDefs() : v.isObject()? rCFG.getLibObjDefs() : null;
					if (defList != null) {
						for (Def defi : defList) {
							if (defi.getN() == nDef && defi.getVar().mayEqualAndAlias(v)) {
								defFound = defi;
								break;
							}
						}
						assert defFound != null;
						
						// search for heap use
						Map<Use,Float> useToProb = interDefUseProb.get(defFound); // get interprocedural du-coverage probability
						Float defToUseprob = null;
						for (Use u : useToProb.keySet()) {
								if (u.getSrcNode() == nUse) {
									if (u.getBranch() != null){ // if the use is a p-use
										defToUseprob = useToProb.get(u) * 0.5f;
									}
									else
										defToUseprob = useToProb.get(u);
									
									break;
							}
						}
				
						if (defToUseprob != null)  // ignore deps with 0 prob (i.e., those for which a use wasn't added to def's list)
							duaIdProbs.put(duaId, probEnToDef * defToUseprob);
						else
							duaIdProbs.put(duaId, 0f);
					}
					else
						duaIdProbs.put(duaId, 0f);
				}
				
			}
			writeMapToFile(duaIds,dua.util.Util.getCreateBaseOutPath() + "duaIdMap.txt");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return duaIdProbs;				
	}
	
	/**
	 * Calculate the probability for each use that it is reached from the entry. Not through any certain def, but all paths. 
	 */
	private static Map<Use, Float> assignEnToUseProbs(List<DUA> duas){		
		Map<Use, Float> enToUseProbs = new HashMap<Use, Float>();	//text version dua--prob
		
		// obtain entry cfg node
		SootMethod mMain = ProgramFlowGraph.inst().getEntryMethods().get(0);
		CFG cfgMain = ProgramFlowGraph.inst().getCFG(mMain);
		CFGNode nEntry = cfgMain.ENTRY;
		
		float probEnToUse = 0f;
		
		for (DUA dua : duas) {
			Use use = dua.getUse();
			
			if(nEntry == use.getN())
				probEnToUse = 1.0f;
			else{
				probEnToUse = ReachingProb.getInterprocCovProb(nEntry, true, use.getN());
			}
			
			if(enToUseProbs.containsKey(use))
				probEnToUse = enToUseProbs.get(use) + probEnToUse;
			
			enToUseProbs.put(use, probEnToUse);
		}
		return enToUseProbs;
	}
	
	public static void printdefToUseProbs(Map<Integer, Float> duaIdProbs){
		System.out.println("======The probability of each DUA pair:======");
		
		Iterator iter = duaIdProbs.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry) iter.next();
			Object key = entry.getKey();  //DUA pair is the key
			Object val = entry.getValue(); //probability is the value
			System.out.println(key+":"+val);
		}
	}
	
	// Sort map REVERSELY -- value from high to low
	public static Map<Integer, Float> rankMapRev(Map<Integer, Float> duaIdProbs){
		Map<Integer, Float> sortedMap =  sortRevByComparator(duaIdProbs);
		return sortedMap;
	}
	
	private static Map<Integer, Float> sortRevByComparator(Map<Integer, Float> unsortMap) {
		 
        List<Entry<Integer, Float>> list = new LinkedList<Entry<Integer, Float>>(unsortMap.entrySet());
 
        //sort list based on comparator
        Collections.sort(list, new Comparator() {
             public int compare(Object o1, Object o2) {
	           return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
             }
        });
 
        //put sorted list into map again
        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
        	Map.Entry entry = (Map.Entry)it.next();
        	sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }	
	
	public static Map<Integer, Float> rankMap(Map<Integer, Float> duaIdProbs){
		Map<Integer, Float> sortedMap =  sortByComparator(duaIdProbs);
		return sortedMap;
	}
	
	// Sort map, value from low to high
	private static Map<Integer, Float> sortByComparator(Map<Integer, Float> unsortMap) {
		 
        List<Entry<Integer, Float>> list = new LinkedList<Entry<Integer, Float>>(unsortMap.entrySet());
 
        //sort list based on comparator
        Collections.sort(list, new Comparator() {
             public int compare(Object o1, Object o2) {
	           return ((Comparable) ((Map.Entry) (o1)).getValue()).compareTo(((Map.Entry) (o2)).getValue());
             }
        });
 
        //put sorted list into map again
        Map sortedMap = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext();) {
        	Map.Entry entry = (Map.Entry)it.next();
        	sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }	
	
	public static Map<Integer, Float> giveRanktoDua(Map<Integer, Float> sortedDuaProbMap){
		Map<Integer, Float> duaProbRankMap = new HashMap<Integer, Float>(); //<duaId -- Rank>

		// The following two list should have the same length
		List<Integer> idList = new ArrayList<Integer>(); // To store dua id one by one
		List<Float> probList = new ArrayList<Float>(); // to store the probs one by one
		List<Float> rankList = new ArrayList<Float>(); // To store the rank one by one

		List<Entry<Integer, Float>> list = new ArrayList<Entry<Integer, Float>>(sortedDuaProbMap.entrySet());
		Iterator iter = list.iterator();
		while(iter.hasNext()){
			Map.Entry entry = (Map.Entry)iter.next();
			Integer duaId = (Integer) entry.getKey();
			Float prob = (Float) entry.getValue();
			
			idList.add(duaId);
			probList.add(prob);
		}

		// Get the first element(prob) in the probList
		Iterator probIter = probList.iterator();
		Float curProb = (Float) probIter.next();
		
		float rank = 0;
		int i;
		int tieCnt = 1;
		int firstTiePos = 1; // First position for a tie case
		int curPos = 1; // Current Position. Rank can be got from #Position
		float nextProb = 0f;
		
		// Start from the second element
		while (probIter.hasNext()){	
			nextProb = (Float) probIter.next();
			if(nextProb==curProb){ // tie
				tieCnt += 1;
				curPos += 1; // Update the current position. Not update the first tie position
			}
			else{
				// Not calculate the rank for the LAST bunch of ties (might only have one element) until encounter a NEW value
				rank = (float)(firstTiePos + (firstTiePos + tieCnt - 1.0f)) * 0.5f;
				for(i=0; i<tieCnt; i++)
					rankList.add(rank);
				// Update curPos and firtiePos for the next step
				curPos += 1;
				firstTiePos = curPos;
				tieCnt = 1;
				curProb = nextProb;
			}
		}
		
		// If the last value in the probList is different from the second last value, the rank is not calculated above
		if (rankList.size() != probList.size()){
			rank = (float)(firstTiePos + (firstTiePos + tieCnt - 1.0f)) * 0.5f;
			for(i=0; i<tieCnt; i++)
				rankList.add(rank);
		}
		
		assert rankList.size() == probList.size();
		
		int duaId;
		for (i=0; i<idList.size(); i++){
			duaId = idList.get(i);
			rank = rankList.get(i);
			duaProbRankMap.put(duaId, rank);
		}
		return duaProbRankMap;
	}
	
	public static void writeMapToFile(Map duaIdProbs, String filepath) {  
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(filepath, false); //"C:\\dua\\ProbOutput\\StatDuaProb.txt"
			Set set = duaIdProbs.entrySet();
			Iterator iter = set.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
			fw.write(str.toString());	
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeListToFile(List<Entry<Integer, Float>> mapList, String filepath){
		try {
			String line = System.getProperty("line.separator");
			StringBuffer str = new StringBuffer();
			FileWriter fw = new FileWriter(filepath, false);
			
			Iterator<Entry<Integer, Float>> iter = mapList.iterator();
			while(iter.hasNext()){
				Map.Entry entry = (Map.Entry)iter.next(); 
				str.append(entry.getKey()+" : "+entry.getValue()).append(line);
			}
		
			fw.write(str.toString());
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	}
	
	/** To be used when dags and maps in this class are no longer needed. */
	private static void freeMemory() {
		ReachingProb.freeMemory();
		
		DuaProb.cfgToIntraDefUse.clear();
		DuaProb.cfgToIntraDefUse = null;
		
		DuaProb.interDefUseProb.clear();
		DuaProb.interDefUseProb = null;
		
		System.gc();
	}
	
}
