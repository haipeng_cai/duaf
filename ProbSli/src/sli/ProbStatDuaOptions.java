package sli;

import java.util.ArrayList;
import java.util.List;

public class ProbStatDuaOptions {
	private static List<Integer> startStmtIds = null;
	private static boolean fwdDynSliceInstr = false; // indicates forward dyn-slicing instrumentation instead of default exec-hist instrum
	private static boolean debugOut = false;
	private static int dist = 5; // default distance
	private static int backEdgeFreq = 4;
	private static boolean simpleLoops = false;
	private static boolean genAllToAll = false;
	private static boolean noPSlice = false;

	private static boolean probEnToDef = false;
	private static boolean simpleProb = false; // This strategy corresponds to IWPD12 PrioDU paper
	private static boolean condProb = false; // Strategy 2: conditional probability
	
	public static List<Integer> getStartStmtIds() { return startStmtIds; }
	public static boolean fwdDynSliceInstr() { return fwdDynSliceInstr; }
	public static boolean debugOut() { return debugOut; }
	public static int dist() { return dist; }
	public static int backEdgeFreq() { return backEdgeFreq; }
	public static boolean simpleLoops() { return simpleLoops; }
	public static boolean genAllToAll() { return genAllToAll; }
	public static boolean noPSlice() { return noPSlice; }

	public static boolean probEnToDef(){ return probEnToDef; }
	public static boolean simpleProb(){ return simpleProb; }// This strategy corresponds to IWPD12 PrioDU paper
	public static boolean condProb() { return condProb; }
	
	public static String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		boolean allowPhantom = true;
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.startsWith("-start:")) {
				assert startStmtIds == null;
				startStmtIds = dua.util.Util.parseIntList(arg.substring("-start:".length()));
			}
			//else if (arg.equals("-fdynslice"))
			//	fwdDynSliceInstr = true;
			//else if (arg.equals("-probslidbg"))
			//	debugOut = true;
			//else if (arg.startsWith("-dist")) {
			//	dist = Integer.parseInt(args[++i]);
			//	assert dist >= 0;
			//}
			else if (arg.equals("-nophantom"))
				allowPhantom = false;
			//else if (arg.equals("-simpleloops"))
			//	simpleLoops = true;
			else if (arg.equals("-genalltoall"))
				genAllToAll = true;
			//else if (arg.equals("-nopslice"))
			//	noPSlice = true;
			//else if (arg.startsWith("-befreq:"))
			//	backEdgeFreq = Integer.valueOf(arg.substring("-befreq:".length()));
			else if (arg.equals("-probentodef"))
				probEnToDef = true;
			else if (arg.equals("-simpleprob")) // This strategy corresponds to IWPD12 PrioDU paper
				simpleProb = true;
			else if ( arg.equals("-condprob"))
				condProb = true;
			else
				argsFiltered.add(arg);
		}
		
		if (allowPhantom)
			argsFiltered.add("-allowphantom");
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}
	
}
