package sli;

import java.util.Comparator;

import dua.global.dep.DependenceFinder.NodePoint;
import fault.StmtMapper;

/* This class is the comparator of the PntPrio class.
 * The comparing rule is:
 * first to sort it by the decreasing order of the priority;
 * If the priorities tie, then sort by the increasing order of stmtID.
 * */

public class PntPrioComparator implements Comparator<PntPrio<NodePoint, Float>>{

	public int compare(PntPrio<NodePoint, Float> pp1, PntPrio<NodePoint, Float> pp2){
		// Use the variable to later compare because otherwise the "==" case will never be true if we compare two Float objects.
		float prio1 = pp1.getPrio();
		float prio2 = pp2.getPrio();
		
		// Sort by decreasing order of prio
		if(prio1 > prio2){
			return -1;
		}
		if(prio1 < prio2){
			return 1;
		}
		if(prio1 == prio2){
			int stmtId1 = StmtMapper.getGlobalStmtId(pp1.getPnt().getN().getStmt());
			int stmtId2 = StmtMapper.getGlobalStmtId(pp2.getPnt().getN().getStmt());
			
			// Sort by increasing order of stmtID
			if(stmtId1 < stmtId2){
				return -1;
			}
			else{
				return 1;
			}
		}
		return 0;
	}
}
