package options;

import java.util.ArrayList;
import java.util.List;

/** Applicable when calling ReachProb directly, not from a client analysis, at least for now. */
public class ReachProbOptions {
	//private static boolean debugOut = false;
	//private static int backEdgeFreq = 4;
	
	//public static boolean debugOut() { return debugOut; }
	//public static int dist() { return dist; }
	
	public static String[] process(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		boolean allowPhantom = true; // override Forensics/Soot default, by default
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			if (arg.equals("-nophantom"))
				allowPhantom = false;
			//else if (arg.equals("-debugout"))
			//	debugOut = true;
			//else if (arg.startsWith("-befreq:"))
			//	backEdgeFreq = Integer.valueOf(arg.substring("-befreq:".length()));
			else
				argsFiltered.add(arg);
		}
		
		if (allowPhantom)
			argsFiltered.add("-allowphantom");
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}
	
}
