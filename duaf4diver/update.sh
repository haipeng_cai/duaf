#!/bin/bash

SRC=$HOME/workspace/DUAForensics/src/
DST=`pwd`
find $SRC -name "*.java" |\
	while read sfn;
	do
		# source java source 
		sjfn=${sfn##*/}
		# target java source 
		dfn=`find $DST -name "$sjfn"`
		cp -f $sfn $dfn
	done

#svn ci -m "DUAForensics adapted for Diver: updated to the latest"
#svn ci -m "DUAForensics adapted for Diver: cover more catch blocks (RequiredBranches)"
#svn ci -m "added 41 more p2 models that are looking important, selected from the reported missing models when running duaf with XMLsec, Schedule1 and Jmeter"
exit 0
