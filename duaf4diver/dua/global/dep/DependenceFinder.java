package dua.global.dep;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.jimple.FieldRef;

import dua.Options;
import dua.global.ProgramFlowGraph;
import dua.global.ReachabilityAnalysis;
import dua.method.CFG;
import dua.method.CFGDefUses;
import dua.method.CallSite;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.Def;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.method.ReachableUsesDefs.NodeReachDefsUses;
import dua.util.Pair;
import fault.StmtMapper;

public class DependenceFinder {
	/** Location within cfg/cdg node, distinguishing pre-execution of rhs (call) and post-rhs. */
	public static class NodePoint {
		/** Location for actual params used/defined if node has app call-site, and for string-const-object params (at app and lib calls). */
		public static int PRE_RHS = 0;
		/** Location for all vars if node has no call or a lib-call; returned var and defined var if it has an app call-site. */
		public static int POST_RHS = 1;
		private final CFGNode n;
		/** PRE_RHS or POST_RHS */
		private final int rhsPos;
		public CFGNode getN() { return n; }
		public int getRhsPos() { return rhsPos; }
		public NodePoint(CFGNode n, int rhsPos) { this.n = n; this.rhsPos = rhsPos; }
		@Override public int hashCode() { return n.hashCode() + rhsPos; }
		@Override public boolean equals(Object o) { return n == ((NodePoint)o).n && rhsPos == ((NodePoint)o).rhsPos; }
		@Override public String toString() { return StmtMapper.getGlobalNodeId(n) + "[" + rhsPos + "]"; }

		public static class NodePointComparator implements Comparator<NodePoint> {
			private NodePointComparator() {}
			public static final NodePointComparator inst = new NodePointComparator();
			public int compare(NodePoint p1, NodePoint p2) {
				final int id1 = StmtMapper.getGlobalNodeId(p1.getN());
				final int id2 = StmtMapper.getGlobalNodeId(p2.getN());
				return (id1 < id2)? -1 : (id1 > id2)? 1 : (p1.getRhsPos() == p2.getRhsPos())? 0 : (p1.getRhsPos() == PRE_RHS)? -1 : 1;
			}
		}
	}

	/** Base class for control and data dependencies. */
	public static abstract class Dependence {
		public static enum DepType { INTER, INTRA, FWD_LINK, BACK_LINK };
		protected final NodePoint src;
		protected final NodePoint tgt;

		public NodePoint getSrc() { return src; }
		public NodePoint getTgt() { return tgt; }

		public Dependence(NodePoint src, NodePoint tgt) { this.src = src; this.tgt = tgt; }

		public abstract DepType getType();

		/** Does the job for subclasses of checking that the other object is not null and that the dependence (sub)class is the same. */
		@Override public boolean equals(Object o) { return o != null && this.getClass() == o.getClass() && src.equals(((Dependence)o).src) && tgt.equals(((Dependence)o).tgt); }
		@Override public int hashCode() { return src.hashCode() + tgt.hashCode(); }
		@Override public String toString() { return src + "->" + tgt; }
		public abstract String toStringNoVarOrBrId();

		public boolean visited = false; // Used in W-slicer to track whether the dependence has been visited

		public static class DepComp implements Comparator<Dependence> {
			private final Map<Dependence, Integer> depToId;
			public DepComp(Map<Dependence, Integer> _depToId) { this.depToId = _depToId; }
			public int compare(Dependence d1, Dependence d2) {
				final int id1 = depToId.get(d1);
				final int id2 = depToId.get(d2);
				return (id1 < id2)? -1 : (id1 == id2)? 0 : 1;
			}
		}
	}
	/** Data dependence between two nodes for some (unspecified) variable of any type. */
	public static class DataDependence extends Dependence {
		/** Defined variable. */
		private final Variable v;
		/** Data dep can be of any of the four types. */
		private final DepType depType;
		public DataDependence(NodePoint src, NodePoint tgt, Variable v, DepType depType) { super(src, tgt); this.v = v; this.depType = depType; }
		public Variable getVar() { return v; }
		@Override public DepType getType() { return depType; }

		@Override public boolean equals(Object o) { return super.equals(o) && v.equals(((DataDependence)o).v); }
		@Override public int hashCode() { return super.hashCode() + v.hashCode(); }
		@Override public String toString() { return toStringNoVarOrBrId() + " " + v; }
		@Override public String toStringNoVarOrBrId() { return "d:" + super.toString(); }
	}
	/** Control dependence between two nodes, either classical (conditional->dependent) or call (cs->callee_dependent). */
	public static class ControlDependence extends Dependence {
		private final int brId;
		public int getBrId() { return brId; }
		public ControlDependence(NodePoint src, int brId, NodePoint tgt) { super(src, tgt); this.brId = brId; }
		public boolean isInterproc() { return src.getN().hasAppCallees(); }
		public int getNumAppCallees() { CallSite csApp = src.getN().getAppCallSite(); return (csApp==null)? 0 : csApp.getAppCallees().size(); }
		public int getNumLibCallees() { CallSite cs = src.getN().getCallSite(); return (cs==null)? 0 : cs.getLibCallees().size(); }
		/** Control dep can only be a fwd link (fwd CD-edge). */
		@Override public DepType getType() { return DepType.FWD_LINK; }

		@Override public boolean equals(Object o) { return super.equals(o) && brId == ((ControlDependence)o).brId; }
		@Override public int hashCode() { return super.hashCode() + brId; }
		@Override public String toString() { return toStringNoVarOrBrId() + " " + brId; }
		@Override public String toStringNoVarOrBrId() { return "c:" + super.toString(); }
	}

	/** Cache of defs globally found for var. */
	private static Map<Pair<CFGNode,Variable>, List<Pair<Def,Integer>>> defsForVars = new HashMap<Pair<CFGNode,Variable>, List<Pair<Def,Integer>>>();
	/** Returns all pairs <def,nodepos> of the used variable that (may) reach that use. Works for all types of variables,
	 *  but excludes param and return local-var links. */
	public static List<Pair<Def,Integer>> getAllDefsForUse(Variable varUse, CFGNode nUse) {
		//assert !nUse.isInCatchBlock(); // commented and changed to the below by hcai
		assert !dua.Options.ignoreCatchBlocks || !nUse.isInCatchBlock();

		List<Pair<Def,Integer>> allDefsForUse = null;

		// if const or local, don't need to check reachability
		if (varUse.isConstant()) {
			// there must be a const def at the same use node
			allDefsForUse = new ArrayList<Pair<Def,Integer>>();
			CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getContainingCFG(nUse);
			for (Def def : cfgDU.getConstDefs())
				if (def.getN() == nUse && def.getVar().mayEqualAndAlias(varUse))  // equality uses string equality distinction
					allDefsForUse.add(new Pair<Def, Integer>(def, nUse.hasAppCallees()? NodePoint.PRE_RHS :  NodePoint.POST_RHS));
		}
		else if (varUse.isLocal()) {  // find intraproc reaching defs for use (local var)
			NodeReachDefsUses nRDUuse = (NodeReachDefsUses) nUse;
			ReachableUsesDefs cfgRDUuse = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nRDUuse);
			BitSet bsRDsAtUse = varUse.isConstant()? nRDUuse.getDGen() : nRDUuse.getDFwdIn();

			// iterate over ALL reaching definitions for local var at use
			allDefsForUse = new ArrayList<Pair<Def,Integer>>();
			List<Def> allCFGDefs = cfgRDUuse.getDefs();
			for (int dId = 0; dId < allCFGDefs.size(); ++dId) {
				if (bsRDsAtUse.get(dId)) {
					Def def = allCFGDefs.get(dId);
					//if (def.isInCatchBlock())  // it's always possible to find reaching defs from catch blocks // commented and changed to the below by hcai
					if (dua.Options.ignoreCatchBlocks && def.isInCatchBlock())	
						continue;
					if (def.getVar().mayEqualAndAlias(varUse))
						allDefsForUse.add(new Pair<Def, Integer>(def, NodePoint.POST_RHS));
				}
			}
		}
		else if (varUse.isFieldRef() || varUse.isArrayRef() || varUse.isObject()) {
			// retrieve from cache
			Pair<CFGNode,Variable> useNodeAndVar = new Pair<CFGNode,Variable>(nUse,varUse);
			allDefsForUse = defsForVars.get(varUse);
			// find defs if not in cache
			if (allDefsForUse == null) {
				allDefsForUse = new ArrayList<Pair<Def,Integer>>();
				defsForVars.put(useNodeAndVar, allDefsForUse); // store defs-list in cache

				List<CFG> cfgs = getProximityCFGs(ProgramFlowGraph.inst().getContainingCFG(nUse)); //ProgramFlowGraph.inst().getCFGs();
				if (varUse.isFieldRef()) {
					for (CFG cfg : cfgs) {
						final boolean isClInit = cfg.getMethod().getName().equals("<clinit>");
						for (Def def : ((CFGDefUses)cfg).getFieldDefs())
							//if (!def.isInCatchBlock() && def.getVar().mayEqualAndAlias(varUse)) // commented out and changed to the below by hcai
							if ( (!dua.Options.ignoreCatchBlocks || !def.isInCatchBlock()) && def.getVar().mayEqualAndAlias(varUse))	
								addDefIfReachesFromBottom(allDefsForUse, def, nUse, isClInit);
					}
				}
				else if (varUse.isArrayRef()) {
					for (CFG cfg : cfgs) {
						final boolean isClInit = cfg.getMethod().getName().equals("<clinit>");
						for (Def def : ((CFGDefUses)cfg).getArrayElemDefs())
							//if (!def.isInCatchBlock() && def.getVar().mayEqualAndAlias(varUse)) // commented out and changed to the below by hcai
							if ((!dua.Options.ignoreCatchBlocks || !def.isInCatchBlock()) && def.getVar().mayEqualAndAlias(varUse))	
								addDefIfReachesFromBottom(allDefsForUse, def, nUse, isClInit);
					}
				}
				else if (varUse.isObject()) {
					if (varUse.isStrConstObj()) {
						// special case: StringConstant use (i.e., intra-lib-call) only has const-arg def at same node
						CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getContainingCFG(nUse);
						for (Def def : cfgDU.getLibObjDefs()) {
							final boolean isClInit = cfgDU.getMethod().getName().equals("<clinit>");
							//if (!def.isInCatchBlock() && def.getN() == nUse && def.getVar().mayEqualAndAlias(varUse)) {  // uses string equality // commented out and changed to the below by hcai
							if ( (!dua.Options.ignoreCatchBlocks || !def.isInCatchBlock()) && def.getN() == nUse && def.getVar().mayEqualAndAlias(varUse)) {	
								addDefIfReachesFromTop(allDefsForUse, def, nUse, isClInit);
								break; // note that there might be more than 1 const-string arg with same string; def does not distinguish which one
							}
						}
					}
					else {
						for (CFG cfg : cfgs) {
							final boolean isClInit = cfg.getMethod().getName().equals("<clinit>");
							for (Def def : ((CFGDefUses)cfg).getLibObjDefs())
								//if (!def.isInCatchBlock() && def.getVar().mayEqualAndAlias(varUse)) // commented out and changed to the below by hcai
								if ((!dua.Options.ignoreCatchBlocks || !def.isInCatchBlock()) && def.getVar().mayEqualAndAlias(varUse))	
									addDefIfReachesFromBottom(allDefsForUse, def, nUse, isClInit);
						}
					}
				}
			}
		}
		else { // shouldn't get here, anyway
			assert false; // variable type not supported
		}

		// unfortunately, there might be no defs if def/use models are missing...
//		assert !allDefsForUse.isEmpty();
		return allDefsForUse;
	}
	/** Helper for getAllUsesForDef: filters out defs that don't reach use from bottom. */
	private static void addDefIfReachesFromBottom(List<Pair<Def,Integer>> defs, Def d, CFGNode nUse, boolean forceAdd) {
		if (forceAdd || ReachabilityAnalysis.reachesFromBottom(d.getN(), nUse, true))
			defs.add(new Pair<Def, Integer>(d, d.getVar().isStrConstObj()? NodePoint.PRE_RHS : NodePoint.POST_RHS)); //NodePoint.POST_RHS));
	}
	/** Helper for getAllUsesForDef: filters out defs that don't reach use from top. */
	private static void addDefIfReachesFromTop(List<Pair<Def,Integer>> defs, Def d, CFGNode nUse, boolean forceAdd) {
		assert d.getVar().isObject(); // used in this context, for now at least
		if (forceAdd || ReachabilityAnalysis.reachesFromTop(d.getN(), nUse, true))
			defs.add(new Pair<Def, Integer>(d, d.getVar().isStrConstObj()? NodePoint.PRE_RHS : NodePoint.POST_RHS));
	}

	private static interface DefRetriever { List<Def> getDefs(CFGDefUses cfg); }
	private final static class FieldDefRetriever implements DefRetriever { public List<Def> getDefs(CFGDefUses cfg) { return cfg.getFieldDefs(); } }
	private final static class ArrayElemDefRetriever implements DefRetriever { public List<Def> getDefs(CFGDefUses cfg) { return cfg.getArrayElemDefs(); } }
	private final static class ObjectDefRetriever implements DefRetriever { public List<Def> getDefs(CFGDefUses cfg) { return cfg.getLibObjDefs(); } }

	/** Cache mapping each field/array/object def to all its possible uses in the program; null means 'not computed yet'. */
	private static Map<Pair<Variable,CFGNode>,List<Use>> usesForDefs = null;
//	private static Map<Variable,List<Pair<Variable,CFGNode>>> defNodesForFieldVars = null;
//	private static Map<Variable,List<Pair<Variable,CFGNode>>> defNodesForArrayVars = null;
//	private static Map<Variable,List<Pair<Variable,CFGNode>>> defNodesForObjectVars = null;
	private static void ensureAllUsesForDefsFound() {
		if (usesForDefs != null)
			return; // already created
		usesForDefs = new HashMap<Pair<Variable,CFGNode>, List<Use>>();

//		defNodesForFieldVars = new HashMap<Variable,List<Pair<Variable,CFGNode>>>();
//		defNodesForArrayVars = new HashMap<Variable,List<Pair<Variable,CFGNode>>>();
//		defNodesForObjectVars = new HashMap<Variable,List<Pair<Variable,CFGNode>>>();
//
//		// 1. Collect all defs
//		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
//			for (CFGNode n : cfg.getNodes()) {
//				if (n.isInCatchBlock() || n.isSpecial())
//					continue;
//				for (Variable vDef : ((NodeDefUses)n).getDefinedVars()) {
//					// associate defined var to def (var,node pair)
//					if (vDef.isFieldRef()) {
//						// *** TODO: the definition of this$0 *SHOULD* be included
//						//   special case: ignore writing of reference to parent (arg 1) in nested class ctor
//						final String fldName = ((FieldRef)vDef.getValue()).getField().getName();
//						if (fldName.equals("class$0") || fldName.equals("this$0"))
//							continue; // skip
//						// get/create list of defs for this var and add def to that list
//						List<Pair<Variable,CFGNode>> defNodes = defNodesForFieldVars.get(vDef);
//						if (defNodes == null)
//							defNodesForFieldVars.put(vDef, defNodes = new ArrayList<Pair<Variable,CFGNode>>());
//						defNodes.add(new Pair<Variable, CFGNode>(vDef, n));
//					}
//					else if (vDef.isArrayRef()) {
//						// get/create list of defs for this var and add def to that list
//						List<Pair<Variable,CFGNode>> defNodes = defNodesForArrayVars.get(vDef);
//						if (defNodes == null)
//							defNodesForArrayVars.put(vDef, defNodes = new ArrayList<Pair<Variable,CFGNode>>());
//						defNodes.add(new Pair<Variable, CFGNode>(vDef, n));
//					}
//					else if (vDef.isObject()) {
//						// get/create list of defs for this var and add def to that list
//						List<Pair<Variable,CFGNode>> defNodes = defNodesForObjectVars.get(vDef);
//						if (defNodes == null)
//							defNodesForObjectVars.put(vDef, defNodes = new ArrayList<Pair<Variable,CFGNode>>());
//						defNodes.add(new Pair<Variable, CFGNode>(vDef, n));
//					}
//				}
//			}
//		}

		// 2. Traverse all uses and associate each one to matching defs
		FieldDefRetriever fldDefRetriever = new FieldDefRetriever();
		ArrayElemDefRetriever arrelDefRetriever = new ArrayElemDefRetriever();
		ObjectDefRetriever objDefRetriever = new ObjectDefRetriever();
		final int DOTS_PER_ROW = 60;
		int dotCount = DOTS_PER_ROW;
		System.out.println("Matching interprocedural field/array/object defs with uses");
		for (CFG cfg : ProgramFlowGraph.inst().getCFGs()) {
			// case 2.1: fields
			associateUsesToDefs(((CFGDefUses)cfg).getFieldUses(), fldDefRetriever);
			// case 2.2: array elements
			associateUsesToDefs(((CFGDefUses)cfg).getArrayElemUses(), arrelDefRetriever);
			// case 2.3: objects
			associateUsesToDefs(((CFGDefUses)cfg).getLibObjUses(), objDefRetriever);

			System.out.print('.');
			if (--dotCount == 0) {
				dotCount = DOTS_PER_ROW;
				System.out.println();
			}
		}
		System.out.println("Finished matching defs to uses: " + (new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date())));
	}
	/** Helper for {@link #ensureAllUsesForDefsFound()} that finds all defs matching this use and adds this use to list of uses for each of those defs. */
	private static void associateUsesToDefs(List<Use> usesInCFG, DefRetriever defRetriever) {
		for (Use use : usesInCFG) {
			//if (use.isInCatchBlock())// commented out and changed to the below by hcai
			if (dua.Options.ignoreCatchBlocks && use.isInCatchBlock())// commented out and changed to the below by hcai	
				continue;
			Variable vUse = use.getVar();
			for (CFG cfgDef : ProgramFlowGraph.inst().getCFGs()) {
				for (Def def : defRetriever.getDefs((CFGDefUses)cfgDef)) {
					//if (def.isInCatchBlock()) // commented out and changed to the below by hcai
					if (dua.Options.ignoreCatchBlocks && def.isInCatchBlock())
						continue;

					Variable vDef = def.getVar();

					// *** TODO: the definition of this$0 *SHOULD* be included
					//   special case: ignore writing of reference to parent (arg 1) in nested class ctor
					if (vDef.isFieldRef()) {
						final String fldName = ((FieldRef)vDef.getValue()).getField().getName();
						if (fldName.equals("class$0") || fldName.equals("this$0"))
							continue; // skip
					}

					if (vUse.mayEqualAndAlias(vDef)) {
						final boolean isClInit = cfgDef.getMethod().getName().equals("<clinit>");
						// get/create uses list for this def, and add use to it
						CFGNode nDef = def.getN();
						Pair<Variable,CFGNode> defVarN = new Pair<Variable,CFGNode>(vDef, nDef);
						List<Use> usesForThisDef = usesForDefs.get(defVarN);
						if (usesForThisDef == null)
							usesForDefs.put(defVarN, usesForThisDef = new ArrayList<Use>());
						addUseIfReachable(usesForThisDef, use, nDef, isClInit);
					}
				}
			}
		}
	}

	/** Returns all uses that the given definition can reach. Works for all types of variables. NEVER returns null, but empty list if no uses found. */
	public static List<Use> getAllUsesForDef(Variable varDef, CFGNode nDef) {
		List<Use> allUsesForDef = null;

		if (varDef.isLocal()) {
			// find intraproc reachable uses for def (local var)
			NodeReachDefsUses nRDUdef = (NodeReachDefsUses) nDef;
			ReachableUsesDefs cfgRDUdef = (ReachableUsesDefs) ProgramFlowGraph.inst().getContainingCFG(nRDUdef);
			BitSet bsRUsAtDef = nRDUdef.getUBackOut();

			// iterate over ALL reachable uses of local var at def
			allUsesForDef = new ArrayList<Use>();
			List<Use> allCFGUses = cfgRDUdef.getUses();
			for (int uId = 0; uId < allCFGUses.size(); ++uId) {
				if (bsRUsAtDef.get(uId)) {
					Use u = allCFGUses.get(uId);
					//if (u.isInCatchBlock()) // commented out and changed to the below by hcai
					if (dua.Options.ignoreCatchBlocks && u.isInCatchBlock())
						continue;
					if (u.getVar().mayEqualAndAlias(varDef))
						allUsesForDef.add(u); // no need to check reachability; definition of local REACHES use
				}
			}
		}
		else if (varDef.isFieldRef() || varDef.isArrayRef() || varDef.isObject()) {
			ensureAllUsesForDefsFound(); // make sure that usesForDefs is created

			allUsesForDef = usesForDefs.get(new Pair<Variable,CFGNode>(varDef, nDef)); // null if there is no use for definition
			if (allUsesForDef == null)
				allUsesForDef = new ArrayList<CFGDefUses.Use>();
		}
		else { // shouldn't get here, anyway
			assert false; // variable type not supported
		}

		return allUsesForDef;
	}

	/** Adds use to provided list if use is reachable from bottom.
	 *  @param forceAdd forces use to be added even if not reachable from def according to graph-based analysis (e.g., if def is in clinit method). */
	private static void addUseIfReachable(List<Use> uses, Use u, CFGNode nDef, boolean forceAdd) {
		if (forceAdd || ReachabilityAnalysis.reachesFromBottom(nDef, u.getSrcNode(), true))
			uses.add(u);
	}

	private static Map<CFG,List<CFG>> mapNearbyCFGs = new HashMap<CFG, List<CFG>>();
	/** Gets the (sorted) list of CFGs within certain distance parameters in the call graph. */
	private static List<CFG> getProximityCFGs(CFG cfg) {
		List<CFG> nearbyCFGsList = mapNearbyCFGs.get(cfg);
		if (nearbyCFGsList == null) {
			// special case: clinit
			if (cfg.getMethod().getName().equals("<clinit>"))
				nearbyCFGsList = ProgramFlowGraph.inst().getCFGs();  // all CFGs are in the proximity of a class
			else {
				// 1) add this cfg, to start with
				Set<CFG> nearbyCFGs = new HashSet<CFG>();
				nearbyCFGs.add(cfg);
				// 2) add backwards cfgs, up to proximity distance
				getBackCFGs(cfg, Options.getProxBackCFGs(), nearbyCFGs); // );
				// 3) add all fwd cfgs from each cfg found so far, up to max fwd distance
				for (CFG cfgFrom : (Set<CFG>) ((HashSet<CFG>)nearbyCFGs).clone())
					getFwdCFGs(cfgFrom, Options.getProxFwdCFGs(), nearbyCFGs);

				// transform set into sorted list and store it in cache map
				nearbyCFGsList = new ArrayList<CFG>(nearbyCFGs);
				Collections.sort(nearbyCFGsList, CFG.comp);
			}
			mapNearbyCFGs.put(cfg, nearbyCFGsList);
		}

		return nearbyCFGsList;
	}

	private static void getBackCFGs(CFG cfg, int distLeft, Set<CFG> result) {
		if (distLeft == 0)
			return;

		for (CFG cfgBack : cfg.getCallgraphPreds()) {
			if (result.add(cfgBack))
				getBackCFGs(cfgBack, distLeft == -1? -1 : distLeft - 1, result);
		}
	}
	private static void getFwdCFGs(CFG cfg, int distLeft, Set<CFG> result) {
		if (distLeft == 0)
			return;

		for (CFG cfgFwd : cfg.getCallgraphSuccs()) {
			if (result.add(cfgFwd))
				getFwdCFGs(cfgFwd, distLeft == -1? -1 : distLeft - 1, result);
		}
	}

}
