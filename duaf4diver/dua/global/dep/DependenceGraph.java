package dua.global.dep;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.Local;
import soot.SootMethod;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AssignStmt;
import soot.jimple.Constant;
import soot.jimple.RetStmt;
import soot.jimple.ReturnStmt;
import soot.jimple.Stmt;
import dua.Options;
import dua.global.ProgramFlowGraph;
import dua.global.ReachabilityAnalysis;
import dua.global.ReqBranchAnalysis;
import dua.global.dep.DependenceFinder.ControlDependence;
import dua.global.dep.DependenceFinder.DataDependence;
import dua.global.dep.DependenceFinder.Dependence;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.Dependence.DepType;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.method.CFG;
import dua.method.CFGDefUses;
import dua.method.CallSite;
import dua.method.MethodTag;
import dua.method.ReachableUsesDefs;
import dua.method.CFG.CFGNode;
import dua.method.CFGDefUses.Branch;
import dua.method.CFGDefUses.CSArgVar;
import dua.method.CFGDefUses.NodeDefUses;
import dua.method.CFGDefUses.ReturnVar;
import dua.method.CFGDefUses.StdVariable;
import dua.method.CFGDefUses.Use;
import dua.method.CFGDefUses.Variable;
import dua.util.Pair;

/** Provides a graph of all dependences forward from a CFG node. Corresponds to the static forward slice from that node.
 *  
 *  Ignores target nodes, for now -- Excludes dependencies not reachable from source or that do not reach target. */
public class DependenceGraph {
	private static final boolean debugOut = false;
	
	/** Start point. */
	private final NodePoint pointStart;
	/** End points. */
	private Set<NodePoint> pointEnds;
	/** Map node->{out-deps} */
	private Map<NodePoint, Set<Dependence>> pntToDeps = new HashMap<NodePoint, Set<Dependence>>();
	/** Used during each of the computation phases of the dependence graph to avoid re-visiting points. */
	private Set<NodePoint> visitedPoints = new HashSet<NodePoint>();
	/** Caches all dependences (edges) in graph. */
	private Set<Dependence> deps = new HashSet<Dependence>();
	
	public NodePoint getStart() { return pointStart; }
	public Set<NodePoint> getEnds() { return pointEnds; }
	public Set<Dependence> getOutDeps(NodePoint p) { return pntToDeps.get(p); }
	public Set<Dependence> getDeps() { return deps; }
	
	public DependenceGraph(NodePoint pStart, int dist) {
		this(pStart, new ArrayList<NodePoint>(), dist);
	}
	
	/** Builds dep graph by finding all transitive dependencies from start point to target.
	 *  Excludes dependencies not reachable from source or that do not reach target.
	 *  
	 *  TODO: Fix imprecision due to data dependence to pre-rhs param use linking to other param defs or str-const defs.
	 */
	public DependenceGraph(NodePoint pStart, List<NodePoint> pTargets, int dist) {
		this.pointStart = pStart;
		this.pointEnds = new HashSet<NodePoint>(pTargets);
		
		// Get all dependencies (data and control) from start point that can affect target
		Set<Pair<NodePoint,Integer>> worklist = new HashSet<Pair<NodePoint,Integer>>(); // nodes from which originate dependences that have to be added
		Set<Pair<NodePoint,Integer>> worklistHist = new HashSet<Pair<NodePoint,Integer>>(); // keeps track of all processed elements for the next phase
		worklist.add(new Pair<NodePoint,Integer>(pStart, dist));
		
		doDepSearchPhase(worklist, worklistHist, false); // if ctx sensitive, only move backward in call graph (don't enter called methods)
		System.out.println("Fwd slice after phase 1: # deps " + deps.size() + " # points " + pntToDeps.keySet().size());
		visitedPoints.clear();
		if (!Options.sliceCtxInsens()) {  // second phase only needed if context sensitive
			doDepSearchPhase(worklistHist, null, true); // only move forward in call graph (don't return to caller methods)
			System.out.println("Fwd slice after phase 2: # deps " + deps.size() + " # points " + pntToDeps.keySet().size());
		}
		
		if (debugOut) {
			// DEBUG
	//		System.out.println("Fwd slice output: # deps " + deps.size() + " # points " + pntToDeps.keySet().size());
			System.out.print('s'); // indicates start point
			printFwdSlice(pointStart, "", new HashSet<NodePoint>());
			System.out.println();
		}
	}
	private void printFwdSlice(NodePoint p, String prefix, Set<NodePoint> visited) {
		if (!visited.add(p)) {
			System.out.print("@" + p);
			return;
		}
		
		final String sP = p + "->";
		System.out.print(sP);
		Set<Dependence> outDeps = pntToDeps.get(p);
		if (outDeps.isEmpty())
			System.out.print("END");
		else {
			List<NodePoint> pSuccs = new ArrayList<NodePoint>();
			Map<NodePoint,Integer> pSuccToDepClass = new HashMap<NodePoint, Integer>(); // 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
			for (Dependence dep : outDeps) {
				NodePoint pTgt = dep.getTgt();
				pSuccs.add(pTgt);
				final boolean isDataDep = dep instanceof DataDependence;
				
				// decide dep type -- 0 = ctrl only, 1 = data only, 2 = both (rare case, but possible)
				Integer clsOld = pSuccToDepClass.get(pTgt);
				Integer clsNew;
				if (clsOld == null)
					clsNew = isDataDep? 1 : 0;
				else if (clsOld == 0)
					clsNew = isDataDep? 2 : 0;
				else if (clsOld == 1)
					clsNew = isDataDep? 1 : 2;
				else {
					assert clsOld == 2;
					clsNew = 2;
				}
				pSuccToDepClass.put(pTgt, clsNew);
			}
			Collections.sort(pSuccs, NodePointComparator.inst);
			
			// create next prefix with empty spaces matching length of last arrow printed
			char[] nextPreffixChars = new char[prefix.length() + sP.length() + 1]; // extra 1 for c/d/b dep class indicator
			Arrays.fill(nextPreffixChars, ' ');
			String nextPrefix = new String(nextPreffixChars);
			boolean first = true;
			for (NodePoint pSucc : pSuccs) {
				if (first)
					first = false;
				else
					System.out.print("\n" + nextPrefix);
				final int depCls = pSuccToDepClass.get(pSucc);
				System.out.print((depCls == 0)? 'c' : (depCls == 1)? 'd' : 'b');
				printFwdSlice(pSucc, nextPrefix, visited);
			}
		}
	}
	
	private void doDepSearchPhase(Set<Pair<NodePoint, Integer>> worklist, Set<Pair<NodePoint, Integer>> worklistHist, boolean fwdOnly) {
		final boolean ctxInsens = Options.sliceCtxInsens();
		while (!worklist.isEmpty()) {
			// extract next dependence-source node
			Pair<NodePoint,Integer> pntAndDist = worklist.iterator().next();
			worklist.remove(pntAndDist);
			if (worklistHist != null)
				worklistHist.add(pntAndDist);
			
			final int currDist = pntAndDist.second();
			NodePoint pSrc = pntAndDist.first();
			
			CFGNode nSrc = pSrc.getN();
			final int posSrc = pSrc.getRhsPos();
			CallSite csSrc = nSrc.getCallSite();
			CFGDefUses cfgDU = (CFGDefUses) ProgramFlowGraph.inst().getContainingCFG(nSrc);
			
			// create set of out-deps for this node;   NOT ANYMORE --- node should be visited only once!
			if (!pntToDeps.containsKey(pSrc))
				pntToDeps.put(pSrc, new HashSet<Dependence>()); // starts empty
			visitedPoints.add(pSrc);
			
			// NOTE: for now, assume that all var(s) defined at a visited node or the predicate in that node are affected
			
			// 1. Data dependencies from n
			// 1.1 Regular variable dependencies
			for (Variable vDef : ((NodeDefUses)nSrc).getDefinedVars()) {
				assert !(nSrc.getStmt() instanceof ReturnStmt) && !(nSrc.getStmt() instanceof RetStmt); // ensure ReturnStmt is used, but never RetStmt
				assert !vDef.isConstant();
				// def can't be actual param, so it is "post-rhs", except for string-const-params which are "pre-rhs"
				final int rhsPosDef = vDef.isStrConstObj()? NodePoint.PRE_RHS : NodePoint.POST_RHS;
				if (posSrc != rhsPosDef)
					continue;
				
				List<Use> usesForDef = DependenceFinder.getAllUsesForDef(vDef, nSrc);
				for (Use u : usesForDef) {
					// use of a param/return var for app call is "pre-rhs"; if it's not a call or it's a lib-call, use is (also) "post-rhs"
					CFGNode nUse = u.getSrcNode();
					CallSite csUse = nUse.getCallSite();
					// use of a param/return var for app call is "pre-rhs" (might be in *addition* to lib-call 'post-rhs')
					DepType depType;
					if (vDef.isLocal())
						depType = DepType.INTRA;
					else {
						depType = DepType.INTER; // regular (non-link) data dep type
						if (!ctxInsens) {
							if (fwdOnly) {
								if (!isFwdReachable(nSrc, u.getSrcNode()))
									continue;
							}
							else {
								if (!isBackFwdReachable(nSrc, u.getSrcNode()))
									continue;
							}
						}
					}
					
					// IMPORTANT: if there are BOTH app and lib targets, there will be both a pre AND a post use (i.e., two dependencies)
					if (csUse != null && csUse.hasAppCallees())
						createDataDep(new NodePoint(nSrc, rhsPosDef), new NodePoint(nUse, NodePoint.PRE_RHS), vDef, depType, worklist, currDist);
					// if use is not a call or it's a lib-call, use is "post-rhs"
					if (csUse == null || csUse.hasLibCallees())
						createDataDep(new NodePoint(nSrc, rhsPosDef), new NodePoint(nUse, NodePoint.POST_RHS), vDef, depType, worklist, currDist);
				}
			}
			// 1.2 Data dependencies linking actual-formal parameters
			if ((fwdOnly || ctxInsens) && posSrc == NodePoint.PRE_RHS && csSrc != null) {
				for (SootMethod mCallee : csSrc.getAppCallees()) {  // link to each identity stmt (formal param decl) in callee
					ReachableUsesDefs ruCFGCallee = (ReachableUsesDefs) ProgramFlowGraph.inst().getCFG(mCallee);
					for (int i = 0; i < ruCFGCallee.getNumFormalParams(); ++i) {
						// NOTE: look at actual/formal params lists, because constants are not considered as uses by NodeDefUses.getUsedVars
						Stmt sIdFormal = ruCFGCallee.getFormalParam(i).getIdStmt();
						Value val = csSrc.getActualParam(i);
						Variable argVar = (val instanceof Constant)? new CSArgVar((Constant)val, csSrc, i) : new StdVariable(val);
						createDataDep(new NodePoint(nSrc, NodePoint.PRE_RHS), // def occurs "before" call
								      new NodePoint(ProgramFlowGraph.inst().getNode(sIdFormal), NodePoint.POST_RHS), // use is in Identity stmt
								      argVar, DepType.FWD_LINK, worklist, currDist);
					}
				}
			}
			// 1.3 Data dependencies linking return-callsite values.
			Stmt s = nSrc.getStmt();
			if ((!fwdOnly || ctxInsens) && s instanceof ReturnStmt) {
				assert posSrc == NodePoint.POST_RHS;
				ReturnStmt sRet = (ReturnStmt)s;
				if (!sRet.getUseBoxes().isEmpty()) {  // returns a value (i.e., it's not a simple 'return' stmt in 'void' method)
					assert sRet.getUseBoxes().size() == 1;
					SootMethod mFrom = ProgramFlowGraph.inst().getContainingMethod(s);
					MethodTag mTag = (MethodTag) mFrom.getTag(MethodTag.TAG_NAME);
					for (CallSite csCaller : mTag.getCallerSites()) {
						if (!csCaller.isReachableFromEntry())
							continue;
						Stmt sCaller = csCaller.getLoc().getStmt();
						if (sCaller instanceof AssignStmt) {
							// there should be one and only one returned local/const
							// NOTE: look at (unique) use box instead of NodeDefUses.getUsedVars which only considers vars, not constants
							Value vRet = ((ValueBox)sRet.getUseBoxes().iterator().next()).getValue();
							assert vRet instanceof Constant || vRet instanceof Local;
							Variable varToReturn = new ReturnVar(vRet, nSrc);
							assert varToReturn.isLocalOrConst();
							createDataDep(new NodePoint(nSrc, NodePoint.POST_RHS), // return stmt has no call inside
										  new NodePoint(ProgramFlowGraph.inst().getNode(sCaller), NodePoint.POST_RHS), // use occurs after call return
										  varToReturn, DepType.BACK_LINK, worklist, currDist);
						}
					}
				}
			}
			
			// 2. Control dependencies from n
			// 2.1 Intraproc: for each node->br_set in CFG of n, look for nTgt's that have a CD br whose source is n
			for (CFGNode nCtrlTgt : cfgDU.getNodes()) {
				if (nCtrlTgt.isInCatchBlock() || nCtrlTgt.toString().equals("EX"))
					continue; // ignore catch blocks (FOR NOW) and exit nodes, which don't contribute anything
				Set<Branch> brsForPossibleTgt = ReqBranchAnalysis.inst().getCDBranches(nCtrlTgt);
				for (Branch br : brsForPossibleTgt) {
					if (br.getSrc() == nSrc) {
						assert posSrc == NodePoint.POST_RHS;
						
						final int brId = nSrc.getOutBranches().indexOf(br);
						assert brId >= 0;
						
						// add CD to 'pre' point if there is an app call
						CallSite csTgt = nCtrlTgt.getCallSite();
						if (csTgt != null && csTgt.hasAppCallees()) {
							createControlDep(new NodePoint(nSrc, NodePoint.POST_RHS), brId, new NodePoint(nCtrlTgt, NodePoint.PRE_RHS), worklist, currDist);
							
							// Transitive interproc: if tgt is a multi-target CS, add callee nodes ctrl-dependent on entry branch of their respective methods
							if ((fwdOnly || ctxInsens) && csTgt.getAppCallees().size() == 1 && !csTgt.hasLibCallees())
								addInterProcCtrlDeps(pSrc, brId, csTgt.getAppCallees().get(0), new HashSet<SootMethod>(), worklist, currDist);
						}
						
						// add CD to 'post' point if there is no call or a lib call (might be in *addition* to app call 'pre-rhs')
						if (csTgt == null || csTgt.hasLibCallees())
							createControlDep(new NodePoint(nSrc, NodePoint.POST_RHS), brId, new NodePoint(nCtrlTgt, NodePoint.POST_RHS), worklist, currDist);
//						break; // forget about remaining branches in set of CDs of tgt
//						*** previous line was commented out because there can be multiple branches to tgt from same src and they will have different brIds!!
					}
				}
			}
			// 2.2 Interproc via multi-target CS: add callee nodes ctrl-dependent on entry branch of their respective methods
			if (posSrc == NodePoint.PRE_RHS && csSrc != null && csSrc.hasAppCallees() && (csSrc.getAppCallees().size() > 1 || csSrc.hasLibCallees())) {
				int brId = -1;
				for (SootMethod mCallee : csSrc.getAppCallees()) {
					++brId;
					CFG cfgCallee = ProgramFlowGraph.inst().getCFG(mCallee);
					Branch brEntryCallee = cfgCallee.getEntryBranch();
					for (CFGNode nCalleeTgt : cfgCallee.getNodes()) {
						if (nCalleeTgt.isInCatchBlock() || nCalleeTgt.isSpecial())
							continue;
						if (ReqBranchAnalysis.inst().getCDBranches(nCalleeTgt).contains(brEntryCallee)) {
							// add CD to 'pre' point if there is an app call
							CallSite csTgt = nCalleeTgt.getCallSite();
							if (csTgt != null && csTgt.hasAppCallees()) {
								createControlDep(new NodePoint(nSrc,NodePoint.PRE_RHS), brId, new NodePoint(nCalleeTgt, NodePoint.PRE_RHS), worklist, currDist);
								
								// Transitive interproc: if tgt is a multi-target CS, add callee nodes ctrl-dependent on entry branch of their respective methods
								if ((fwdOnly || ctxInsens) && csTgt.getAppCallees().size() == 1 && !csTgt.hasLibCallees())
									addInterProcCtrlDeps(pSrc, brId, csTgt.getAppCallees().get(0), new HashSet<SootMethod>(), worklist, currDist);
							}
							// add CD to 'post' point if there is no call or a lib call (might be in *addition* to app call 'pre-rhs')
							if (csTgt == null || csTgt.hasLibCallees())
								createControlDep(new NodePoint(nSrc,NodePoint.PRE_RHS), brId, new NodePoint(nCalleeTgt, NodePoint.POST_RHS), worklist, currDist);
						}
					}
				}
			}
		}
	}
	private void addInterProcCtrlDeps(NodePoint pSrc, int brId, SootMethod m, Set<SootMethod> visited, Set<Pair<NodePoint, Integer>> worklist, int currDist) {
		if (!visited.add(m))
			return; // already visited
		
		// look for all nodes in this method that depend on entry branch
		CFG cfg = ProgramFlowGraph.inst().getCFG(m);
		Branch brEntry = cfg.getEntryBranch();
		for (CFGNode nTgt : cfg.getNodes()) {  // look for which nodes in callee are directly ctrl-dependent on entry branch
			if (nTgt.isInCatchBlock() || nTgt.isSpecial())
				continue;
			if (ReqBranchAnalysis.inst().getCDBranches(nTgt).contains(brEntry)) {
				// add CD to 'pre' point if there is an app call
				CallSite csTgt = nTgt.getCallSite();
				if (csTgt != null && csTgt.hasAppCallees()) {
					createControlDep(pSrc, brId, new NodePoint(nTgt, NodePoint.PRE_RHS), worklist, currDist);
					// in addition, transitively look for single-target calls to methods and the nodes in those methods depending on their entry branches
					if (csTgt.getAppCallees().size() == 1 && !csTgt.hasLibCallees()) {
						SootMethod mCallee = csTgt.getAppCallees().get(0);
						addInterProcCtrlDeps(pSrc, brId, mCallee, visited, worklist, currDist);
					}
				}
				// add CD to 'post' point if there is no call or a lib call (might be in *addition* to app call 'pre-rhs')
				if (csTgt == null || csTgt.hasLibCallees())
					createControlDep(pSrc, brId, new NodePoint(nTgt, NodePoint.POST_RHS), worklist, currDist);
			}
		}
	}
	
	/** Helper method that creates and stores data dependencies in corresponding set and map, updating given worklist.
	 *  @param var Identifies the defined variable in the dependence; for actual-formal links, it's the argument var. */
	private void createDataDep(NodePoint pntDef, NodePoint pntUse, Variable var, DepType depType, Set<Pair<NodePoint,Integer>> worklist, int dist) {
		Dependence dep = new DataDependence(pntDef, pntUse, var, depType);
		deps.add(dep);
		// add new data dep to dep list for src
		pntToDeps.get(pntDef).add(dep);
		if (dist > 0 || (dist == -1 && !visitedPoints.contains(pntUse)))
			worklist.add(new Pair<NodePoint,Integer>(pntUse, dist>0? dist-1 : dist)); // add nUse to worklist if not processed before in current phase
	}
	/** Helper method that creates and stores control dependencies in corresponding set and map, updating given worklist. */
	private void createControlDep(NodePoint pntSrc, int brId, NodePoint pntTgt, Set<Pair<NodePoint,Integer>> worklist, int dist) {
		Dependence dep = new ControlDependence(pntSrc, brId, pntTgt);
		deps.add(dep);
		// add new ctrl dep to dep list for src
		pntToDeps.get(pntSrc).add(dep);
		if (dist > 0 || (dist == -1 && !visitedPoints.contains(pntTgt)))
			worklist.add(new Pair<NodePoint,Integer>(pntTgt, dist>0? dist-1 : dist)); // add nTgt to worklist if not processed before in current phase
	}
	
	/** REACHABILITY analysis should be used instead. */
	private boolean isFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);
		
		// first, look intraprocedurally; they might be in same cfg, but n2 must be reachable from n1 within that cfg
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;
		
		// then, look interprocedurally in (transitive) callees ONLY from callsites at cfg1 intraproc-reachable from n1
		Set<CFG> cfgsFwd = new HashSet<CFG>();
		for (CallSite cs : cfg1.getCallSites()) {
			if (!cs.hasAppCallees())
				continue;
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			if (ReachabilityAnalysis.reachesFromBottom(n1, nCS, false))
				for (SootMethod mTgt : cs.getAppCallees())
					cfgsFwd.add(ProgramFlowGraph.inst().getCFG(mTgt));
		}
		
		Set<CFG> workset = new HashSet<CFG>(cfgsFwd);
		while (!workset.isEmpty()) {
			CFG cfg = workset.iterator().next();
			workset.remove(cfg);
			cfgsFwd.add(cfg);
			
			for (CFG cfgSucc : cfg.getCallgraphSuccs())
				if (!cfgsFwd.contains(cfgSucc))
					workset.add(cfgSucc);
		}
		
		return cfgsFwd.contains(cfg2);
	}
	/** REACHABILITY analysis should be used instead. */
	private boolean isBackFwdReachable(CFGNode n1, CFGNode n2) {
		CFG cfg1 = ProgramFlowGraph.inst().getContainingCFG(n1);
		CFG cfg2 = ProgramFlowGraph.inst().getContainingCFG(n2);
		
		// look intraprocedurally only in cfg1, NOT forward from cfg1!
		if (cfg1 == cfg2 && ReachabilityAnalysis.reachesFromBottom(n1, n2, false))
			return true;
		
		Set<CallSite> backCSs = new HashSet<CallSite>();
		Set<CallSite> workset = new HashSet<CallSite>(cfg1.getCallerSites());
		while (!workset.isEmpty()) {
			CallSite cs = workset.iterator().next();
			CFGNode nCS = ProgramFlowGraph.inst().getNode(cs.getLoc().getStmt());
			workset.remove(cs);
			backCSs.add(cs);
			
			if (isFwdReachable(nCS, n2))
				return true;
			
			CFG cfg = ProgramFlowGraph.inst().getContainingCFG(nCS);
			for (CallSite csBack : cfg.getCallerSites())
				if (!backCSs.contains(csBack))
					workset.add(csBack);
		}
		
		return false;
	}
	
	/** Whether forward slices have any node in common. */
	public boolean intersects(DependenceGraph depGraph2) {
		// gather collection of affected nodes for this graph
		Set<CFGNode> pointsHere = new HashSet<CFGNode>();
		for (Dependence dep : deps)
			pointsHere.add(dep.getTgt().getN());
		
		for (Dependence depOther : depGraph2.deps)
			if (pointsHere.contains(depOther.getTgt().getN()))
				return true;
		return false;
	}
	
	public Set<NodePoint> getPointsInSlice() {
		// start with src pnts of deps in slice
		Set<NodePoint> pointsInSlice = new HashSet<NodePoint>(pntToDeps.keySet());
		// add tgt pnts of deps out of those src pnts
		for (Set<Dependence> outDeps : pntToDeps.values())
			for (Dependence outDep : outDeps)
				pointsInSlice.add(outDep.getTgt());
		return pointsInSlice;
	}
	
}
