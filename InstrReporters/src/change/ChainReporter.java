package change;

import profile.ChainExaminer;

public class ChainReporter {
	/** Although it's not used, this method exists so that the reporter classes are loaded by Soot! */
	public static void __link() { }
	
	public static void reportDef(int sId, int frameId, int varId, Object o, int idx) {
		ChainExaminer.reportDef(sId, frameId, varId, o, idx);
	}
	public static void reportUse(int sId, int varId, Object o, int idx) {
		ChainExaminer.reportUse(sId, varId, o, idx);
	}
	public static void reportCtrlSrc(int branchId, int frameId) {
		ChainExaminer.reportCtrlSrc(branchId, frameId);
	}
	public static void reportCtrlTgt(int sId, int branchId) {
		ChainExaminer.reportCtrlTgt(sId, branchId);
	}
	
}
