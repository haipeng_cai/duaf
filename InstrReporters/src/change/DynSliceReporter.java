package change;

import profile.CommonReporter;
import profile.DynSliceExaminer;

public class DynSliceReporter {
	public final static DynSliceReporter inst = new DynSliceReporter();
	
	/** Although it's not used, this method exists so that the reporter classes are loaded by Soot! */
	public static void __link() { CommonReporter.__link(); }
	
	/** DO NOT DELETE -- this is called by inserted instrumentation. */
	public static void initialize() {
		if (CommonReporter.cleanupPerTest)
			DynSliceExaminer.initialize();
	}
	
	public static void reportChange(int nId, boolean pre) {
		DynSliceExaminer.inst().reportChange(nId, pre);
	}
	public static void reportArrDef(int nId, boolean pre, int frameId, boolean entry, int varGroupId, Object oBase, int size, Object[] usedVarValues) {
		DynSliceExaminer.inst().reportArrDef(nId, pre, frameId, entry, varGroupId, oBase, size, usedVarValues);
	}
	public static void reportDef(int nId, boolean pre, int frameId, boolean entry, int varGroupId, boolean interproc, Object oBase, int idx, Object[] usedVarValues) {
		DynSliceExaminer.inst().reportDef(nId, pre, frameId, entry, varGroupId, interproc, oBase, idx, usedVarValues);
	}
	public static void reportUse(int nId, boolean pre, int frameId, boolean entry, int varGroupId, Object oBase, int idx) {
		DynSliceExaminer.inst().reportUse(nId, pre, frameId, entry, varGroupId, oBase, idx);
	}
	public static void reportBranch(int nId, boolean pre, int brId, int frameId, Object[] usedVarValues) {
		DynSliceExaminer.inst().reportBranch(nId, pre, brId, frameId, usedVarValues);
	}
	public static void reportCallSrc(int nId, int frameId, Object[] usedVarValues) {
		DynSliceExaminer.inst().reportCallSrc(nId, frameId, usedVarValues);
	}
	public static void reportCallTgt(int mtdId, int frameId) {
		DynSliceExaminer.inst().reportCallTgt(mtdId, frameId);
	}
	public static void reportCDTgt(int nId, boolean pre, int frameId, boolean entry) {
		DynSliceExaminer.inst().reportCDTgt(nId, pre, frameId, entry);
	}
	
}
