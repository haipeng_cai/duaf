package change;

import java.util.ArrayList;
import java.util.List;

import profile.LocalsBox;
import profile.SPAExaminer;

public class SPAReporter {
	/** Used only to force Soot to load this class when analyzing */
	public static void __link() { }//LocalsBox.__link(); }
	
	private static List lbs = new ArrayList();
	
	public static void addLB(LocalsBox lb) {
		System.out.println("SPA: Add LB");
		if (!lbs.isEmpty())
			lb.setPrevLB((LocalsBox)lbs.get(lbs.size() - 1));
		lbs.add(lb);
	}
	public static void removeTopLB() {
		System.out.println("SPA: Remove LB");
		lbs.remove(lbs.size() - 1);
	}
	
	public static void examine() {
		System.out.println("*** EXAMINING ***  (SPA)  SINGLE CHANGE");
		SPAExaminer.runExaminer(-1, (LocalsBox)lbs.get(lbs.size() - 1));
	}
	
	public static void examine(int changeId) {
		System.out.println("*** EXAMINING ***  (SPA)  CHANGE SID " + changeId);
		SPAExaminer.runExaminer(changeId, (LocalsBox)lbs.get(lbs.size() - 1));
	}
	
}
