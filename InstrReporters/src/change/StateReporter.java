package change;

public class StateReporter {
	/** Although it's not used, this method exists so that the reporter classes are loaded by Soot! */
	public static void __link() { }
	
	public static void pushFrame(int sCallerId, Object[] locals) {
		System.out.println("STATE REP: push " + sCallerId + ", " + locals.length);
	}
	
	public static void popFrame() {
		System.out.println("STATE REP: pop");
	}
	
	/** @param locals Current values of locals at top activation frame */
	public static void dumpState(Object[] locals) {
		System.out.println("STATE REP: DUMP");
	}
	
}
