package change;

import java.util.ArrayList;
import java.util.List;

import profile.MatrixExaminer;
import profile.LocalsBox;

public class ChangeReporter {
	/** Used only to force Soot to load this class when analyzing */
	public static void __link() { }//LocalsBox.__link(); }
	
	private static List lbs = new ArrayList();
	
	public static void addLB(LocalsBox lb) {
		if (!lbs.isEmpty())
			lb.setPrevLB((LocalsBox)lbs.get(lbs.size() - 1));
		lbs.add(lb);
	}
	public static void removeTopLB() { lbs.remove(lbs.size() - 1); }
	
	public static void examine() {
		System.out.println("*** EXAMINING ***  (MaTRIX)");
		MatrixExaminer.runExaminer(-1, (LocalsBox)lbs.get(lbs.size() - 1));
	}
	
	public static void examine(int changeId) {
		System.out.println("*** EXAMINING ***  (MaTRIX)");
		MatrixExaminer.runExaminer(changeId, (LocalsBox)lbs.get(lbs.size() - 1));
	}
	
}
